"use strict";

define([
	"app",
	"consumer/consumer.controller",
	"consumer/settings/views/ManageDevicesPage.view",
	"consumer/settings/views/DataCollection.view",
	"consumer/settings/views/CategorySettingsPage.view",
	"consumer/settings/views/EquipmentSettingsPage.view",
	"consumer/settings/views/EditProfile.view",
	"consumer/settings/views/AboutAppPage.view",
	"consumer/settings/views/ChangePassword.view",
    "consumer/settings/views/ResetPassword.view"
], function (App, ConsumerController) {

	App.module("Consumer.Settings", function (Settings) {
		Settings.Controller = {
			// routing methods
			gateways: function gateways() {
					var page = new App.Consumer.Settings.Views.ManageDevicesPage();

					ConsumerController.showLayout(page, true, ["devices"]);
			},
			dataCollection: function dataCollection() {
				App.salusConnector.getSessionUserPromise().then(function (userModel) {
					var dataCollectionLayout = new App.Consumer.Settings.Views.DataCollectionLayout({ model: userModel });
					ConsumerController.showLayout(dataCollectionLayout, true);
				});
			},
			categorySettings: function categorySettings(type) {
				var page = new App.Consumer.Settings.Views.CategorySettingsPageView({type:type});
				ConsumerController.showLayout(page, true, ["devices"]);
			},
			equipmentSettings: function equipmentSettings(dsn) {
				var page = new App.Consumer.Settings.Views.EquipmentSettingsPageView({dsn:dsn});
				ConsumerController.showLayout(page, true, ["devices"]);
			},
			profileSettings: function profileSettings() {
				var page = new App.Consumer.Settings.Views.EditProfilePage({
					model: App.salusConnector.getSessionUser()
				});
				ConsumerController.showLayout(page, true);
			},
			aboutApp: function aboutApp() {
				var page = new App.Consumer.Settings.Views.AboutAppPage();
                if(App.salusConnector.isLoggedIn() || App.salusConnector.isLoggingIn()) {
                    ConsumerController.showLayout(page, true);
                } else {
                    ConsumerController.showLayout(page, false);
                }
			},
			changePassword: function changePassword() {
				var page = new App.Consumer.Settings.Views.ChangePasswordPage({
					model: App.salusConnector.getSessionUser()
				});
				ConsumerController.showLayout(page, true);
			},
            resetPassword: function resetPassword() {
				var page = new App.Consumer.Settings.Views.ResetPasswordPage({
					model: App.salusConnector.getSessionUser()
				});
				ConsumerController.showLayout(page, true);
			},

			// utility methods
			createShare: function (shareModel, dsnList) {
				var userData = shareModel.get("user_profile");

				// we use a users existing devices as our device to be shared
				shareModel.set({
					"resource_id": App.salusConnector.getDeviceCollection().first().get("dsn")
				});

				// try to add the share
				return shareModel.add().catch(function () {
					// the assumption is that if the share failed to add, it was because the user does not exist
					// so we make a call to create the proxy user
					return App.salusConnector._salusWSConnector.registerProxyUser(userData).then(function () {
						// after the user is created, try to create the share again
						App.salusConnector.getShareCollection().batchShareDevicesByDSN(userData.email, dsnList);
						return true;
					});
				}).then(function (enteredCatch) {
					//If we entered catch to create new user, do not exclude anything since the initial add() failed
					if (enteredCatch) {
						return App.salusConnector.getShareCollection().batchShareDevicesByDSN(userData.email, dsnList);
					} else {
						return App.salusConnector.getShareCollection().batchShareDevicesByDSN(userData.email, dsnList, [shareModel.get("resource_id")]);
					}

				});
			},
			updateShare: function (shareModel, dsnList) {
				var userData = shareModel.get("user_profile");

				// remove devices that are no longer checked
				var sharedDevices = App.salusConnector.getShareCollection();
				var devicesToRemove = App.salusConnector.getShareCollection().filter(function (device) {
					return !dsnList.includes(device.get("resource_id")) && shareModel.get("user_email") === device.get("user_email");
				});

				return sharedDevices.removeShares(devicesToRemove).then(function() {
					// update the shareModel and do a batchShare
					return shareModel.update().then(function () {
						return App.salusConnector.getShareCollection().batchShareDevicesByDSN(userData.email, dsnList);
					});
				});
			},
			deleteDeviceShare: function (userEmail) {
				return App.salusConnector.getShareCollection().removeAllSharesTo(userEmail).then(function () {
					return App.salusConnector.getShareCollection().refresh();
				});
			}
		};
	});

	return App.Consumer.Settings.Controller;
});