"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/views/mixins/mixin.salusView",
	"consumer/settings/views/DeviceForm.view"
], function (App, templates) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.DeviceListRowView = Mn.LayoutView.extend({
			template: templates["settings/manageDevices/deviceListRow"],
			tagName: "li",
			className: "list-table-item",
			regions: {
				"editRegion": ".bb-edit-region"
			},
			events: {
				"click .bb-collapsed-view": "edit"
			},
			initialize: function () {
				_.bindAll(this, "edit");

				this.editView = new Views.DeviceForm({
					model: this.model,
					mode: "edit"
				});

				this.registerRegion("editRegion", this.editView);

				this.listenTo(this.editView, "cancel:click", this.edit);
				this.listenTo(this.editView, "save:success", this.edit);
				this.expanded = false;
			},
			edit: function () {
				var isCollapsed = this.editView.isCollapsed();

				this.editView.collapseOrExpand(!isCollapsed);
				this.$el.toggleClass("edit-enabled", isCollapsed);
			},
			templateHelpers: function () {
				var shareUserModel = this.model.get("user_profile");
				return {
					"user_name": shareUserModel.firstname + " " + shareUserModel.lastname
				};
			}

		}).mixin([App.Consumer.Views.Mixins.RegisteredRegions, App.Consumer.Views.Mixins.SalusView]);
	});

	return App.Consumer.Settings.Views.DeviceListRowView;
});