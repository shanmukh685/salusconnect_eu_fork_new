"use strict";

define([
	"app",
	"bluebird",
	"common/config"
], function (App, P, config) {

	App.module("Consumer.Models", function (Models, App, B, Mn, $, _) {
		Models.GatewayInfo = B.Model.extend({
			defaults: {
				dsn: null,
				gateway: null,
				backgroundImage: null,
				lastScannedDate: null
			},
			idAttribute: "dsn"
		});

		/**
		 * Main model fed into the dashboard to control its views.
		 *
		 * The dashboard manager should know about all of the user's gateways and all devices connected to its currently
		 * shown gateway.
		 */
		Models.DashboardManager = B.Model.extend({
			defaults: {
				currentGateway: null,
				deviceCollection: null,
				gatewayCollection: null,
				sessionUser: null,
				isScanningDevices: false,
				currentScanIndex: 0
			},
			initialize: function () {
//              var that = this;
				_.bindAll(this, "setBackgroundFromGateway", "getPhotoForGateway");

				this.loadPromise = null;

				this.listenTo(App.vent, "gateway:added", function (gateway) {
					var gatewayInfoCollection = this.get("gatewayInfoCollection");
					if (gatewayInfoCollection) {
						gatewayInfoCollection.add(new Models.GatewayInfo({
							dsn: gateway.get('dsn'),
							gateway: gateway,
							backgroundImage: null,
							lastScannedDate: new Date()
						}));
					}
				});

				this.listenTo(App.vent, "gateway:removed", function (gateway) {
					var gatewayInfoCollection = this.get("gatewayInfoCollection");
					if (gatewayInfoCollection) {
						var info = gatewayInfoCollection.findWhere({"dsn": gateway.get("dsn")});
						if (info) {
							gatewayInfoCollection.remove(info);
						}
					}

//					if (that.get("currentGateway") === gateway) {
//						that.set("currentGateway", null);
//					}
                    if (App.getCurrentGateway() === gateway) {
						App.salusConnector.set("currentGateway", null);
					}

					if (App.getCurrentGateway() === gateway) {
                        App.navigate("/equipment");
						//App.salusConnector.nextGateway();
					}
				});

			},
			/**
			 * Fills in the dashboard manager with the data retrieved from its user.
			 */
			loadData: function () {
				var that = this;

				this.loadPromise = App.salusConnector.getDoOnLoginPromise().then(function () {
					return App.salusConnector.getDataLoadPromise(["devices"]).then(function (/*data*/) {
						var gatewayCollection = App.salusConnector.getGatewayCollection();

						if (gatewayCollection) {
							that.set({
								deviceCollection: App.salusConnector.getUserDeviceCollection(),
								gatewayInfoCollection: that._buildGatewayInfoCollec(gatewayCollection),
								currentGateway: App.getCurrentGateway(),
								sessionUser: App.salusConnector.getSessionUser()
							});

							that.listenTo(App.salusConnector, "change:currentGateway", function (salusConnector, currentGateway) {
								that.set("currentGateway", currentGateway);
								var dc = that.get("deviceCollection");
								that.set("deviceCollection", App.salusConnector.getUserDeviceCollection());
								dc.destroy(); //destroy after set.
								
								that.trigger("update:manager");
							});

							that.trigger("gatewayInfo:loaded");
						}

						return that;
					}).then(function() {
						that.setBackgroundFromGateway(false);
					});
				});

				return this.loadPromise;
			},

			getNonActiveGateways: function () {
				var gatewayInfos = this.get("gatewayInfoCollection"),
					outputArray = [],
					that = this;

				gatewayInfos.each(function (gatewayInfo) {
					if (that.get("currentGateway").get("dsn") !== gatewayInfo.get("dsn")) {
						outputArray.push(gatewayInfo.get("gateway"));
					}
				});

				return new B.Collection(outputArray);
			},

			getPhotoForGateway: function (gateway) {
				var that = this;

				return gateway.fetchPhotoUrl().then(function (data) {
					var backgroundUrl;

					if (data && data.datum && data.datum.key === "photourl") {
						backgroundUrl = data.datum.value;

						// check backgroundUrl contains is part of googleusercontent
						if (backgroundUrl.indexOf("googleusercontent.com") !== -1) {
							if (App.onMobile()) {
								backgroundUrl = backgroundUrl.replace("http:", "https:");
								backgroundUrl += "=s560";
							} else {
								//Make url http or https indifferent, so now url should start with //
								backgroundUrl = backgroundUrl.replace("http:", "");
								backgroundUrl += "=s940";
							}
						}
					} else {
						backgroundUrl = that.defaultBackground(gateway);
					}

					return backgroundUrl;
				}).catch(function () {

					return that.defaultBackground(gateway);
				}).then(function (backgroundUrl) {
					var info = that.getGatewayInfo(gateway);
					if (info) {
						info.set({
							backgroundImage: backgroundUrl
						});
					}

					that.trigger("gatewayPhoto:loaded");
					return that;
				});
			},

			/**
			 * Gets the current Gateway's background url and updates this managers background property.
			 * If the gateway has no url then a default one is used.
			 * @returns {Promise}
			 */
			setBackgroundFromGateway: function (newPhoto) {
				var gateway = this.get("currentGateway");

				if (gateway) {
					if (this.photoForGatewaysPromise && !newPhoto) {
						this.trigger("allPhotos:loaded");
						return this.photoForGatewaysPromise;
					}
					
					var that = this;
					this.photoForGatewaysPromise = this.getPhotoForGateway(gateway).then(function () {
						P.all(that.getNonActiveGateways().map(that.getPhotoForGateway)).then(function () {
							that.trigger("allPhotos:loaded");
						});
					});

					return this.photoForGatewaysPromise;
				} else {
					this.set({
						backgroundImage: this.defaultBackground()
					});

					return P.resolve(this);
				}
			},

			/**
			 * Gets a default login images to be used as the gateway background image.
			 * The image is chosen based on the hash of the gateways dsn to be deterministic.
			 * @returns {*}
			 * @private
			 */
			defaultBackground: function (gateway) {
				var hashKey = Math.random().toString(),
					backgroundUrl = App.Consumer.Login.backgroundImageUrl;

				gateway = gateway || this.get("currentGateway");

				if (gateway) {
					hashKey = gateway.get("dsn");
				}

				//default to deterministic background image from login image list;
				var numberOfImages = config.appBackgroundImages.length;
				var index = Math.abs(App.hashString(hashKey)) % numberOfImages;

				if (!isNaN(index)) {
					backgroundUrl = config.appBackgroundImages[index];
				}

				return backgroundUrl;
			},

			/**
			 * this is called from mobile. it uploads the image through the web service
			 * then calls to set the background url on the gateway
			 * @param sourceType
			 * @returns {*}
			 */
			takePhoto: function (sourceType) {
				var that = this;
				return this._chooseBackgroundImage(sourceType).then(function (imageFile) {
					return App.salusConnector.getPhotoUploadUrl().then(function (url) {
						var options = new window.FileUploadOptions();
						options.fileKey = "file";
						options.fileName = imageFile.substr(imageFile.lastIndexOf('/') + 1);
						options.params = {
							dsn: that.get("currentGateway").get("dsn")
						};

						App.salusConnector.addAuthHeaders(options);

						var ft = new window.FileTransfer();

						return new P.Promise(function (resolve, reject) {
							ft.upload(imageFile, encodeURI(url), resolve, reject, options);
						}).then(function() {
							that.setBackgroundFromGateway(true);
						});
					});
				}).catch(function (err) {
					App.log(err);

					// did not supply a photo
					return P.reject(that);
				});
			},

			getGatewayInfo: function (gateway) {
				if (!!gateway) {
					return this.get("gatewayInfoCollection").findWhere({
						dsn: gateway.get("dsn")
					});
				} else {
					return null;
				}
			},

			/**
			 * Refreshes the device models of all devices connected to the gateway currently being shown
			 */
			startScan: function () {
				var that = this;
				var deviceRefreshPromises = [];

				if (this.has("deviceCollection")) {

					this.set({
						isScanningDevices: true,
						currentScanIndex: 0
					});

					deviceRefreshPromises = App.salusConnector.getUserDeviceCollection().map(function (deviceModel) {
						return deviceModel.refresh().then(function () {
							that.set({
								currentScanIndex: that.get("currentScanIndex") + 1
							});
						});
					});

					P.all(deviceRefreshPromises).then(function () {
						that.set({
							isScanningDevices: false
						});

						that.getGatewayInfo(that.get("currentGateway")).set({lastScannedDate: new Date()});
					});

				}
			},
			stopScan: function () {
				this.set({
					isScanningDevices: false
				});
			},

			getLastScanned: function (gateway) {
				var gatewayInfoObj = this.getGatewayInfo(gateway);

				if (gatewayInfoObj) {
					return gatewayInfoObj.get("lastScannedDate");
				}

				return null;
			},

			saveDesktopPhotoInGateway: function (form) {
				var that = this;

				return App.salusConnector.getPhotoUploadUrl().then(function (url) {
					var data = new FormData(form[0]),
						dsn = that.get("currentGateway").get("dsn");

					data.append('dsn', dsn);

					return App.salusConnector.makeFormAjaxCall(url, data);
				}).then(function() {
					that.setBackgroundFromGateway(true);
				});
			},

			_buildGatewayInfoCollec: function (gateways) {
				if (this.gatewayInfoCollecPromise) {
					return this.gatewayInfoCollecPromise;
				}

				this.gatewayInfoCollecPromise = new B.Collection(gateways.map(function (gateway) {
					return new Models.GatewayInfo({
						dsn: gateway.get('dsn'),
						gateway: gateway,
						backgroundImage: null,
						lastScannedDate: new Date()
					});
				}));

				return this.gatewayInfoCollecPromise;
			},

			/**
			 * Chooses a background image from the native camera or photo library.
			 * @param sourceType  PictureSourceType [CAMERA,PHOTOLIBRARY]. Defaults CAMERA
			 * @returns {Promise} Once resolved, contain the image file path
			 * @private
			 */
			_chooseBackgroundImage: function (sourceType) {

				var errorNoCameraAvailable = "No camera available",
				//More errors that are not currently being used.
				//errorNoImageSelected = "no image selected",
				//errorHasNoAccessToCamera = "has no access to camera",
				//errorCapturingImage = "Error capturing image."
					camera = navigator.camera;

				if (!App.onMobile() || !camera) {
					return P.reject(errorNoCameraAvailable);
				}

				if (sourceType === undefined) {
					sourceType = camera.PictureSourceType.CAMERA;
				}
				//Notes: We want the target size of the image to be 1000px in width.
				// If the do a portrait photo with 4/3 ratio image the height should be 1333px.
				var cameraOptions = {
					quality: 80,
					sourceType: sourceType, //camera.PictureSourceType.CAMERA,
					targetWidth: 1000, //options will scale down to the smallest dimension (width)
					targetHeight: 1333,
					correctOrientation: true,
					destinationType: camera.DestinationType.FILE_URI
				};

				return new P.Promise(function (resolve, reject) {
					camera.getPicture(resolve, reject, cameraOptions);
				}).catch(function (err) {
					if (sourceType === camera.PictureSourceType.CAMERA && err === errorNoCameraAvailable) {
						//if The device doesn't have a camera try picking from a photo libaray.
						return new P.Promise(function (resolve2, reject2) {
							//SetTimeout is needed to get over a crash with ios having the previous web thread locked.
							setTimeout(function () {
								cameraOptions.sourceType = camera.PictureSourceType.PHOTOLIBRARY;
								return camera.getPicture(resolve2, reject2, cameraOptions);
							}, 0);
						});
					} else {
						return P.reject(err);
					}
				});
			}
		});
	});

	return App.Consumer.Models.DashboardManager;
});
