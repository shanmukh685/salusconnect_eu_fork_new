"use strict";

define([
	"app",
	"common/commonTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, commonTemplates, SalusViewMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn, $, _) {
		Views.AuthenticatingPageView = Mn.ItemView.extend({
			id: "authenticating-page",
			routeCallBack: null,
			routeArgs: null,
			template: commonTemplates.authenticatingPage,
			templateHelpers: function () {
				return {
					header: App.translate("common.loadingMessages.loading")
				};
			},
			initialize: function (options) {
				_.bindAll(this, "onAuth", "onLoginFailure");

				this.router = options.router;
				this.routeArgs = options.routeArgs;
				this.routeCallBack = options.routeCallBack;
				this.isUnauthenticatedRoute = options.isUnauthenticatedRoute;
				this.listenTo(App.vent, "authenticated", this.onAuth);
				this.listenTo(this, "onLoginFailure", this.onLoginFailure);
			},

			onAuth: function () {
				// will return false if we are not on the mobile app
				if (!this._mobileSpecific()) {
					if (!this.isUnauthenticatedRoute) {
						// if the original route is not for unauthenticated access
						// execute the callback to navigate to it
						this.executeCb();
					} else {
						App.salusConnector.firstTimeLoginRouteCheck();
					}
				}
			},

			executeCb: function () {
				this.routeCallBack.apply(this.router, this.routeArgs);
			},

			onLoginFailure: function () {
				// will return false if we are not on the mobile app
				if (!this._mobileSpecific()) {
					if (this.isUnauthenticatedRoute) {
						// if the original route is for unauthenticated access execute
						// the callback because you can hit it
						this.executeCb();
					} else {
						// else navigate to login welcome
						App.navigate("login");
					}
				}
			},

			_mobileSpecific: function () {
				if (App.onMobile()) {
					if (App.salusConnector.isLoggedIn()){
						App.salusConnector.firstTimeLoginRouteCheck();
					} else {
						App.navigate("login");
					}
					return true;
				} else {
					return false;
				}
			}
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Views.AuthenticatingPageView;
});