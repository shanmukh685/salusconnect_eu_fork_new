"use strict";

define([
	"app",
	"bluebird",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/SalusModal.view",
	"consumer/equipment/views/widgets/TRVEditModal.view",
	"consumer/settings/views/EmptyList.view"
], function (App, P, constants, consumerTemplates, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.TRVItemView = Mn.ItemView.extend({
			className: "trv-item col-xs-12",
			template: consumerTemplates["equipment/widgets/trvItem"],
			bindings: {
				".bb-trv-name": {
					observe: "name"
				}
			},
			events: {
				"click .bb-trv-name": "handleClick"
			},
			handleClick: function (event) {
				event.preventDefault();
				App.navigate("equipment/myEquipment/" + this.model.get("dsn"));
			}
		}).mixin([SalusView]);

		Views.TRVCollectionView = Mn.CollectionView.extend({
			className: "trv-collection row",
			template: false,
			childView: Views.TRVItemView,
			emptyView: App.Consumer.Settings.Views.EmptyListView,
			emptyViewOptions: {
				tagName: "div",
				className: "trv-item col-xs-12 padding-t-10",
				i18nTextKey: "equipment.trv.noTrvs"
			},
			initialize: function () {
				var that = this;

				_.bindAll(this, "updateCollection");

				this.collection = new B.Collection();
				this.updateCollection();

				this.listenTo(this.model.get("PairedTRVShortID"), "propertiesSynced", this.updateCollection);
				this.listenTo(this.model.get("PairedTRVShortID"), "propertiesFailedSync", function () {
					that.$(".bb-spinner-wrapper").detach();
					App.Consumer.Controller.showDeviceSyncError(that.model);
				});
			},
			updateCollection: function () {
				var trvs = this.model.getAssociatedTRVs(),
					models = _.map(trvs, function (hex) {
						return App.salusConnector.getTRVByShortID(hex);
					});

				this.$(".bb-spinner-wrapper").detach();

				this.collection.set(models);
			}
		}).mixin([SalusView]);

		Views.TRVWidgetContentView = Mn.LayoutView.extend({
			className: "trv-widget-content",
			template: consumerTemplates["equipment/widgets/trvWidgetContent"],
			regions: {
				trvCollectionRegion: ".bb-trv-collection-region"
			},
			ui: {
				icon: ".bb-edit-add-icon",
				error: ".bb-error",
				addText: ".bb-add-text"
			},
			events: {
				"click .bb-button-wrapper": "handleAddEditButtonClick"
			},
			initialize: function () {
				var that = this;
				this.dependencies = [];
                //this.trvDependencies = [];

				_.bindAll(this, "handleAddEditButtonClick");
                
                App.salusConnector.getTRVCollection().each(function (device) {
                    if(device.get("EUID") === undefined || 
                           (device.get("EUID") !== undefined && device.get("EUID").getProperty() === null)){
                        that.dependencies.push(device.getDeviceProperties(true));
                    }else{
                        that.dependencies.push(device.getDevicePropertiesPromise());
                    }
                     
				});

				

				// get it600 so we can see which trv connected to which
				App.salusConnector.getUserDeviceCollection().getIT600s().each(function (device) {
                    that.dependencies.push(device.getDevicePropertiesPromise());
				});
			},
			onRender: function () {
				var that = this, associated;
                P.all(that.dependencies).then(function () {
                    associated = that.model.getAssociatedTRVs();
                    
                    if(that.model.get("LeaveNetwork") && that.model.get("OnlineStatus_i") && (that.model.get("connection_status")!=="Offline")){
                        if(that.model.getPropertyValue("LeaveNetwork") === 0 && that.model.getPropertyValue("OnlineStatus_i") === 1){
                            if (_.isArray(associated) && associated.length > 0) {
                                that.ui.icon.addClass("edit");
                                that.ui.addText.addClass("hidden");
                            } else {
                                that.ui.icon.addClass("add");
                                that.ui.addText.removeClass("hidden");
                            }
                        }
                    }

                    that.trvCollectionView = new Views.TRVCollectionView({
                        model: that.model
                    });

                    that.trvCollectionRegion.show(that.trvCollectionView);
                });

			},
			handleAddEditButtonClick: function () {
				if (App.salusConnector.getTRVCollection().isEmpty()) {
					return App.navigate("setup/pairing");
				}

				var that = this, contentView = new App.Consumer.Equipment.Views.TRVEditModalView({
					model: this.model
				});

				this.listenTo(contentView, "save:associations", function () {
					that.trvCollectionView.$el.append(
						'<div class="bb-spinner-wrapper col-xs-12 margin-t-15">' +
						'<div class="spinner center-block margin-t-15"></div>' +
						'</div>'
					);
				});

				App.modalRegion.show(new App.Consumer.Views.SalusModalView({
					size: "modal-lg",
					contentView: contentView
				}));

				App.showModal();
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.TRVWidgetContentView;
});
