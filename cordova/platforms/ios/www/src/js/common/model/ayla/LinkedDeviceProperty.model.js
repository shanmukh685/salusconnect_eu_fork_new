"use strict";

define([
	"app",
	"moment", // requiring in real moment as this is just for ease of parsing and does not require timezones
	"bluebird",
	"common/config",
	"common/AylaConfig",
	"common/model/ayla/DeviceProperty.model"
], function (App, moment, P, config) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		/**
		 * This represents two ayla model property that are linked as setters and getters
		 */
		Models.LinkedDevicePropertyModel = B.Model.extend({
			isLinkedDeviceProperty: true,
			defaults: {
				getterProperty: null,
				setterProperty: null,
				value: null,
				display_name: null,
				name: null
			},

			initialize: function (data, options) {
				var that = this;
				_.bindAll(this, "getterPollHelper", "updateData");
				this.getterPollTimeout = null;
				this.set("getterProperty", new Models.DevicePropertyModel(options.getterProp));
				this.set("setterProperty", new Models.DevicePropertyModel(options.setterProp));
				this.set("value", this.get("getterProperty").getProperty());
				this.set("display_name", this.get("getterProperty").get("display_name"));
				this.set("name", this.get("getterProperty").get('name'));

				this.listenTo(this.get("getterProperty"), "change:value", function (model) {
					if (this._handleGetterChange()) {
						that.set("value", model.get('value'));
						that.trigger("change", model);
					}
				});

				this._isGetterRunning = false;
			},

			refresh: function (options) {
				return this.get("getterProperty").load(options);
			},

			initiateGetterPoll: function () {
				this._isGetterRunning = true;

				this.currentPollIndex = 0;
				if (this.getterPollTimeout) {
					clearTimeout(this.getterPollTimeout);
				}
				this.getterPollTimeout = setTimeout(this.getterPollHelper,
						config.pollingTimeAccumulatorArray[this.currentPollIndex] * 1000);
			},

			setAttributes: function  (data) {
				this.set(data);
			},

			getterPollHelper: function () {
				var that = this;
				if (this.currentPollIndex++ > config.pollingTimeAccumulatorArray.length){
					var device = App.salusConnector.getDevice(this.get("getterProperty").get("device_key")),
							getter = this.get("getterProperty"),
							setter = this.get("setterProperty");

					this.trigger("propertiesFailedSync");
//					App.warn("Device (name/dsn): " + device.getDisplayName() + "/" + device.get("dsn") + "\n " +
//							 "Get Property (name/id/value): " + getter.get("name") + " / " + getter.get("key") + "/" + getter.get("value") + "\n" +
//							 "Set Property (name/id/value): " + setter.get("name") + " / " + setter.get("key") + "/" + setter.get("value"));
					this._isGetterRunning = false;
				} else {
					this.refresh({ "queryString": { "pollIndex": this.currentPollIndex} }).then(function () {
						that.trigger("getOtherProperty");
						if (that._isGetterRunning) {
							that.getterPollTimeout = setTimeout(that.getterPollHelper,
									config.pollingTimeAccumulatorArray[that.currentPollIndex] * 1000);
						}
					});
				}
			},

			getProperty: function () {
				return this.get("value");
			},

			getDatapoints: function () {
				return this.get("getterProperty").getDatapoints();
			},

			getSetterVal: function () {
				return this.get("setterProperty").getProperty();
			},

			setProperty: function (value) {
				var that = this;
				return this.get("setterProperty").setProperty(value).then(function () {
//					var oldVal = that.get("value");
					that.set('value', value);
//					var setDataUpdatedAt = moment(that.get("setterProperty").get("data_updated_at"));
//					setDataUpdatedAt = setDataUpdatedAt.add(1, "seconds");
//					that.get("getterProperty").set("data_updated_at", setDataUpdatedAt.toISOString());
//					that.get("getterProperty").set("data_updated_at", that.get("setterProperty").get("data_updated_at"));
//					that.dontTriggerChange = true;
//					that.set("value", oldVal);
//					that.dontTriggerChange = false;

					that.trigger("propertiesSynced");
					that.initiateGetterPoll();
				}).catch(function() {
					that.trigger("propertiesSynced");
				});
			},
			getDisplayNameTranslated: function () {
				return this.get("getterProperty").getDisplayNameTranslated();
			},
			updateData: function (arg1, props) {
				this.get("setterProperty").updateData(props.setterProp);
				this.get("getterProperty").updateData(props.getterProp);
			},

			_triggerSync: function () {
				this.trigger("propertiesSynced");
				clearTimeout(this.getterPollTimeout);
				this.getterPollTimeout = null;
				this._isGetterRunning = false;
			},

			/***
			 * returns true if value on this Linked Device Property model should be updated to the getter
			 * this occurs when the properties have synced, or the poll is not running
			 * */
			_handleGetterChange: function () {
				if (this._isGetterRunning) {
					var getterProp = this.get("getterProperty"),
						setterProp = this.get("setterProperty"),
						getVal = getterProp.get("value"),
						setVal = setterProp.get("value"),
						getTimeStamp = moment(getterProp.get("data_updated_at")),
						setTimeStamp = moment(setterProp.get("data_updated_at"));
					// if string values come down with any white space they may not be compared correctly
					if (_.isString(getVal)) {
						getVal = getVal.trim();
					}

					if (_.isString(setVal)) {
						setVal = setVal.trim();
					}

					if (getVal === setVal) {
						this._triggerSync();
						return true;
					} else if (setTimeStamp.add(1, "seconds") < getTimeStamp) {
						// we've become out of sync
						this.get("setterProperty").load();
						this._triggerSync();
						return true;
					}

					return false;
				} else {
					return true;
				}
			}
		});
	});

	return App.Models.LinkedDevicePropertyModel;
});
