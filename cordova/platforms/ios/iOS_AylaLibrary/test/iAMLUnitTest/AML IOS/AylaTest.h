//
//  AylaTest.h
//  AML IOS
//
//  Created by Yipei Wang on 1/31/13.
//  Copyright (c) 2013 Ayla Networks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AylaNetworks.h"


/* test sections you wanna enable here */
//#define ENABLE_SETTING_TEST
//#define ENABLE_USER_SERVICE_TEST
//#define ENABLE_DEVICE_TEST
//#define ENABLE_DEVICE_PROPERTY_TEST
//#define ENABLE_DEVICE_PROPERTY_TRIGGER_TEST
//#define ENABLE_DEVICE_REGISTRATION_TEST
//#define AML_TEST_DEVICE_NOTIFICATION
//#define AML_TEST_DEVICE_FILE_PROPERTY

//-----> Changes required for your system: <-----
//-----> Update DEVICE SPECIFIC TEST CRITERIA section with your company informaiton <-----

//------------------- BEGIN DEVICE SPECIFIC TEST CRITERIA ------------------------------
// Use your developer account email address and password from https://developer.aylanetworks.com
#define AML_TEST_USER_NAME @"me@mycompany.com"// @"me@mycompany.com"
#define AML_TEST_PASSWORD @"myPassword"//@"myPassword"

// Replace these with the application ID and secret issued to your company by Ayla Networks
#define AML_TEST_APP_ID @"yourAppId"            // Ayla Control App ID = @"iMCA-id"
#define AML_TEST_APP_SECRET @"yourAppSecret"    // Ayla Control App Secret = @"iMCA-8839749"

//change AML_DEVICE_SSID_REGEX to match your products unique SSID AP signature
#define AML_DEVICE_SSID_REGEX @"(^Ayla)-[0-9A-Fa-f]{12}"

// Use this timeout value to avoid test flows being blocked
#define AML_RUNLOOP_MAX_TIMEOUT 30

// change LAN_MODE_TEST_LOOPS to set number of loop DeviceLanMode test should run.
#define LAN_MODE_TEST_LOOPS 1

// Size of string properties used for testing
#define TEST_STRING_LEN     0

// Uncomment to run OPTIONAL tests
//#define AML_TEST_RESEND_CONFIRMATION_EMAIL  @"resendConfirmationEmail.com"        // Uncomment and set to a signed-up but not confirmed e-mail address
//#define AML_TEST_RESET_PASSWORD_EMAIL       @"resetPasswordEmail.com"               // Uncomment and set to an e-mail address of which you want to reset the password

//#define AML_TEST_SHARE_EMAIL                @"targetUserEmail.com"                  // share resource with this registered user

//-------------------MATCH FOLLOWING VALUES TO YOUR SITUATION IN .M FILE-----------------

// Replace with the product name of your Standard Demo Kit
extern NSString *gblAmlProductName;

// Replace with the property name you want to test
extern NSString *gblAmlPropertyName; // use "Green_LED" for the Standard Demo Kit
//extern NSString *gblAmlPropertyName = @"mypropertyname"; // add your own for your company product
//extern NSString *gblAmlPropertyName = @"rndInt"; // these are used to test supported property types
//extern NSString *gblAmlPropertyName = @"rndBool";
//extern NSString *gblAmlPropertyName = @"aFloat";
//extern NSString *gblAmlPropertyName = @"aDecimal";
//extern NSString *gblAmlPropertyName = @"aString";

extern NSString *gblBooleanTypePropertyName1;
extern NSString *gblStringTypePropertyName1;
extern NSString *gblIntegerTypePropertyName1;
extern NSString *gblDecimalTypePropertyName1;
extern NSString *gblStreamTypePropertyName1;
extern NSString *gblFloatTypePropertyName1;
extern NSString *gblFilePropertyName; // use "Green_LED" for the Standard Demo Kit

// Replace with values used by triggers & apps
extern NSString *gblAmlCountryCode ;
extern NSString *gblAmlPhoneNumber ; // sms trigger alerts are sent here
extern NSString *gblAmlEmailAddress ; // email trigger alerts are sent here
//------------------- END DEVICE SPECIFIC TEST CRITERIA ------------------------------


// track success/failure of tests
extern int gblNumberOfTestSections;
extern int gblTestSectionsCompleted;
extern int gblNumberOfTests ; // added as tests are executed
extern int gblTestsPassed ;
extern int gblTestsFailed ;
extern int gblTestTimeout ; // seconds to wait for all tests to complete
extern bool gblLoopTesting ;

// test objects
extern AylaDevice *gblAmlTestDevice ;
extern AylaProperty *gblAmlTestProperty;
extern AylaProperty *gblAmlTestFileProperty;
extern AylaPropertyTrigger *gblAmlTestPropertyTrigger;
extern AylaApplicationTrigger *gblAmlTestApplicationTrigger;
extern AylaDatum *gblAmlTestDatum;
extern AylaShare *gblAmlTestShare;
extern NSString *outputMsgs;
extern NSString *gblTestString;
extern bool runAllInALoop;

#define PFLogRaw(_status_, ...) \
    NSString *displayMsg = [NSString stringWithFormat: @"%@, %@", _status_, \
                     [[NSStringFromSelector(_cmd) componentsSeparatedByString:@":"] objectAtIndex:0]]; /* method name */ \
    NSString *logMsg = [NSString stringWithFormat: @"%@, amlTest, %@", _status_, \
                     [[NSStringFromSelector(_cmd) componentsSeparatedByString:@":"] objectAtIndex:0]]; /* method name */ \
    NSString *myMsg = [NSString stringWithFormat: @"" __VA_ARGS__]; \
    if ([myMsg length] != 0) { \
        displayMsg = [NSString stringWithFormat: @"%@, %@", displayMsg, myMsg]; \
        logMsg = [NSString stringWithFormat: @"%@, %@", logMsg, myMsg]; \
    } \
    if ([_status_ isEqualToString:@"P"]) { \
        gblTestsPassed++; \
        gblNumberOfTests++; \
    } \
    if ([_status_ isEqualToString:@"F"]) { \
        gblTestsFailed++; \
        gblNumberOfTests++; \
    }

#define PFLog(_status_, ...) \
    do { \
        PFLogRaw(_status_, ##__VA_ARGS__) \
        [_textView setText: [NSString stringWithFormat: @"%@%@\n", _textView.text, displayMsg]]; \
        saveToLog(logMsg); \
    } while (0)

#define PFLogOnly(_status_, ...) \
    do { \
        PFLogRaw(_status_, ##__VA_ARGS__) \
        saveToLog(logMsg); \
    } while (0)


@interface AylaTest : NSObject

@end

@interface TestSequencer : NSObject {
    int nextTestIndex;
    NSArray *testSuite;
}

- (TestSequencer *)initWithTestSuite:(NSArray *)myTestSuite;

+ (TestSequencer *)testSequencerWithTestSuite:(NSArray *)myTestSuite;

- (void)executeNextTest:
/*success:*/(void (^)(void))successBlock
    failure:(void (^)(void))failureBlock;

- (int)numberOfTests;

@end

