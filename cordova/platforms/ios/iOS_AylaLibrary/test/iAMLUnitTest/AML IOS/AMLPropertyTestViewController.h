//
//  AMLPropertyTestViewController.h
//  AML IOS
//
//  Created by Yipei Wang on 4/17/13.
//  Copyright (c) 2013 Ayla Networks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMLPropertyTestViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *boolInputTextField;
@property (weak, nonatomic) IBOutlet UITextField *boolOutputTextField;
@property (weak, nonatomic) IBOutlet UITextField *boolHttpTextField;
@property (weak, nonatomic) IBOutlet UITextField *intInputTextField;
@property (weak, nonatomic) IBOutlet UITextField *intOutputTextField;
@property (weak, nonatomic) IBOutlet UITextField *intHttpTextField;
@property (weak, nonatomic) IBOutlet UITextField *decimalInputTextField;
@property (weak, nonatomic) IBOutlet UITextField *decimalOutputTextField;
@property (weak, nonatomic) IBOutlet UITextField *decimalHttpTextField;
@property (weak, nonatomic) IBOutlet UITextField *strInputTextField;
@property (weak, nonatomic) IBOutlet UITextField *strOutputTextField;
@property (weak, nonatomic) IBOutlet UITextField *strHttpTextField;
@property (weak, nonatomic) IBOutlet UITextField *streamInputTextField;
@property (weak, nonatomic) IBOutlet UITextField *streamOutputTextField;
@property (weak, nonatomic) IBOutlet UITextField *streamHttpTextField;
@property (weak, nonatomic) IBOutlet UISwitch *enableLanModeSwitch;

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UITextField *statusField;
@property (weak, nonatomic) IBOutlet UIButton *boolButton;
@property (weak, nonatomic) IBOutlet UIButton *intButton;
@property (weak, nonatomic) IBOutlet UIButton *decimalButton;
@property (weak, nonatomic) IBOutlet UIButton *strButton;
@property (weak, nonatomic) IBOutlet UIButton *streamButton;

@property (weak, nonatomic) IBOutlet UILabel *boolLabel;
@property (weak, nonatomic) IBOutlet UILabel *intLabel;
@property (weak, nonatomic) IBOutlet UILabel *decimalLabel;
@property (weak, nonatomic) IBOutlet UILabel *strLabel;
@property (weak, nonatomic) IBOutlet UILabel *streamLabel;



@end
