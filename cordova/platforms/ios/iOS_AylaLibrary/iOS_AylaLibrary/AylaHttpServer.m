//
//  AylaHttpServer.m
//  Ayla Mobile Library
//
//  Created by Yipei Wang on 2/8/13.
//  Copyright (c) 2015 Ayla Networks. All rights reserved.
//

#import "AylaHttpServer.h"
#import "HTTPDataResponse.h"
#import "HTTPLogging.h"
#import "HTTPMessage.h"
#import "AylaSystemUtilsSupport.h"
#import "AylaLanModeSupport.h"
#import "AylaReachabilitySupport.h"
#import "AylaNotify.h"
#import "AylaEncryption.h"
#import "AylaSecurity.h"
#import "AylaSecuritySupport.h"
#import "AylaDeviceSupport.h"
#import "AylaSetupSupport.h"
#import "AylaCacheSupport.h"
static const int httpLogLevel = HTTP_LOG_LEVEL_WARN;

@implementation AylaHttpServer 

- (id)initWithPort:(int) portNum
{
    self = [super init];
    if(self){
        //[self setType: @"_http._tcp."];
        [self setPort: portNum];
        //[self setConnectionClass:[AylaHttpServerConnection class]];
    }
    return self;
}


- (BOOL)start:(NSError *__autoreleasing *)errPtr
{
    [super setType: @"_http._tcp."];
    [super setConnectionClass:[AylaHttpServerConnection class]];
    return [super start:errPtr];
}

@end



@implementation AylaHttpServerConnection

- (BOOL)supportsMethod:(NSString *)method atPath:(NSString *)path
{
	HTTPLogTrace();
	// Add support for POST/PUT/DELETE
	if ([method isEqualToString:@"POST"])
	{
        /*
        if ([path isEqualToString:@"/local_reg/post.html"])
		{
			return requestContentLength < 50;
		}
        */
        return true;
	}
	else if ([method isEqualToString:@"PUT"])
	{
        return true;
	}
    else if ([method isEqualToString:@"DELETE"])
	{
        return true;
	}
    
    return [super supportsMethod:method atPath:path];
}

- (BOOL)expectsRequestBodyFromMethod:(NSString *)method atPath:(NSString *)path
{
	HTTPLogTrace();
	// Inform HTTP server that we expect a body to accompany a POST request
	if([method isEqualToString:@"POST"])
		return YES;
	return [super expectsRequestBodyFromMethod:method atPath:path];
}

- (NSObject<HTTPResponse> *)httpResponseForMethod:(NSString *)method URI:(NSString *)path
{
	HTTPLogTrace();
    
	if ([method isEqualToString:@"POST"]) {
        
        NSString *postStr = nil;
		NSData *postData = [request body];
		if (postData) {
			postStr = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
		}
        
        if ([path rangeOfString:@"local_lan/key_exchange.json"].location != NSNotFound) {
            
            [[AylaLanMode sessionTimer] stop];
            [AylaLanMode setSessionState:KEY_EXCHANGE];
            
            // do key exchange
            NSError *jerr;
            id responseJSON = [NSJSONSerialization JSONObjectWithData:postData options:NSJSONReadingMutableContainers error:&jerr];
            if(responseJSON == NULL){
                saveToLog(@"%@, %@, %@:%d, %@", @"E", @"httpServer", @"httpCode", 400, @"httpResponseForMethod.key_exchange");
                 NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                 AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:400 headerParam:headerDict dataParam:nil];
                [[AylaLanMode sessionTimer] start];
                return resp;
            }
            
            NSDictionary *jsonDict = responseJSON;
            NSDictionary *info = [jsonDict objectForKey:@"key_exchange"];
            
            NSString *localRnd = [AylaEncryption randomToken:16];
            NSNumber *curTime = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]*1000000];
            
            //Check key exchange mechanism
            BOOL isKeyExchangeRequiredByModule = NO;
            if(![info objectForKey:@"key_id"]){
                isKeyExchangeRequiredByModule = YES;
            }
            
            if(isKeyExchangeRequiredByModule) {

                NSString *respStr = [AylaSecurity processRSAKeyExhangeWithDictionary:info];
                if(!respStr) {
                    [[AylaLanMode sessionTimer] start];
                    saveToLog(@"%@, %@, %@:%d, %@", @"E", @"httpServer", @"httpCode", 400, @"httpResponseForMethod.key_exchange_rsa");
                    NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                    AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:400 headerParam:headerDict dataParam:nil];
                    return resp;
                }
                
                NSData *respData = [respStr dataUsingEncoding:NSUTF8StringEncoding];
                saveToLog(@"%@, %@, %@:%d, %@:%@", @"I", @"httpServer", @"httpCode", 200, @"resp", respStr, [NSString stringWithFormat:@"%@.%@",@"httpResponseForMethod.key_exchange",localSetupUri]);
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:200 headerParam:headerDict dataParam:respData];
                
                //ready to start wifi setup, currently only RSA key exchange supported
                [AylaSetup securityType:AylaSetupSecurityTypeRSA];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if([AylaSetup continueBlock]) {
                        [AylaSetup continueBlock](YES);
                    }
                });
                
                [[AylaLanMode sessionTimer] start];
                return resp;
            }
            else {
                [AylaEncryption setVersion:[info valueForKey:@"ver"]];
                [AylaEncryption setProto1:[info valueForKey:@"proto"]];
                [AylaEncryption setKeyId1:[info valueForKey:@"key_id"]];
                
                saveToLog(@"%@, %@, %@:%@, %@:%@, %@:%@, %@", @"I", @"keyInfo", @"ver", [AylaEncryption version], @"proto", [AylaEncryption proto1] , @"key_id", [AylaEncryption keyId1], @"httpResponseForMethod.key_exchange");
                
                if([[AylaEncryption version] intValue]!= 1){
                    saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"httpServer", @"httpCode", 426, @"version", @"!= 1", @"httpResponseForMethod.key_exchange");
                    NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                    AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:426 headerParam:headerDict dataParam:nil];
                    
                    if([AylaLanMode inSetupMode]) {
                        [AylaSecurity startKeyExchange:nil];
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSDictionary *returnNotify = [AylaNotify initNotifyDictionaryWithType:AML_NOTIFY_TYPE_SESSION dsn:[AylaLanMode device].dsn status:400 description:nil values:nil];
                            [AylaNotify returnNotify:returnNotify];
                        });
                        [AylaReachability setDeviceReachability:AML_REACHABILITY_LAN_MODE_DISABLED];
                    }
                    [[AylaLanMode sessionTimer] start];
                    return resp;
                }
                if([AylaLanMode device] ==nil || [[AylaLanMode device] lanModeConfig]==nil){
                    NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                    AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:400 headerParam:headerDict dataParam:nil];
                    
                    if([AylaLanMode inSetupMode]) {
                        saveToLog(@"%@, %@, %@:%d, %@:%@, %@", @"W", @"httpServer", @"httpCode", 400, @"device", @"==nil", @"httpResponseForMethod.key_exchange");
                        [AylaSecurity startKeyExchange:nil];
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSDictionary *returnNotify = [AylaNotify initNotifyDictionaryWithType:AML_NOTIFY_TYPE_SESSION dsn:[AylaLanMode device].dsn status:400 description:nil values:nil];
                            [AylaNotify returnNotify:returnNotify];
                        });
                        [AylaReachability setDeviceReachability:AML_REACHABILITY_LAN_MODE_DISABLED];
                        saveToLog(@"%@, %@, %@:%d, %@:%@, %@", @"E", @"httpServer", @"httpCode", 400, @"device", @"==nil", @"httpResponseForMethod.key_exchange");
                    }
                    [[AylaLanMode sessionTimer] start];
                    return resp;
                }
                if(![[AylaEncryption keyId1] isEqualToNumber:[[[AylaLanMode device] lanModeConfig] lanipKeyId]]){
                    
                    [AylaCache save:AML_CACHE_LAN_CONFIG withIdentifier:[[AylaLanMode device] dsn] andObject:nil];
                    
                    saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"httpServer", @"httpCode", 412, @"KeyId", @"can't match", @"httpResponseForMethod.key_exchange");
                    
                    NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                    AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:412 headerParam:headerDict dataParam:nil];
                    
                    //set lanKeyIdToInvalid
                    [[[AylaLanMode device] lanModeConfig] setLanipKeyId:[NSNumber numberWithInt:-1]];
                    //[[AylaLanMode sessionTimer] start];
                    //[[AylaLanMode device] getLanModeConfig]
                    
                    if([AylaLanMode inSetupMode]) {
                        [AylaSecurity startKeyExchange:nil];
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSDictionary *returnNotify = [AylaNotify initNotifyDictionaryWithType:AML_NOTIFY_TYPE_SESSION dsn:[AylaLanMode device].dsn status:412 description:nil values:nil];
                            [AylaNotify returnNotify:returnNotify];
                        });
                        [AylaReachability setDeviceReachability:AML_REACHABILITY_LAN_MODE_DISABLED];
                    }
                        
                    [[AylaLanMode sessionTimer] start];
                    return resp;
                }
                if([[AylaEncryption proto1] intValue]!= 1){
                    saveToLog(@"%@, %@, %@:%d, %@:%@", @"E", @"httpServer", @"httpCode", 426, @"proto", @"!= 1", @"httpResponseForMethod.key_exchange");
                    NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                    AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:426 headerParam:headerDict dataParam:nil];
                    //[[AylaLanMode sessionTimer] start];
                    
                    if([AylaLanMode inSetupMode]) {
                        [AylaSecurity startKeyExchange:nil];
                    }
                    else {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSDictionary *returnNotify = [AylaNotify initNotifyDictionaryWithType:AML_NOTIFY_TYPE_SESSION dsn:[AylaLanMode device].dsn status:426 description:nil values:nil];
                            [AylaNotify returnNotify:returnNotify];
                        });
                        [AylaReachability setDeviceReachability:AML_REACHABILITY_LAN_MODE_DISABLED];
                    }
                    
                    [[AylaLanMode sessionTimer] start];
                    return resp;
                }
                
                [AylaEncryption generateSessionkeys:nil sRnd1:[info valueForKey:@"random_1"] nTime1:[info valueForKey:@"time_1"]
                                                        sRnd2:localRnd nTime2:curTime];
                
                NSString *respStr = [NSString stringWithFormat:@"{\"random_2\":\"%@\",\"time_2\":%@}", localRnd, curTime];            
                NSData *respData = [respStr dataUsingEncoding:NSUTF8StringEncoding];            
                
                [AylaLanMode setSessionState: UP];
                saveToLog(@"%@, %@, %@:%d, %@:%@", @"I", @"httpServer", @"httpCode", 200, @"resp", respStr, @"httpResponseForMethod.key_exchange");
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:200 headerParam:headerDict dataParam:respData];
                
                if([AylaLanMode inSetupMode]) {
                    [AylaSetup securityType:AylaSetupSecurityTypeToken];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if([AylaSetup continueBlock]) {
                            [AylaSetup continueBlock](YES);
                        }
                    });
                    [[AylaLanMode sessionTimer] start];
                }
                else {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        NSDictionary *returnNotify = [AylaNotify initNotifyDictionaryWithType:AML_NOTIFY_TYPE_SESSION dsn:[AylaLanMode device].dsn status:200 description:nil values:[NSArray new]];
                        [AylaNotify returnNotify:returnNotify];
                    });
                
                    [[AylaLanMode sessionTimer] start];
                }
                [AylaReachability setDeviceReachability:AML_REACHABILITY_REACHABLE];
                return resp;
            }
        }
        else if([path rangeOfString:@"local_lan/property/datapoint.json"].location != NSNotFound){
            
            NSError *jerr;
            
            id responseJSON = [NSJSONSerialization JSONObjectWithData:postData options:NSJSONReadingMutableContainers error:&jerr];
            if(responseJSON == NULL){
                
                saveToLog(@"%@, %@, %@:%d, %@", @"E", @"httpServer", @"httpCode", 400, @"httpResponseForMethod.datapoint");
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:400 headerParam:headerDict dataParam:nil];
                return resp;
            }
            
            NSDictionary *jsonDict = responseJSON;
            NSString *sign = [jsonDict valueForKeyPath:@"sign"];
            NSString *enc = [jsonDict valueForKeyPath:@"enc"];
            
            NSData *decodedSign = [AylaEncryption base64Decode:sign];
            NSData *decodedEnc = [AylaEncryption base64Decode:enc];
            
            NSString *decryptedEnc = [AylaEncryption lanModeDecryptInStream:decodedEnc];
            if(decryptedEnc == nil){
                [[AylaLanMode sessionTimer] stop];
                saveToLog(@"%@, %@, %@:%@", @"E", @"httpServer", @"decryption", @"can't be completed", @"httpResponseForMethod.datapoint");
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:400 headerParam:headerDict dataParam:nil];
                [[AylaLanMode sessionTimer] start];
                [AylaDevice extendLanModeSession:POST_LOCAL_REGISTRATION haveDataToSend:false];
                return resp;
            }
            
            NSData *calcSign = [AylaEncryption hmacForKeyAndData:[AylaEncryption devSignKey] data:[decryptedEnc dataUsingEncoding:NSUTF8StringEncoding]];
            if(![decodedSign isEqualToData:calcSign]){
                
                saveToLog(@"%@, %@, %@:%@", @"E", @"httpServer", @"signature", @"is invalid", @"httpResponseForMethod.datapoint");
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:401 headerParam:headerDict dataParam:nil];
                return resp;
            }
            
            id encapJSON = [NSJSONSerialization JSONObjectWithData:[decryptedEnc dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&jerr];
            if(encapJSON == NULL){
                
                saveToLog(@"%@, %@, %@:%@, %@:%d, %@", @"E", @"httpServer", @"JSON", @"is invalid", @"httpCode", 400, @"httpResponseForMethod.datapoint");
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:400 headerParam:headerDict dataParam:nil];
                return resp;
            }
            
            jsonDict = encapJSON;
            NSDictionary *data = [jsonDict valueForKeyPath:@"data"];
            NSString *dpName = [data valueForKeyPath:@"name"];
            NSString *dpValue = [data valueForKeyPath:@"value"]? [NSString stringWithFormat:@"%@", [data valueForKeyPath:@"value"]]: nil;
            
            [[AylaLanMode sessionTimer] stop];
            
            // NOTE:
            // currently only support one device each time
            AylaDevice *device = [AylaLanMode device];
            int respStatusCode = 200;
            
            if(!device) {
                saveToLog(@"%@, %@, %@:%@, %@", @"W", @"httpServer", @"device", @"is null", @"httpResponseForMethod.datapoint");
                respStatusCode = 404;
            }
            else {
                // Check if it's a datapoint or cmd update
                static NSString * const defaultLanDatapointPath = @"local_lan/property/datapoint.json?cmd_id=";
                static NSString * const defaultLanDatapointPathWithSlash = @"/local_lan/property/datapoint.json?cmd_id=";
                NSRange slashRange = [path rangeOfString:@"/"];
                NSScanner *scanner = [NSScanner scannerWithString:path];
                [scanner scanString:slashRange.location == 0? defaultLanDatapointPathWithSlash: defaultLanDatapointPath intoString:nil];
                int cmdId = 0;
                BOOL isCommand = [scanner scanInt: &cmdId];
                if(isCommand) {
                    int status = 0;
                    [scanner scanString:@"&status=" intoString:nil];
                    [scanner scanInt:&status];
                    NSInteger statusResp =
                    [device lanModeUpdateWithCmdId:cmdId status:status propertyName:dpName andValue:dpValue];
                    respStatusCode = (int)statusResp;
                }
                else { // datapoint update
                    NSInteger statusResp = [device lanModeUpdateWithPropertyName:dpName andValue:dpValue];
                    respStatusCode = (int)statusResp;
                }
            }
            
            NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
            AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:respStatusCode headerParam:headerDict dataParam:nil];
            
            [AylaLanMode setSessionState: UP];
            [[AylaLanMode sessionTimer] start];
            
            saveToLog(@"%@, %@, %@:%d, %@", @"I", @"httpServer", @"httpCode", respStatusCode, @"httpResponseForMethod.datapoint");
            
            return resp;
        }
        else if([path rangeOfString:[NSString stringWithFormat:@"%@/connect_status?", localSetupUri]].location != NSNotFound){
            
            //[[AylaLanMode sessionTimer] stop];
            // local setup
            NSError *jerr;
            
            id responseJSON = [NSJSONSerialization JSONObjectWithData:postData options:NSJSONReadingMutableContainers error:&jerr];
            if(responseJSON == NULL){
                
                saveToLog(@"%@, %@, %@:%d, %@", @"E", @"httpServer", @"httpCode", 400, @"httpResponseForMethod.datapoint");
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:400 headerParam:headerDict dataParam:nil];
                return resp;
            }
            
            NSDictionary *jsonDict = responseJSON;
            NSString *sign = [jsonDict valueForKeyPath:@"sign"];
            NSString *enc = [jsonDict valueForKeyPath:@"enc"];
            
            NSData *decodedSign = [AylaEncryption base64Decode:sign];
            NSData *decodedEnc = [AylaEncryption base64Decode:enc];
            
            NSString *decryptedEnc = [AylaEncryption lanModeDecryptInStream:decodedEnc];
            if(decryptedEnc == nil){
                saveToLog(@"%@, %@, %@:%@", @"E", @"httpServer", @"decryption", @"can't be completed", @"httpResponseForMethod.datapoint");
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:400 headerParam:headerDict dataParam:nil];
                [AylaDevice extendLanModeSession:POST_LOCAL_REGISTRATION haveDataToSend:false];
                return resp;
            }
            
            NSData *calcSign = [AylaEncryption hmacForKeyAndData:[AylaEncryption devSignKey] data:[decryptedEnc dataUsingEncoding:NSUTF8StringEncoding]];
            if(![decodedSign isEqualToData:calcSign]){
                
                saveToLog(@"%@, %@, %@:%@", @"E", @"httpServer", @"signature", @"is invalid", @"httpResponseForMethod.datapoint");
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:401 headerParam:headerDict dataParam:nil];
                return resp;
            }
            
            //NSString *test = [[NSString alloc] initWithData:[decryptedEnc dataUsingEncoding:NSUTF8StringEncoding] encoding:NSUTF8StringEncoding];
            //NSLog(@"decrypted : %@", test);
            
            id encapJSON = [NSJSONSerialization JSONObjectWithData:[decryptedEnc dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableContainers error:&jerr];
            if(encapJSON == NULL){
                
                saveToLog(@"%@, %@, %@:%@, %@:%d, %@", @"E", @"httpServer", @"JSON", @"is invalid", @"httpCode", 400, @"httpResponseForMethod.datapoint");
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:400 headerParam:headerDict dataParam:nil];
                return resp;
            }
            
            jsonDict = encapJSON;
            
            int respStatusCode = 200;
            saveToLog(@"%@, %@, %@:%d, %@", @"I", @"httpServer", @"httpCode", respStatusCode, @"httpResponseForMethod.datapoint");
            NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
            AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:respStatusCode headerParam:headerDict dataParam:nil];
            
            NSScanner *scanner = [NSScanner scannerWithString: path];
            [scanner scanString:[NSString stringWithFormat:@"%@/connect_status?cmd_id=", localSetupUri] intoString:nil];
            BOOL success = YES;
            int cmdId = 0;
            success = [scanner scanInt: &cmdId];
            
            if(!success){
                saveToLog(@"%@, %@, %@:%d, %@", @"I", @"httpServer", @"httpCode", respStatusCode, @"httpResponseForMethod.datapoint");
            }
            else{
                //command resp
                int status = 0;
                [scanner scanString:@"&status=" intoString:nil];
                [scanner scanInt:&status];
                
                void(^block)(NSDictionary*) =  [AylaLanMode getCommandsOutstanding:[NSString stringWithFormat:@"%d", cmdId]];
                if(block==nil){
                    saveToLog(@"%@, %@, %@:%d, %@", @"I", @"httpServer", @"noReturnBlockForCommandResp-Discard", path, @"httpResponseForMethod.datapoint");
                }
                
                if(status < 200 || status > 299){
                    NSDictionary *errorDict = [[NSDictionary alloc] initWithObjectsAndKeys: [NSNumber numberWithInt:status] , @"httpStatusCode",
                                               [jsonDict objectForKey:@"data"], @"data", nil];
                    NSDictionary *error = [[NSDictionary alloc] initWithObjectsAndKeys:errorDict, @"error", nil];
                    saveToLog(@"%@, %@, %@:%d, %@", @"E", @"httpServer", @"statusFromDevice", status, @"httpResponseForMethod.datapoint");
                    if(block!=nil){
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            block(error);
                        });
                    }
                }
                else{
                    [[AylaLanMode sessionTimer] start];
                    NSDictionary *resp = [[NSDictionary alloc] initWithObjectsAndKeys: @"success" , @"status", nil];
                    saveToLog(@"%@, %@, %@:%d, %@", @"I", @"httpServer", @"statusFromDevice", status, @"httpResponseForMethod.datapoint");
                    if(block!=nil){
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            block(resp);
                        });
                    }
                }
                [AylaLanMode removeCommandsOutstanding:cmdId];
            }
            return resp;
        }
     
    }
    else if ([method isEqualToString:@"GET"]) {
        
        NSString *postStr = nil;
		NSData *postData = [request body];
		if (postData){
			postStr = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
		}
        
        if([path rangeOfString:@"/commands.json"].location != NSNotFound){
            
            int statusCode = 200;
            NSString *send = nil;
            if([AylaLanMode lanModeState]==UP) {
                AylaLanCommandEntity *command = [AylaLanMode nextInQueue];
                NSString *jsonStr = [command jsonString];
                
                //jsonProperty = @"{\"property\": {\"name\": \"Blue_LED\",\"value\":1,\"base_type\":\"boolean\"}}";
                if(jsonStr!=nil){
                    
                    send = [AylaEncryption encryptEncapsulateSign:[AylaLanMode seqNo] jsonString:jsonStr baseType: [command baseType] sign:[AylaEncryption appSignKey]];
                    [AylaLanMode addSeqNo];
                    [AylaLanMode deQueue];
                    if([AylaLanMode nextInQueue]!=nil){
                        statusCode = 206;
                    }                    
                    //saveToLog(@"%@, %@, %@:%@, %@:%d %@", @"I", @"httpServer", @"sendJson", jsonStr, @"statusCode", statusCode, @"httpResponseForMethod.commands" );
                    saveToLog(@"%@, %@, %@%@, %@:%d %@", @"I", @"httpServer", @"sendJson", @"", @"statusCode", statusCode, @"httpResponseForMethod.commands" );
                    
                }
                else{
                    send = [AylaEncryption encryptEncapsulateSign:[AylaLanMode seqNo] jsonString:@"{}" baseType: -1 sign:[AylaEncryption appSignKey]];
                    if([AylaEncryption appSignKey] == nil)
                        statusCode = 400;
                    else{
                        [AylaLanMode addSeqNo];
                        statusCode = 200;
                    }
                    saveToLog(@"%@, %@, %@:%d, %@:%@, %@", @"I", @"httpServer", @"httpCode", statusCode, @"resp", @"no content", @"httpResponseForMethod.commands");
                }
                
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                NSData *respData = send!=nil?[send dataUsingEncoding:NSUTF8StringEncoding]:[@"[]" dataUsingEncoding:NSUTF8StringEncoding];
                
                AylaDevice *device = [AylaLanMode device];
                if(device) {
                    [device lanModeWillSendCmdEntity:command];
                }
                else {
                    saveToLog(@"%@, %@, %@:%@, %@", @"W", @"httpServer", @"device", @"not found", @"httpResponseForMethod.commands.get");
                }
                
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:statusCode headerParam:headerDict dataParam:respData];
                return resp;
            }
            else{
                saveToLog(@"%@, %@, %@:%d, %@:%d, %@", @"E", @"httpServer", @"httpCode", 404, @"lanModeState", [AylaLanMode lanModeState], @"httpResponseForMethod.commands");
                NSData *respData = [@"<html><head><head><body><h1>Unknown command</h1></body></html>" dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *headerDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"json/application", @"Content-Type", nil];
                AylaHttpResponce *resp = [[AylaHttpResponce alloc] initWithData:404 headerParam:headerDict dataParam:respData];
                return resp;
            }
            
        }
    
    }
	return [super httpResponseForMethod:method URI:path];
}

- (void)prepareForBodyWithSize:(UInt64)contentLength
{
	HTTPLogTrace();
	// If we supported large uploads,
	// we might use this method to create/open files, allocate memory, etc.
}

- (void)processBodyData:(NSData *)postDataChunk
{
	HTTPLogTrace();
	
	// Remember: In order to support LARGE POST uploads, the data is read in chunks.
	// This prevents a 50 MB upload from being stored in RAM.
	// The size of the chunks are limited by the POST_CHUNKSIZE definition.
	// Therefore, this method may be called multiple times for the same POST request.
	
	BOOL result = [request appendData:postDataChunk];
	if (!result)
	{
		HTTPLogError(@"%@[%p]: %@ - Couldn't append bytes!", THIS_FILE, self, THIS_METHOD);
	}
}

@end



@implementation AylaHttpResponce : HTTPDataResponse

int status;
NSDictionary * headers;


- (id)initWithData:(int)httpStatus headerParam:(NSDictionary *)headerParam dataParam:(NSData *)dataParam
{
	if((self = [super init]))
	{
		HTTPLogTrace();
		
		offset = 0;
		data = dataParam;
        if(headerParam!=nil)
            headers = headerParam;
        else
            headers = nil;
        status = httpStatus;
	}
	return self;
}

/**
 * Status code for response.
 * Allows for responses such as redirect (301), etc.
 **/
- (NSInteger)status
{
    return status;
}

/**
 * If you want to add any extra HTTP headers to the response,
 * simply return them in a dictionary in this method.
 **/
- (NSDictionary *)httpHeaders
{
    return headers;
};

/**
 * If you don't know the content-length in advance,
 * implement this method in your custom response class and return YES.
 *
 * Important: You should read the discussion at the bottom of this header.
 **/
//- (BOOL)isChunked;

/**
 * This method is called from the HTTPConnection class when the connection is closed,
 * or when the connection is finished with the response.
 * If your response is asynchronous, you should implement this method so you know not to
 * invoke any methods on the HTTPConnection after this method is called (as the connection may be deallocated).
 **/
//- (void)connectionDidClose;


@end


