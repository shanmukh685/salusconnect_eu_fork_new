aml
===

Ayla Mobile Libraries


To generate documentation
=========================

Run 'gradle generateDebugJavadoc' in command line                                                                     
Documentation will be available at Android_AylaLibrary/build/docs/javadoc/index.html




To generate jar
=========================

Run 'gradle clean compileReleaseJava jar' in command line                                                                     
Jar file will be generated at Android_AylaLibrary/build/libs.
External jars required by the library need to be included in applications using this jar (gson-2.2.4, log4j-1.2.17).                                                       
