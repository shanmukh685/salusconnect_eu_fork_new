/*
 * AMLEVBUnitTestCaseServiceOnly.java
 * Ayla Mobile Library
 * 
 * Created by Di Wang on 05/04/2015.
 * Copyright (c) 2015 Ayla Networks Inc. All rights reserved.
 * */

package com.aylanetworks.AMLUnitTest.evbTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import android.annotation.SuppressLint;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;

import com.aylanetworks.AMLUnitTest.AMLUnitTestConfig;
import com.aylanetworks.AMLUnitTest.PushNotification;
import com.aylanetworks.AMLUnitTest.facility.Test;
import com.aylanetworks.AMLUnitTest.facility.TestSequencer;
import com.aylanetworks.AMLUnitTest.interfaces.DefaultScreen;
import com.aylanetworks.AMLUnitTest.interfaces.ITestResultDisplayable;
import com.aylanetworks.AMLUnitTest.interfaces.IUnitTestable;
import com.aylanetworks.aaml.AylaAppNotification;
import com.aylanetworks.aaml.AylaApplicationTrigger;
import com.aylanetworks.aaml.AylaDatapoint;
import com.aylanetworks.aaml.AylaDatum;
import com.aylanetworks.aaml.AylaDevice;
import com.aylanetworks.aaml.AylaDeviceGateway;
import com.aylanetworks.aaml.AylaDeviceNotification;
import com.aylanetworks.aaml.AylaHost;
import com.aylanetworks.aaml.AylaHostNetworkConnection;
import com.aylanetworks.aaml.AylaLanMode;
import com.aylanetworks.aaml.AylaNetworks;
import com.aylanetworks.aaml.AylaNotify;
import com.aylanetworks.aaml.AylaProperty;
import com.aylanetworks.aaml.AylaPropertyTrigger;
import com.aylanetworks.aaml.AylaReachability;
import com.aylanetworks.aaml.AylaRestService;
import com.aylanetworks.aaml.AylaSchedule;
import com.aylanetworks.aaml.AylaShare;
import com.aylanetworks.aaml.AylaSystemUtils;
import com.aylanetworks.aaml.AylaUser;
import com.aylanetworks.aaml.AylaNetworks.lanMode;

@SuppressLint({ "HandlerLeak", "DefaultLocale" })
public class AMLEVBUnitTestCaseServiceOnly implements IUnitTestable {

	private static final String tag = AMLEVBUnitTestCaseServiceOnly.class.getSimpleName();
	private ITestResultDisplayable mScreen = null;
	
	private TestSequencer testSequencer = null;
	private Runnable mEndHandler = null;
	
	

	private AylaUser gblAmlTestUser = null;
	private AylaDevice gblAmlTestDevice = null;
	private AylaProperty gblAmlTestProperty = null;
	private AylaPropertyTrigger gblAmlTestPropertyTrigger = null;
	private AylaApplicationTrigger gblAmlTestApplicationTrigger = null;
	private AylaDeviceNotification gblAmlTestDeviceNotification = null;
	private AylaAppNotification gblAmlTestAppNotification = null;
	private AylaSchedule gblAmlTestSchedule = null;
	private AylaShare gblAmlTestShare = null;
	private PushNotification gblAmlTestPushNotification = null;
	private AylaRestService restService1 = null;
	private AylaRestService restService4 = null;
	private AylaRestService rsUserLogin = null;
	private AylaRestService rsCreateDatapoint = null;
	private AylaRestService rsCreatePropertyTrigger = null;
	private AylaRestService rsGetPropertyTriggers = null;
	private AylaRestService rsDestroyPropertyTrigger = null;
	private AylaRestService rsCreateSMSApplicationTrigger = null;
	private AylaRestService rsCreateEmailApplicationTrigger = null;
	private AylaRestService rsGetApplicationTriggers = null;
	private AylaRestService rsDestroyApplicationTrigger = null;
	private Message msg = new Message();
	private AylaDeviceGateway gblAmlTestGateway = null;
	
	private String gblAmlPropertyName = null;
	private String gblAmlBaseType = null;
	private String gblAmlSValue = null;
	private Number gblAmlNValue = null;
	
	private boolean isWifi = false;
	private boolean isGateway = false;
	private boolean isNode = false;
	private boolean gblAmlTestLanModeEnabled = false;
	private int gblStringLen = 0;	// Size of test string value
	
	
	
	
	@Override
	public void onResume() {
		if (mScreen == null) {
			mScreen = new DefaultScreen();
		}
		mScreen.init();
		
		initTestSuite();
		
		AylaSystemUtils.lanModeState = lanMode.DISABLED; // disable lan mode, only access cloud service
		
		gblAmlTestPushNotification = new PushNotification();
		gblAmlTestPushNotification.init(
				AMLUnitTestConfig.senderId
				, AMLUnitTestConfig.userName
				, AMLUnitTestConfig.appId);
		
		AylaNetworks.onResume();
	}// end of onResume         
	
	
	
	@Override
	public void start() {
		if (testSequencer == null) {
			onResume();
		}
		
		testSequencer.nextTest().execute();
	}// end of start            

	
	
	@Override
	public void onPause() {
		if (testSequencer == null) {
			onResume();
		}
		
		AylaNetworks.onPause(false);
	}// end of onPause
	
	
	@Override
	public void onDestroy() {
		AylaNetworks.onDestroy();
		//TODO: GC work done here.
		testSequencer = null;
	}// end of onDestroy     
	

	@Override
	public void onEnd() {
		if (mEndHandler!=null) {
			mEndHandler.run();
		}
	}



	@Override
	public void setEndHandler(Runnable r) {
		mEndHandler = r;
	}

	@Override
	public void setResultScreen(ITestResultDisplayable screen) {
		if (screen == null) {
			mScreen = new DefaultScreen();
		} else {
			mScreen = screen;
		}
	}// end of setResultScreen
	
	
	@Override
	public boolean isRunning() {
		if (testSequencer != null) {
			return testSequencer.isTestSuiteRunning();
		}
		return false;
	}
	
	
	// TODO: implement params for different test cases, deviceServiceCoreTestList only for now
	// deviceServiceShareTestList
	// /deviceServiceReceivedShareTestList
	// /deviceServiceConfirmEmailTestList
	private void initTestSuite() {
		List<Test> deviceServiceOnlyTestList = new ArrayList<Test>(settingsTestList);     
		
		deviceServiceOnlyTestList.addAll(deviceServiceStartTestList);
		deviceServiceOnlyTestList.addAll(deviceServiceCoreTestList);
		deviceServiceOnlyTestList.addAll(endList);
		
		testSequencer = new TestSequencer(deviceServiceOnlyTestList);
	}// end of initTestSuite            
	
	
	private List<Test> settingsTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestNothing(); }
	}));
	
	
	private List<Test> deviceServiceStartTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestReturnHostWiFiState2(); } },
			new Test() { public void execute() { amlTestUserLogin2(); } },
			// devices
			new Test() { public void execute() { amlTestGetDevices(); } }
			, new Test() { public void execute() { amlTestCreateDeviceNotification(); } },
			new Test() { public void execute() { amlTestUpdateDeviceNotification(); } },
			new Test() { public void execute() { amlTestGetDeviceNotifications(); } },
			new Test() { public void execute() { amlTestCreateAppNotification(); } },
			new Test() { public void execute() { amlTestUpdateAppNotification(); } },
			new Test() { public void execute() { amlTestGetAppNotifications(); } },
			new Test() { public void execute() { amlTestDeleteAppNotification(); } },
			new Test() { public void execute() { amlTestDeleteDeviceNotification(); } }
			//new Test() { public void execute() { amlTestGetDeviceDetail(); } }	TBD
			));
	
	
	List<Test> deviceServiceCoreTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() {
					gblAmlPropertyName = "cmd";		// string
					Map<String, String> callParams = new HashMap<String, String>();
					callParams.put("names", gblAmlPropertyName);

					amlTestGetProperties(callParams);		// Get a single property
				} },
			new Test() { public void execute() { amlTestGetPropertyDetail(); } },
			new Test() { public void execute() {
					gblAmlBaseType = "string";
					gblAmlNValue = null;
					gblAmlSValue = "Hello - ";
					
					// Append a bunch of random characters to make a long string
					Random randGen = new Random();
					while (gblAmlSValue.length() < gblStringLen) {
						gblAmlSValue += (char) (randGen.nextInt(95) + 32);		// Valid ASCII characters
					}

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.sValue(gblAmlSValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() {
					gblAmlSValue = "Goodbye";

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.sValue(gblAmlSValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() {
					gblAmlPropertyName = "Blue_LED";	// boolean
					amlTestGetProperties();				// Get all properties, filtering out ours
				} },
			new Test() { public void execute() { amlTestGetPropertyDetail(); } },
			new Test() { public void execute() {
					gblAmlBaseType = "boolean";
					gblAmlSValue = null;
					Random random = new Random();
					boolean bValue = random.nextBoolean();
					gblAmlNValue = (bValue) ? 1 : 0;

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.nValue(gblAmlNValue);
					
					amlTestCreateDatapoint(myDatapoint); } },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() {
					gblAmlPropertyName = "decimal_in";		// decimal
					amlTestGetProperties();
				} },
			new Test() { public void execute() { amlTestGetPropertyDetail(); } },
			new Test() { public void execute() {
					gblAmlBaseType = "decimal";
					gblAmlSValue = null;
					gblAmlNValue = 123456.78;

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.nValue(gblAmlNValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() {
					gblAmlNValue = 12345678.90;

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.nValue(gblAmlNValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() {
					gblAmlPropertyName = "input";		// integer
					amlTestGetProperties();
				} },
			new Test() { public void execute() { amlTestGetPropertyDetail(); } },
			new Test() { public void execute() {
					gblAmlBaseType = "integer";
					gblAmlSValue = null;
					Random random = new Random();
					gblAmlNValue = random.nextInt();

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.nValue(gblAmlNValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() {		// Try to generate some activity
					gblAmlBaseType = "integer";
					gblAmlSValue = null;
					Random random = new Random();
					gblAmlNValue = random.nextInt();

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.nValue(gblAmlNValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() { amlTestCreatePropertyTrigger(); } },
			new Test() { public void execute() { amlTestUpdatePropertyTrigger(); } },
			new Test() { public void execute() { amlTestGetPropertyTriggers(); } },
			new Test() { public void execute() { amlTestCreateSMSApplicationTrigger(); } },
			new Test() { public void execute() { amlTestUpdateSMSApplicationTrigger(); } },
			new Test() { public void execute() { amlTestCreateEmailApplicationTrigger(); } },
			new Test() { public void execute() { amlTestGetApplicationTriggers(); } },
			new Test() { public void execute() { amlTestDestroyApplicationTrigger(); } },
			new Test() { public void execute() { amlTestDestroyPropertyTrigger(); } },
			new Test() { public void execute() { amlTestSchedules(); } },
			new Test() { public void execute() { amlTestCreateDeviceDatum(); } },
			new Test() { public void execute() { amlTestUpdateDeviceDatum(); } },
			new Test() { public void execute() { amlTestGetDeviceDatum(); } },
			new Test() { public void execute() { amlTestGetDeviceDatums(); } },
			new Test() { public void execute() { amlTestDeleteDeviceDatum(); } },
			new Test() { public void execute() { amlTestCreateUserDatum(); } },
			new Test() { public void execute() { amlTestUpdateUserDatum(); } },
			new Test() { public void execute() { amlTestGetUserDatum(); } },
			new Test() { public void execute() { amlTestGetUserDatums(); } },
			new Test() { public void execute() { amlTestDeleteUserDatum(); } },
//			new Test() { public void execute() { amlTestGetBlobs(); } },
			new Test() { public void execute() { amlTestUserLogout2(); } }
			));
	
	
	private List<Test> endList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { onEnd(); } }
			));
	
	
	private void amlTestNothing() {
		String passMsg = String.format("%s, %s", "P", "TestNothing");
		amlTestStatusMsg("Pass", "TestNothing", passMsg);
		testSequencer.nextTest().execute();
	}
	
	
	/**
	 * Get WiFi state from host
	 */
	private void amlTestReturnHostWiFiState2() {
		
		if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY 
				|| AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY) {
			
			if ( AylaReachability.isCloudServiceAvailable()) {
				if (AylaLanMode.lanModeState != lanMode.RUNNING) {
					AylaLanMode.resume();	// start the Lan Mode service. Only required for iterative testing
				}
				AylaHost.returnHostWiFiState(returnHostWiFiState2);
			} else {
				String failMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "device", "not reachable", "returnHostWiFiState2");
				amlTestStatusMsg("Fail", "returnHostWiFiState2.reachability", failMsg);	
			}
		} else {
			AylaHost.returnHostWiFiState(returnHostWiFiState2);
		}
	}

	private final Handler returnHostWiFiState2 = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaHostNetworkConnection state = AylaSystemUtils.gson.fromJson(jsonResults,  AylaHostNetworkConnection.class);
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "state", state.state, "returnHostWiFiState2");
				amlTestStatusMsg("Pass", "returnHostWiFiState2", passMsg);	

				testSequencer.nextTest().execute();
//				amlTestUserLogin2();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
				amlTestStatusMsg("Fail", "returnHostWiFiState", errMsg);
			}
		}		
	};
	
	
	
	private void amlTestUserLogin2() {
		if (AMLUnitTestConfig.gblAmlAsyncMode) {
			AylaUser.login(login2, 
					AMLUnitTestConfig.userName, 
					AMLUnitTestConfig.password, 
					AMLUnitTestConfig.appId, 
					AMLUnitTestConfig.appSecret);
		} else {
			// sync
			msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;

			Thread thread = new Thread(new Runnable() {
				public void run()
				{
					AylaRestService rs = AylaUser.login(
							AMLUnitTestConfig.userName, 
							AMLUnitTestConfig.password, 
							AMLUnitTestConfig.appId, 
							AMLUnitTestConfig.appSecret);
					msg =  rs.execute();
				}
			});
			thread.start();
			try {
				thread.join();		// Wait for thread to finish
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			doLogin2(msg);
		}
	}
	
	private final Handler login2 = new Handler() {
		public void handleMessage(Message msg) {
			doLogin2(msg);
		}		
	};
	
	void doLogin2(Message msg) {
		String jsonResults = (String)msg.obj;
		if (msg.what == AylaNetworks.AML_ERROR_OK) {
	
			AylaUser aylaUser = AylaSystemUtils.gson.fromJson(jsonResults,  AylaUser.class);
			aylaUser = AylaUser.setCurrent(aylaUser);
			gblAmlTestUser = aylaUser;		// save for unit test
			
			String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "secondsToExpiry", aylaUser.accessTokenSecondsToExpiry(), "login2");
			amlTestStatusMsg("Pass", "login2", passMsg);
	
			testSequencer.nextTest().execute();
//			amlTestGetDevices(); // next test
		} else {
			String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "login2");
			amlTestStatusMsg("Fail", "" +
					"", errMsg);
		}
	}
	
	
	private void amlTestGetDevices() {
		AylaDevice.getDevices(getDevices);
	} 

	private final Handler getDevices = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDevice[] devices = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice[].class);
				gblAmlTestDevice = null;
				for (AylaDevice device : devices) {
					if ( TextUtils.equals(device.dsn, AMLUnitTestConfig.gblAmlTestEVBDsn) ) {
						
						mScreen.displayProgressInfo(device.toString());
						gblAmlTestLanModeEnabled = false;
						gblAmlTestDevice = device; // assign device under test
						gblAmlTestDevice.lanModeEnable(); //Start a lan mode session with this device
						//String model = device.model;
						//String dsn = device.dsn;
						//String oemMode = device.oemModel;
						//String connectedAt = device.connectedAt;
						//String mac = device.mac;
						//String lanIp = device.lanIp;
						//String token = device.token;
						// check device class type
						isWifi = device.isWifi(); 
						isGateway = device.isGateway(); 
						isNode = device.isNode();
						if (isWifi && 
								(AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.DEVICE_SERVICE_ONLY) 
								&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.SETUP_REGISTRATION_AND_DEVICE_SERVICE) 
								&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.LAN_MODE_ONLY) // LAN_MODE_ONLY would need to get devices first.
								|| isGateway 
								&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.GATEWAY_SERVICE_ONLY) 
								&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY))
						{
							String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "device class & suite", "mismatch", "getDevices - class & suite must match");
							amlTestStatusMsg("Fail", "getDevices", errMsg);
						}

						if (isGateway) {
							gblAmlTestGateway = (AylaDeviceGateway) device;
							//gblAmlTestGateway = (AylaDeviceZigbeeGateway) device;				// if only one kind of gateway for the app
							
						}
					}
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "amlTest", "DSN", device.dsn, "amOwner", device.amOwner(), "getDevices");
				}

				if (gblAmlTestDevice != null) { // simulate user selection of a device
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", devices.length, "getDevices");
					amlTestStatusMsg("Pass", "getDevices", passMsg);	

					if ( (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.LAN_MODE_ONLY) 
							&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY) ) {
						synchronized (testSequencer) {
							testSequencer.nextTest().execute();
						}
					} else {
						// notifierHandle only works when a property changes under lanMode, not for the first time lib retrieves devices from cloud. 
//						testSequencer.nextTest().execute();
					}
//					synchronized (testSequencer) {
//						testSequencer.nextTest().execute();
//					}
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestDevice", "null", "getDevices - no device assigned");
					amlTestStatusMsg("Fail", "getDevices", errMsg);
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDevices");
				amlTestStatusMsg("Fail", "getDevices", errMsg);
			}
		}		
	};
	
	private void amlTestCreateDeviceNotification() {
		AylaDeviceNotification deviceNotification = new AylaDeviceNotification();
		deviceNotification.notificationType = AylaDeviceNotification.aylaDeviceNotificationTypeOnConnectionLost;
		deviceNotification.message = "WiFi connection dropped";
		deviceNotification.threshold = 600;
		deviceNotification.deviceNickname = "deviceNotificaitonNickname";
		//Random random = new Random(); Integer anInt = random.nextInt();

		restService4 = gblAmlTestDevice.createNotification(createOrUpdateDeviceNotification, deviceNotification);
	}

	private final Handler createOrUpdateDeviceNotification = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestDevice.deviceNotification = AylaSystemUtils.gson.fromJson(jsonResults, AylaDeviceNotification.class);
				AylaDeviceNotification deviceNotification = gblAmlTestDevice.deviceNotification;

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "threshold", deviceNotification.threshold, "createOrUpdateDeviceNotification");
				amlTestStatusMsg("Pass", "createDeviceNotification", passMsg);
				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateDeviceNotification");
				amlTestStatusMsg("Fail", "createDeviceNotification", errMsg);
			}		
		}
	};
	
	private void amlTestUpdateDeviceNotification() {
		gblAmlTestDevice.deviceNotification.threshold = 3600;
		
		//gblAmlTestDevice.updateNotification(createOrUpdateDeviceNotification, gblAmlTestDevice.deviceNotification);
		gblAmlTestDevice.deviceNotification.updateNotification(createOrUpdateDeviceNotification);
	}
	
	private void amlTestGetDeviceNotifications() {
		gblAmlTestDevice.getNotifications(getDeviceNotifications, null);
		//gblAmlTestDevice.deviceNotification.getNotifications(getDeviceNotifications, null);
	}
	
	private final Handler getDeviceNotifications = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestDevice.deviceNotifications = AylaSystemUtils.gson.fromJson(jsonResults, AylaDeviceNotification[].class);
				//AylaDeviceNotification deviceNotification = gblAmlTestDevice.deviceNotification;

				if (gblAmlTestDevice.deviceNotifications.length > 0) {
					gblAmlTestDeviceNotification = gblAmlTestDevice.deviceNotifications[0];
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "threshold", gblAmlTestDeviceNotification.threshold, "getDeviceNotifications");
					amlTestStatusMsg("Pass", "getDeviceNotifications", passMsg);
					testSequencer.nextTest().execute();
				} else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDeviceNotifications_err1");
					amlTestStatusMsg("Fail", "createDeviceNotification", errMsg);
				}
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDeviceNotifications_err2");
				amlTestStatusMsg("Fail", "getDeviceNotifications", errMsg);
			}		
		}
	};
	
	private void amlTestDeleteDeviceNotification() {
		gblAmlTestDevice.destroyNotification(destroyDeviceNotification, gblAmlTestDevice.deviceNotification);
		// gblAmlTestDevice.deviceNotification.destroyNotification(destroyDeviceNotification);
	}
	
	private final Handler destroyDeviceNotification = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {		
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "destroyDeviceNotification");
				amlTestStatusMsg("Pass", "destroyDeviceNotification", passMsg);

				testSequencer.nextTest().execute();
				
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "destroyDeviceNotification");
				amlTestStatusMsg("Fail", "destroyDeviceNotification", errMsg);
			}		
		}
	};
	
	
	private void amlTestCreateAppNotification() {
		AylaAppNotification appNotification = new AylaAppNotification();
		
		// pick an appType
		appNotification.appType = AylaAppNotification.aylaAppNotificationTypeSms;
		//appNotification.appType = AylaAppNotification.aylaAppNotificationTypeEmail;
		//appNotification.appType = AylaAppNotification.aylaAppNotificationTypePush;
		
		appNotification.notificationAppParameters.username = "yourNameHere";
		
		// Params for the appType picked
		if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeSms)) {
			appNotification.notificationAppParameters.countryCode = AMLUnitTestConfig.gblAmlCountryCode;
			appNotification.notificationAppParameters.phoneNumber = AMLUnitTestConfig.gblAmlPhoneNumber;
			appNotification.notificationAppParameters.contactId = AMLUnitTestConfig.gblAmlContactID;
			appNotification.notificationAppParameters.message = "Testing create device notification sms application";
		} else if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeEmail)) {
			appNotification.notificationAppParameters.email = AMLUnitTestConfig.gblAmlEmailAddress;
			appNotification.notificationAppParameters.message = "Testing create device notification email application";
		} else if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypePush)) {
			appNotification.notificationAppParameters.registrationId = PushNotification.registrationId;
			appNotification.notificationAppParameters.message = "Testing create device notification push application";
			appNotification.notificationAppParameters.pushSound = "default";
			appNotification.notificationAppParameters.pushMdata = "my meta data";
		}
		
		gblAmlTestDevice.deviceNotification.createApp(createOrUpdateAppNotification, appNotification);
	}

	private final Handler createOrUpdateAppNotification = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaAppNotification appNotification = AylaSystemUtils.gson.fromJson(jsonResults,AylaAppNotification.class);
				gblAmlTestDevice.deviceNotification.appNotification = appNotification;

				String countryCode = appNotification.notificationAppParameters.countryCode;
				String phoneNumber = appNotification.notificationAppParameters.phoneNumber;
				String message = appNotification.notificationAppParameters.message;

				String passMsg = String.format("%s, %s, %s,:%s, %s:%s, %s", "P", "amlTest", "phoneNumber", phoneNumber, "message", message, "createOrUpdateAppNotification_handler");
				amlTestStatusMsg("Pass", "createOrUpdateAppNotification", passMsg);

				testSequencer.nextTest().execute();
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateAppNotification_handler");
				amlTestStatusMsg("Fail", "createOrUpdateAppNotification", errMsg);
			}
		}
	};
	
	private void amlTestUpdateAppNotification() {
		AylaAppNotification appNotification = gblAmlTestDevice.deviceNotification.appNotification;
		
		// pick an appType
		appNotification.appType = AylaAppNotification.aylaAppNotificationTypeSms;
		//appNotification.appType = AylaAppNotification.aylaAppNotificationTypeEmail;
		//appNotification.appType = AylaAppNotification.aylaAppNotificationTypePush;
		
		// Params for the appType picked
		if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeSms)) {
			appNotification.notificationAppParameters.countryCode = AMLUnitTestConfig.gblAmlCountryCode;
			appNotification.notificationAppParameters.phoneNumber = AMLUnitTestConfig.gblAmlPhoneNumber;
			appNotification.notificationAppParameters.message = "Testing update device notification sms application";
		} else
		if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeEmail)) {
			appNotification.notificationAppParameters.username = "yourNameHere";
			appNotification.notificationAppParameters.email = AMLUnitTestConfig.gblAmlEmailAddress;
			appNotification.notificationAppParameters.message = "Testing update device notification email application";
		} else
		if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypePush)) {
			appNotification.notificationAppParameters.registrationId = PushNotification.registrationId;
			appNotification.notificationAppParameters.message = "Testing update device notification push application";
			appNotification.notificationAppParameters.pushSound = "default";
			appNotification.notificationAppParameters.pushMdata = "my meta data";
		}
		
		gblAmlTestDevice.deviceNotification.updateApp(createOrUpdateAppNotification, appNotification);
	}

	private void amlTestGetAppNotifications() {
		//Map<String, String> s = new HashMap<String, String>(); TBD
		//callParams.put("count", "3");
		//callParams.put("test", "def");
		Map<String, String> callParams = null; // cappParams is unused at this time
		gblAmlTestDevice.deviceNotification.getApps(getAppNotifications, callParams);
	}

	private final Handler getAppNotifications = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestDevice.deviceNotification.appNotifications = AylaSystemUtils.gson.fromJson(jsonResults, AylaAppNotification[].class);

				for (AylaAppNotification appNotification : gblAmlTestDevice.deviceNotification.appNotifications) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "P", "amlTest", "message", appNotification.notificationAppParameters.message, "getAppNotifications");
					gblAmlTestAppNotification = appNotification;
				}

				String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestDevice.deviceNotification.appNotifications.length, "getAppNotifications");
				amlTestStatusMsg("Pass", "getAppNotifications", passMsg);

				testSequencer.nextTest().execute();
//				amlTestDestroyApplicationTrigger();
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getAppNotifications");
				amlTestStatusMsg("Fail", "getAppNotifications", errMsg);
			}
		}
	};

	private void amlTestDeleteAppNotification() {
		gblAmlTestDeviceNotification.destroyApp(destroyAppNotification, gblAmlTestAppNotification);
		//gblAmlTestDeviceNotification.appNotification.destroy(destroyAppNotification); 
	}

	private final Handler destroyAppNotification = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {		
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "destroyAppNotification");
				amlTestStatusMsg("Pass", "destroyApplicationTrigger", passMsg);

				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "destroyAppNotification");
				amlTestStatusMsg("Fail", "destroyAppNotification", errMsg);
			}		
		}
	};
	
	
	
	private void amlTestGetDatapointsByActivity(int count) {
		Map<String, String> callParams = new HashMap<String, String>();
		callParams.put("count", Integer.toString(count));
		rsCreateDatapoint = gblAmlTestProperty.getDatapointsByActivity(getDatapointsByActivity, callParams); // next test: no delay
	}

	private final Handler getDatapointsByActivity = new Handler() {
		public void handleMessage(Message msg) {
			boolean pass = false;
			String jsonResults = (String)msg.obj;
			AylaSystemUtils.saveToLog("%s, %s, jsonResults:%s.", "D", "getDatapointByActivity", jsonResults);
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestProperty.datapoints = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatapoint[].class);
				String baseType = gblAmlTestProperty.baseType();
				pass = baseType.equals(gblAmlBaseType);
				if (pass) {					
					pass = false;
					// Print all of the datapoints
					for (AylaDatapoint datapoint : gblAmlTestProperty.datapoints) {
						String sValue = datapoint.sValueFormatted(baseType);
						Number nValue = datapoint.nValueFormatted(baseType);
	
						String nValueStr = nValue.toString();
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "nValue", nValueStr, "sValue", sValue, "getDatapointsByActivity");
						//Log.i("datapoint.toString():", datapoint.toString());
					}
					// Check most recent value
					int mostRecent = gblAmlTestProperty.datapoints.length - 1;
					if ( TextUtils.equals(baseType, "integer") || TextUtils.equals(baseType, "boolean")) {
						Number nValue = gblAmlTestProperty.datapoints[mostRecent].nValueFormatted(baseType);
						int nValueInt = nValue.intValue();
						int gblAmlNValueInt = gblAmlNValue.intValue();
						pass = (nValueInt == gblAmlNValueInt);
						if (pass) {
							String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "nValue", nValueInt, "getDatapointsByActivity");
							amlTestStatusMsg("Pass", "getDatapointsByActivity", passMsg);
						} else {
							String errMsg = String.format("%s, %s, %s:%s, %d!=%d, %s", "F", "amlTest", "error", "nValueMismatch", nValueInt, gblAmlNValueInt, "getDatapointsByActivity");
							amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);							
						}
					} else if ( TextUtils.equals(baseType, "float") || TextUtils.equals(baseType, "decimal")) {
						Number nValue = gblAmlTestProperty.datapoints[mostRecent].nValueFormatted(baseType);
						double nValueDouble = nValue.doubleValue();
						double gblAmlNValueDouble = gblAmlNValue.doubleValue();
						pass = (nValueDouble == gblAmlNValueDouble);
						if (pass) {
							String passMsg = String.format("%s, %s, %s:%f, %s", "P", "amlTest", "nValue", nValueDouble, "getDatapointsByActivity");
							amlTestStatusMsg("Pass", "getDatapointsByActivity", passMsg);
						} else {
							String errMsg = String.format("%s, %s, %s:%s, %f!=%f, %s", "F", "amlTest", "error", "nValueMismatch", nValueDouble, gblAmlNValueDouble, "getDatapointsByActivity");
							amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);							
						}
					} else if (TextUtils.equals(baseType, "string")) {
						String sValue = gblAmlTestProperty.datapoints[mostRecent].sValueFormatted(baseType);
						pass = TextUtils.equals(sValue, gblAmlSValue);
						if (pass) {
							String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "sValue", sValue, "getDatapointsByActivity");
							amlTestStatusMsg("Pass", "getDatapointsByActivity", passMsg);
						} else {
							String errMsg = String.format("%s, %s, %s:%s, %s!=%s, %s", "F", "amlTest", "error", "sValueMismatch", sValue, gblAmlSValue, "getDatapointsByActivity");
							amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);							
						}
					} else {
						String errMsg = String.format("%s, %s, %s:%s, %s, %s", "F", "amlTest", "error", "illegalBaseType", baseType, "getDatapointsByActivity");
						amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);												
					}
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s!=%s, %s", "F", "amlTest", "error", "baseTypeMismatch", baseType, gblAmlBaseType, "getDatapointsByActivity");
					amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);					
				}
				if (pass) {
					synchronized (testSequencer) {
						testSequencer.nextTest().execute();
					}
				}
//				amlTestCreatePropertyTrigger();
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getDatapointsByActivity");
				amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);
			}		
		}
	};
	
	
	private void amlTestCreateDatapoint(final AylaDatapoint datapoint) {
		if (AMLUnitTestConfig.gblAmlAsyncMode) {
			// asynchronous
			rsCreateDatapoint = gblAmlTestProperty.createDatapoint(createDatapoint, datapoint); // next test: no delay
		} else {
			// sync
			msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;
			
			// use synchronous method calls
			Thread thread = new Thread(new Runnable() {
				public void run()
				{
						AylaRestService rs;
						
						// get all schedules associated with this device
						rs = gblAmlTestProperty.createDatapoint(datapoint); // next test: no delay
						msg = rs.execute();
				}
			});
			thread.start();
			try {
				thread.join();		// Wait for thread to finish
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			doCreateDatapoint(msg);
		}
	}

	private final Handler createDatapoint = new Handler() {
		public void handleMessage(Message msg) {
			doCreateDatapoint(msg);
		}
	};
	
	private void doCreateDatapoint(Message msg) {
		String jsonResults = (String)msg.obj;
		AylaSystemUtils.saveToLog("%s, %s, jsonResults:%s.", "D", "doCreateDatapoint", jsonResults);
		if (msg.what == AylaNetworks.AML_ERROR_OK) {
			gblAmlTestProperty.datapoint = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatapoint.class);
			AylaDatapoint datapoint= gblAmlTestProperty.datapoint;

			String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datapoint.value(), "createDatapoint");
			amlTestStatusMsg("Pass", "createDatapoint", passMsg);
			new CountDownTimer(3000, 1000) {
				@Override
				public void onFinish() {
					synchronized (testSequencer) {
						testSequencer.nextTest().execute();
					}
				}

				@Override
				public void onTick(long millisUntilFinished) {
				}
			}.start();
		}  else {	
			String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createDatapoint");
			amlTestStatusMsg("Fail", "createDatapoint", errMsg);
		}		
	}
	
	
	
	
	
public void amlTestGetProperties() {
		
		Map<String, String> param = new HashMap<String, String>();
		//Blue_LED is necessary to update the GblTestProperty
		param.put("names", "attr_read_cmd attr_read_data cmd log attr_set_cmd attr_set_result Blue_LED Green_LED decimal_in input"); 
		amlTestGetProperties(param);
		// get all properties in one API in lan_mode would randomly cause property not found(404), which is a constraint from gateway
//		amlTestGetProperties(null);
	}

	private void amlTestGetProperties(final Map<String, String> callParams) {
		// test async interface
		if (AMLUnitTestConfig.gblAmlAsyncMode) {
			gblAmlTestDevice.getProperties(getProperties, callParams); // next test: no delay
		} else {
			// sync
			msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;

			// test synchronous interface
			Thread thread = new Thread(new Runnable() {
				public void run()
				{
					AylaRestService rs = gblAmlTestDevice.getProperties(callParams);
					msg = rs.execute();
				}
			});
			thread.start();
			try {
				thread.join();		// Wait for thread to finish
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			doGetProperties(msg);
		}
	}

	private final Handler getProperties = new Handler() {
		public void handleMessage(Message msg) {
			doGetProperties(msg);
		};
	};

	private void doGetProperties(Message msg) {
		String jsonResults = (String)msg.obj;
		AylaSystemUtils.saveToLog("%s, %s, jsonResults:%s.", "D", "doGetProperties", jsonResults);
		AylaSystemUtils.saveToLog("%s, %s, gblAmlPropertyName:%s.", "D", "doGetProperties", gblAmlPropertyName);
		if (msg.what == AylaNetworks.AML_ERROR_OK) {
			// for unit test purpose 
			AylaNotify.notifyAcknowledge();
			AylaProperty[] ps = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty[].class);
			gblAmlTestDevice.mergeNewProperties(ps);
			
			if (TextUtils.isEmpty(gblAmlPropertyName)) {
				String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestDevice.properties.length, "getProperties");
				amlTestStatusMsg("Pass", "getProperties", passMsg);
				
				if ( AylaSystemUtils.lanModeState != lanMode.ENABLED ) {
					synchronized (testSequencer) {
						testSequencer.nextTest().execute();
					}
				}
				return;
			}
			
			gblAmlTestProperty = null;
			for (AylaProperty property : gblAmlTestDevice.properties) {
				String name = property.name();
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "name", name, "value", property.value, "getProperties");
				if (TextUtils.equals(name, gblAmlPropertyName)) { // simulate user selection of a property
					gblAmlTestProperty = property; // assign property under test
				}
			}

			
			if (gblAmlTestProperty != null ) {
				String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestDevice.properties.length, "getProperties");
				amlTestStatusMsg("Pass", "getProperties", passMsg);
				
				if ( AylaSystemUtils.lanModeState != lanMode.ENABLED ) {
					synchronized (testSequencer) {
						testSequencer.nextTest().execute();
					}
				}
				
			} else {
				String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestProperty", "null", "getProperties - no property assigned");
				amlTestStatusMsg("Fail", "getProperties", errMsg);
			}
		}  else {
			String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getProperties");
			amlTestStatusMsg("Fail", "getProperties", errMsg);
		}		
	}
	
	
	// usually not get property detail is not necessary, use get devices instead
		private void amlTestGetPropertyDetail() {
			//Map<String, String> callParams = new HashMap<String, String>();
			//callParams.put("class", "service");
			Map<String, String> callParams = null; // callParams is unused at this time
			gblAmlTestProperty.getPropertyDetail(getPropertyDetail, callParams); // next test: no delay		
		}

		private final Handler getPropertyDetail = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaProperty property = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty.class);

					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", property.name(), "getPropertyDetail");
					amlTestStatusMsg("Pass", "getPropertyDetail", passMsg);

					if (AylaSystemUtils.lanModeState != lanMode.ENABLED) {
						synchronized (testSequencer) {
							testSequencer.nextTest().execute();
						}
					}
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getPropertyDetail");
					amlTestStatusMsg("Fail", "getPropertyDetail", errMsg);
				}		
			}
		};
		
		
		
		
		private void amlTestCreatePropertyTrigger() {
			AylaPropertyTrigger propertyTrigger = new AylaPropertyTrigger();
			propertyTrigger.triggerType = "on_change";
			//propertyTrigger.compareType = "<";
			//propertyTrigger.value = "0";
			//propertyTrigger.deviceNickname = "abc";
			Random random = new Random(); Integer anInt = random.nextInt();
			propertyTrigger.propertyNickname = "TriggerName_" + anInt.toString();

			// {"trigger_type":"on_change" }
			rsCreatePropertyTrigger = gblAmlTestProperty.createTrigger(createPropertyTrigger, propertyTrigger);
		}

		private final Handler createPropertyTrigger = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					gblAmlTestProperty.propertyTrigger = AylaSystemUtils.gson.fromJson(jsonResults,AylaPropertyTrigger.class);
					AylaPropertyTrigger propertyTrigger = gblAmlTestProperty.propertyTrigger;

					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", propertyTrigger.value, "createPropertyTrigger");
					amlTestStatusMsg("Pass", "createPropertyTrigger", passMsg);
					testSequencer.nextTest().execute();
				}  else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createPropertyTrigger");
					amlTestStatusMsg("Fail", "createPropertyTrigger", errMsg);
				}		
			}
		};
		
		private void amlTestUpdatePropertyTrigger() {
			AylaPropertyTrigger propertyTrigger = gblAmlTestProperty.propertyTrigger;
			propertyTrigger.triggerType = "compare_absolute";
			propertyTrigger.compareType = ">=";
			propertyTrigger.value = "1";
			//propertyTrigger.deviceNickname = "abc";
			Random random = new Random(); Integer anInt = random.nextInt();
			propertyTrigger.propertyNickname = "TriggerName_" + anInt.toString();

			// {"trigger_type":"compare_absolute", "compare_type":">=", "value":60 }
			rsCreatePropertyTrigger = gblAmlTestProperty.updateTrigger(updatePropertyTrigger, propertyTrigger);
		}

		private final Handler updatePropertyTrigger = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					gblAmlTestProperty.propertyTrigger = AylaSystemUtils.gson.fromJson(jsonResults,AylaPropertyTrigger.class);
					AylaPropertyTrigger propertyTrigger = gblAmlTestProperty.propertyTrigger;

					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", propertyTrigger.value, "updatePropertyTrigger");
					amlTestStatusMsg("Pass", "updatePropertyTrigger", passMsg);
					testSequencer.nextTest().execute();
				}  else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "updatePropertyTrigger");
					amlTestStatusMsg("Fail", "updatePropertyTrigger", errMsg);
				}		
			}
		};

		private void amlTestGetPropertyTriggers() {
			//Map<String, String> callParams = new HashMap<String, String>(); TBD
			//callParams.put("count", "3");
			//callParams.put("test", "def");
			Map<String, String> callParams = null; // callParams unused at this time
			rsGetPropertyTriggers = gblAmlTestProperty.getTriggers(getPropertyTriggers, callParams);
		}

		private final Handler getPropertyTriggers = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					gblAmlTestProperty.propertyTriggers = AylaSystemUtils.gson.fromJson(jsonResults,AylaPropertyTrigger[].class);

					for (AylaPropertyTrigger propertyTrigger : gblAmlTestProperty.propertyTriggers) {
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "compareType", propertyTrigger.compareType, "value", propertyTrigger.value, "getPropertyTrigger");
						gblAmlTestPropertyTrigger = propertyTrigger;
					}

					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestProperty.propertyTriggers.length, "getPropertyTriggers");
					amlTestStatusMsg("Pass", "getPropertyTriggers", passMsg);

					testSequencer.nextTest().execute();
//					amlTestCreateSMSApplicationTrigger();
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getPropertyTriggers");
					amlTestStatusMsg("Fail", "getPropertyTriggers", errMsg);
				}
			}
		};

		private void amlTestDestroyPropertyTrigger() {
			//rsDestroyPropertyTrigger = gblAmlTestProperty.destroyTrigger(destroyPropertyTrigger, gblAmlTestPropertyTrigger);
			rsDestroyPropertyTrigger = gblAmlTestPropertyTrigger.destroyTrigger(destroyPropertyTrigger);
		}

		private final Handler destroyPropertyTrigger = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == AylaNetworks.AML_ERROR_OK) {		
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "destroyPropertyTrigger");
					amlTestStatusMsg("Pass", "destroyPropertyTrigger", passMsg);

					testSequencer.nextTest().execute();
//					amlTestSchedules();
					
				}  else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "destroyPropertyTrigger");
					amlTestStatusMsg("Fail", "destroyPropertyTrigger", errMsg);
				}		
			}
		};
		
		
		private void amlTestCreateSMSApplicationTrigger() {
			AylaApplicationTrigger applicationTrigger = new AylaApplicationTrigger();
			//applicationTrigger.countryCode = gblAmlCountryCode;
			applicationTrigger.phoneNumber = AMLUnitTestConfig.gblAmlPhoneNumber;
			applicationTrigger.message = "Testing Ayla Mobile Library";

			rsCreateSMSApplicationTrigger = gblAmlTestPropertyTrigger.createSMSApplicationTrigger(createSMSApplicationTrigger, applicationTrigger);
		}

		private final Handler createSMSApplicationTrigger = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					gblAmlTestPropertyTrigger.applicationTrigger = AylaSystemUtils.gson.fromJson(jsonResults,AylaApplicationTrigger.class);
					AylaApplicationTrigger applicationTrigger= gblAmlTestPropertyTrigger.applicationTrigger;

					//String countryCode = applicationTrigger.countryCode;
					String phoneNumber = applicationTrigger.phoneNumber;
					String message = applicationTrigger.message;

					String passMsg = String.format("%s, %s, %s,:%s, %s:%s, %s", "P", "amlTest", "phoneNumber", phoneNumber, "message", message, "createSmsApplicationTrigger");
					amlTestStatusMsg("Pass", "createSMSApplicationTrigger", passMsg);

					testSequencer.nextTest().execute();
//					amlTestCreateEmailApplicationTrigger();
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createApplicationTrigger");
					amlTestStatusMsg("Fail", "createSMSApplicationTrigger", errMsg);
				}
			}
		};

		private void amlTestUpdateSMSApplicationTrigger() {
			AylaApplicationTrigger applicationTrigger = gblAmlTestPropertyTrigger.applicationTrigger;
			applicationTrigger.countryCode = AMLUnitTestConfig.gblAmlCountryCode;
			//applicationTrigger.phoneNumber = gblAmlPhoneNumber;
			//applicationTrigger.message = "Testing Ayla Mobile Library";

			rsCreateSMSApplicationTrigger = gblAmlTestPropertyTrigger.updateSMSApplicationTrigger(updateSMSApplicationTrigger, applicationTrigger);
		}

		private final Handler updateSMSApplicationTrigger = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					gblAmlTestPropertyTrigger.applicationTrigger = AylaSystemUtils.gson.fromJson(jsonResults,AylaApplicationTrigger.class);
					AylaApplicationTrigger applicationTrigger= gblAmlTestPropertyTrigger.applicationTrigger;

					String countryCode = applicationTrigger.countryCode;
					String phoneNumber = applicationTrigger.phoneNumber;
					String message = applicationTrigger.message;

					String passMsg = String.format("%s, %s, %s,:%s, %s:%s, %s", "P", "amlTest", "countryCode", countryCode, "message", message, "updateSmsApplicationTrigger");
					amlTestStatusMsg("Pass", "updateSMSApplicationTrigger", passMsg);

					testSequencer.nextTest().execute();
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "updateApplicationTrigger");
					amlTestStatusMsg("Fail", "updateSMSApplicationTrigger", errMsg);
				}
			}
		};
		private void amlTestCreateEmailApplicationTrigger() {
			AylaApplicationTrigger applicationTrigger = new AylaApplicationTrigger();
			applicationTrigger.username = "MDA";
			applicationTrigger.emailAddress = AMLUnitTestConfig.gblAmlEmailAddress;
			applicationTrigger.message = "Testing Ayla Mobile Library";

			// {"trigger_type":"compare_absolute", "compare_type":">=", "value":60 }
			rsCreateEmailApplicationTrigger = gblAmlTestPropertyTrigger.createEmailApplicationTrigger(createEmailApplicationTrigger, applicationTrigger);
		}

		private final Handler createEmailApplicationTrigger = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					gblAmlTestPropertyTrigger.applicationTrigger = AylaSystemUtils.gson.fromJson(jsonResults,AylaApplicationTrigger.class);
					AylaApplicationTrigger applicationTrigger= gblAmlTestPropertyTrigger.applicationTrigger;

					String userName = applicationTrigger.username;
					String emailAddress = applicationTrigger.emailAddress;
					//String message = applicationTrigger.message;

					String passMsg = String.format("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "userName", userName, "emailAddress", emailAddress, "createEmailApplicationTrigger");
					amlTestStatusMsg("Pass", "createEmailApplicationTrigger", passMsg);

					testSequencer.nextTest().execute();
//					amlTestGetApplicationTriggers();
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createApplicationTrigger");
					amlTestStatusMsg("Fail", "createEmailApplicationTrigger", errMsg);
				}
			}
		};

		private void amlTestGetApplicationTriggers() {
			//Map<String, String> callParams = new HashMap<String, String>(); TBD
			//callParams.put("count", "3");
			//callParams.put("test", "def");
			Map<String, String> callParams = null; // cappParams is unused at this time
			rsGetApplicationTriggers = gblAmlTestPropertyTrigger.getTriggers(getApplicationTriggers, callParams);
		}

		private final Handler getApplicationTriggers = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					gblAmlTestPropertyTrigger.applicationTriggers = AylaSystemUtils.gson.fromJson(jsonResults,AylaApplicationTrigger[].class);

					for (AylaApplicationTrigger applicationTrigger : gblAmlTestPropertyTrigger.applicationTriggers) {
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "name", applicationTrigger.name, "username", applicationTrigger.username, "getApplicationTrigger");
						gblAmlTestApplicationTrigger = applicationTrigger;
					}

					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestPropertyTrigger.applicationTriggers.length, "getApplicationTriggers");
					amlTestStatusMsg("Pass", "getApplicationTriggers", passMsg);

					testSequencer.nextTest().execute();
//					amlTestDestroyApplicationTrigger();
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getApplicationTriggers");
					amlTestStatusMsg("Fail", "getApplicationTriggers", errMsg);
				}
			}
		};

		private void amlTestDestroyApplicationTrigger() {
			//rsDestroyApplicationTrigger = gblAmlTestPropertyTrigger.destroyTrigger(destroyApplicationTrigger, gblAmlTestApplicationTrigger);
			rsDestroyApplicationTrigger = gblAmlTestApplicationTrigger.destroyTrigger(destroyApplicationTrigger);
		}

		private final Handler destroyApplicationTrigger = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == AylaNetworks.AML_ERROR_OK) {		
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "destroyApplicationTrigger");
					amlTestStatusMsg("Pass", "destroyApplicationTrigger", passMsg);

					testSequencer.nextTest().execute();
				}  else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "destroyApplicationTrigger");
					amlTestStatusMsg("Fail", "destroyApplicationTrigger", errMsg);
				}		
			}
		};
		
		
		private void amlTestSchedules() {
			if (AMLUnitTestConfig.gblScheduleName != null) {
				msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;
				
				// use synchronous method calls
				Thread thread = new Thread(new Runnable() {
					public void run()
					{
						AylaRestService rs;
						
						// get all schedules associated with this device
						rs = gblAmlTestDevice.getAllSchedules(null);
						msg = rs.execute();
						if (msg.what == AylaNetworks.AML_ERROR_OK) {
							// find one schedule in the array of returned devices
							gblAmlTestDevice.schedules = AylaSystemUtils.gson.fromJson((String)msg.obj,AylaSchedule[].class);
							gblAmlTestDevice.schedule = gblAmlTestDevice.findSchedule(AMLUnitTestConfig.gblScheduleName); // find "sched#" or "schedule_in"
							if (msg.what == AylaNetworks.AML_ERROR_OK) {
								
								HashMap<String, String> callParams = new HashMap<String, String>();
								callParams.put("name", AMLUnitTestConfig.gblScheduleName);
								rs = gblAmlTestDevice.getScheduleByName(callParams);
								msg = rs.execute();
								if (msg.what == AylaNetworks.AML_ERROR_OK) {
									gblAmlTestDevice.schedule = AylaSystemUtils.gson.fromJson((String)msg.obj, AylaSchedule.class);
									boolean saveSchedActive = gblAmlTestDevice.schedule.active;
									gblAmlTestDevice.schedule.active = !saveSchedActive; // flip the active bit
									if (gblAmlTestDevice.schedule.scheduleActions.length > 0) {
										boolean saveActionActive = gblAmlTestDevice.schedule.scheduleActions[0].active;
										gblAmlTestDevice.schedule.scheduleActions[0].active = !saveActionActive; // flip the active bit
										
										// update the schedules and actions
										rs = gblAmlTestDevice.updateSchedule(gblAmlTestDevice.schedule, gblAmlTestDevice.schedule.scheduleActions);
										msg = rs.execute();
										if (msg.what == AylaNetworks.AML_ERROR_OK) {
											gblAmlTestSchedule = new AylaSchedule();
											gblAmlTestSchedule = AylaSystemUtils.gson.fromJson((String)msg.obj, AylaSchedule.class);
											if (gblAmlTestSchedule.active == saveSchedActive || gblAmlTestSchedule.scheduleActions[0].active == saveActionActive ) {
												msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // check flipped bits
											}
										}
									} else {
										AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "No Schedule Action to test", "Schedules");								
									}
								}
							}
						}
					}
				});
				thread.start();
				try {
					thread.join();		// Wait for thread to complete
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				if (msg.arg1 == 200) {
					String passMsg = String.format(Locale.getDefault(), "%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "Schedules");
					amlTestStatusMsg("Pass", "Schedules", passMsg);
				} else if (msg.arg1 == 404) {
					String errMsg = String.format(Locale.getDefault(), "%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "Schedules - either create one or set to null");
					amlTestStatusMsg("Fail", "Schedules", errMsg);
				} else {
					String errMsg = String.format(Locale.getDefault(), "%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "Schedules");
					amlTestStatusMsg("Fail", "Schedules", errMsg);
				}
			} else {
				// Generate a warning log message too
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "Not testing schedules", "Schedules");
				String passMsg = String.format(Locale.getDefault(), "%s, %s, %s:%s, %s", "P", "amlTest", "warning", "Not testing schedules", "Schedules");
				amlTestStatusMsg("Pass", "Schedules", passMsg);
			}
					
			testSequencer.nextTest().execute();
//			amlTestUserLogout2(); // last test
		}
		
		
		private void amlTestUserLogout2() {
			Map<String, String> callParams = new HashMap<String, String>();
			callParams.put("access_token", AylaUser.user.getauthHeaderValue());
			restService1 = AylaUser.logout(logout2, callParams);
		}

		private final Handler logout2 = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaUser aylaUser = AylaSystemUtils.gson.fromJson(jsonResults,  AylaUser.class);
					String logout = aylaUser.getLogout();
					gblAmlTestUser = null;		// save for unit test

					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "logout", logout, "logout");
					amlTestStatusMsg("Pass", "logout", passMsg);

					// End of Device Service Unit Tests
					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "logout");
					amlTestStatusMsg("Fail", "logout", errMsg);
				}
			}		
		};
		
		
		
		
		private void amlTestCreateDeviceDatum() {
			// create a metadata object
			AylaDatum datum = new AylaDatum();
			datum.key = "aDevKey";
			datum.value = "aValue1";

			gblAmlTestDevice.createDatum(createDeviceDatum, datum);
			
			// save for later comparison after get
			gblAmlTestDevice.datum = new AylaDatum();
			gblAmlTestDevice.datum.key = datum.key;
			gblAmlTestDevice.datum.value = datum.value;
		}

		private void amlTestUpdateDeviceDatum() {
			// update metadata for this object
			AylaDatum datum = new AylaDatum();
			datum.key = "aDevKey";
			datum.value = "aValue2";

			gblAmlTestDevice.updateDatum(createDeviceDatum, datum);
			
			// save for later comparison after get
			gblAmlTestDevice.datum = new AylaDatum();
			gblAmlTestDevice.datum.key = datum.key;
			gblAmlTestDevice.datum.value = datum.value;
		}
		
		private final Handler createDeviceDatum = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaDatum datum = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatum.class);
					gblAmlTestDevice.datum = datum;

					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "createDeviceDatum");
					amlTestStatusMsg("Pass", "createDeviceDatum", passMsg);

					testSequencer.nextTest().execute();
//					if (amlTestSuite == testSuite.LAN_MODE_ONLY) {
//						// next Lan Mode test TBD
//					} else {
//						amlTestGetDeviceDatum();
//					}
				}  else {	
					if (msg.arg1 == 422) { // allow recovery from previous failure
						String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "value", gblAmlTestDevice.datum.value, "createDeviceDatum");
						amlTestStatusMsg("Pass", "createDeviceDatum", passMsg);

						testSequencer.nextTest().execute();
						return;
					}
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createDeviceDatum");
					amlTestStatusMsg("Fail", "createDeviceDatum", errMsg);
				}		
			}
		};
		

		private void amlTestGetDeviceDatum() {
			gblAmlTestDevice.getDatumWithKey(getDeviceDatum, "aDevKey");
		}

		private final Handler getDeviceDatum = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaDatum datum = AylaSystemUtils.gson.fromJson(jsonResults, AylaDatum.class);
					gblAmlTestDevice.datum = datum;
					
					//if (gblAmlTestDevice.metadata != null && datum.value.equals(gblAmlTestDevice.metadata.datum.value)) {
						//AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "getDatum");
					//} 
						String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "getDatum");
						amlTestStatusMsg("Pass", "getDatum", passMsg);

						testSequencer.nextTest().execute();
//						amlTestDeleteDeviceDatum(); // next test
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getDatums");
					amlTestStatusMsg("Fail", "getDeviceDatum", errMsg);
				}
			}
		};
		
		private enum WhatToTest {getAll, matchPattern, getNamed}
		private void amlTestGetDeviceDatums() {
			
			Map<String, ArrayList<String>> callParams = null;
			ArrayList<String> filters = null;
			
			WhatToTest whatToTest = WhatToTest.getAll;
			switch (whatToTest) {
				case getAll:
					// NOP
					break;
				case matchPattern:
					// get metadata with keys matching a pattern
					callParams = new HashMap<String, ArrayList<String>>();
					filters = new ArrayList<String>();
					filters.add("%Dev%");
					callParams.put("filters", filters);
					break;
				case getNamed:
					// get metadata with kyes matching one or more names
					callParams = new HashMap<String, ArrayList<String>>();
					filters = new ArrayList<String>();
					filters.add("aDevKey");
					//filters.add("anOtherDevKey");
					callParams.put("filters", filters);
					break;
				default:
					//error
			}
			
			gblAmlTestDevice.getDatum(getDeviceDatums, callParams);
		}

		private final Handler getDeviceDatums = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					String aDatumValue = "";
					
					AylaDatum[] datum = AylaSystemUtils.gson.fromJson(jsonResults, AylaDatum[].class);
					if (datum.length > 0) {
						gblAmlTestDevice.datum = datum[0];
						aDatumValue = datum[0].value;
					} else {
						gblAmlTestDevice.datum = null;
					}

					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", aDatumValue, "getDeviceDatums");
					amlTestStatusMsg("Pass", "getDeviceDatums", passMsg);

					testSequencer.nextTest().execute();
//					amlTestDeleteUserDatum(); // next test
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getDeviceDatums");
					amlTestStatusMsg("Fail", "getDeviceDatums", errMsg);
				}
			}
		};

		private void amlTestDeleteDeviceDatum() {
			gblAmlTestDevice.datum.key = "aDevKey";
			gblAmlTestDevice.deleteDatum(deleteDeviceDatum, gblAmlTestDevice.datum);
		}

		private final Handler deleteDeviceDatum = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == AylaNetworks.AML_ERROR_OK) {		
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "deleteDatum");
					amlTestStatusMsg("Pass", "deleteDeviceDatum", passMsg);

					testSequencer.nextTest().execute();
					// next test
				}  else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteDatum");
					amlTestStatusMsg("Fail", "deleteDatum", errMsg);
				}		
			}
		};
	
	
		private void amlTestCreateUserDatum() {
			// create a metadata object
			AylaDatum datum = new AylaDatum();
			datum.key = "aUserKey";
			datum.value = "aValue1";

			gblAmlTestUser.createDatum(createUserDatum, datum);

			// save for later comparison after get
			gblAmlTestUser.datum = new AylaDatum();
			gblAmlTestUser.datum.key = datum.key;
			gblAmlTestUser.datum.value = datum.value;
		}

		private void amlTestUpdateUserDatum() {
			// update metadata for this object
			AylaDatum datum = new AylaDatum();
			datum.key = "aUserKey";
			datum.value = "aValue2";

			gblAmlTestUser.updateDatum(createUserDatum, datum);

			// save for later comparison after get
			gblAmlTestUser.datum = new AylaDatum();
			gblAmlTestUser.datum.key = datum.key;
			gblAmlTestUser.datum.value = datum.value;
		}

		private final Handler createUserDatum = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaDatum datum = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatum.class);
					gblAmlTestUser.datum = datum;

					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "createUserDatum");
					amlTestStatusMsg("Pass", "createUserDatum", passMsg);

					testSequencer.nextTest().execute();
//					if (amlTestSuite == testSuite.LAN_MODE_ONLY) {
//						// next Lan Mode test TBD
//					} else {
//						amlTestGetUserDatum();
//					}
				}  else {
					if (msg.arg1 == 422) { // allow recovery from previous failure
						String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "value", gblAmlTestUser.datum.value, "createUserDatum");
						amlTestStatusMsg("Pass", "createUserDatum", passMsg);

						testSequencer.nextTest().execute();
						return;
					}
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createUserDatum");
					amlTestStatusMsg("Fail", "createUserDatum", errMsg);
				}		
			}
		};


		private void amlTestGetUserDatum() {
			gblAmlTestUser.getDatumWithKey(getUserDatum, "aUserKey");
		}

		private final Handler getUserDatum = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					AylaDatum datum = AylaSystemUtils.gson.fromJson(jsonResults, AylaDatum.class);
					gblAmlTestUser.datum = datum;

					//if (gblAmlTestUser.metadata != null && datum.value.equals(gblAmlTestUser.metadata.datum.value)) {
					//AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "getUserDatum");
					//} 
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "getUserDatum");
					amlTestStatusMsg("Pass", "getUserDatum", passMsg);

					testSequencer.nextTest().execute();
//					amlTestGetUserDatums(); // next test
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getUserDatums");
					amlTestStatusMsg("Fail", "getUserDatum", errMsg);
				}
			}
		};
		
		private void amlTestGetUserDatums() {
				
			Map<String, ArrayList<String>> callParams = null;
			ArrayList<String> filters = null;
			
			WhatToTest whatToTest = WhatToTest.getNamed;
			switch (whatToTest) {
				case getAll:
					// NOP
					break;
				case matchPattern:
					// get metadata with keys matching a pattern
					callParams = new HashMap<String, ArrayList<String>>();
					filters = new ArrayList<String>();
					filters.add("%UserKey");
					callParams.put("filters", filters);
					break;
				case getNamed:
					// get metadata with keys matching one or more names
					callParams = new HashMap<String, ArrayList<String>>();
					filters = new ArrayList<String>();
					filters.add("aUserKey");
					//filters.add("anOtherUserKey");
					callParams.put("filters", filters);
					break;
				default:
					//error
			}
			
			gblAmlTestUser.getDatum(getUserDatums, callParams);
		}

		private final Handler getUserDatums = new Handler() {
			public void handleMessage(Message msg) {
				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					String aDatumValue = "";
					
					AylaDatum[] datum = AylaSystemUtils.gson.fromJson(jsonResults, AylaDatum[].class);
					if (datum.length > 0) {
						gblAmlTestUser.datum = datum[0];
						aDatumValue = datum[0].value;
					} else {
						gblAmlTestUser.datum = null;
					}

					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", aDatumValue, "getUserDatums");
					amlTestStatusMsg("Pass", "getUserDatums", passMsg);

					testSequencer.nextTest().execute();
//					amlTestDeleteUserDatum(); // next test
				}  else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getUserDatums");
					amlTestStatusMsg("Fail", "getUserDatums", errMsg);
				}
			}
		};

		private void amlTestDeleteUserDatum() {
			gblAmlTestUser.datum.key = "aUserKey";
			gblAmlTestUser.deleteDatum(deleteUserDatum, gblAmlTestUser.datum);
		}

		private final Handler deleteUserDatum = new Handler() {
			public void handleMessage(Message msg) {
				if (msg.what == AylaNetworks.AML_ERROR_OK) {		
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "deleteDatum");
					amlTestStatusMsg("Pass", "deleteUserDatum", passMsg);

					testSequencer.nextTest().execute();
					// next test
				}  else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteDatum");
					amlTestStatusMsg("Fail", "deleteDatum", errMsg);
				}		
			}
		};
		
		
	// TODO: extract as a common utility function
	private void amlTestStatusMsg(String passFail, String methodName,
			String methodMsg) {
		String testMsg = "";
		if (passFail != null && passFail.contains("Pass")) {
			AylaSystemUtils.saveToLog("\n%s", methodMsg);
			testMsg = String.format("%s %s", methodName, "passed");

			if (!passFail.contains("Sync")) {
				mScreen.displayFailMessage(testMsg + "\n\n");
			} else {
				Log.i(tag, testMsg);
			}
		} else if (passFail != null && passFail.contains("Fail")) {
			AylaSystemUtils.saveToLog("\n%s", methodMsg);
			testMsg = String.format("%s %s.", methodName, "failed");
			if (!passFail.contains("Sync")) {
				mScreen.displayPassMessage(testMsg + "\n\n");
			} else {
				Log.e(tag, testMsg);
			}
		} else {
			// TODO: passFail null or no match.
		}
		AylaSystemUtils.saveToLog("%s", testMsg);

	}// end of amlTestStatusMsg

}// end of AMLEVBUnitTestCaseServiceOnly class      






