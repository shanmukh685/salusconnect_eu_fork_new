/*
 * AMLUnitTestActivity.java    
 * Ayla Mobile Library
 * 
 * Created by Daniel Myers on 10/23/12.
 * Copyright (c) 2012 Ayla Networks. All rights reserved.  
 * 
 * 
 * Stable Unit Tests so far, for future reference in the zigbee unit test separation implementation.
 * Deprecated on 05/05/2015.
 * */

package com.aylanetworks.AMLUnitTest;


import com.aylanetworks.AMLUnitTest.TestCasePool.AMLBlobUnitTest;
import com.aylanetworks.AMLUnitTest.TestCasePool.AMLContactsUnitTest;
import com.aylanetworks.AMLUnitTest.facility.Test;
import com.aylanetworks.AMLUnitTest.facility.TestSequencer;
import com.aylanetworks.AMLUnitTest.interfaces.ITestResultDisplayable;
import com.aylanetworks.aaml.*;
import com.aylanetworks.aaml.AylaNetworks.lanMode;
import com.aylanetworks.aaml.zigbee.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

@SuppressLint({ "HandlerLeak", "DefaultLocale" })
public class OldAMLUnitTest 
extends Activity 
implements ITestResultDisplayable
{
	String aAMLVersion = "4.17_Zigbee_lan_merge3";
	
	
	//------------------- BEGIN PRIVATE DATA ------------------------------
	OldAMLUnitTest activity = this;
	private final static String TAG = "AMLUnitTest";
	TextView tv_test1, tv_test2, tv_test3 = null;
	int numberOfClicks = 0;
	String getDeviceResponse = null;
	AylaRestService restService1 = null, restService2 = null, restService3 = null, restService4 = null;
	AylaRestService rsUserLogin = null, rsCreateDatapoint = null, rsCreatePropertyTrigger = null, rsGetPropertyTriggers = null, rsDestroyPropertyTrigger = null;
	AylaRestService rsCreateSMSApplicationTrigger = null, rsCreateEmailApplicationTrigger = null, rsGetApplicationTriggers = null, rsDestroyApplicationTrigger = null;
	boolean delayExecution = false;
	Message msg = new Message();
	private String passMsg = "";
	private String errMsg = "";
	boolean isWifi = false, isGateway = false, isNode = false;

	// test specific variable
	// Used for WiFi Setup & registration
	AylaDevice gblAmlNewDevice = null;
	String gblAmlSetupToken = null;
	String gblAmlLanIp = null;

	AylaUser gblAmlTestUser = null;
	AylaDevice gblAmlTestDevice = null;
	AylaDeviceGateway gblAmlTestGateway = null;
	AylaDeviceZigbeeGateway gblAmlTestZigbeeGateway = null;
	AylaGroupZigbee gblAmlTestSmartPlugLightSocketGroup = null;
	AylaSceneZigbee gblAmlTestSmartPlugLightSocketNodeScene = null;
	AylaSceneZigbee gblAmlTestSmartPlugLightSocketGroupScene = null;
	AylaBindingZigbee gblAmlTestPropertyBinding = null;
	AylaBindingZigbee gblAmlTestGroupBinding = null;
	AylaDeviceNode gblAmlTestSmartPlugNode = null;
	AylaDeviceNode gblAmlTestLightSocketNode = null;
	AylaDeviceNode gblAmlTestRemoteControlNode = null;
	String gblAmlTestSmartPlugNodePropertyName = "1_in_0x0006_0x0000";			// Smart plug "in" property name
	String gblAmlTestLightSocketNodePropertyName = "1_in_0x0006_0x0000";		// Light socket "in" property name
	String gblAmlTestRemoteControlNodePropertyName = "1_out_0x0006_0x0000";		// Remote control "out" property name
	AylaProperty gblAmlTestSmartPlugNodeProperty = null;			// Smart plug "in" property
	AylaProperty gblAmlTestLightSocketNodeProperty = null;			// Light socket "in" property
	AylaProperty gblAmlTestRemoteControlNodeProperty = null;		// Remote control "out" property
	int gblAmlTestSmartPlugNodePropertyValue = 0;			// Smart plug "in" property value
	int gblAmlTestLightSocketNodePropertyValue = 0;			// Light socket "in" property value
	AylaProperty gblAmlTestProperty = null;
	AylaPropertyTrigger gblAmlTestPropertyTrigger = null;
	AylaApplicationTrigger gblAmlTestApplicationTrigger = null;
	AylaDeviceNotification gblAmlTestDeviceNotification = null;
	AylaAppNotification gblAmlTestAppNotification = null;
	AylaSchedule gblAmlTestSchedule = null;
	AylaShare gblAmlTestShare = null;
	PushNotification gblAmlTestPushNotification = null;

	int gblAmlTestHostSaveHostNetId = -3; // save original WAN net id. Impossible value if set programatically
	int gblAmlTestHostNetId = -2; // Save module net id to delete. impossible value if set programatically
	String gblAmlDefaultLanIp = "192.168.0.1";
	String gblAmlTestLanSsid = null;
	int gblAmlTestWiFiTimeout = 10;
	boolean gblAmlTestLanModeEnabled = false;
	int gblStringLen = 0;	// Size of test string value	

	//track success/failure of tests
	int numberOfTests = 0; // total number of tests
	static int testsPassed = 0; // number of individual tests in a suite that passed
	static int testsFailed = 0; // those that failed
	static int numberOfIterations = 0;
	static int iterationsPassed = 0; // number of complete test suite iterations that passed
	static int iterationsFailed = 0; // those that failed
	
	long iterationStartTime;		// in nanoseconds

	private TestSequencer testSequencer = null;
	
	private void setUserSettings() {
//		AylaSystemUtils.serviceType = AylaNetworks.AML_STAGING_SERVICE; // use the development service. Default is AML_DEVICE_SERVICE DNS service based djm
		AylaSystemUtils.serviceType = AylaNetworks.AML_DEVELOPMENT_SERVICE; // use the development service. Default is AML_DEVICE_SERVICE DNS service based
		AylaSystemUtils.loggingLevel = AylaNetworks.AML_LOGGING_LEVEL_INFO; // support for debugging. Default is AML_LOGGING_LEVEL_ERROR
		AylaSystemUtils.saveCurrentSettings();
	}

	// TODO: Put back settings tests
	ArrayList<Test> settingsTestListORIG = new ArrayList<Test>(Arrays.asList(
			// Example of passing multiple args
//			new Test() { public void execute(Object arg1, Object arg2, Object arg3) {
//					amlTestConnectNewDeviceToService((String) arg1, (String) arg2, (String) arg3);
//			} },
			new Test() { public void execute() { amlTestLoadSavedSettings(); } },
			new Test() { public void execute() { amlTestSaveCurrentSettings(); } },
			new Test() { public void execute() { amlTestSaveDefaultSettings(); } }
			));
	
	// TODO: Put back settings tests
	ArrayList<Test> settingsTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestNothing(); }
			}));
				
	ArrayList<Test> setupStartTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestReturnHostScanForNewDevices(); } },
			new Test() { public void execute() { amlTestConnectToNewDevice(); } },
			new Test() { public void execute() { amlTestNewDeviceScanForAPs(); } },
			new Test() { public void execute() { amlTestConnectNewDeviceToService(); } },
			new Test() { public void execute() { amlTestConfirmNewDeviceToServiceConnection(); } }
			));
				
	ArrayList<Test> setupEndTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestDeleteDeviceWifiProfile(); } }
			));
				
	ArrayList<Test> registrationStartTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestReturnHostWiFiState(); } },
			new Test() { public void execute() { amlTestUserLogin(); } },
			new Test() { public void execute() { amlTestRegisterNewDevice(); } }
			));
			
	ArrayList<Test> registrationEndTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestUnregisterDevice(); } },
			new Test() { public void execute() { amlTestUserLogout(); } }
			));
				
	ArrayList<Test> deviceServiceStartTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestReturnHostWiFiState2(); } },
			new Test() { public void execute() { amlTestUserLogin2(); } },
			// devices
			new Test() { public void execute() { amlTestGetDevices(); } },
			new Test() { public void execute() { amlTestCreateDeviceNotification(); } },
			new Test() { public void execute() { amlTestUpdateDeviceNotification(); } },
			new Test() { public void execute() { amlTestGetDeviceNotifications(); } },
			new Test() { public void execute() { amlTestCreateAppNotification(); } },
			new Test() { public void execute() { amlTestUpdateAppNotification(); } },
			new Test() { public void execute() { amlTestGetAppNotifications(); } },
			new Test() { public void execute() { amlTestDeleteAppNotification(); } },
			new Test() { public void execute() { amlTestDeleteDeviceNotification(); } }
			//new Test() { public void execute() { amlTestGetDeviceDetail(); } }	TBD
			));
	
	ArrayList<Test> gatewayServiceStartTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestReturnHostWiFiState2(); } },
			new Test() { public void execute() { amlTestUserLogin2(); } },
			// gateways
			new Test() { public void execute() { amlTestGetDevices(); } },
			new Test() { public void execute() { amlTestOpenGatewayJoinWindow(); } },
//			new Test() { public void execute() { amlTestGetGatewayCandidates(); } },
//			new Test() { public void execute() { amlTestRegisterGatewayCandidate(); } },
			new Test() { public void execute() { amlTestGetGatewayNodes(); } },
			
			new Test() { public void execute() { amlTestGetSmartPlugProperties(); } },
			new Test() { public void execute() { amlTestGetLightSocketProperties(); } },
			new Test() { public void execute() { amlTestGetRemoteControlProperties(); } },
//			new Test() { public void execute() { amlTestIdentify(); } },
			
			new Test() { public void execute() { amlTestCreateSmartPlugDatapoint(); } },
			new Test() { public void execute() { amlTestGetSmartPlugDatapointsByActivity(1); } },

			new Test() { public void execute() { amlTestDeleteAllGroups(); } },
			new Test() { public void execute() { amlTestCreateGroup(); } },	
			new Test() { public void execute() { amlTestUpdateGroup(); } },	
			new Test() { public void execute() { amlTestGetGroups(); } },
			new Test() { public void execute() { amlTestGetGroup(); } },
			
			// BUG: There is a node firmware bug with delete scene returning ZE_ZCB_INVALID_FIELD.
			// Deleting twice in a row works! :-)
			new Test() { public void execute() { amlTestDeleteAllScenes(); } },
			new Test() { public void execute() { amlTestDeleteAllScenes(); } },
			new Test() { public void execute() { amlTestCreateNodeScene(); } },	
			new Test() { public void execute() { amlTestUpdateNodeScene(); } },	
			new Test() { public void execute() { amlTestGetScenes(); } },
			new Test() { public void execute() { amlTestRecallNodeScene(); } },
			new Test() { public void execute() { amlTestDeleteNodeScene(); } },
			new Test() { public void execute() { amlTestDeleteNodeScene(); } },
			new Test() { public void execute() { amlTestCreateGroupScene(); } },	
			new Test() { public void execute() { amlTestGetScene(); } },
			new Test() { public void execute() { amlTestRecallGroupScene(); } },
			new Test() { public void execute() { amlTestDeleteGroupScene(); } },
			new Test() { public void execute() { amlTestDeleteGroupScene(); } },

			new Test() { public void execute() { amlTestDeleteAllBindings(); } },
			new Test() { public void execute() { amlTestCreateBindingToProperty(); } },
			new Test() { public void execute() { amlTestUpdateBinding(); } },
			new Test() { public void execute() { amlTestGetBinding(); } },
			new Test() { public void execute() { amlTestDeletePropertyBinding(); } },
			new Test() { public void execute() { amlTestCreateBindingToGroup(); } },
			new Test() { public void execute() { amlTestGetBindings(); } },
			new Test() { public void execute() { amlTestDeleteGroupBinding(); } },			
			
			new Test() { public void execute() { amlTestDeleteGroup(); } }			// Used by binding and scene tests
/*
			new Test() { public void execute() { amlTestCreateDeviceNotification(); } },
			new Test() { public void execute() { amlTestUpdateDeviceNotification(); } },
			new Test() { public void execute() { amlTestGetDeviceNotifications(); } },
			new Test() { public void execute() { amlTestCreateAppNotification(); } },
			new Test() { public void execute() { amlTestUpdateAppNotification(); } },
			new Test() { public void execute() { amlTestGetAppNotifications(); } },
			new Test() { public void execute() { amlTestDeleteAppNotification(); } },
			new Test() { public void execute() { amlTestDeleteDeviceNotification(); } }
*/
			));

	ArrayList<Test> deviceServiceShareTestList = new ArrayList<Test>(Arrays.asList(
			// owned shares
			new Test() { public void execute() { amlTestCreateShare(); } },
			new Test() { public void execute() { amlTestUpdateShare(); } },
			new Test() { public void execute() { amlTestGetShares(); } },
			new Test() { public void execute() { amlTestGetShare(); } },
			// new Test() { public void execute() { amlTestDeleteShares(); } }
			new Test() { public void execute() { amlTestDeleteShare(); } }
			));
	
	ArrayList<Test> deviceServiceReceivedShareTestList = new ArrayList<Test>(Arrays.asList(
			// received shares
			new Test() { public void execute() { amlTestGetReceivedShares(); } },
			new Test() { public void execute() { amlTestGetReceivedShare(); } },
			new Test() { public void execute() { amlTestDeleteShare(); } }
			));
	
	ArrayList<Test> deviceServiceConfirmEmailTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestResendConfirmation(); } }
			));
	
	ArrayList<Test> deviceServiceResetPasswordTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestResetPassword(); } }
			));
	
	ArrayList<Test> deviceServiceCoreTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() {
					AMLUnitTestConfig.gblAmlPropertyName = "cmd";		// string
					Map<String, String> callParams = new HashMap<String, String>();
					callParams.put("names", AMLUnitTestConfig.gblAmlPropertyName);

					amlTestGetProperties(callParams);		// Get a single property
				} },
			new Test() { public void execute() { amlTestGetPropertyDetail(); } },
			new Test() { public void execute() {
					AMLUnitTestConfig.gblAmlBaseType = "string";
					AMLUnitTestConfig.gblAmlNValue = null;
					AMLUnitTestConfig.gblAmlSValue = "Hello - ";
					
					// Append a bunch of random characters to make a long string
					Random randGen = new Random();
					while (AMLUnitTestConfig.gblAmlSValue.length() < gblStringLen) {
						AMLUnitTestConfig.gblAmlSValue += (char) (randGen.nextInt(95) + 32);		// Valid ASCII characters
					}

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.sValue(AMLUnitTestConfig.gblAmlSValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() {
					AMLUnitTestConfig.gblAmlSValue = "Goodbye";

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.sValue(AMLUnitTestConfig.gblAmlSValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() {
					AMLUnitTestConfig.gblAmlPropertyName = "Blue_LED";	// boolean
					amlTestGetProperties();				// Get all properties, filtering out ours
				} },
			new Test() { public void execute() { amlTestGetPropertyDetail(); } },
			new Test() { public void execute() {
					AMLUnitTestConfig.gblAmlBaseType = "boolean";
					AMLUnitTestConfig.gblAmlSValue = null;
					Random random = new Random();
					boolean bValue = random.nextBoolean();
					AMLUnitTestConfig.gblAmlNValue = (bValue) ? 1 : 0;

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.nValue(AMLUnitTestConfig.gblAmlNValue);
					
					amlTestCreateDatapoint(myDatapoint); } },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() {
					AMLUnitTestConfig.gblAmlPropertyName = "decimal_in";		// decimal
					amlTestGetProperties();
				} },
			new Test() { public void execute() { amlTestGetPropertyDetail(); } },
			new Test() { public void execute() {
					AMLUnitTestConfig.gblAmlBaseType = "decimal";
					AMLUnitTestConfig.gblAmlSValue = null;
					AMLUnitTestConfig.gblAmlNValue = 123456.78;

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.nValue(AMLUnitTestConfig.gblAmlNValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() {
					AMLUnitTestConfig.gblAmlNValue = 12345678.90;

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.nValue(AMLUnitTestConfig.gblAmlNValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() {
					AMLUnitTestConfig.gblAmlPropertyName = "input";		// integer
					amlTestGetProperties();
				} },
			new Test() { public void execute() { amlTestGetPropertyDetail(); } },
			new Test() { public void execute() {
					AMLUnitTestConfig.gblAmlBaseType = "integer";
					AMLUnitTestConfig.gblAmlSValue = null;
					Random random = new Random();
					AMLUnitTestConfig.gblAmlNValue = random.nextInt();

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.nValue(AMLUnitTestConfig.gblAmlNValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() {		// Try to generate some activity
					AMLUnitTestConfig.gblAmlBaseType = "integer";
					AMLUnitTestConfig.gblAmlSValue = null;
					Random random = new Random();
					AMLUnitTestConfig.gblAmlNValue = random.nextInt();

					final AylaDatapoint myDatapoint = new AylaDatapoint();
					myDatapoint.nValue(AMLUnitTestConfig.gblAmlNValue);
					
					amlTestCreateDatapoint(myDatapoint);
				} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() { amlTestCreatePropertyTrigger(); } },
			new Test() { public void execute() { amlTestUpdatePropertyTrigger(); } },
			new Test() { public void execute() { amlTestGetPropertyTriggers(); } },
			new Test() { public void execute() { amlTestCreateSMSApplicationTrigger(); } },
			new Test() { public void execute() { amlTestUpdateSMSApplicationTrigger(); } },
			new Test() { public void execute() { amlTestCreateEmailApplicationTrigger(); } },
			new Test() { public void execute() { amlTestGetApplicationTriggers(); } },
			new Test() { public void execute() { amlTestDestroyApplicationTrigger(); } },
			new Test() { public void execute() { amlTestDestroyPropertyTrigger(); } },
			new Test() { public void execute() { amlTestSchedules(); } },
			new Test() { public void execute() { amlTestCreateDeviceDatum(); } },
			new Test() { public void execute() { amlTestUpdateDeviceDatum(); } },
			new Test() { public void execute() { amlTestGetDeviceDatum(); } },
			new Test() { public void execute() { amlTestGetDeviceDatums(); } },
			new Test() { public void execute() { amlTestDeleteDeviceDatum(); } },
			new Test() { public void execute() { amlTestCreateUserDatum(); } },
			new Test() { public void execute() { amlTestUpdateUserDatum(); } },
			new Test() { public void execute() { amlTestGetUserDatum(); } },
			new Test() { public void execute() { amlTestGetUserDatums(); } },
			new Test() { public void execute() { amlTestDeleteUserDatum(); } },
//			new Test() { public void execute() { amlTestGetBlobs(); } },
			new Test() { public void execute() { amlTestUserLogout2(); } }
			));

	ArrayList<Test> userLogoutTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestUserLogout(); } }
			));
				
	ArrayList<Test> lanModeTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestReturnHostWiFiState2(); } },
			new Test() { public void execute() { amlTestUserLogin2(); } },
			new Test() { public void execute() { amlTestGetDevices(); } },
//			new Test() { public void execute() { amlTestGetDeviceDetail(); } },
			
			// TODO: Some global stuff does not get cleaned up. Need to restart the app to pass this test for now. 
			
			new Test() { public void execute() {
				AMLUnitTestConfig.gblAmlBaseType = "boolean";
				AMLUnitTestConfig.gblAmlPropertyName = "Blue_LED";	// boolean
				amlTestGetProperties();				// Get all properties, filtering out ours
			} },
			
			new Test() { public void execute() {
				AMLUnitTestConfig.gblAmlBaseType = "boolean";
				AMLUnitTestConfig.gblAmlPropertyName = "Blue_LED";
				Map<String, String> callParams = new HashMap<String, String>();
				callParams.put("names", AMLUnitTestConfig.gblAmlPropertyName);

				amlTestGetProperties(callParams);		// Get a single property
			} },
			
			new Test() { public void execute() { amlTestGetPropertyDetail(); } },
			new Test() { public void execute() {
				AMLUnitTestConfig.gblAmlBaseType = "boolean";
				AMLUnitTestConfig.gblAmlNValue = 1 ;
				AMLUnitTestConfig.gblAmlSValue = "Hello - ";
				// Append a bunch of random characters to make a long string
				Random randGen = new Random();
				while (AMLUnitTestConfig.gblAmlSValue.length() < gblStringLen) {
					AMLUnitTestConfig.gblAmlSValue += (char) (randGen.nextInt(95) + 32);		// Valid ASCII characters
				}

				final AylaDatapoint myDatapoint = new AylaDatapoint();
				myDatapoint.sValue(AMLUnitTestConfig.gblAmlSValue);
				myDatapoint.nValue(AMLUnitTestConfig.gblAmlNValue);
				amlTestCreateDatapoint(myDatapoint);
			} },
			new Test() { public void execute() { 
				amlTestGetDatapointsByActivity(2); 
			} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },		// Go to the service this time
			
			new Test() { public void execute() {
				AMLUnitTestConfig.gblAmlPropertyName = "cmd";		// string
				Map<String, String> callParams = new HashMap<String, String>();
				callParams.put("names", AMLUnitTestConfig.gblAmlPropertyName);

				amlTestGetProperties(callParams);		// Get a single property
			} },
			new Test() { public void execute() {
				AMLUnitTestConfig.gblAmlPropertyName = "cmd";		// string
				amlTestGetProperties();
			} },
			new Test() { public void execute() {
				AMLUnitTestConfig.gblAmlBaseType = "string";
				AMLUnitTestConfig.gblAmlNValue = 1 ;
				AMLUnitTestConfig.gblAmlSValue = "Hello - ";
				// Append a bunch of random characters to make a long string
				Random randGen = new Random();
				while (AMLUnitTestConfig.gblAmlSValue.length() < gblStringLen) {
					AMLUnitTestConfig.gblAmlSValue += (char) (randGen.nextInt(95) + 32);		// Valid ASCII characters
				}

				final AylaDatapoint myDatapoint = new AylaDatapoint();
				myDatapoint.sValue(AMLUnitTestConfig.gblAmlSValue);
				myDatapoint.nValue(AMLUnitTestConfig.gblAmlNValue);
				amlTestCreateDatapoint(myDatapoint);
			} },
			new Test() { public void execute() { 
				amlTestGetDatapointsByActivity(2); 
			} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } }
			
			));
	
				
	ArrayList<Test> gatewayLanModeTestList = new ArrayList<Test>(Arrays.asList(
			new Test() { public void execute() { amlTestReturnHostWiFiState2(); } },
			new Test() { public void execute() { amlTestUserLogin2(); } },
			new Test() { public void execute() { amlTestGetDevices(); } },
//			new Test() { public void execute() { amlTestGetDeviceDetail(); } },
			new Test() { public void execute() { amlTestGetGatewayNodes(); } },
			new Test() { public void execute() {
				AMLUnitTestConfig.gblAmlPropertyName = "cmd";		// string
				amlTestGetProperties();
			} },
			new Test() { public void execute() { amlTestGetSmartPlugProperties(); } },
			new Test() { public void execute() { amlTestGetLightSocketProperties(); } },
			new Test() { public void execute() { amlTestCreateSmartPlugDatapoint(); } },
			// Try to get node datapoint immediately after creating would get the old value. 
//			new Test() { public void execute() { amlTestGetSmartPlugDatapointsByActivity(1); } },
//			new Test() { public void execute() { amlTestGetSmartPlugDatapointsByActivity(2); } },	
			new Test() { public void execute() {
				AMLUnitTestConfig.gblAmlBaseType = "string";
				AMLUnitTestConfig.gblAmlNValue = null;
				AMLUnitTestConfig.gblAmlSValue = "Hello - ";
				
				// Append a bunch of random characters to make a long string
				Random randGen = new Random();
				while (AMLUnitTestConfig.gblAmlSValue.length() < gblStringLen) {
					AMLUnitTestConfig.gblAmlSValue += (char) (randGen.nextInt(95) + 32);		// Valid ASCII characters
				}

				final AylaDatapoint myDatapoint = new AylaDatapoint();
				myDatapoint.sValue(AMLUnitTestConfig.gblAmlSValue);
				
				amlTestCreateDatapoint(myDatapoint);
			} },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(1); } },
			new Test() { public void execute() { amlTestGetSmartPlugDatapointsByActivity(1); } },
			new Test() { public void execute() { amlTestGetSmartPlugDatapointsByActivity(2); } },
			new Test() { public void execute() { amlTestGetDatapointsByActivity(2); } }		
			));
				
	//------------------- END PRIVATE DATA ------------------------------
	

	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		AylaNetworks.onDestroy();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.testrest);
		tv_test1 = (TextView) findViewById(R.id.t_query1); //Setup Views and Button for response
		tv_test1.setMovementMethod(new ScrollingMovementMethod());
		tv_test2 = (TextView) findViewById(R.id.t_query2); //Setup Views and Button for response
		tv_test2.setMovementMethod(new ScrollingMovementMethod());
		tv_test3 = (TextView) findViewById(R.id.t_query3); //Setup Views and Button for response
		tv_test3.setMovementMethod(new ScrollingMovementMethod());

		//--------------------- Begin Unit test ------------------------
		
		// must be called once in onCreate() for library initialization
		AylaNetworks.init(this, AMLUnitTestConfig.gblAmlDeviceSsidRegex, AMLUnitTestConfig.appId); 
		setUserSettings();
		AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "W", "aAMLUnitTest", "version", aAMLVersion, "onCreate");
		AylaCache.clearAll();
		
		if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY 
				|| AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY) {
			AylaSystemUtils.lanModeState = lanMode.ENABLED; // enable lan mode
			AylaLanMode.enable(notifierHandle, reachabilityHandle); // start http server, set notifier and reachability handlers
		} else {
			AylaSystemUtils.lanModeState = lanMode.DISABLED; // disable lan mode, only access cloud service
		}
		
		gblAmlTestPushNotification = new PushNotification();
		gblAmlTestPushNotification.init(AMLUnitTestConfig.senderId, 
				AMLUnitTestConfig.userName, 
				AMLUnitTestConfig.appId);
		

		//----------------------------------------------
		// kick off the tests automatically
		if (AMLUnitTestConfig.amlRunMode == AMLUnitTestConfig.runMode.AUTO) {
			amlTestSelectTestSuite();
			testSequencer.nextTest().execute();		// Kick off the tests
		}

		// kick off the tests by pressing button1
		Button button1 = (Button) findViewById(R.id.button1);
		button1.setOnClickListener(new View.OnClickListener(){
			public void onClick(View view){
				try {
					tv_test1.setText(""); tv_test2.setText(""); tv_test3.setText(""); // clear text
					testsPassed = testsFailed = 0; // reset pass/fail count
					if (testSequencer != null && testSequencer.isTestSuiteRunning() && numberOfClicks++ == 0) {
						amlTestSelectTestSuite();		// Just setup the next test to be executed
					} else {
						numberOfClicks = 0;				// If the user restarts twice, come here
						amlTestSelectTestSuite();
						testSequencer.nextTest().execute();		// Kick off the tests
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


	@Override
	protected void onResume() {
		super.onResume();
//		AylaLanMode.resume();
		AylaNetworks.onResume();
	}	

	@Override
	protected void onPause() {
		super.onPause();
//		AylaLanMode.pause(false);
		AylaNetworks.onPause(false);
	}
	
	//
	// -------------------------------- Select unit test suite here ----------------------------------------------
	//
	void amlTestSelectTestSuite() {
		// All test suites begin with testing the settings
		ArrayList<Test> setupOnlyTestList = new ArrayList<Test>(settingsTestList);
		ArrayList<Test> setupAndRegistrationTestList = new ArrayList<Test>(settingsTestList);
		ArrayList<Test> deviceServiceOnlyTestList = new ArrayList<Test>(settingsTestList);
		ArrayList<Test> lanModeOnlyTestList = new ArrayList<Test>(settingsTestList);
		ArrayList<Test> gatewayLanModeOnlyTestList = new ArrayList<Test>(settingsTestList);

		setupOnlyTestList.addAll(setupStartTestList);
		setupOnlyTestList.addAll(setupEndTestList);

		setupAndRegistrationTestList.addAll(setupStartTestList);
		if ( (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.SETUP_AND_REGISTRATION_ONLY) 
				|| (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.SETUP_REGISTRATION_AND_DEVICE_SERVICE)) {	// DEVICE_SERVICE_ONLY
			setupAndRegistrationTestList.addAll(registrationStartTestList);
		}

		// Construct the main device service test list
		ArrayList<Test> deviceServiceTestList = new ArrayList<Test>(deviceServiceStartTestList); // assume wifi device DEVICE_SERVICE_ONLY 
			
		if ( (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_SERVICE_ONLY)) {
			deviceServiceTestList = new ArrayList<Test>(gatewayServiceStartTestList);
		}
		if (! AMLUnitTestConfig.shareWithEmailAddress.equals("shareWith@email.com")) {			// Skip testing shares until shareWithEmailAddress is defined
			deviceServiceTestList.addAll(deviceServiceShareTestList);
		}
		if (! AMLUnitTestConfig.shareReceiveFromEmailAddress.equals("shareReceiveFrom@email.com")) {				// Skip testing shares until shareReceiveFromEmailAddress is defined
			deviceServiceTestList.addAll(deviceServiceReceivedShareTestList);
		}
		if (! AMLUnitTestConfig.resendConfirmationEmailAddress.equals("resendConfirmationEmail.com")) {			// Skip testing shares until resendConfirmationEmailAddress is defined
			deviceServiceTestList.addAll(deviceServiceConfirmEmailTestList);
		}
		if (! AMLUnitTestConfig.resetPasswordEmailAddress.equals("resetPasswordEmail.com")) {			// Skip testing shares until resetPasswordEmailAddress is defined
			deviceServiceTestList.addAll(deviceServiceResetPasswordTestList);
		}
		deviceServiceTestList.addAll(deviceServiceCoreTestList);

		deviceServiceOnlyTestList.addAll(deviceServiceTestList);
		
		// assume wifi device GATEWAY_SERVICE_ONLY
		ArrayList<Test> zigbeeServiceTestList = new ArrayList<Test>(deviceServiceStartTestList);  
		if ( (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_SERVICE_ONLY)) {
//			zigbeeServiceTestList.addAll(gatewayServiceStartTestList);
			zigbeeServiceTestList = new ArrayList<Test>(gatewayServiceStartTestList);  
		}
		if (! AMLUnitTestConfig.shareWithEmailAddress.equals("shareWith@email.com")) {			// Skip testing shares until shareWithEmailAddress is defined
			zigbeeServiceTestList.addAll(deviceServiceShareTestList);
		}
		if (! AMLUnitTestConfig.shareReceiveFromEmailAddress.equals("shareReceiveFrom@email.com")) {				// Skip testing shares until shareReceiveFromEmailAddress is defined
			zigbeeServiceTestList.addAll(deviceServiceReceivedShareTestList);
		}
		if (! AMLUnitTestConfig.resendConfirmationEmailAddress.equals("resendConfirmationEmail.com")) {			// Skip testing shares until resendConfirmationEmailAddress is defined
			zigbeeServiceTestList.addAll(deviceServiceConfirmEmailTestList);
		}
		if (! AMLUnitTestConfig.resetPasswordEmailAddress.equals("resetPasswordEmail.com")) {			// Skip testing shares until resetPasswordEmailAddress is defined
			zigbeeServiceTestList.addAll(deviceServiceResetPasswordTestList);
		}

		ArrayList<Test> setupRegistrationAndDeviceServiceTestList = new ArrayList<Test>(setupAndRegistrationTestList);
		setupRegistrationAndDeviceServiceTestList.addAll(userLogoutTestList);
		setupRegistrationAndDeviceServiceTestList.addAll(deviceServiceTestList);

		setupAndRegistrationTestList.addAll(setupEndTestList);
		setupAndRegistrationTestList.addAll(registrationEndTestList);

		lanModeOnlyTestList.addAll(lanModeTestList);

		gatewayLanModeOnlyTestList.addAll(gatewayLanModeTestList);

		if ( (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.SETUP_ONLY)) {
			// SETUP UNIT TEST SUITE
			// Prerequisites: module in AP mode & unregistered
			testSequencer = new TestSequencer(setupOnlyTestList);
		} else if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.SETUP_AND_REGISTRATION_ONLY) {
			// SETUP & REGISTRATION UNIT TEST SUITE
			// Prerequisites: module in AP mode & unregistered, host phone/tablet connected to a WLAN with Internet access
			testSequencer = new TestSequencer(setupAndRegistrationTestList);
		} else if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.SETUP_REGISTRATION_AND_DEVICE_SERVICE) {
			// SETUP, REGISTRATION, & DEVICE UNIT TEST SUITE
			// Prerequisites: module in AP mode & unregistered, host phone/tablet connected to a WLAN with Internet access
			testSequencer = new TestSequencer(setupRegistrationAndDeviceServiceTestList);
		} else if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.DEVICE_SERVICE_ONLY) {
			// DEVICE SERVICE UNIT TEST SUITE
			// Prerequisites: module connected and registered, host & module connected to the same SSID
			testSequencer = new TestSequencer(deviceServiceOnlyTestList);
		} else if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_SERVICE_ONLY) {
			// DEVICE SERVICE UNIT TEST SUITE
			// Prerequisites: module connected and registered, host & module connected to the same SSID
			testSequencer = new TestSequencer(zigbeeServiceTestList);
		}else if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY) {
			// LAN_MODE_ONLY UNIT TEST SUITE
			// Prerequisites: module connected and registered, host & module connected to the same SSID
			testSequencer = new TestSequencer(lanModeOnlyTestList);
		}else if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY) {
			// GATEWAY_LAN_MODE_ONLY UNIT TEST SUITE
			// Prerequisites: module connected and registered, host & module connected to the same SSID
			testSequencer = new TestSequencer(gatewayLanModeOnlyTestList);
		} else {
			// unknown unit test suite
			String errMsg = String.format("%s, %s, %s:%s, %s", "F", "Settings", "testSuite", "Unknown", "saveDefaultSettings");
			amlTestStatusMsg("Fail", "saveDefaultSettings", errMsg);
		}
		//track success/failure of tests
		numberOfTests = testSequencer.numberOfTests();	// total number of tests
		testsPassed = 0; // those that passed
		testsFailed = 0; // those that failed
		
		iterationStartTime = System.nanoTime();
	}

	//------------------------------------ Settings ----------------------------------------------
	// FIX: Temporarily take out settings tests, since they interfere with cached values (BUG AML-40)
	private void amlTestNothing() {
		String passMsg = String.format("%s, %s", "P", "TestNothing");
		amlTestStatusMsg("Pass", "TestNothing", passMsg);
		testSequencer.nextTest().execute();
	}

	private void amlTestLoadSavedSettings() {
		// ensure default settings were loaded
		int thisWifiTimeout = AylaSystemUtils.wifiTimeout;
		if (thisWifiTimeout == AylaNetworks.DEFAULT_WIFI_TIMEOUT) {
			String passMsg = String.format("%s, %s, %s:%s, %s", "P", "Settings", "thisWifiTimeout", thisWifiTimeout, "loadSavedSettings");
			amlTestStatusMsg("Pass", "loadSavedSettings", passMsg);
		} else {
			String errMsg = String.format("%s, %s, %s:%s, %s", "F", "Settings", "thisWifiTimeout", thisWifiTimeout, "loadSavedSettings");
			amlTestStatusMsg("Fail", "loadSavedSettings", errMsg);
		}
		testSequencer.nextTest().execute();
	}

	private void amlTestSaveCurrentSettings() {
		// change wifi time-out value to a non-default value
		AylaSystemUtils.wifiTimeout = 11;
		AylaSystemUtils.saveCurrentSettings();
		// read config values and check for non-default wifi time-out value
		AylaSystemUtils.loadSavedSettings();
		int thisWifiTimeout = AylaSystemUtils.wifiTimeout;
		if (thisWifiTimeout == 11) {
			String passMsg = String.format("%s, %s, %s:%s, %s", "P", "Settings", "thisWifiTimeout", thisWifiTimeout, "saveCurrentSettings");
			amlTestStatusMsg("Pass", "saveCurrentSettings", passMsg);
		} else {
			String errMsg = String.format("%s, %s, %s:%s, %s", "F", "Settings", "thisWifiTimeout", thisWifiTimeout, "saveCurrentSettings");
			amlTestStatusMsg("Fail", "saveCurrentSettings", errMsg);
		}
		testSequencer.nextTest().execute();
	}

	private void amlTestSaveDefaultSettings() {
		// reset wifi time-out to default value
		AylaSystemUtils.saveDefaultSettings();
		// read configs and ensure wifi time-out value is set to the default
		AylaSystemUtils.loadSavedSettings();
		int thisWifiTimeout = AylaSystemUtils.wifiTimeout;
		if (thisWifiTimeout == AylaNetworks.DEFAULT_WIFI_TIMEOUT) {
			String passMsg = String.format("%s, %s, %s:%s, %s", "P", "Settings", "thisWifiTimeout", thisWifiTimeout, "saveDefaultSettings");
			amlTestStatusMsg("Pass", "saveDefaultSettings", passMsg);
		} else {
			String errMsg = String.format("%s, %s, %s:%s, %s", "F", "Settings", "thisWifiTimeout", thisWifiTimeout, "saveDefaultSettings");
			amlTestStatusMsg("Fail", "saveDefaultSettings", errMsg);
		}
		setUserSettings();		// Restore the settings that were overwritten
		testSequencer.nextTest().execute();
	}

	//-------------------------------------- BEGIN LAN MODE ONLY UNIT TEST SUITE ------------------------------------
	//
	//-------------------Begin Lan Mode Enablement ---------------------
	// Must Enable lan mode as below for AylaLanMode.enable() & disable to engage
	//----------------------- LAN MODE Support Methods --------------------------------

	// called when there is a change in device or service reachability
	private final Handler reachabilityHandle = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "P", "amlTest", "reachability", jsonResults, "reachability");
			}
		}
	};
	
	// Called when a property value changes
	private final Handler notifierHandle = new Handler() {
		public void handleMessage(Message msg) {
		String jsonResults = (String)msg.obj;
		AylaSystemUtils.saveToLog("%s, %s, jsonResults:%s.", "D", "notifierHandle", jsonResults);
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				if (msg.arg1 == 200 && (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY 
						|| AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY)) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "P", "amlTest", "notifier", jsonResults, "notifier");
					AylaNotify notifyMsg = AylaSystemUtils.gson.fromJson(jsonResults, AylaNotify.class);
					
					// Only trigger the next test to for the first "session" message
					if (!gblAmlTestLanModeEnabled && notifyMsg.type.equals("session")) {
						gblAmlTestLanModeEnabled = true;
						synchronized (testSequencer) {
							testSequencer.nextTest().execute();
						}
					}
				} else {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "amlTest", "notifier", jsonResults, "notifier");
				}
			} else {
				AylaNotify.notifyAcknowledge();
			}
		}
	};


	// ------------------------------------------ BEGIN SETUP & REGISTRATION TEST SUITE -------------------------------

	// ---------------------------- Return Host Scan For New Devices --------------------------------
	/**
	 * This MUST be the first Setup call
	 * Finds all new Ayla devices within wifi distance of the host
	 * Present the scan results array to the user to select one
	 * The goal of Setup is to connect the new device to the Ayla device service
	 */
	private void amlTestReturnHostScanForNewDevices() {
		AylaSetup.returnHostScanForNewDevices(returnHostScanForNewDevices);
	}

	private final Handler returnHostScanForNewDevices = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaHostScanResults[] scanResults = AylaSystemUtils.gson.fromJson(jsonResults,  AylaHostScanResults[].class);

				String ssid = "";
				AylaSetup.newDevice.hostScanResults = null;
				for (AylaHostScanResults scanResult : scanResults) {
					ssid = scanResult.ssid;
					if (ssid.equals(AMLUnitTestConfig.gblAmlModuleSsid)) { // simulate user selection of a new device
						AylaSetup.newDevice.hostScanResults = scanResult;
					}
				}
				if (AylaSetup.newDevice.hostScanResults != null) {
					int lastTask = AylaSetup.lastMethodCompleted;
					String passMsg = String.format(Locale.getDefault(), "%s, %s, %s:%s, %s:%d, %s", "P", "amlTest", "ssid", AylaSetup.newDevice.hostScanResults.ssid, "lastTask", lastTask, "returnHostScanForNewDevices");
					amlTestStatusMsg("Pass", "AylaSetup.returnHostScanForNewDevices", passMsg);

					testSequencer.nextTest().execute();
//					amlTestConnectToNewDevice();  // next Setup call
				} else {
					String errMsg = String.format(Locale.getDefault(), "%s, %s, %s:%s, %s", "F", "amlTest", "ssid", "null", "returnHostScanForNewDevices - no device assigned");
					AylaSetup.exit(); // retry this task, or call exit and begin setup again
					amlTestStatusMsg("Fail", "AylaSetup.returnHostScanForNewDevices", errMsg);
				}
			} else {
				String errMsg = String.format(Locale.getDefault(), "%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
				amlTestStatusMsg("Fail", "AylaSetup.returnHostScanForNewDevices", errMsg);
			}
		}		
	};

	// ------------------------- Connect To New Device --------------------
	/**
	 * Connect host to new device selected by user
	 */
	private void amlTestConnectToNewDevice() {
		AylaSetup.connectToNewDevice(connectToNewDevice);
	}

	private final Handler connectToNewDevice = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaSetup.newDevice = AylaSystemUtils.gson.fromJson(jsonResults,AylaModule.class); // special case parsing for "DSN" in api v1.0
				String dsn = AylaSetup.newDevice.dsn;
				String build = AylaSetup.newDevice.build;
				
				String passMsg = String.format("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "dsn", dsn, "build", build, "connectToNewDevice");
				amlTestStatusMsg("Pass", "AylaSetup.connectToNewDevice", passMsg);

				testSequencer.nextTest().execute();
//				amlTestNewDeviceScanForAPs(); // next Setup call
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
				amlTestStatusMsg("Fail", "AylaSetup.connectToNewDevice", errMsg);
			}
		}		
	};

	// ---------------------------- Return New Device Scan For Access Points --------------------------------
	private void amlTestNewDeviceScanForAPs() {
		AylaSetup.getNewDeviceScanForAPs(getDeviceScanForAPs);
	}

	private final Handler getDeviceScanForAPs = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaModuleScanResults[] scanResults = AylaSystemUtils.gson.fromJson(jsonResults,  AylaModuleScanResults[].class);

				String ssid = "";
				for (AylaModuleScanResults scanResult : scanResults) {
					ssid = scanResult.ssid;
					if (ssid.equals(AMLUnitTestConfig.gblAmlLanSsid)) { // simulate user selected ssid
						AylaSetup.lanSsid = ssid;
						AylaSetup.lanPassword = AMLUnitTestConfig.gblAmlLanSsidPassword; // simulate user entered password
						AylaSetup.lanSecurityType = scanResult.security; // v1.2.0
						break;
					}
				}
				if (AylaSetup.lanSsid != null) {
					int lastTask = AylaSetup.lastMethodCompleted;
					String passMsg = String.format("%s, %s, %s:%s, %s:%d, %s", "P", "amlTest", "lanSsid", AylaSetup.lanSsid, "lastTask", lastTask, "getDeviceScanForAPs");
					amlTestStatusMsg("Pass", "AylaSetup.getDeviceScanForAPs", passMsg);

					testSequencer.nextTest().execute();
//					amlTestConnectNewDeviceToService(); // next setup call
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlLanSsid", AMLUnitTestConfig.gblAmlLanSsid, "getDeviceScanForAPs - no device assigned");
					amlTestStatusMsg("Fail", "AylaSetup.getDeviceScanForAPs", errMsg);
					AylaSetup.exit(); // retry this task only, or call exit and begin setup again
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
				amlTestStatusMsg("Fail", "AylaSetup.getDeviceScanForAPs", errMsg);
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
			}
		}		
	};

	// --------------------------------------------- Set Device Connect To Network ------------------------------
	private void amlTestConnectNewDeviceToService() {
		AylaSetup.connectNewDeviceToService(connectNewDeviceToService);
	}

	private final Handler connectNewDeviceToService = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				int lastTask = AylaSetup.lastMethodCompleted;
				String passMsg = String.format("%s, %s, %s:%s, %s:%d, %s", "P", "amlTest", "jsonResults", jsonResults, "lastTask", lastTask, "connectNewDeviceToService");
				amlTestStatusMsg("Pass", "AylaSetup.connectNewDeviceToService", passMsg);

				testSequencer.nextTest().execute();
//				amlTestConfirmNewDeviceToServiceConnection();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "connectNewDeviceToService");
				amlTestStatusMsg("Fail", "AylaSetup.connectNewDeviceToService", errMsg);
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
			}
		}
	};

	// ------------------------------ Begin Confirm Device To Service Connection ---------------------
	private void amlTestConfirmNewDeviceToServiceConnection() {
		AylaSetup.confirmNewDeviceToServiceConnection(confirmNewDeviceToServiceConnection);
	}

	private final Handler confirmNewDeviceToServiceConnection = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				//int lastTask = AylaSetup.lastMethodCompleted;
				int retries = AylaSetup.newDeviceToServiceConnectionRetries;
				String passMsg = String.format("%s, %s, %s:%s, %s:%d, %s", "P", "amlTest", "jsonResults", jsonResults, "retries:", retries, "confirmNewDeviceToServiceConnection");
				amlTestStatusMsg("Pass", "AylaSetup.confirmNewDeviceToServiceConnection", passMsg);

				testSequencer.nextTest().execute();
//				if ( amlTestSuite == testSuite.SETUP_ONLY) {
//					amlTestDeleteDeviceWifiProfile(); // success - delete the wifi profile from the new device
//				} else {
//					amlTestReturnHostWiFiState();  //numberOfTests += 3;// Begin Device Service Unit Test (less Settings tests)
//				}		

			} else {
				int responseCode = (int)msg.arg1;
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", responseCode, msg.obj, "confirmNewDeviceToServiceConnection");
				amlTestStatusMsg("Fail", "AylaSetup.confirmNewDeviceToServiceConnection", errMsg);

				if (responseCode != AylaNetworks.AML_WIFI_ERROR) {
					amlTestGetNewDeviceWiFiStatus(); numberOfTests++; // final Setup call if confirmation failed
				} else {
					AylaSetup.exit(); // retry this task, or call exit and begin setup again
				}
			}
		}
	};

	// ----------------------------------------- GET DEVICE WIFI STATUS ------------------------------------
	private void amlTestGetNewDeviceWiFiStatus() {
		AylaSetup.getNewDeviceWiFiStatus(getNewDeviceWiFiStatus);
	}

	private final Handler getNewDeviceWiFiStatus = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaWiFiStatus wifiStatus = AylaSystemUtils.gson.fromJson(jsonResults,AylaWiFiStatus.class);
				//String dns = wifiStatus.dns;
				//String deviceService = wifiStatus.deviceService;
				String mac = wifiStatus.mac;
				//int ant = wifiStatus.ant;
				//int rssi = wifiStatus.rssi; // always zero in AP mode
				//int bars = wifiStatus.bars;

				int count = 0;
				AylaWiFiConnectHistory[] wifiConnectHistories = null;
				if (wifiStatus.connectHistory != null) {
					wifiConnectHistories = new AylaWiFiConnectHistory[wifiStatus.connectHistory.length];
					for (AylaWiFiConnectHistory connectHistory : wifiStatus.connectHistory) {
						wifiConnectHistories[count] = connectHistory; // first error is the most recent
						AylaSystemUtils.saveToLog("%s, %s, %s:%d, %s:%s, %s", "I", "amlTest", "error", wifiConnectHistories[count].error, "Connect Message", wifiConnectHistories[count].msg, "getNewDeviceWiFiStatus");
						count++;
					}
				}
				String passMsg = String.format("%s, %s, %s:%s, %s:%d, %s", "P", "amlTest", "mac", mac, "wifiConnectHistories", count, "getDeviceWiFiStatus");
				amlTestStatusMsg("Pass", "AylaSetup.getDeviceWiFiStatus", passMsg);

				// ---------------- END OF UNSUCCESSFUL SETUP ----------------------------

			} else {
				int responseCode = (int)msg.arg1;
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", responseCode, msg.obj, "");
				amlTestStatusMsg("Fail", "AylaSetup.getDeviceWiFiStatus", errMsg);
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
			}
		}		
	};

	// ------------------------------------------- Return Host WiFi State --------------------------
	/**
	 * Get WiFi state from host
	 */
	private void amlTestReturnHostWiFiState() {
		AylaHost.returnHostWiFiState(returnHostWiFiState);
	}

	private final Handler returnHostWiFiState = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaHostNetworkConnection state = AylaSystemUtils.gson.fromJson(jsonResults,  AylaHostNetworkConnection.class);
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "state", state.state, "returnHostWiFiState");
				amlTestStatusMsg("Pass", "returnHostWiFiState", passMsg);	

				testSequencer.nextTest().execute();
//				amlTestUserLogin();  // wifi connection is good, begin Setup calls
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
				amlTestStatusMsg("Fail", "returnHostWiFiState", errMsg);
			}
		}		
	};

	// ------------------------------ User Login  ------------------------------------
	private void amlTestUserLogin() {
		restService1 = AylaUser.login(login, 
				AMLUnitTestConfig.userName, 
				AMLUnitTestConfig.password, 
				AMLUnitTestConfig.appId, 
				AMLUnitTestConfig.appSecret);
	}

	private final Handler login = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {

				AylaUser aylaUser = AylaSystemUtils.gson.fromJson(jsonResults,  AylaUser.class);
				aylaUser = AylaUser.setCurrent(aylaUser);
				gblAmlTestUser = aylaUser;		// save for unit test
				//String accessToken = aylaUser.getAccessToken();
				//String refreshToken = aylaUser.getRefreshToken();
				//int expiresIn = aylaUser.getExpiresIn();

				String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "secondsToExpiry", aylaUser.accessTokenSecondsToExpiry(), "login");
				amlTestStatusMsg("Pass", "login", passMsg);

				testSequencer.nextTest().execute();
//				amlTestRegisterNewDevice(); // next test
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "login");
				amlTestStatusMsg("Fail", "login", errMsg);
			}
		}		
	};

	//-------------------------------------- Wifi Device Registration ----------------------------------------------------
	private void amlTestRegisterNewDevice() {
		// retrieved most recently setup device
		AylaDevice newDevice;
		String jsonNewDevice = AylaSetup.get(); // read cached new device info
		if (!jsonNewDevice.equals("")) {
			newDevice = AylaSystemUtils.gson.fromJson(jsonNewDevice,  AylaDevice.class);
		} else { // Use Same-Lan as default
			newDevice = new AylaDevice();
			newDevice.registrationType = AylaNetworks.AML_REGISTRATION_TYPE_SAME_LAN;
			newDevice.dsn = null;
		}

		newDevice.registerNewDevice(registerNewDevice);

	}

	private final Handler registerNewDevice = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestDevice = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice.class);
				String productName = gblAmlTestDevice.getProductName();

				tv_test1.setText(gblAmlTestDevice.toString());

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "productName", productName, "registerNewDevice");
				amlTestStatusMsg("Pass", "registerNewDevice1", passMsg);

				testSequencer.nextTest().execute();
//				if ( (amlTestSuite == testSuite.SETUP_ONLY) || (amlTestSuite == testSuite.SETUP_AND_REGISTRATION_ONLY) ) {
//					amlTestDeleteDeviceWifiProfile(); // delete the wifi profile from the device
//				} else {
//					amlTestUserLogout(); // leave the device connected and registered
//				}
			} else {
				String subMethodFailure = "";
     			if (msg.arg2 == AylaNetworks.AML_REGISTER_NEW_DEVICE) {
    				subMethodFailure = "getRegisterNewDevice";
    				if (msg.arg1 == AylaNetworks.AML_ERROR_NOT_FOUND) {
    					// Unsupported registration type
    				} else if (msg.arg1 == AylaNetworks.AML_NO_ITEMS_FOUND) {
    					// Setup token not found for AP-Mode registration
    				}
    			} else
    			if (msg.arg2 == AylaNetworks.AML_GET_REGISTRATION_CANDIDATE) {
    				subMethodFailure = "getRegistrationCandidate";
    				// No registration candidates were found
    			} else if (msg.arg2 == AylaNetworks.AML_GET_MODULE_REGISTRATION_TOKEN) {
    				subMethodFailure = "getModuleRegistrationCandidate";
    				// Failed to connect to new device for registration

    			}  else if (msg.arg2 == AylaNetworks.AML_REGISTER_DEVICE) {
    				subMethodFailure = "registerDevice";
    				// Failed to register the new device
    			}
				String errMsg = String.format("%s, %s, %s:%d, %s, %s.%s", "F", "amlTest", "error", msg.arg1, msg.obj, "registerNewDevice",  subMethodFailure);
				amlTestStatusMsg("Fail", "registerNewDevice", errMsg);
			}
		}		
	};

	//-------------------------------------- Wifi Device Registration ----------------------------------------------------
	private void amlTestRegisterNewNodes() {
		// no setup for ethernet gateway
		//AylaDevice newDevice;
		//String jsonNewDevice = AylaSetup.get(); // read cached new device info
		//if (!jsonNewDevice.equals("")) {
		//	newDevice = AylaSystemUtils.gson.fromJson(jsonNewDevice,  AylaDevice.class);
		//} else { // Use Same-Lan as default
		//	newDevice = new AylaDevice();
		
		// register new nodes to this gateway
		gblAmlTestDevice.registrationType = AylaNetworks.AML_REGISTRATION_TYPE_NODE;
		gblAmlTestDevice.dsn = null;
		//}

		gblAmlTestDevice.registerNewDevice(registerNewGateway);	// will register the first registration candidate

	}

	private final Handler registerNewGateway = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestDevice = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice.class);
				String productName = gblAmlTestDevice.getProductName();

				tv_test1.setText(gblAmlTestDevice.toString());

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "productName", productName, "registerNewDevice");
				amlTestStatusMsg("Pass", "registerNewDevice1", passMsg);

				testSequencer.nextTest().execute();
//				if ( (amlTestSuite == testSuite.SETUP_ONLY) || (amlTestSuite == testSuite.SETUP_AND_REGISTRATION_ONLY) ) {
//					amlTestDeleteDeviceWifiProfile(); // delete the wifi profile from the device
//				} else {
//					amlTestUserLogout(); // leave the device connected and registered
//				}
			} else {
				String subMethodFailure = "";
     			if (msg.arg2 == AylaNetworks.AML_REGISTER_NEW_DEVICE) {
    				subMethodFailure = "getRegisterNewDevice";
    				if (msg.arg1 == AylaNetworks.AML_ERROR_NOT_FOUND) {
    					// Unsupported registration type
    				} else if (msg.arg1 == AylaNetworks.AML_NO_ITEMS_FOUND) {
    					// Setup token not found for AP-Mode registration
    				}
    			} else
    			if (msg.arg2 == AylaNetworks.AML_GET_REGISTRATION_CANDIDATE) {
    				subMethodFailure = "getRegistrationCandidate";
    				// No registration candidates were found
    			} else if (msg.arg2 == AylaNetworks.AML_GET_MODULE_REGISTRATION_TOKEN) {
    				subMethodFailure = "getModuleRegistrationCandidate";
    				// Failed to connect to new device for registration

    			}  else if (msg.arg2 == AylaNetworks.AML_REGISTER_DEVICE) {
    				subMethodFailure = "registerDevice";
    				// Failed to register the new device
    			}
				String errMsg = String.format("%s, %s, %s:%d, %s, %s.%s", "F", "amlTest", "error", msg.arg1, msg.obj, "registerNewDevice",  subMethodFailure);
				amlTestStatusMsg("Fail", "registerNewDevice", errMsg);
			}
		}		
	};

	//--------------------------------------------- Delete Device Wifi Profile ------------------------------
	private void amlTestDeleteDeviceWifiProfile() {
		String lanIp = AylaSetup.lanIp;
		String lanSsid = AylaSetup.lanSsid;
		AylaModule.deleteDeviceWifiProfile(deleteDeviceWifiProfile, lanIp, lanSsid);
	}

	private final Handler deleteDeviceWifiProfile = new Handler() {
		public void handleMessage(Message msg) {
			//String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "profile", AylaSetup.lanIp, "amlDeleteDeviceWifiProfile");
				amlTestStatusMsg("Pass", "amlDeleteDeviceWifiProfile", passMsg);


				if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.SETUP_ONLY || 
						AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.SETUP_AND_REGISTRATION_ONLY )
				{
					//
					// ---------------------- END OF SUCCESSFUL SETUP ONLY UNIT TEST SUITE --------------------------
					//
					final Handler handler = new Handler();
				    handler.postDelayed(new Runnable() {	// allow time for new device AP to establish
				        @Override
				        public void run() {
							testSequencer.nextTest().execute();
				        }
				    }, 5000);	// Delay 5 seconds
				} else {
					testSequencer.nextTest().execute();
//					amlTestUnregisterDevice(); // Unregister the device from the device service
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "amlDeleteDeviceWifiProfile");
				amlTestStatusMsg("Fail", "amlDeleteDeviceWifiProfile", errMsg);
				AylaSetup.exit(); // retry this task, or call exit and begin setup again
			}
		}
	};

	//------------------------------------------ Registration -------------------------
	private void amlTestUnregisterDevice() {
		gblAmlTestDevice.unregisterDevice(unregisterDevice);
	}

	private final Handler unregisterDevice = new Handler() {
		public void handleMessage(Message msg) {
			//String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "unregisterDevice");
				amlTestStatusMsg("Pass", "unregisterDevice", passMsg);

				testSequencer.nextTest().execute();
//				amlTestUserLogout();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "unregisterDevice");
				amlTestStatusMsg("Fail", "unregisterDevice", errMsg);
			}
		}		
	};

	//------------------------------ User Logout ------------------------------------
	private void amlTestUserLogout() {
		Map<String, String> callParams = new HashMap<String, String>();
		callParams.put("access_token", AylaUser.user.getauthHeaderValue());
		restService1 = AylaUser.logout(logout, callParams);
	}

	private final Handler logout = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaUser aylaUser = AylaSystemUtils.gson.fromJson(jsonResults,  AylaUser.class);
				String logout = aylaUser.getLogout();
				gblAmlTestUser = null;		// save for unit test

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "logout", logout, "logout");
				amlTestStatusMsg("Pass", "logout", passMsg);

				//
				// ---------------------- END OF SUCCESSFUL SETUP & REGISTRATION UNIT TEST SUITE --------------------------
				//
				testSequencer.nextTest().execute();
//				if (amlTestSuite == testSuite.SETUP_REGISTRATION_AND_DEVICE_SERVICE) {
//					amlTestReturnHostWiFiState2(); numberOfTests += 16;// Begin Device Service Unit Test (less Settings tests)
//				}		
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "logout");
				amlTestStatusMsg("Fail", "logout", errMsg);
			}
		}		
	};

	//
	//-------------------------------------- BEGIN DEVICE SERVICE UNIT && LAN MODE TEST SUITE ------------------------------------
	//
	//------------------------------------------- Return Host WiFi State --------------------------
	//
	/**
	 * Get WiFi state from host
	 */
	private void amlTestReturnHostWiFiState2() {
		
		if (AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY 
				|| AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY) {
			boolean waitForResults = true;
//			AylaReachability.determineReachability(waitForResults);
			if ( AylaReachability.isCloudServiceAvailable()) {
				if (AylaLanMode.lanModeState != lanMode.RUNNING) {
					AylaLanMode.resume();	// start the Lan Mode service. Only required for iterative testing
				}
				AylaHost.returnHostWiFiState(returnHostWiFiState2);
			} else {
				String failMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "device", "not reachable", "returnHostWiFiState2");
				amlTestStatusMsg("Fail", "returnHostWiFiState2.reachability", failMsg);	
			}
		} else {
			AylaHost.returnHostWiFiState(returnHostWiFiState2);
		}
	}

	private final Handler returnHostWiFiState2 = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaHostNetworkConnection state = AylaSystemUtils.gson.fromJson(jsonResults,  AylaHostNetworkConnection.class);
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "state", state.state, "returnHostWiFiState2");
				amlTestStatusMsg("Pass", "returnHostWiFiState2", passMsg);	

				testSequencer.nextTest().execute();
//				amlTestUserLogin2();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "");
				amlTestStatusMsg("Fail", "returnHostWiFiState", errMsg);
			}
		}		
	};

	//------------------------------ User Login - Begin Device Service Tests ------------------------------------
	private void amlTestUserLogin2() {
		if (AMLUnitTestConfig.gblAmlAsyncMode) {
			AylaUser.login(login2, 
					AMLUnitTestConfig.userName, 
					AMLUnitTestConfig.password, 
					AMLUnitTestConfig.appId, 
					AMLUnitTestConfig.appSecret);
		} else {
			// sync
			msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;

			Thread thread = new Thread(new Runnable() {
				public void run()
				{
					AylaRestService rs = AylaUser.login(
							AMLUnitTestConfig.userName, 
							AMLUnitTestConfig.password, 
							AMLUnitTestConfig.appId, 
							AMLUnitTestConfig.appSecret);
					msg =  rs.execute();
				}
			});
			thread.start();
			try {
				thread.join();		// Wait for thread to finish
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			doLogin2(msg);
		}
	}

	private final Handler login2 = new Handler() {
		public void handleMessage(Message msg) {
			doLogin2(msg);
		}		
	};
	
	void doLogin2(Message msg) {
		String jsonResults = (String)msg.obj;
		if (msg.what == AylaNetworks.AML_ERROR_OK) {
	
			AylaUser aylaUser = AylaSystemUtils.gson.fromJson(jsonResults,  AylaUser.class);
			aylaUser = AylaUser.setCurrent(aylaUser);
			gblAmlTestUser = aylaUser;		// save for unit test
			
			String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "secondsToExpiry", aylaUser.accessTokenSecondsToExpiry(), "login2");
			amlTestStatusMsg("Pass", "login2", passMsg);
	
			testSequencer.nextTest().execute();
//			amlTestGetDevices(); // next test
		} else {
			String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "login2");
			amlTestStatusMsg("Fail", "" +
					"", errMsg);
		}
	}
	//------------------------------------------ Get Devices -------------------------
	private void amlTestGetDevices() {
		AylaDevice.getDevices(getDevices);
	} 

	private final Handler getDevices = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDevice[] devices = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice[].class);
				gblAmlTestDevice = null;
				for (AylaDevice device : devices) {
					if ( TextUtils.equals(device.dsn, AMLUnitTestConfig.gblAmlTestEVBDsn) ) {
						tv_test1.setText(device.toString());
						gblAmlTestLanModeEnabled = false;
						gblAmlTestDevice = device; // assign device under test
						gblAmlTestDevice.lanModeEnable(); //Start a lan mode session with this device
						//String model = device.model;
						//String dsn = device.dsn;
						//String oemMode = device.oemModel;
						//String connectedAt = device.connectedAt;
						//String mac = device.mac;
						//String lanIp = device.lanIp;
						//String token = device.token;
						// check device class type
						isWifi = device.isWifi(); 
						isGateway = device.isGateway(); 
						isNode = device.isNode();
						if (isWifi && 
								(AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.DEVICE_SERVICE_ONLY) 
								&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.SETUP_REGISTRATION_AND_DEVICE_SERVICE) 
								&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.LAN_MODE_ONLY) // LAN_MODE_ONLY would need to get devices first.
								|| isGateway 
								&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.GATEWAY_SERVICE_ONLY) 
								&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY))
						{
							String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "device class & suite", "mismatch", "getDevices - class & suite must match");
							amlTestStatusMsg("Fail", "getDevices", errMsg);
						}

						if (isGateway) {
							gblAmlTestGateway = (AylaDeviceGateway) device;
							//gblAmlTestGateway = (AylaDeviceZigbeeGateway) device;				// if only one kind of gateway for the app
							if (gblAmlTestGateway.isZigbeeGateway()) {
								gblAmlTestZigbeeGateway = (AylaDeviceZigbeeGateway) device;
							}
						}
					}
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "amlTest", "DSN", device.dsn, "amOwner", device.amOwner(), "getDevices");
				}

				if (gblAmlTestDevice != null) { // simulate user selection of a device
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", devices.length, "getDevices");
					amlTestStatusMsg("Pass", "getDevices", passMsg);	

					if ( (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.LAN_MODE_ONLY) 
							&& (AMLUnitTestConfig.amlTestSuite != AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY) ) {
						synchronized (testSequencer) {
							testSequencer.nextTest().execute();
						}
					} else {
						// notifierHandle only works when a property changes under lanMode, not for the first time lib retrieves devices from cloud. 
//						testSequencer.nextTest().execute();
					}
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestDevice", "null", "getDevices - no device assigned");
					amlTestStatusMsg("Fail", "getDevices", errMsg);
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDevices");
				amlTestStatusMsg("Fail", "getDevices", errMsg);
			}
		}		
	};
	

	
	private void amlTestOpenGatewayJoinWindow() {
		final String joinWindowOpenTime = "60";	// optional window open duration, defaults to 200 seconds
		Map<String, String> callParams = new HashMap<String, String>();
		
		callParams.put(AylaDevice.kAylaDeviceJoinWindowDuration, joinWindowOpenTime);
		gblAmlTestZigbeeGateway.openRegistrationJoinWindow(openGatewayJoinWindow, callParams);
	}
	
	private final Handler openGatewayJoinWindow = new Handler() {
		public void handleMessage(Message msg) {
			//String jsonResults = (String)msg.obj;	// success = 204
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "openGatewayJoinWindow");
				amlTestStatusMsg("Pass", "openGatewayJoinWindow", passMsg);	
				testSequencer.nextTest().execute();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "openGatewayJoinWindow");
				amlTestStatusMsg("Fail", "openGatewayJoinWindow", errMsg);
			}
		}		
	};
	
	// -------------------------------------------- Get Gateway Candidate Nodes ----------------------------
	private void amlTestGetGatewayCandidates() {
		Map<String, String> callParams = null;
		callParams = new HashMap<String, String>();
		
		gblAmlTestZigbeeGateway.getRegistrationCandidates(getGatewayCandidates, callParams);
		//gblAmlTestGateway.getRegistrationCandidates(getGatewayCandidates, callParams);
	}
	
	private final Handler getGatewayCandidates = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDeviceNode[] nodes = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDeviceNode[].class);
				gblAmlTestSmartPlugNode = null;
				for (AylaDeviceNode node : nodes) {
					if (node.dsn.equals(AMLUnitTestConfig.gblAmlTestSmartPlugNodeDsn)) {
						tv_test1.setText(node.toString());
//						gblAmlTestLanModeEnabled = false;
						gblAmlTestSmartPlugNode = node; // assign device under test
						//gblAmlTestSmartPlugNode.lanModeEnable(); //Start a lan mode session with this device
						//String model = device.model;
						//String dsn = device.dsn;
						//String oemMode = device.oemModel;
						//String connectedAt = device.connectedAt;
						//String mac = device.mac;
						//String lanIp = device.lanIp;
						//String token = device.token;
					}
					
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "amlTest", "DSN", node.dsn, "amOwner", node.amOwner(), "getGatewayCandidates");
				}

				if (gblAmlTestSmartPlugNode != null) { // simulate user selection of a device
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", nodes.length, "getGatewayCandidates");
					amlTestStatusMsg("Pass", "getCandidates", passMsg);	

//					if (!(amlTestSuite == testSuite.LAN_MODE_ONLY) && !(amlTestSuite == testSuite.GATEWAY_LAN_MODE_ONLY)) {
						testSequencer.nextTest().execute();
//					} 
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestDevice", "null", "getGatewayCandidates - no node assigned");
					amlTestStatusMsg("Fail", "getDevices", errMsg);
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getGatewayCandidates");
				amlTestStatusMsg("Fail", "getDevices", errMsg);
			}
		}		
	};

	private void amlTestRegisterGatewayCandidate() {
		gblAmlTestZigbeeGateway.registerCandidate(registerGatewayCandidate, gblAmlTestSmartPlugNode);
		//gblAmlTestGateway.registerCandidate(registerGatewayCandidate, gblAmlTestSmartPlugNode);
	}

	private final Handler registerGatewayCandidate = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestSmartPlugNode = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDeviceNode.class);
				String productName = gblAmlTestSmartPlugNode.getProductName();

				tv_test1.setText(gblAmlTestSmartPlugNode.toString());

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "productName", productName, "registerGatewayCandidate");
				amlTestStatusMsg("Pass", "registerGatewayCandidate", passMsg);

				testSequencer.nextTest().execute();
//				if ( (amlTestSuite == testSuite.SETUP_ONLY) || (amlTestSuite == testSuite.SETUP_AND_REGISTRATION_ONLY) ) {
//					amlTestDeleteDeviceWifiProfile(); // delete the wifi profile from the device
//				} else {
//					amlTestUserLogout(); // leave the device connected and registered
//				}
			} else {
				String subMethodFailure = "";
     			if (msg.arg2 == AylaNetworks.AML_REGISTER_NEW_DEVICE) {
    				subMethodFailure = "getRegisterNewDevice";
    				if (msg.arg1 == AylaNetworks.AML_ERROR_NOT_FOUND) {
    					// Unsupported registration type
    				} else if (msg.arg1 == AylaNetworks.AML_NO_ITEMS_FOUND) {
    					// Setup token not found for AP-Mode registration
    				}
    			} else
    			if (msg.arg2 == AylaNetworks.AML_GET_REGISTRATION_CANDIDATE) {
    				subMethodFailure = "getRegistrationCandidate";
    				// No registration candidates were found
    			} else if (msg.arg2 == AylaNetworks.AML_GET_MODULE_REGISTRATION_TOKEN) {
    				subMethodFailure = "getModuleRegistrationCandidate";
    				// Failed to connect to new device for registration

    			}  else if (msg.arg2 == AylaNetworks.AML_REGISTER_DEVICE) {
    				subMethodFailure = "registerDevice";
    				// Failed to register the new device
    			}
				String errMsg = String.format("%s, %s, %s:%d, %s, %s.%s", "F", "amlTest", "error", msg.arg1, msg.obj, "registerGatewayCandidate",  subMethodFailure);
				amlTestStatusMsg("Fail", "registerGatewayCandidate", errMsg);
			}
		}				
	};

	//------------------------------------------ Get Device Detail -------------------------
	// getting device details is not usually required, use get devices for most cases
	// This call provides a simple example of using delayed execution (delayExecution)
	private void amlTestGetDeviceDetail() {
	     final AylaRestService rs = gblAmlTestDevice.getDeviceDetail(getDeviceDetail, delayExecution = true);
	    // ***** Do other work here *****
	    rs.execute(); // then execute getDeviceDetail() later
	}
	
	private final Handler getDeviceDetail = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDevice device = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDevice.class);
				String productName = device.getProductName();
				tv_test1.setText(device.toString());

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "productName", productName, "getDeviceDetail");
				amlTestStatusMsg("Pass", "getDeviceDetail", passMsg);

				testSequencer.nextTest().execute();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDeviceDetail");
				amlTestStatusMsg("Fail", "getDeviceDetail", errMsg);
			}
		}		
	};
	
	private void amlTestGetGatewayNodes() {
		//((AylaDeviceGateway)gblAmlTestDevice).getNodes(getNodes, null);
		gblAmlTestGateway.getNodes(getNodes, null);
	} 

	private final Handler getNodes = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDeviceNode[] nodes = AylaSystemUtils.gson.fromJson(jsonResults,  AylaDeviceNode[].class);
				gblAmlTestSmartPlugNode = null;
				gblAmlTestLightSocketNode = null;
				gblAmlTestRemoteControlNode = null;
				for (AylaDeviceNode node : nodes) {
					if (node.dsn.equals(AMLUnitTestConfig.gblAmlTestSmartPlugNodeDsn)) {
						tv_test1.setText(node.toString());
//						gblAmlTestLanModeEnabled = false;
						gblAmlTestSmartPlugNode = node; // assign device under test
						//gblAmlTestSmartPlugNode.lanModeEnable(); //Start a lan mode session with this device
						//String model = device.model;
						//String dsn = device.dsn;
						//String oemMode = device.oemModel;
						//String connectedAt = device.connectedAt;
						//String mac = device.mac;
						//String lanIp = device.lanIp;
						//String token = device.token;
					} else if (node.dsn.equals(AMLUnitTestConfig.gblAmlTestLightSocketNodeDsn)) {
						gblAmlTestLightSocketNode = node; // assign device under test
					} else if (node.dsn.equals(AMLUnitTestConfig.gblAmlTestRemoteControlNodeDsn)) {
						gblAmlTestRemoteControlNode = node; // assign device under test
					}
					
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "I", "amlTest", "DSN", node.dsn, "amOwner", node.amOwner(), "getNodes");
				}

				if (gblAmlTestSmartPlugNode != null) { // simulate user selection of a device
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", nodes.length, "getNodes");
					amlTestStatusMsg("Pass", "getNodes", passMsg);	

//					if (!(amlTestSuite == testSuite.LAN_MODE_ONLY) && !(amlTestSuite == testSuite.GATEWAY_LAN_MODE_ONLY)) {
						testSequencer.nextTest().execute();
//					} 
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestDevice", "null", "getDevices - no device assigned");
					amlTestStatusMsg("Fail", "getDevices", errMsg);
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDevices");
				amlTestStatusMsg("Fail", "getDevices", errMsg);
			}
		}		
	};
	
	
	// -------------------- Identify the Node --------------------------
	
	/*
	 * Used to identify a node by blinking a light, making a sound, vibrating, etc
	 * Must be supported by node host FW
	 * callParams
	 *   	{"value":"On","time":"55"}
	 *      	the key "value" may be "On" or "Off"
     *      	 The key "time" has a corresponding value from 0 to 255 in seconds, passed as a string
     *       
     * Returns
     *     {"id":"12345"} for the "On" and "Off" option, or 
     *     {"id":"on_0x123456789abc","status":"success"} for the "Result" option
     *     
     * Errors
     * 		401 - Unauthorized
     * 		404 - Node not found
     * 		405 - Not supported for this node
	 */
	private void amlTestIdentify() {
		Map<String, String> callParams = new HashMap<String, String>();
		
		callParams.put(AylaDeviceNode.kAylaNodeParamIdentifyValue, AylaDeviceNode.kAylaNodeParamIdentifyOn);
		//callParams.put(AylaDeviceNode.kAylaNodeParamIdentifyValue, AylaDeviceNode.kAylaNodeParamIdentifyOff);
		//callParams.put(AylaDeviceNode.kAylaNodeParamIdentifyValue, AylaDeviceNode.kAylaNodeParamIdentifyResult);
		callParams.put(AylaDeviceNode.kAylaNodeParamIdentifyTime, "20");
		gblAmlTestSmartPlugNode.identify(identifyNodeHandler, callParams);
	}
	
	private final Handler identifyNodeHandler = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "jsonResults", jsonResults, "identifyNode_Handler");
				amlTestStatusMsg("Pass", "identifyNode", passMsg);
				// Blinking is something last for some time, 
				// need to delay the next test for couple of seconds in order to make it work.
				new CountDownTimer(30000, 10000) {

					@Override
					public void onFinish() {
						testSequencer.nextTest().execute();
					}

					@Override
					public void onTick(long millisUntilFinished) {
					}
				}.start();
				
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "identifyNode_Handler");
				amlTestStatusMsg("Fail", "identifyNode", errMsg);
			}		
		}
	};
	
	// ----------------------------------- Device Notifications --------------------------
	private void amlTestCreateDeviceNotification() {
		AylaDeviceNotification deviceNotification = new AylaDeviceNotification();
		deviceNotification.notificationType = AylaDeviceNotification.aylaDeviceNotificationTypeOnConnectionLost;
		deviceNotification.message = "WiFi connection dropped";
		deviceNotification.threshold = 600;
		deviceNotification.deviceNickname = "deviceNotificaitonNickname";
		//Random random = new Random(); Integer anInt = random.nextInt();

		restService4 = gblAmlTestDevice.createNotification(createOrUpdateDeviceNotification, deviceNotification);
	}

	private final Handler createOrUpdateDeviceNotification = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestDevice.deviceNotification = AylaSystemUtils.gson.fromJson(jsonResults, AylaDeviceNotification.class);
				AylaDeviceNotification deviceNotification = gblAmlTestDevice.deviceNotification;

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "threshold", deviceNotification.threshold, "createOrUpdateDeviceNotification");
				amlTestStatusMsg("Pass", "createDeviceNotification", passMsg);
				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateDeviceNotification");
				amlTestStatusMsg("Fail", "createDeviceNotification", errMsg);
			}		
		}
	};
	
	private void amlTestUpdateDeviceNotification() {
		gblAmlTestDevice.deviceNotification.threshold = 3600;
		
		//gblAmlTestDevice.updateNotification(createOrUpdateDeviceNotification, gblAmlTestDevice.deviceNotification);
		gblAmlTestDevice.deviceNotification.updateNotification(createOrUpdateDeviceNotification);
	}
	
	private void amlTestGetDeviceNotifications() {
		gblAmlTestDevice.getNotifications(getDeviceNotifications, null);
		//gblAmlTestDevice.deviceNotification.getNotifications(getDeviceNotifications, null);
	}
	
	private final Handler getDeviceNotifications = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestDevice.deviceNotifications = AylaSystemUtils.gson.fromJson(jsonResults, AylaDeviceNotification[].class);
				//AylaDeviceNotification deviceNotification = gblAmlTestDevice.deviceNotification;

				if (gblAmlTestDevice.deviceNotifications.length > 0) {
					gblAmlTestDeviceNotification = gblAmlTestDevice.deviceNotifications[0];
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "threshold", gblAmlTestDeviceNotification.threshold, "getDeviceNotifications");
					amlTestStatusMsg("Pass", "getDeviceNotifications", passMsg);
					testSequencer.nextTest().execute();
				} else {	
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDeviceNotifications_err1");
					amlTestStatusMsg("Fail", "createDeviceNotification", errMsg);
				}
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getDeviceNotifications_err2");
				amlTestStatusMsg("Fail", "getDeviceNotifications", errMsg);
			}		
		}
	};
	
	private void amlTestDeleteDeviceNotification() {
		gblAmlTestDevice.destroyNotification(destroyDeviceNotification, gblAmlTestDevice.deviceNotification);
		// gblAmlTestDevice.deviceNotification.destroyNotification(destroyDeviceNotification);
	}
	
	private final Handler destroyDeviceNotification = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {		
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "destroyDeviceNotification");
				amlTestStatusMsg("Pass", "destroyDeviceNotification", passMsg);

				testSequencer.nextTest().execute();
				
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "destroyDeviceNotification");
				amlTestStatusMsg("Fail", "destroyDeviceNotification", errMsg);
			}		
		}
	};
	
	// ------------------------------------- Device Application Notifications -------------------------------------------------
	private void amlTestCreateAppNotification() {
		AylaAppNotification appNotification = new AylaAppNotification();
		
		// pick an appType
		appNotification.appType = AylaAppNotification.aylaAppNotificationTypeSms;
		//appNotification.appType = AylaAppNotification.aylaAppNotificationTypeEmail;
		//appNotification.appType = AylaAppNotification.aylaAppNotificationTypePush;
		
		appNotification.notificationAppParameters.username = "yourNameHere";
		
		// Params for the appType picked
		if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeSms)) {
			appNotification.notificationAppParameters.countryCode = AMLUnitTestConfig.gblAmlCountryCode;
			appNotification.notificationAppParameters.phoneNumber = AMLUnitTestConfig.gblAmlPhoneNumber;
			appNotification.notificationAppParameters.contactId = AMLUnitTestConfig.gblAmlContactID;
			appNotification.notificationAppParameters.message = "Testing create device notification sms application";
		} else if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeEmail)) {
			appNotification.notificationAppParameters.email = AMLUnitTestConfig.gblAmlEmailAddress;
			appNotification.notificationAppParameters.message = "Testing create device notification email application";
		} else if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypePush)) {
			appNotification.notificationAppParameters.registrationId = PushNotification.registrationId;
			appNotification.notificationAppParameters.message = "Testing create device notification push application";
			appNotification.notificationAppParameters.pushSound = "default";
			appNotification.notificationAppParameters.pushMdata = "my meta data";
		}
		
		gblAmlTestDevice.deviceNotification.createApp(createOrUpdateAppNotification, appNotification);
	}

	private final Handler createOrUpdateAppNotification = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaAppNotification appNotification = AylaSystemUtils.gson.fromJson(jsonResults,AylaAppNotification.class);
				gblAmlTestDevice.deviceNotification.appNotification = appNotification;

				String countryCode = appNotification.notificationAppParameters.countryCode;
				String phoneNumber = appNotification.notificationAppParameters.phoneNumber;
				String message = appNotification.notificationAppParameters.message;

				String passMsg = String.format("%s, %s, %s,:%s, %s:%s, %s", "P", "amlTest", "phoneNumber", phoneNumber, "message", message, "createOrUpdateAppNotification_handler");
				amlTestStatusMsg("Pass", "createOrUpdateAppNotification", passMsg);

				testSequencer.nextTest().execute();
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateAppNotification_handler");
				amlTestStatusMsg("Fail", "createOrUpdateAppNotification", errMsg);
			}
		}
	};
	
	private void amlTestUpdateAppNotification() {
		AylaAppNotification appNotification = gblAmlTestDevice.deviceNotification.appNotification;
		
		// pick an appType
		appNotification.appType = AylaAppNotification.aylaAppNotificationTypeSms;
		//appNotification.appType = AylaAppNotification.aylaAppNotificationTypeEmail;
		//appNotification.appType = AylaAppNotification.aylaAppNotificationTypePush;
		
		// Params for the appType picked
		if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeSms)) {
			appNotification.notificationAppParameters.countryCode = AMLUnitTestConfig.gblAmlCountryCode;
			appNotification.notificationAppParameters.phoneNumber = AMLUnitTestConfig.gblAmlPhoneNumber;
			appNotification.notificationAppParameters.message = "Testing update device notification sms application";
		} else
		if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypeEmail)) {
			appNotification.notificationAppParameters.username = "yourNameHere";
			appNotification.notificationAppParameters.email = AMLUnitTestConfig.gblAmlEmailAddress;
			appNotification.notificationAppParameters.message = "Testing update device notification email application";
		} else
		if (appNotification.appType.equals(AylaAppNotification.aylaAppNotificationTypePush)) {
			appNotification.notificationAppParameters.registrationId = PushNotification.registrationId;
			appNotification.notificationAppParameters.message = "Testing update device notification push application";
			appNotification.notificationAppParameters.pushSound = "default";
			appNotification.notificationAppParameters.pushMdata = "my meta data";
		}
		
		gblAmlTestDevice.deviceNotification.updateApp(createOrUpdateAppNotification, appNotification);
	}

	private void amlTestGetAppNotifications() {
		//Map<String, String> s = new HashMap<String, String>(); TBD
		//callParams.put("count", "3");
		//callParams.put("test", "def");
		Map<String, String> callParams = null; // cappParams is unused at this time
		gblAmlTestDevice.deviceNotification.getApps(getAppNotifications, callParams);
	}

	private final Handler getAppNotifications = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestDevice.deviceNotification.appNotifications = AylaSystemUtils.gson.fromJson(jsonResults, AylaAppNotification[].class);

				for (AylaAppNotification appNotification : gblAmlTestDevice.deviceNotification.appNotifications) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "P", "amlTest", "message", appNotification.notificationAppParameters.message, "getAppNotifications");
					gblAmlTestAppNotification = appNotification;
				}

				String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestDevice.deviceNotification.appNotifications.length, "getAppNotifications");
				amlTestStatusMsg("Pass", "getAppNotifications", passMsg);

				testSequencer.nextTest().execute();
//				amlTestDestroyApplicationTrigger();
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getAppNotifications");
				amlTestStatusMsg("Fail", "getAppNotifications", errMsg);
			}
		}
	};

	private void amlTestDeleteAppNotification() {
		gblAmlTestDeviceNotification.destroyApp(destroyAppNotification, gblAmlTestAppNotification);
		//gblAmlTestDeviceNotification.appNotification.destroy(destroyAppNotification); 
	}

	private final Handler destroyAppNotification = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {		
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "destroyAppNotification");
				amlTestStatusMsg("Pass", "destroyApplicationTrigger", passMsg);

				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "destroyAppNotification");
				amlTestStatusMsg("Fail", "destroyAppNotification", errMsg);
			}		
		}
	};

	// -------------------------------------- Get Properties ------------------------------
	public void amlTestGetProperties() {
		
		Map<String, String> param = new HashMap<String, String>();
		//Blue_LED is necessary to update the GblTestProperty
		param.put("names", "attr_read_cmd attr_read_data cmd log attr_set_cmd attr_set_result Blue_LED Green_LED decimal_in input"); 
		amlTestGetProperties(param);
		// get all properties in one API in lan_mode would randomly cause property not found(404), which is a constraint from gateway
//		amlTestGetProperties(null);
	}

	private void amlTestGetProperties(final Map<String, String> callParams) {
		// test async interface
		if (AMLUnitTestConfig.gblAmlAsyncMode) {
			gblAmlTestDevice.getProperties(getProperties, callParams); // next test: no delay
		} else {
			// sync
			msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;

			// test synchronous interface
			Thread thread = new Thread(new Runnable() {
				public void run()
				{
					AylaRestService rs = gblAmlTestDevice.getProperties(callParams);
					msg = rs.execute();
				}
			});
			thread.start();
			try {
				thread.join();		// Wait for thread to finish
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			doGetProperties(msg);
		}
	}

	private final Handler getProperties = new Handler() {
		public void handleMessage(Message msg) {
			doGetProperties(msg);
		};
	};

	private void doGetProperties(Message msg) {
		String jsonResults = (String)msg.obj;
		AylaSystemUtils.saveToLog("%s, %s, jsonResults:%s.", "D", "doGetProperties", jsonResults);
		AylaSystemUtils.saveToLog("%s, %s, gblAmlPropertyName:%s.", "D", "doGetProperties", AMLUnitTestConfig.gblAmlPropertyName);
		if (msg.what == AylaNetworks.AML_ERROR_OK) {
			// for unit test purpose 
			AylaNotify.notifyAcknowledge();
			AylaProperty[] ps = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty[].class);
			gblAmlTestDevice.mergeNewProperties(ps);
			
			if (TextUtils.isEmpty(AMLUnitTestConfig.gblAmlPropertyName)) {
				String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestDevice.properties.length, "getProperties");
				amlTestStatusMsg("Pass", "getProperties", passMsg);
				
				if ( AylaSystemUtils.lanModeState != lanMode.ENABLED ) {
					synchronized (testSequencer) {
						testSequencer.nextTest().execute();
					}
				}
				return;
			}
			
			gblAmlTestProperty = null;
			for (AylaProperty property : gblAmlTestDevice.properties) {
				String name = property.name();
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "name", name, "value", property.value, "getProperties");
				if (TextUtils.equals(name, AMLUnitTestConfig.gblAmlPropertyName)) { // simulate user selection of a property
					gblAmlTestProperty = property; // assign property under test
				}
			}

			
			if (gblAmlTestProperty != null ) {
				String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestDevice.properties.length, "getProperties");
				amlTestStatusMsg("Pass", "getProperties", passMsg);
				
				if ( AylaSystemUtils.lanModeState != lanMode.ENABLED ) {
					synchronized (testSequencer) {
						testSequencer.nextTest().execute();
					}
				}
				
			} else {
				String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestProperty", "null", "getProperties - no property assigned");
				amlTestStatusMsg("Fail", "getProperties", errMsg);
			}
		}  else {
			String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getProperties");
			amlTestStatusMsg("Fail", "getProperties", errMsg);
		}		
	}

	//---------------------------------------- Get Property Detail ----------------------------------
	// usually not get property detail is not necessary, use get devices instead
	private void amlTestGetPropertyDetail() {
		//Map<String, String> callParams = new HashMap<String, String>();
		//callParams.put("class", "service");
		Map<String, String> callParams = null; // callParams is unused at this time
		gblAmlTestProperty.getPropertyDetail(getPropertyDetail, callParams); // next test: no delay		
	}

	private final Handler getPropertyDetail = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaProperty property = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty.class);

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", property.name(), "getPropertyDetail");
				amlTestStatusMsg("Pass", "getPropertyDetail", passMsg);

				if (AylaSystemUtils.lanModeState != lanMode.ENABLED) {
					synchronized (testSequencer) {
						testSequencer.nextTest().execute();
					}
				}
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getPropertyDetail");
				amlTestStatusMsg("Fail", "getPropertyDetail", errMsg);
			}		
		}
	};

	//--------------------------------------- Create Datapoint ---------------------------------------
	private void amlTestCreateDatapoint(final AylaDatapoint datapoint) {
		if (AMLUnitTestConfig.gblAmlAsyncMode) {
			// asynchronous
			rsCreateDatapoint = gblAmlTestProperty.createDatapoint(createDatapoint, datapoint); // next test: no delay
		} else {
			// sync
			msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;
			
			// use synchronous method calls
			Thread thread = new Thread(new Runnable() {
				public void run()
				{
						AylaRestService rs;
						
						// get all schedules associated with this device
						rs = gblAmlTestProperty.createDatapoint(datapoint); // next test: no delay
						msg = rs.execute();
				}
			});
			thread.start();
			try {
				thread.join();		// Wait for thread to finish
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			doCreateDatapoint(msg);
		}
	}

	private final Handler createDatapoint = new Handler() {
		public void handleMessage(Message msg) {
			doCreateDatapoint(msg);
		}
	};
	
	private void doCreateDatapoint(Message msg) {
		String jsonResults = (String)msg.obj;
		AylaSystemUtils.saveToLog("%s, %s, jsonResults:%s.", "D", "doCreateDatapoint", jsonResults);
		if (msg.what == AylaNetworks.AML_ERROR_OK) {
			gblAmlTestProperty.datapoint = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatapoint.class);
			AylaDatapoint datapoint= gblAmlTestProperty.datapoint;

			String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datapoint.value(), "createDatapoint");
			amlTestStatusMsg("Pass", "createDatapoint", passMsg);
			new CountDownTimer(3000, 1000) {
				@Override
				public void onFinish() {
					synchronized (testSequencer) {
						testSequencer.nextTest().execute();
					}
				}

				@Override
				public void onTick(long millisUntilFinished) {
				}
			}.start();
		}  else {	
			String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createDatapoint");
			amlTestStatusMsg("Fail", "createDatapoint", errMsg);
		}		
	}

	// ----------------------------- Get Datapoints By Activity --------------------------
	private void amlTestGetDatapointsByActivity(int count) {
		Map<String, String> callParams = new HashMap<String, String>();
		callParams.put("count", Integer.toString(count));
		rsCreateDatapoint = gblAmlTestProperty.getDatapointsByActivity(getDatapointsByActivity, callParams); // next test: no delay
	}

	private final Handler getDatapointsByActivity = new Handler() {
		public void handleMessage(Message msg) {
			boolean pass = false;
			String jsonResults = (String)msg.obj;
			AylaSystemUtils.saveToLog("%s, %s, jsonResults:%s.", "D", "getDatapointByActivity", jsonResults);
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestProperty.datapoints = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatapoint[].class);
				String baseType = gblAmlTestProperty.baseType();
				pass = baseType.equals(AMLUnitTestConfig.gblAmlBaseType);
				if (pass) {					
					pass = false;
					// Print all of the datapoints
					for (AylaDatapoint datapoint : gblAmlTestProperty.datapoints) {
						String sValue = datapoint.sValueFormatted(baseType);
						Number nValue = datapoint.nValueFormatted(baseType);
	
						String nValueStr = nValue.toString();
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "nValue", nValueStr, "sValue", sValue, "getDatapointsByActivity");
						//Log.i("datapoint.toString():", datapoint.toString());
					}
					// Check most recent value
					int mostRecent = gblAmlTestProperty.datapoints.length - 1;
					if ( TextUtils.equals(baseType, "integer") || TextUtils.equals(baseType, "boolean")) {
						Number nValue = gblAmlTestProperty.datapoints[mostRecent].nValueFormatted(baseType);
						int nValueInt = nValue.intValue();
						int gblAmlNValueInt = AMLUnitTestConfig.gblAmlNValue.intValue();
						pass = (nValueInt == gblAmlNValueInt);
						if (pass) {
							String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "nValue", nValueInt, "getDatapointsByActivity");
							amlTestStatusMsg("Pass", "getDatapointsByActivity", passMsg);
						} else {
							String errMsg = String.format("%s, %s, %s:%s, %d!=%d, %s", "F", "amlTest", "error", "nValueMismatch", nValueInt, gblAmlNValueInt, "getDatapointsByActivity");
							amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);							
						}
					} else if ( TextUtils.equals(baseType, "float") || TextUtils.equals(baseType, "decimal")) {
						Number nValue = gblAmlTestProperty.datapoints[mostRecent].nValueFormatted(baseType);
						double nValueDouble = nValue.doubleValue();
						double gblAmlNValueDouble = AMLUnitTestConfig.gblAmlNValue.doubleValue();
						pass = (nValueDouble == gblAmlNValueDouble);
						if (pass) {
							String passMsg = String.format("%s, %s, %s:%f, %s", "P", "amlTest", "nValue", nValueDouble, "getDatapointsByActivity");
							amlTestStatusMsg("Pass", "getDatapointsByActivity", passMsg);
						} else {
							String errMsg = String.format("%s, %s, %s:%s, %f!=%f, %s", "F", "amlTest", "error", "nValueMismatch", nValueDouble, gblAmlNValueDouble, "getDatapointsByActivity");
							amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);							
						}
					} else if (TextUtils.equals(baseType, "string")) {
						String sValue = gblAmlTestProperty.datapoints[mostRecent].sValueFormatted(baseType);
						pass = TextUtils.equals(sValue, AMLUnitTestConfig.gblAmlSValue);
						if (pass) {
							String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "sValue", sValue, "getDatapointsByActivity");
							amlTestStatusMsg("Pass", "getDatapointsByActivity", passMsg);
						} else {
							String errMsg = String.format("%s, %s, %s:%s, %s!=%s, %s", "F", "amlTest", "error", "sValueMismatch", sValue, AMLUnitTestConfig.gblAmlSValue, "getDatapointsByActivity");
							amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);							
						}
					} else {
						String errMsg = String.format("%s, %s, %s:%s, %s, %s", "F", "amlTest", "error", "illegalBaseType", baseType, "getDatapointsByActivity");
						amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);												
					}
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s!=%s, %s", "F", "amlTest", "error", "baseTypeMismatch", baseType, AMLUnitTestConfig.gblAmlBaseType, "getDatapointsByActivity");
					amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);					
				}
				if (pass) {
					synchronized (testSequencer) {
						testSequencer.nextTest().execute();
					}
				}
//				amlTestCreatePropertyTrigger();
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getDatapointsByActivity");
				amlTestStatusMsg("Fail", "getDatapointsByActivity", errMsg);
			}		
		}
	};

	//--------------------------------------- Property Triggers ---------------------------------------
	private void amlTestCreatePropertyTrigger() {
		AylaPropertyTrigger propertyTrigger = new AylaPropertyTrigger();
		propertyTrigger.triggerType = "on_change";
		//propertyTrigger.compareType = "<";
		//propertyTrigger.value = "0";
		//propertyTrigger.deviceNickname = "abc";
		Random random = new Random(); Integer anInt = random.nextInt();
		propertyTrigger.propertyNickname = "TriggerName_" + anInt.toString();

		// {"trigger_type":"on_change" }
		rsCreatePropertyTrigger = gblAmlTestProperty.createTrigger(createPropertyTrigger, propertyTrigger);
	}

	private final Handler createPropertyTrigger = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestProperty.propertyTrigger = AylaSystemUtils.gson.fromJson(jsonResults,AylaPropertyTrigger.class);
				AylaPropertyTrigger propertyTrigger = gblAmlTestProperty.propertyTrigger;

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", propertyTrigger.value, "createPropertyTrigger");
				amlTestStatusMsg("Pass", "createPropertyTrigger", passMsg);
				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createPropertyTrigger");
				amlTestStatusMsg("Fail", "createPropertyTrigger", errMsg);
			}		
		}
	};
	
	private void amlTestUpdatePropertyTrigger() {
		AylaPropertyTrigger propertyTrigger = gblAmlTestProperty.propertyTrigger;
		propertyTrigger.triggerType = "compare_absolute";
		propertyTrigger.compareType = ">=";
		propertyTrigger.value = "1";
		//propertyTrigger.deviceNickname = "abc";
		Random random = new Random(); Integer anInt = random.nextInt();
		propertyTrigger.propertyNickname = "TriggerName_" + anInt.toString();

		// {"trigger_type":"compare_absolute", "compare_type":">=", "value":60 }
		rsCreatePropertyTrigger = gblAmlTestProperty.updateTrigger(updatePropertyTrigger, propertyTrigger);
	}

	private final Handler updatePropertyTrigger = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestProperty.propertyTrigger = AylaSystemUtils.gson.fromJson(jsonResults,AylaPropertyTrigger.class);
				AylaPropertyTrigger propertyTrigger = gblAmlTestProperty.propertyTrigger;

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", propertyTrigger.value, "updatePropertyTrigger");
				amlTestStatusMsg("Pass", "updatePropertyTrigger", passMsg);
				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "updatePropertyTrigger");
				amlTestStatusMsg("Fail", "updatePropertyTrigger", errMsg);
			}		
		}
	};

	private void amlTestGetPropertyTriggers() {
		//Map<String, String> callParams = new HashMap<String, String>(); TBD
		//callParams.put("count", "3");
		//callParams.put("test", "def");
		Map<String, String> callParams = null; // callParams unused at this time
		rsGetPropertyTriggers = gblAmlTestProperty.getTriggers(getPropertyTriggers, callParams);
	}

	private final Handler getPropertyTriggers = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestProperty.propertyTriggers = AylaSystemUtils.gson.fromJson(jsonResults,AylaPropertyTrigger[].class);

				for (AylaPropertyTrigger propertyTrigger : gblAmlTestProperty.propertyTriggers) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "compareType", propertyTrigger.compareType, "value", propertyTrigger.value, "getPropertyTrigger");
					gblAmlTestPropertyTrigger = propertyTrigger;
				}

				String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestProperty.propertyTriggers.length, "getPropertyTriggers");
				amlTestStatusMsg("Pass", "getPropertyTriggers", passMsg);

				testSequencer.nextTest().execute();
//				amlTestCreateSMSApplicationTrigger();
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getPropertyTriggers");
				amlTestStatusMsg("Fail", "getPropertyTriggers", errMsg);
			}
		}
	};

	private void amlTestDestroyPropertyTrigger() {
		//rsDestroyPropertyTrigger = gblAmlTestProperty.destroyTrigger(destroyPropertyTrigger, gblAmlTestPropertyTrigger);
		rsDestroyPropertyTrigger = gblAmlTestPropertyTrigger.destroyTrigger(destroyPropertyTrigger);
	}

	private final Handler destroyPropertyTrigger = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {		
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "destroyPropertyTrigger");
				amlTestStatusMsg("Pass", "destroyPropertyTrigger", passMsg);

				testSequencer.nextTest().execute();
//				amlTestSchedules();
				
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "destroyPropertyTrigger");
				amlTestStatusMsg("Fail", "destroyPropertyTrigger", errMsg);
			}		
		}
	};

	//--------------------------------------- Application Triggers ---------------------------------------
	private void amlTestCreateSMSApplicationTrigger() {
		AylaApplicationTrigger applicationTrigger = new AylaApplicationTrigger();
		//applicationTrigger.countryCode = gblAmlCountryCode;
		applicationTrigger.phoneNumber = AMLUnitTestConfig.gblAmlPhoneNumber;
		applicationTrigger.message = "Testing Ayla Mobile Library";

		rsCreateSMSApplicationTrigger = gblAmlTestPropertyTrigger.createSMSApplicationTrigger(createSMSApplicationTrigger, applicationTrigger);
	}

	private final Handler createSMSApplicationTrigger = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestPropertyTrigger.applicationTrigger = AylaSystemUtils.gson.fromJson(jsonResults,AylaApplicationTrigger.class);
				AylaApplicationTrigger applicationTrigger= gblAmlTestPropertyTrigger.applicationTrigger;

				//String countryCode = applicationTrigger.countryCode;
				String phoneNumber = applicationTrigger.phoneNumber;
				String message = applicationTrigger.message;

				String passMsg = String.format("%s, %s, %s,:%s, %s:%s, %s", "P", "amlTest", "phoneNumber", phoneNumber, "message", message, "createSmsApplicationTrigger");
				amlTestStatusMsg("Pass", "createSMSApplicationTrigger", passMsg);

				testSequencer.nextTest().execute();
//				amlTestCreateEmailApplicationTrigger();
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createApplicationTrigger");
				amlTestStatusMsg("Fail", "createSMSApplicationTrigger", errMsg);
			}
		}
	};

	private void amlTestUpdateSMSApplicationTrigger() {
		AylaApplicationTrigger applicationTrigger = gblAmlTestPropertyTrigger.applicationTrigger;
		applicationTrigger.countryCode = AMLUnitTestConfig.gblAmlCountryCode;
		//applicationTrigger.phoneNumber = gblAmlPhoneNumber;
		//applicationTrigger.message = "Testing Ayla Mobile Library";

		rsCreateSMSApplicationTrigger = gblAmlTestPropertyTrigger.updateSMSApplicationTrigger(updateSMSApplicationTrigger, applicationTrigger);
	}

	private final Handler updateSMSApplicationTrigger = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestPropertyTrigger.applicationTrigger = AylaSystemUtils.gson.fromJson(jsonResults,AylaApplicationTrigger.class);
				AylaApplicationTrigger applicationTrigger= gblAmlTestPropertyTrigger.applicationTrigger;

				String countryCode = applicationTrigger.countryCode;
				String phoneNumber = applicationTrigger.phoneNumber;
				String message = applicationTrigger.message;

				String passMsg = String.format("%s, %s, %s,:%s, %s:%s, %s", "P", "amlTest", "countryCode", countryCode, "message", message, "updateSmsApplicationTrigger");
				amlTestStatusMsg("Pass", "updateSMSApplicationTrigger", passMsg);

				testSequencer.nextTest().execute();
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "updateApplicationTrigger");
				amlTestStatusMsg("Fail", "updateSMSApplicationTrigger", errMsg);
			}
		}
	};
	private void amlTestCreateEmailApplicationTrigger() {
		AylaApplicationTrigger applicationTrigger = new AylaApplicationTrigger();
		applicationTrigger.username = "MDA";
		applicationTrigger.emailAddress = AMLUnitTestConfig.gblAmlEmailAddress;
		applicationTrigger.message = "Testing Ayla Mobile Library";

		// {"trigger_type":"compare_absolute", "compare_type":">=", "value":60 }
		rsCreateEmailApplicationTrigger = gblAmlTestPropertyTrigger.createEmailApplicationTrigger(createEmailApplicationTrigger, applicationTrigger);
	}

	private final Handler createEmailApplicationTrigger = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestPropertyTrigger.applicationTrigger = AylaSystemUtils.gson.fromJson(jsonResults,AylaApplicationTrigger.class);
				AylaApplicationTrigger applicationTrigger= gblAmlTestPropertyTrigger.applicationTrigger;

				String userName = applicationTrigger.username;
				String emailAddress = applicationTrigger.emailAddress;
				//String message = applicationTrigger.message;

				String passMsg = String.format("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "userName", userName, "emailAddress", emailAddress, "createEmailApplicationTrigger");
				amlTestStatusMsg("Pass", "createEmailApplicationTrigger", passMsg);

				testSequencer.nextTest().execute();
//				amlTestGetApplicationTriggers();
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createApplicationTrigger");
				amlTestStatusMsg("Fail", "createEmailApplicationTrigger", errMsg);
			}
		}
	};

	private void amlTestGetApplicationTriggers() {
		//Map<String, String> callParams = new HashMap<String, String>(); TBD
		//callParams.put("count", "3");
		//callParams.put("test", "def");
		Map<String, String> callParams = null; // cappParams is unused at this time
		rsGetApplicationTriggers = gblAmlTestPropertyTrigger.getTriggers(getApplicationTriggers, callParams);
	}

	private final Handler getApplicationTriggers = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestPropertyTrigger.applicationTriggers = AylaSystemUtils.gson.fromJson(jsonResults,AylaApplicationTrigger[].class);

				for (AylaApplicationTrigger applicationTrigger : gblAmlTestPropertyTrigger.applicationTriggers) {
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "name", applicationTrigger.name, "username", applicationTrigger.username, "getApplicationTrigger");
					gblAmlTestApplicationTrigger = applicationTrigger;
				}

				String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestPropertyTrigger.applicationTriggers.length, "getApplicationTriggers");
				amlTestStatusMsg("Pass", "getApplicationTriggers", passMsg);

				testSequencer.nextTest().execute();
//				amlTestDestroyApplicationTrigger();
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getApplicationTriggers");
				amlTestStatusMsg("Fail", "getApplicationTriggers", errMsg);
			}
		}
	};

	private void amlTestDestroyApplicationTrigger() {
		//rsDestroyApplicationTrigger = gblAmlTestPropertyTrigger.destroyTrigger(destroyApplicationTrigger, gblAmlTestApplicationTrigger);
		rsDestroyApplicationTrigger = gblAmlTestApplicationTrigger.destroyTrigger(destroyApplicationTrigger);
	}

	private final Handler destroyApplicationTrigger = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {		
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "destroyApplicationTrigger");
				amlTestStatusMsg("Pass", "destroyApplicationTrigger", passMsg);

				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "destroyApplicationTrigger");
				amlTestStatusMsg("Fail", "destroyApplicationTrigger", errMsg);
			}		
		}
	};
	
	// ----------------------------------------- Schedules -------------------------------------
	private void amlTestSchedules() {
		if (AMLUnitTestConfig.gblScheduleName != null) {
			msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // assume failure;
			
			// use synchronous method calls
			Thread thread = new Thread(new Runnable() {
				public void run()
				{
					AylaRestService rs;
					
					// get all schedules associated with this device
					rs = gblAmlTestDevice.getAllSchedules(null);
					msg = rs.execute();
					if (msg.what == AylaNetworks.AML_ERROR_OK) {
						// find one schedule in the array of returned devices
						gblAmlTestDevice.schedules = AylaSystemUtils.gson.fromJson((String)msg.obj,AylaSchedule[].class);
						gblAmlTestDevice.schedule = gblAmlTestDevice.findSchedule(AMLUnitTestConfig.gblScheduleName); // find "sched#" or "schedule_in"
						if (msg.what == AylaNetworks.AML_ERROR_OK) {
							
							HashMap<String, String> callParams = new HashMap<String, String>();
							callParams.put("name", AMLUnitTestConfig.gblScheduleName);
							rs = gblAmlTestDevice.getScheduleByName(callParams);
							msg = rs.execute();
							if (msg.what == AylaNetworks.AML_ERROR_OK) {
								gblAmlTestDevice.schedule = AylaSystemUtils.gson.fromJson((String)msg.obj, AylaSchedule.class);
								boolean saveSchedActive = gblAmlTestDevice.schedule.active;
								gblAmlTestDevice.schedule.active = !saveSchedActive; // flip the active bit
								if (gblAmlTestDevice.schedule.scheduleActions.length > 0) {
									boolean saveActionActive = gblAmlTestDevice.schedule.scheduleActions[0].active;
									gblAmlTestDevice.schedule.scheduleActions[0].active = !saveActionActive; // flip the active bit
									
									// update the schedules and actions
									rs = gblAmlTestDevice.updateSchedule(gblAmlTestDevice.schedule, gblAmlTestDevice.schedule.scheduleActions);
									msg = rs.execute();
									if (msg.what == AylaNetworks.AML_ERROR_OK) {
										gblAmlTestSchedule = new AylaSchedule();
										gblAmlTestSchedule = AylaSystemUtils.gson.fromJson((String)msg.obj, AylaSchedule.class);
										if (gblAmlTestSchedule.active == saveSchedActive || gblAmlTestSchedule.scheduleActions[0].active == saveActionActive ) {
											msg.arg1 = AylaNetworks.AML_ERROR_FAIL; // check flipped bits
										}
									}
								} else {
									AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "No Schedule Action to test", "Schedules");								
								}
							}
						}
					}
				}
			});
			thread.start();
			try {
				thread.join();		// Wait for thread to complete
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if (msg.arg1 == 200) {
				String passMsg = String.format(Locale.getDefault(), "%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "Schedules");
				amlTestStatusMsg("Pass", "Schedules", passMsg);
			} else if (msg.arg1 == 404) {
				String errMsg = String.format(Locale.getDefault(), "%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "Schedules - either create one or set to null");
				amlTestStatusMsg("Fail", "Schedules", errMsg);
			} else {
				String errMsg = String.format(Locale.getDefault(), "%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "Schedules");
				amlTestStatusMsg("Fail", "Schedules", errMsg);
			}
		} else {
			// Generate a warning log message too
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "Not testing schedules", "Schedules");
			String passMsg = String.format(Locale.getDefault(), "%s, %s, %s:%s, %s", "P", "amlTest", "warning", "Not testing schedules", "Schedules");
			amlTestStatusMsg("Pass", "Schedules", passMsg);
		}
				
		testSequencer.nextTest().execute();
//		amlTestUserLogout2(); // last test
	}

	// ------------------------------ User Logout ------------------------------------
	private void amlTestUserLogout2() {
		Map<String, String> callParams = new HashMap<String, String>();
		callParams.put("access_token", AylaUser.user.getauthHeaderValue());
		restService1 = AylaUser.logout(logout2, callParams);
	}

	private final Handler logout2 = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaUser aylaUser = AylaSystemUtils.gson.fromJson(jsonResults,  AylaUser.class);
				String logout = aylaUser.getLogout();
				gblAmlTestUser = null;		// save for unit test

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "logout", logout, "logout");
				amlTestStatusMsg("Pass", "logout", passMsg);

				// End of Device Service Unit Tests
				testSequencer.nextTest().execute();
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "logout");
				amlTestStatusMsg("Fail", "logout", errMsg);
			}
		}		
	};

	// -------------- device metadata unit tests -----------------
	
	private void amlTestCreateDeviceDatum() {
		// create a metadata object
		AylaDatum datum = new AylaDatum();
		datum.key = "aDevKey";
		datum.value = "aValue1";

		gblAmlTestDevice.createDatum(createDeviceDatum, datum);
		
		// save for later comparison after get
		gblAmlTestDevice.datum = new AylaDatum();
		gblAmlTestDevice.datum.key = datum.key;
		gblAmlTestDevice.datum.value = datum.value;
	}

	private void amlTestUpdateDeviceDatum() {
		// update metadata for this object
		AylaDatum datum = new AylaDatum();
		datum.key = "aDevKey";
		datum.value = "aValue2";

		gblAmlTestDevice.updateDatum(createDeviceDatum, datum);
		
		// save for later comparison after get
		gblAmlTestDevice.datum = new AylaDatum();
		gblAmlTestDevice.datum.key = datum.key;
		gblAmlTestDevice.datum.value = datum.value;
	}
	
	private final Handler createDeviceDatum = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDatum datum = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatum.class);
				gblAmlTestDevice.datum = datum;

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "createDeviceDatum");
				amlTestStatusMsg("Pass", "createDeviceDatum", passMsg);

				testSequencer.nextTest().execute();
//				if (amlTestSuite == testSuite.LAN_MODE_ONLY) {
//					// next Lan Mode test TBD
//				} else {
//					amlTestGetDeviceDatum();
//				}
			}  else {	
				if (msg.arg1 == 422) { // allow recovery from previous failure
					String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "value", gblAmlTestDevice.datum.value, "createDeviceDatum");
					amlTestStatusMsg("Pass", "createDeviceDatum", passMsg);

					testSequencer.nextTest().execute();
					return;
				}
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createDeviceDatum");
				amlTestStatusMsg("Fail", "createDeviceDatum", errMsg);
			}		
		}
	};
	

	private void amlTestGetDeviceDatum() {
		gblAmlTestDevice.getDatumWithKey(getDeviceDatum, "aDevKey");
	}

	private final Handler getDeviceDatum = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDatum datum = AylaSystemUtils.gson.fromJson(jsonResults, AylaDatum.class);
				gblAmlTestDevice.datum = datum;
				
				//if (gblAmlTestDevice.metadata != null && datum.value.equals(gblAmlTestDevice.metadata.datum.value)) {
					//AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "getDatum");
				//} 
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "getDatum");
					amlTestStatusMsg("Pass", "getDatum", passMsg);

					testSequencer.nextTest().execute();
//					amlTestDeleteDeviceDatum(); // next test
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getDatums");
				amlTestStatusMsg("Fail", "getDeviceDatum", errMsg);
			}
		}
	};
	
	private enum WhatToTest {getAll, matchPattern, getNamed}
	private void amlTestGetDeviceDatums() {
		
		Map<String, ArrayList<String>> callParams = null;
		ArrayList<String> filters = null;
		
		WhatToTest whatToTest = WhatToTest.getAll;
		switch (whatToTest) {
			case getAll:
				// NOP
				break;
			case matchPattern:
				// get metadata with keys matching a pattern
				callParams = new HashMap<String, ArrayList<String>>();
				filters = new ArrayList<String>();
				filters.add("%Dev%");
				callParams.put("filters", filters);
				break;
			case getNamed:
				// get metadata with kyes matching one or more names
				callParams = new HashMap<String, ArrayList<String>>();
				filters = new ArrayList<String>();
				filters.add("aDevKey");
				//filters.add("anOtherDevKey");
				callParams.put("filters", filters);
				break;
			default:
				//error
		}
		
		gblAmlTestDevice.getDatum(getDeviceDatums, callParams);
	}

	private final Handler getDeviceDatums = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String aDatumValue = "";
				
				AylaDatum[] datum = AylaSystemUtils.gson.fromJson(jsonResults, AylaDatum[].class);
				if (datum.length > 0) {
					gblAmlTestDevice.datum = datum[0];
					aDatumValue = datum[0].value;
				} else {
					gblAmlTestDevice.datum = null;
				}

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", aDatumValue, "getDeviceDatums");
				amlTestStatusMsg("Pass", "getDeviceDatums", passMsg);

				testSequencer.nextTest().execute();
//				amlTestDeleteUserDatum(); // next test
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getDeviceDatums");
				amlTestStatusMsg("Fail", "getDeviceDatums", errMsg);
			}
		}
	};

	private void amlTestDeleteDeviceDatum() {
		gblAmlTestDevice.datum.key = "aDevKey";
		gblAmlTestDevice.deleteDatum(deleteDeviceDatum, gblAmlTestDevice.datum);
	}

	private final Handler deleteDeviceDatum = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {		
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "deleteDatum");
				amlTestStatusMsg("Pass", "deleteDeviceDatum", passMsg);

				testSequencer.nextTest().execute();
				// next test
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteDatum");
				amlTestStatusMsg("Fail", "deleteDatum", errMsg);
			}		
		}
	};
	
	// -------------- Testing Contacts API ---------------------
		private void amlTestContactsAPI (final boolean isRegressionTesting) {
			AMLContactsUnitTest contactsTests = new AMLContactsUnitTest(this);  
			if (isRegressionTesting) {
				contactsTests.setUser(gblAmlTestUser);
				contactsTests.start();
			} else {
				contactsTests.onResume();
			}
		}
		
	// -------------- Testing Blob API -------------------------
		private void amlTestBlobAPI (final boolean isRegressionTesting) {
			AMLBlobUnitTest blobTests = new AMLBlobUnitTest(this);
			if (isRegressionTesting) {
				blobTests.setUser(gblAmlTestUser);
				blobTests.setDevice(gblAmlTestDevice);
				blobTests.setProperty(gblAmlTestProperty);
				blobTests.start();
			} else {
				blobTests.onResume();
			}
		}
	
	// -------------- user metadata unit tests -----------------
	
	private void amlTestCreateUserDatum() {
		// create a metadata object
		AylaDatum datum = new AylaDatum();
		datum.key = "aUserKey";
		datum.value = "aValue1";

		gblAmlTestUser.createDatum(createUserDatum, datum);

		// save for later comparison after get
		gblAmlTestUser.datum = new AylaDatum();
		gblAmlTestUser.datum.key = datum.key;
		gblAmlTestUser.datum.value = datum.value;
	}

	private void amlTestUpdateUserDatum() {
		// update metadata for this object
		AylaDatum datum = new AylaDatum();
		datum.key = "aUserKey";
		datum.value = "aValue2";

		gblAmlTestUser.updateDatum(createUserDatum, datum);

		// save for later comparison after get
		gblAmlTestUser.datum = new AylaDatum();
		gblAmlTestUser.datum.key = datum.key;
		gblAmlTestUser.datum.value = datum.value;
	}

	private final Handler createUserDatum = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDatum datum = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatum.class);
				gblAmlTestUser.datum = datum;

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "createUserDatum");
				amlTestStatusMsg("Pass", "createUserDatum", passMsg);

				testSequencer.nextTest().execute();
//				if (amlTestSuite == testSuite.LAN_MODE_ONLY) {
//					// next Lan Mode test TBD
//				} else {
//					amlTestGetUserDatum();
//				}
			}  else {
				if (msg.arg1 == 422) { // allow recovery from previous failure
					String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "value", gblAmlTestUser.datum.value, "createUserDatum");
					amlTestStatusMsg("Pass", "createUserDatum", passMsg);

					testSequencer.nextTest().execute();
					return;
				}
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createUserDatum");
				amlTestStatusMsg("Fail", "createUserDatum", errMsg);
			}		
		}
	};


	private void amlTestGetUserDatum() {
		gblAmlTestUser.getDatumWithKey(getUserDatum, "aUserKey");
	}

	private final Handler getUserDatum = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaDatum datum = AylaSystemUtils.gson.fromJson(jsonResults, AylaDatum.class);
				gblAmlTestUser.datum = datum;

				//if (gblAmlTestUser.metadata != null && datum.value.equals(gblAmlTestUser.metadata.datum.value)) {
				//AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "getUserDatum");
				//} 
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datum.value, "getUserDatum");
				amlTestStatusMsg("Pass", "getUserDatum", passMsg);

				testSequencer.nextTest().execute();
//				amlTestGetUserDatums(); // next test
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getUserDatums");
				amlTestStatusMsg("Fail", "getUserDatum", errMsg);
			}
		}
	};
	
	private void amlTestGetUserDatums() {
			
		Map<String, ArrayList<String>> callParams = null;
		ArrayList<String> filters = null;
		
		WhatToTest whatToTest = WhatToTest.getNamed;
		switch (whatToTest) {
			case getAll:
				// NOP
				break;
			case matchPattern:
				// get metadata with keys matching a pattern
				callParams = new HashMap<String, ArrayList<String>>();
				filters = new ArrayList<String>();
				filters.add("%UserKey");
				callParams.put("filters", filters);
				break;
			case getNamed:
				// get metadata with keys matching one or more names
				callParams = new HashMap<String, ArrayList<String>>();
				filters = new ArrayList<String>();
				filters.add("aUserKey");
				//filters.add("anOtherUserKey");
				callParams.put("filters", filters);
				break;
			default:
				//error
		}
		
		gblAmlTestUser.getDatum(getUserDatums, callParams);
	}

	private final Handler getUserDatums = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String aDatumValue = "";
				
				AylaDatum[] datum = AylaSystemUtils.gson.fromJson(jsonResults, AylaDatum[].class);
				if (datum.length > 0) {
					gblAmlTestUser.datum = datum[0];
					aDatumValue = datum[0].value;
				} else {
					gblAmlTestUser.datum = null;
				}

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", aDatumValue, "getUserDatums");
				amlTestStatusMsg("Pass", "getUserDatums", passMsg);

				testSequencer.nextTest().execute();
//				amlTestDeleteUserDatum(); // next test
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getUserDatums");
				amlTestStatusMsg("Fail", "getUserDatums", errMsg);
			}
		}
	};

	private void amlTestDeleteUserDatum() {
		gblAmlTestUser.datum.key = "aUserKey";
		gblAmlTestUser.deleteDatum(deleteUserDatum, gblAmlTestUser.datum);
	}

	private final Handler deleteUserDatum = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {		
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "deleteDatum");
				amlTestStatusMsg("Pass", "deleteUserDatum", passMsg);

				testSequencer.nextTest().execute();
				// next test
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteDatum");
				amlTestStatusMsg("Fail", "deleteDatum", errMsg);
			}		
		}
	};

	
// ----------------------- Sharing unit tests ---------------------
	
	private void amlTestCreateShare() {
		// create a share for this device
		
		//AylaShare share = new AylaShare();
		//gblAmlTestDevice.share = new AylaShare();
		gblAmlTestShare = new AylaShare();
		//gblAmlTestDevice.share.resourceName = "device";
		//gblAmlTestDevice.share.resourceId = gblAmlTestDevice.dsn;
		//gblAmlTestDevice.share.userEmail = shareEmailAddress;
		gblAmlTestShare.userEmail = AMLUnitTestConfig.shareWithEmailAddress;
		//share.operation = "read";					// can be defaulted to read
		//share.startDateAt = "2014-04-06 12:00:00";
		//share.endDateAt = "2024-04-06 12:00:00";
		gblAmlTestDevice.createShare(createUpdateShare, gblAmlTestShare);
		//gblAmlTestDevice.share.create(createUpdateShare, gblAmlTestDevice);
		//gblAmlTestShare.create(createUpdateShare, gblAmlTestDevice);
	}

	private void amlTestUpdateShare() {		

		// update a share for this device
		gblAmlTestDevice.share.operation = "write";
	
		// set start time to now // 2014-04-30 22:57:15
		gblAmlTestDevice.share.startDateAt = AylaSystemUtils.gmtFmt.format(new Date());

		// set end share dateTime "2024-05-30 12:00:00"
		Date date = new GregorianCalendar(2024, Calendar.APRIL, 30).getTime();
		gblAmlTestDevice.share.endDateAt = AylaSystemUtils.gmtFmt.format(date);

		gblAmlTestDevice.share.update(createUpdateShare);
	}
	
	private final Handler createUpdateShare = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaShare share = AylaSystemUtils.gson.fromJson(jsonResults,AylaShare.class);

				int len = 0;
				boolean found = false;
				if (share.resourceName.equals("device") && (share.resourceId.equals(gblAmlTestDevice.dsn)) ) {
					gblAmlTestDevice.share = new AylaShare();
					gblAmlTestDevice.share = share; // save as a share for this device

					// add to list of shares for this device
					if (gblAmlTestDevice.shares == null) {
						gblAmlTestDevice.shares = new AylaShare[1];
						gblAmlTestDevice.shares[0] = share;
					} else
					if (gblAmlTestDevice.shares != null) {
						len = gblAmlTestDevice.shares.length;
						for (int i = 0; i < len; i++) {
							if (gblAmlTestDevice.shares[i].resourceId.equals(share.resourceId) && 
							    gblAmlTestDevice.shares[i].userEmail.equals(share.userEmail) ) 
							{
								gblAmlTestDevice.shares[i] = share;	// update existing
								found = true;
								break;
							}
						}
						if (found == false) {
							gblAmlTestDevice.shares[len] = share;	// add to end
						}
					}
				}

				// add to list of shares for this user
				gblAmlTestUser.share = share;
				found = false;
				if (gblAmlTestUser.shares == null) {
					gblAmlTestUser.shares = new AylaShare[1];
					gblAmlTestUser.shares[0] = share;
				} else
				if (gblAmlTestUser.shares != null) {
					len = gblAmlTestUser.shares.length;
					for (int i = 0; i < len; i++) {
						if (gblAmlTestUser.shares[i].resourceId.equals(share.resourceId) && 
						    gblAmlTestUser.shares[i].userEmail.equals(share.userEmail) ) 
						{
							gblAmlTestUser.shares[i] = share;	// update existing
							found = true;
							break;
						}
					}
					if (found == false) {
						gblAmlTestUser.shares[len] = share;	// add to end
					}
				}

				String passMsg = String.format("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "shareId", share.id, "userEmail", share.userEmail, "createUpdateShare");
				amlTestStatusMsg("Pass", "createUpdateShare", passMsg);

				testSequencer.nextTest().execute();
			}  else {	
				if (msg.arg1 == 422) { 
					gblAmlTestShare.id = "63"; gblAmlTestDevice.share = gblAmlTestShare; 	// allow recovery from failure post create
					String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "userEmail", gblAmlTestDevice.share.userEmail, "createUpdateShare_recovery");
					amlTestStatusMsg("Pass", "createUpdateShare", passMsg);

					testSequencer.nextTest().execute();
					return;
				}
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createUpdateShare");
				amlTestStatusMsg("Fail", "createUpdateShare", errMsg);
			}		
		}
	};
	
	private enum SharesToTest {allShares, matchResourceClass, matchResourceId}
	private boolean receivedShares = false;
	
	private void amlTestGetShares() {
		receivedShares = false; // only get shares owned by user
		
		amlTestGetOwnedOrReceivedShares();
	}
	
	private void amlTestGetReceivedShares() {
		receivedShares = true; // only get shares received by user
		
		amlTestGetOwnedOrReceivedShares();
	}
	
	private void amlTestGetOwnedOrReceivedShares() {

		Map<String, String> callParams = null;
		//ArrayList<String> filters = null;
		SharesToTest sharesToTest;
	
		//sharesToTest = SharesToTest.allShares;
		//sharesToTest = SharesToTest.matchResourceClass;
		sharesToTest = SharesToTest.matchResourceId;
		switch (sharesToTest) {
			case allShares:
				// NOP
				break;
			case matchResourceClass:
				// get metadata with keys matching a pattern
				callParams = new HashMap<String, String>();
				//filters = new ArrayList<String>();
				//filters.add("%Dev%");
				callParams.put("resourceName", "device");
				break;
			case matchResourceId:
				// get metadata with kyes matching one or more names
				callParams = new HashMap<String, String>();
				//filters = new ArrayList<String>();
				//filters.add("aDevKey");
				//filters.add("anOtherDevKey");
				//callParams.put("resourceName", "device");
				if (receivedShares) {
					callParams.put("resourceName", "device");			// Required for AylaUser or AylaShare call
					callParams.put("resourceId", AMLUnitTestConfig.shareReceiveFromDSN);
				} else {
					callParams.put("resourceId", gblAmlTestDevice.dsn);
				}
				break;
			default:
				//error
		}
		
		if (receivedShares) {
			//gblAmlTestDevice.getReceivedShares(getShares);			// dynamic pass through will default resourceName to class type & set resourceId to DSN
			AylaDevice.getAllReceivedShares(getShares);					// static pass through will default resourceName to class type
			//AylaUser.getAllReceivedShares(getShares, callParams);		// static pass through, must set callParams
		} else {
			gblAmlTestDevice.getShares(getShares);						// pass through will default resourceName to class type
			//gblAmlTestDevice.getShares(getShares, callParams);		// Can retrieve any class type. Must set resourceName if resoruceId is specified
			//AylaShare.get(getShares, gblAmlTestDevice, callParams);	// using device object will default resourceName to class type
			//AylaShare.get(getShares, gblAmlTestUser, callParams);		// Can retrieve any class type. Must set resourceName if resoruceId is specified
			//AylaUser.getCurrent().getShares(getShares, callParams); 	// dynamic pass through, must set callParams
			//AylaUser.getAllShares(getShares, callParams);				// static pass through, must set callParams
		}
	}

	private final Handler getShares = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String aShareEmailLabel = "";
				String aShareEmail = "";
				String aShareId = "";
				// String aShareFirstName = "";
				gblAmlTestDevice.share = null;
				boolean found = false;
				
				AylaShare[] shares = AylaSystemUtils.gson.fromJson(jsonResults, AylaShare[].class);
				if (shares != null) {
					int len = shares.length;
					gblAmlTestDevice.shares = new AylaShare[len];

					for (int i = 0, j = 0; i < len; i++) {													// loop only needed for AylaShare & AylaUser shares
						if (shares[i].resourceName.equals("device")) {										// a share of type/class device
							if ( (!receivedShares && gblAmlTestDevice.dsn.equals(shares[i].resourceId)) ||	// a share for this owned/received device
								 (receivedShares && AMLUnitTestConfig.shareReceiveFromDSN.equals(shares[i].resourceId))
							   ) 
							{
								gblAmlTestDevice.shares[j++] = shares[i];				// save it as a share for this device
								
								// is this the share we want to delete
								if (receivedShares) {
									aShareEmailLabel = "receiveFrom";
									aShareEmail = AMLUnitTestConfig.shareReceiveFromEmailAddress;
									// aShareFirstName = shares[0].ownerProfile.firstname;
									if (aShareEmail.equals(shares[i].ownerProfile.email))	{
										gblAmlTestDevice.share = shares[i];					// save this share for share.get() test
										aShareId = shares[0].id;
										
										found = true;
										//break; // break to optimize or continue depending on filtering in the getShares() & application requirements
									}
								} else {
									aShareEmailLabel = "sharedTo";
									aShareEmail = AMLUnitTestConfig.shareWithEmailAddress;
									// aShareFirstName = shares[0].userProfile.firstname;
									if (aShareEmail.equals(shares[i].userProfile.email))	{
										gblAmlTestDevice.share = shares[i];					// save this share for share.get() test
										aShareId = shares[0].id;
										
										found = true;
										//break; // break to optimize or continue depending on filtering in the getShares() & application requirements
									}
								}
							}
						}
					}
				}
				
				if (found == false) {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getShares");
					amlTestStatusMsg("Fail", "getShares", errMsg);
				} else {
					String passMsg = String.format("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "shareId", aShareId, aShareEmailLabel, aShareEmail, "getShares");
					amlTestStatusMsg("Pass", "getShares", passMsg);
	
					testSequencer.nextTest().execute();
	//				amlTestDeleteUserShare(); // next test
				}
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getShares");
				amlTestStatusMsg("Fail", "getShares", errMsg);
			}
		}
	};


	private void amlTestGetReceivedShare() {
		receivedShares = true; // only get shares received by user
		
		// AylaShare.getWithId(getShare,  gblAmlTestDevice.share.id);
		gblAmlTestDevice.getShare(getShare, gblAmlTestDevice.share.id);	// get a specific received share
	}

	private void amlTestGetShare() {
		receivedShares = false;	// only get shares owned by user
		
		// AylaShare.getWithId(getShare,  gblAmlTestDevice.share.id);
		gblAmlTestDevice.getShare(getShare, gblAmlTestDevice.share.id);	// get a specific saved shareId
	}
	
	private final Handler getShare = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String aShareEmailLabel = "";
				String aShareEmail = "";
				
				AylaShare share = AylaSystemUtils.gson.fromJson(jsonResults, AylaShare.class);
				gblAmlTestDevice.share = share;

				if (receivedShares) {
					aShareEmailLabel = "receiveFrom";
					aShareEmail = share.ownerProfile.email;
				} else {
					aShareEmailLabel = "sharedTo";
					aShareEmail = share.userProfile.email;
				}
				
				String passMsg = String.format("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "shareId", share.id, aShareEmailLabel, aShareEmail, "getShare");
				amlTestStatusMsg("Pass", "getShare", passMsg);

				testSequencer.nextTest().execute();
//				amlTestDeleteUserShare(); // next test
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getShare");
				amlTestStatusMsg("Fail", "getShare", errMsg);
			}
		}
	};

	// delete an owned or received share
	private void amlTestDeleteShare() {

		//gblAmlTestShare.delete(deleteShare);
		gblAmlTestDevice.share.delete(deleteShare);
		//gblAmlTestDevice.deleteShare(deleteShare, gblAmlTestDevice.share);
	}

	private final Handler deleteShare = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {		
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "deleteShare");
				amlTestStatusMsg("Pass", "deleteShare", passMsg);

				testSequencer.nextTest().execute();
				// next test
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteShare");
				amlTestStatusMsg("Fail", "deleteShare", errMsg);
			}		
		}
	};
	
	// delete an owned or received share
	private void amlTestDeleteShares() {
		Map<String, String> callParams = null;
		//ArrayList<String> filters = null;

		SharesToTest sharesToTest = SharesToTest.matchResourceClass;
		switch (sharesToTest) {
			case allShares:
				// NOP
				break;
			case matchResourceClass:
				// get metadata with keys matching a pattern
				callParams = new HashMap<String, String>();
				//filters = new ArrayList<String>();
				//filters.add("%Dev%");
				callParams.put("resourceName", "device");
				break;
			case matchResourceId:
				// get metadata with kyes matching one or more names
				callParams = new HashMap<String, String>();
				//filters = new ArrayList<String>();
				//filters.add("aDevKey");
				//filters.add("anOtherDevKey");
				//callParams.put("resourceName", "device");
				callParams.put("resourceId", gblAmlTestDevice.dsn);
				break;
			default:
				//error
		}
		

		//gblAmlTestDevice.deleteShares(deleteShare);						// pass through will default resourceName to class type, ignores callParams
		//AylaShare.deletes(deleteShare, gblAmlTestDevice, callParams);	// using device object will default resourceName to class type
		//AylaShare.deletes(deleteShare, gblAmlTestUser, callParams);		// Can retrieve any class type. Must set resourceName if resoruceId is specified
	}
	
	// ----------------------- Resend Confirmation & Reset Password unit tests ---------------------
	
	private void amlTestResendConfirmation() {
		Map<String, String> callParams = new HashMap<String, String>();
		callParams.put(AylaNetworks.AML_EMAIL_TEMPLATE_ID, "ayla_confirmation_template_02");
		callParams.put(AylaNetworks.AML_EMAIL_SUBJECT, "Test of resendConfirmation()");
		callParams.put(AylaNetworks.AML_EMAIL_BODY_HTML, "This is a test of resendConfirmation()");

		AylaUser.resendConfirmation(
				resendConfirmation, 
				AMLUnitTestConfig.resendConfirmationEmailAddress, 
				AMLUnitTestConfig.appId, 
				AMLUnitTestConfig.appSecret, 
				callParams);
	}

	private final Handler resendConfirmation = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {		
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "resendConfirmation");
				amlTestStatusMsg("Pass", "resendConfirmation", passMsg);

				testSequencer.nextTest().execute();		// next test
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "resendConfirmation");
				amlTestStatusMsg("Fail", "resendConfirmation", errMsg);
			}		
		}
	};

	private void amlTestResetPassword() {
		Map<String, String> callParams = new HashMap<String, String>();
		callParams.put(AylaNetworks.AML_EMAIL_TEMPLATE_ID, "ayla_passwd_reset_template_02");
		callParams.put(AylaNetworks.AML_EMAIL_SUBJECT, "Test of resetPassword()");
		callParams.put(AylaNetworks.AML_EMAIL_BODY_HTML, "This is a test of resetPassword()");

		AylaUser.resetPassword(
				resetPassword, 
				AMLUnitTestConfig.resetPasswordEmailAddress, 
				AMLUnitTestConfig.appId, 
				AMLUnitTestConfig.appSecret, 
				callParams);
	}

	private final Handler resetPassword = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {		
				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "return", "success", "resetPassword");
				amlTestStatusMsg("Pass", "resetPassword", passMsg);

				testSequencer.nextTest().execute();		// next test
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "resetPassword");
				amlTestStatusMsg("Fail", "resetPassword", errMsg);
			}		
		}
	};


	// -------------- gateway groups, bindings, & scenes unit tests -----------------

	// -------------- groups unit tests -----------------
	
	// getAllGroups and then delete them
	private void amlTestDeleteAllGroups_ORIG() {
		Thread thread = new Thread(new Runnable() {
			public void run()
			{
				AylaGroupZigbee[] groups;
				
				AylaRestService rs1 = gblAmlTestZigbeeGateway.getGroups(null);
				msg = rs1.execute();

				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					groups = AylaSystemUtils.gson.fromJson(jsonResults, AylaGroupZigbee[].class);
					int len = groups.length;
					for (int i = 0; i < len; i++) {
						AylaRestService rs2 = gblAmlTestZigbeeGateway.deleteGroup(groups[i], null);
						msg = rs2.execute();
					}
					jsonResults = (String)msg.obj;
					
					if (msg.what == AylaNetworks.AML_ERROR_OK) {
						passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "joinStatus", jsonResults, "deleteAllGroups");
					} else {
						errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllGroups");
					}
				} else {
					errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllGroups");
				}
			}
		});
		thread.start();
		try {
			thread.join();		// Wait for thread to finish
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (msg.arg1 >= 200 && msg.arg1 <= 299) {
			amlTestStatusMsg("Pass", "DeleteAllGroups", passMsg);
		} else {
			amlTestStatusMsg("Fail", "DeleteAllGroups", errMsg);
		}
		
		if (!(AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY) 
				&& !(AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY)) {
			testSequencer.nextTest().execute();
		}
	}
	
	// getAllGroups and then delete them
	private void amlTestDeleteAllGroups() {
		gblAmlTestZigbeeGateway.getGroups(deleteAllGroups, null);
	}

	private final Handler deleteAllGroups = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaGroupZigbee[] groups = AylaSystemUtils.gson.fromJson(jsonResults, AylaGroupZigbee[].class);
				int len = groups.length;
				for (int i = 0; i < len; i++) {
					gblAmlTestZigbeeGateway.deleteGroup(deleteOneGroup, groups[i], null);
				}
//				gblAmlGroupAck = new GroupAck(groupAck, gblAmlTestZigbeeGateway, groups);
//				gblAmlGroupAck.waitForGroupsAck();
				new CountDownTimer(3000, 1000) {
					@Override
					public void onFinish() {
						testSequencer.nextTest().execute();
					}
					@Override
					public void onTick(long millisUntilFinished) {
					}
				}.start();
			} else {
				errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllGroups");
			}
		}
	};
	
	private final Handler deleteOneGroup = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteGroup");
				amlTestStatusMsg("Pass", "deleteGroup", passMsg);
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteGroup");
				amlTestStatusMsg("Fail", "deleteGroup", errMsg);
			}		
		}
	};
	
	private final Handler groupAck = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				if (msg.arg1 == 206) {		// Any failing nodes?
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "groupAck");
					amlTestStatusMsg("Fail", "groupAck", errMsg);
				} else {
					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "groupAck");
					amlTestStatusMsg("Pass", "groupAck", passMsg);

					if (!(AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY) 
							&& !(AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY)) {
						new CountDownTimer(3000, 1000) {
							@Override
							public void onFinish() {
								testSequencer.nextTest().execute();
							}
							@Override
							public void onTick(long millisUntilFinished) {
							}
						}.start();
					}
				}
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "groupAck");
				amlTestStatusMsg("Fail", "groupAck", errMsg);
			}		
		}
	};

	private void amlTestCreateGroup() {
		AylaGroupZigbee group = new AylaGroupZigbee();
		group.groupName = "SmartPlugGroup";
		group.nodeDsns = new String[1];
		group.nodeDsns[0] = AMLUnitTestConfig.gblAmlTestSmartPlugNodeDsn;

		gblAmlTestZigbeeGateway.createGroup(createGroup, group, null, delayExecution);
	}

	private void amlTestUpdateGroup() {
		gblAmlTestSmartPlugLightSocketGroup.groupName = "SmartPlugLightSocketGroup";
		gblAmlTestSmartPlugLightSocketGroup.nodeDsns = new String[2];
		gblAmlTestSmartPlugLightSocketGroup.nodeDsns[0] = AMLUnitTestConfig.gblAmlTestSmartPlugNodeDsn;
		gblAmlTestSmartPlugLightSocketGroup.nodeDsns[1] = AMLUnitTestConfig.gblAmlTestLightSocketNodeDsn;
		
		gblAmlTestZigbeeGateway.updateGroup(createGroup, gblAmlTestSmartPlugLightSocketGroup, null, delayExecution);
	}

	private final Handler createGroup = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
//				AylaGroupZigbee[] groups = AylaSystemUtils.gson.fromJson(jsonResults, AylaGroupZigbee[].class);
//				if (groups != null) {
//					gblAmlTestSmartPlugLightSocketGroup = groups[0];
//				}
				gblAmlTestSmartPlugLightSocketGroup = AylaSystemUtils.gson.fromJson(jsonResults, AylaGroupZigbee.class);

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestSmartPlugLightSocketGroup.groupName, "createOrUpdateGroup");
				amlTestStatusMsg("Pass", "createOrUpdateGroup", passMsg);

//				gblAmlGroupAck = new GroupAck(groupAck, gblAmlTestZigbeeGateway, gblAmlTestSmartPlugLightSocketGroup);
//				gblAmlGroupAck.waitForGroupsAck();
				testSequencer.nextTest().execute();
			}  else {	
				if (msg.arg1 == 422) { // allow recovery from previous failure
					String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "group already exists", "createOrUpdateGroup");
					amlTestStatusMsg("Pass", "createOrUpdateGroup", passMsg);

					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateGroup");
					amlTestStatusMsg("Fail", "createOrUpdateGroup", errMsg);
				}
			}		
		}
	};

	private void amlTestGetGroups() {
		// optional call parameters
		Map<String, Object> callParams = new HashMap<String, Object>();
		callParams.put(AylaNetworksZigbee.kAylaZigbeeNodeParamDetail, "false");		// default is "false"
		//callParams.put(AylaNetworksZigbee.kAylaZigbeeNodeParamStatusFilter, "all");	// default is "active"
		
		gblAmlTestZigbeeGateway.getGroups(getGroups, callParams, delayExecution);
	}

	private final Handler getGroups = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaGroupZigbee[] newGroups = AylaSystemUtils.gson.fromJson(jsonResults, AylaGroupZigbee[].class);

/*
				// For lib testing multiple groups only djm
				if (newGroups.length >= 1) {	
					gblAmlTestSmartPlugLightSocketGroup = newGroups[0];
				}
				testSequencer.nextTest().execute();
				return;
*/
				if (newGroups.length == 1) {
					// Minimal check to see if the groupName matches
					if (newGroups[0].groupName.equals(gblAmlTestSmartPlugLightSocketGroup.groupName)) {
						String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", newGroups[0].groupName, "getGroups");
						amlTestStatusMsg("Pass", "getGroups", passMsg);
	
						testSequencer.nextTest().execute();
					} else {
						String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected groupName", newGroups[0].groupName, "getGroups");
						amlTestStatusMsg("Fail", "getGroups", errMsg);					
					}
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s", "F", "amlTest", "unexpected number of groups", newGroups.length, "getGroups");
					amlTestStatusMsg("Fail", "getGroups", errMsg);										
				}

			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getGroups");
				amlTestStatusMsg("Fail", "getGroups", errMsg);
			}	
	
		}

	};

	private void amlTestGetGroup() {
		//AylaGroupZigbee group = new AylaGroupZigbee();
		//group.id = gblAmlTestSmartPlugLightSocketGroup.id;

		gblAmlTestZigbeeGateway.getGroup(getGroup, gblAmlTestSmartPlugLightSocketGroup, null, delayExecution);
	}

	private final Handler getGroup = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaGroupZigbee newGroup = AylaSystemUtils.gson.fromJson(jsonResults, AylaGroupZigbee.class);
				
				// Minimal check to see if the groupName matches
				if (newGroup.groupName.equals(gblAmlTestSmartPlugLightSocketGroup.groupName)) {
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestSmartPlugLightSocketGroup.groupName, "getGroup");
					amlTestStatusMsg("Pass", "getGroup", passMsg);

					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected groupName", newGroup.groupName, "getGroup");
					amlTestStatusMsg("Fail", "getGroup", errMsg);					
				}
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getGroup");
				amlTestStatusMsg("Fail", "getGroup", errMsg);
			}		
		}
	};
	
	private void amlTestDeleteGroup() {
		AylaGroupZigbee group = new AylaGroupZigbee();
		//group.id = gblAmlTestSmartPlugLightSocketGroup.id;

		gblAmlTestZigbeeGateway.deleteGroup(deleteGroup, gblAmlTestSmartPlugLightSocketGroup, null, delayExecution);
	}

	private final Handler deleteGroup = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteGroup");
				amlTestStatusMsg("Pass", "deleteGroup", passMsg);

//				gblAmlGroupAck = new GroupAck(groupAck, gblAmlTestZigbeeGateway, gblAmlTestSmartPlugLightSocketGroup);
//				gblAmlGroupAck.waitForGroupsAck();
				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteGroup");
				amlTestStatusMsg("Fail", "deleteGroup", errMsg);
			}		
		}
	};
	

	
	// -------------------------------------- Get Node Properties ------------------------------

	private void amlTestGetSmartPlugProperties() {
		gblAmlTestSmartPlugNode.getProperties(getSmartPlugProperties);
	}

	private final Handler getSmartPlugProperties = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestSmartPlugNode.properties = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty[].class);
				String name;
				for (AylaProperty property : gblAmlTestSmartPlugNode.properties) {
					name = property.name();
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "name", name, "value", property.value, "getSmartPlugProperties");
					if (name.equals(gblAmlTestSmartPlugNodePropertyName)) { // simulate user selection of a property
						gblAmlTestSmartPlugNodeProperty = property; // assign property under test
					}
				}

				if (gblAmlTestSmartPlugNodeProperty != null) {
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestSmartPlugNode.properties.length, "getSmartPlugProperties");
					amlTestStatusMsg("Pass", "getSmartPlugProperties", passMsg);

					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestProperty", "null", "getSmartPlugProperties - no property assigned");
					amlTestStatusMsg("Fail", "getSmartPlugProperties", errMsg);
				}
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getSmartPlugProperties");
				amlTestStatusMsg("Fail", "getSmartPlugProperties", errMsg);
			}		
		};
	};

	private void amlTestGetLightSocketProperties() {
		gblAmlTestLightSocketNode.getProperties(getLightSocketProperties);
	}

	private final Handler getLightSocketProperties = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestLightSocketNode.properties = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty[].class);
				String name;
				for (AylaProperty property : gblAmlTestLightSocketNode.properties) {
					name = property.name();
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "name", name, "value", property.value, "getLightSocketProperties");
					if (name.equals(gblAmlTestLightSocketNodePropertyName)) { // simulate user selection of a property
						gblAmlTestLightSocketNodeProperty = property; // assign property under test
					}
				}

				if (gblAmlTestLightSocketNodeProperty != null) {
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestSmartPlugNode.properties.length, "getLightSocketProperties");
					amlTestStatusMsg("Pass", "getLightSocketProperties", passMsg);

					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestProperty", "null", "getLightSocketProperties - no property assigned");
					amlTestStatusMsg("Fail", "getLightSocketProperties", errMsg);
				}
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getLightSocketProperties");
				amlTestStatusMsg("Fail", "getLightSocketProperties", errMsg);
			}		
		};
	};

	private void amlTestGetRemoteControlProperties() {
		gblAmlTestRemoteControlNode.getProperties(getRemoteControlProperties);
	}

	private final Handler getRemoteControlProperties = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestRemoteControlNode.properties = AylaSystemUtils.gson.fromJson(jsonResults,AylaProperty[].class);
				String name;
				for (AylaProperty property : gblAmlTestRemoteControlNode.properties) {
					name = property.name();
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "name", name, "value", property.value, "getRemoteControlProperties");
					if (name.equals(gblAmlTestRemoteControlNodePropertyName)) { // simulate user selection of a property
						gblAmlTestRemoteControlNodeProperty = property; // assign property under test
					}
				}

				if (gblAmlTestRemoteControlNodeProperty != null) {
					String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "count", gblAmlTestSmartPlugNode.properties.length, "getRemoteControlProperties");
					amlTestStatusMsg("Pass", "getRemoteControlProperties", passMsg);

					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "gblAmlTestProperty", "null", "getRemoteControlProperties - no property assigned");
					amlTestStatusMsg("Fail", "getRemoteControlProperties", errMsg);
				}
			}  else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getRemoteControlProperties");
				amlTestStatusMsg("Fail", "getRemoteControlProperties", errMsg);
			}		
		};
	};

	// -------------------------------------- Create Node Datapoints ------------------------------

	private void amlTestCreateSmartPlugDatapoint() {
		gblAmlTestSmartPlugNodePropertyValue = 1 - gblAmlTestSmartPlugNodePropertyValue;		// toggle boolean value

		final AylaDatapoint datapoint = new AylaDatapoint();
		datapoint.nValue(gblAmlTestSmartPlugNodePropertyValue);
		
		gblAmlTestGateway.createNodeDatapoint(createSmartPlugDatapoint, datapoint, gblAmlTestSmartPlugNode, gblAmlTestSmartPlugNodeProperty, delayExecution);
	}

	private final Handler createSmartPlugDatapoint = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestSmartPlugNodeProperty.datapoint = AylaSystemUtils.gson.fromJson(jsonResults,AylaDatapoint.class);
				AylaDatapoint datapoint= gblAmlTestSmartPlugNodeProperty.datapoint;

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "value", datapoint.value(), "createSmartPlugDatapoint");
				amlTestStatusMsg("Pass", "createSmartPlugDatapoint", passMsg);

				if (AylaLanMode.lanModeSession.UP != AylaLanMode.getSessionState()) {
					final Handler handler = new Handler();
				    handler.postDelayed(new Runnable() {
				        @Override
				        public void run() {
							testSequencer.nextTest().execute();
				        }
				    }, 1000);	// Delay 1 second
				} else {
					new CountDownTimer(5000000, 100) {

						@Override
						public void onFinish() {
						}

						@Override
						public void onTick(long millisUntilFinished) {
							if (AylaNotify.getNotifyOutstanding()) {
								testSequencer.nextTest().execute();
								this.cancel();
							}
						}// onTick
					}.start();
				}

			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createSmartPlugDatapoint");
				amlTestStatusMsg("Fail", "createSmartPlugDatapoint", errMsg);
			}		
		}
	};
	
	// -------------------------------------- Get Node Datapoints ------------------------------

	private void amlTestGetSmartPlugDatapointsByActivity(int count) {
		Map<String, String> callParams = new HashMap<String, String>();
		callParams.put("count", Integer.toString(count));
		gblAmlTestGateway.getNodeDatapointsByActivity(getSmartPlugDatapointsByActivity, callParams, gblAmlTestSmartPlugNode, gblAmlTestSmartPlugNodeProperty, delayExecution);
	}

	private final Handler getSmartPlugDatapointsByActivity = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestSmartPlugNodeProperty.datapoints = AylaSystemUtils.gson.fromJson(jsonResults, AylaDatapoint[].class);
				String baseType = gblAmlTestSmartPlugNodeProperty.baseType();
				if (TextUtils.equals("boolean", baseType)) {					
					// Print all of the datapoints
					for (AylaDatapoint datapoint : gblAmlTestSmartPlugNodeProperty.datapoints) {
						String sValue = datapoint.sValueFormatted(baseType);
						Number nValue = datapoint.nValueFormatted(baseType);
	
						String nValueStr = nValue.toString();
						AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "P", "amlTest", "nValue", nValueStr, "sValue", sValue, "getSmartPlugDatapointsByActivity");
					}
					// Check most recent value
					int mostRecent = gblAmlTestSmartPlugNodeProperty.datapoints.length - 1;
					Number nValue = gblAmlTestSmartPlugNodeProperty.datapoints[mostRecent].nValueFormatted(baseType);
					int nValueInt = nValue.intValue();
					if (nValueInt == gblAmlTestSmartPlugNodePropertyValue) {
						String passMsg = String.format("%s, %s, %s:%d, %s", "P", "amlTest", "nValue", nValueInt, "getSmartPlugDatapointsByActivity");
						amlTestStatusMsg("Pass", "getSmartPlugDatapointsByActivity", passMsg);

						testSequencer.nextTest().execute();
					} else {
						String errMsg = String.format("%s, %s, %s:%s, %d!=%d, %s", "F", "amlTest", "error", "nValueMismatch", nValueInt, gblAmlTestSmartPlugNodePropertyValue, "getSmartPlugDatapointsByActivity");
						amlTestStatusMsg("Fail", "getSmartPlugDatapointsByActivity", errMsg);							
					}
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s!=%s, %s", "F", "amlTest", "error", "baseTypeMismatch", baseType, AMLUnitTestConfig.gblAmlBaseType, "getSmartPlugDatapointsByActivity");
					amlTestStatusMsg("Fail", "getSmartPlugDatapointsByActivity", errMsg);					
				}
			} else {
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, jsonResults, "getSmartPlugDatapointsByActivity");
				amlTestStatusMsg("Fail", "getSmartPlugDatapointsByActivity", errMsg);
			}		
		}
	};

	// -------------- bindings unit tests -----------------
	
	// getAllBindings and then delete them
	private void amlTestDeleteAllBindings_ORIG() {
		Thread thread = new Thread(new Runnable() {
			public void run()
			{
				AylaBindingZigbee[] groups;
				
				AylaRestService rs1 = gblAmlTestZigbeeGateway.getBindings(null);
				msg = rs1.execute();

				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					groups = AylaSystemUtils.gson.fromJson(jsonResults, AylaBindingZigbee[].class);
					int len = groups.length;
					for (int i = 0; i < len; i++) {
						AylaRestService rs2 = gblAmlTestZigbeeGateway.deleteBinding(groups[i], null);
						msg = rs2.execute();
					}
					jsonResults = (String)msg.obj;
					
					if (msg.what == AylaNetworks.AML_ERROR_OK) {
						passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "joinStatus", jsonResults, "deleteAllBindings");
					} else {
						errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllBindings");
					}
				} else {
					errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllBindings");
				}
			}
		});
		thread.start();
		try {
			thread.join();		// Wait for thread to finish
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		if (msg.arg1 >= 200 && msg.arg1 <= 299) {
			amlTestStatusMsg("Pass", "DeleteAllBindings", passMsg);
		} else {
			amlTestStatusMsg("Fail", "DeleteAllBindings", errMsg);
		}
		
		if (!(AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY) 
				&& !(AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY)) {
			testSequencer.nextTest().execute();
		}
	}
	
	// getAllGroups and then delete them
	private void amlTestDeleteAllBindings() {
		// <---- Set BREAK POINT here so the user has time to press the remote control button, to wake it up
		gblAmlTestZigbeeGateway.getBindings(deleteAllBindings, null);
	}

	private final Handler deleteAllBindings = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaBindingZigbee[] bindings = AylaSystemUtils.gson.fromJson(jsonResults, AylaBindingZigbee[].class);
				int len = bindings.length;
				for (int i = 0; i < len; i++) {
					gblAmlTestZigbeeGateway.deleteBinding(deleteOneBinding, bindings[i], null);
				}
//				gblAmlBindingAck = new BindingAck(bindingAck, gblAmlTestZigbeeGateway, bindings);
//				gblAmlBindingAck.waitForBindingsAck();
				new CountDownTimer(3000, 1000) {
					@Override
					public void onFinish() {
						testSequencer.nextTest().execute();
					}
					@Override
					public void onTick(long millisUntilFinished) {
					}
				}.start();
			} else {
				errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllBindings");
			}
		}
	};
	
	private final Handler deleteOneBinding = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteBinding");
				amlTestStatusMsg("Pass", "deleteBinding", passMsg);
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteBinding");
				amlTestStatusMsg("Fail", "deleteBinding", errMsg);
			}		
		}
	};
	
	private final Handler bindingAck = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				if (msg.arg1 == 206) {		// Any failing nodes?
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "bindingAck");
					amlTestStatusMsg("Fail", "bindingAck", errMsg);
				} else {
					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "bindingAck");
					amlTestStatusMsg("Pass", "bindingAck", passMsg);
					
					testSequencer.nextTest().execute();
				}
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "bindingAck");
				amlTestStatusMsg("Fail", "bindingAck", errMsg);
			}		
		}
	};

	private void amlTestCreateBindingToProperty() {
		AylaBindingZigbee binding = new AylaBindingZigbee();
		
		binding.bindingName = "aRemoteControlToSmartPlugBindinga";
//		binding.bindingName = "aLightSocketToSmartPlugBindinga";
		binding.fromId = gblAmlTestRemoteControlNode.dsn;
		binding.fromName = gblAmlTestRemoteControlNodeProperty.name;
//		binding.fromId = gblAmlTestLightSocketNode.dsn;
//		binding.fromName = gblAmlTestLightSocketNodeProperty.name;
		binding.toId = gblAmlTestSmartPlugNode.dsn;
		binding.toName = gblAmlTestSmartPlugNodeProperty.name;
		
		gblAmlTestZigbeeGateway.createBinding(createBindingToProperty, binding, null, delayExecution);
	}

	private void amlTestUpdateBinding() {
		
		AylaBindingZigbee binding = new AylaBindingZigbee();
		
		binding.bindingName = "RemoteControlToSmartPlugBinding";		// Only the name can be updated
//		binding.bindingName = "aLightSocketToSmartPlugBindinga";
		binding.fromId = gblAmlTestPropertyBinding.fromId;
		binding.fromName = gblAmlTestPropertyBinding.fromName;
		binding.toId = gblAmlTestPropertyBinding.toId;
		binding.toName = gblAmlTestPropertyBinding.toName;
		binding.gatewayDsn = gblAmlTestPropertyBinding.gatewayDsn;
		binding.id = gblAmlTestPropertyBinding.getId();

		gblAmlTestZigbeeGateway.updateBinding(createBindingToProperty, binding, null, delayExecution);
		//gblAmlTestZigbeeGateway.updateBinding(createBindingToProperty, gblAmlTestPropertyBinding, null, delayExecution);
	}

	private final Handler createBindingToProperty = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestPropertyBinding = AylaSystemUtils.gson.fromJson(jsonResults, AylaBindingZigbee.class);

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestPropertyBinding.bindingName, "createOrUpdateBindingToProperty");
				amlTestStatusMsg("Pass", "createOrUpdateBindingToProperty", passMsg);

//				gblAmlBindingAck = new BindingAck(bindingAck, gblAmlTestZigbeeGateway, gblAmlTestPropertyBinding);
//				gblAmlBindingAck.waitForBindingsAck();
				testSequencer.nextTest().execute();
			}  else {	
				if (msg.arg1 == 422) { // allow recovery from previous failure
					String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "binding already exists", "createOrUpdateBindingToProperty");
					amlTestStatusMsg("Pass", "createOrUpdateBindingToProperty", passMsg);

					testSequencer.nextTest().execute();
//					amlTestDeleteAllBindings();
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateBindingToProperty");
					amlTestStatusMsg("Fail", "createOrUpdateBindingToProperty", errMsg);
				}
			}		
		}
	};

	private void amlTestCreateBindingToGroup() {
		AylaBindingZigbee binding = new AylaBindingZigbee();
		
		binding.bindingName = "RemoteControlToSmartPlugLightSocketBinding";
		binding.fromId = gblAmlTestRemoteControlNode.dsn;
		binding.fromName = gblAmlTestRemoteControlNodeProperty.name;
		binding.toId = gblAmlTestSmartPlugLightSocketGroup.getId().toString();
		binding.isGroup = true;
		
		gblAmlTestZigbeeGateway.createBinding(createBindingToGroup, binding, null, delayExecution);
	}

	private final Handler createBindingToGroup = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				gblAmlTestGroupBinding = AylaSystemUtils.gson.fromJson(jsonResults, AylaBindingZigbee.class);

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestGroupBinding.bindingName, "createBindingToGroup");
				amlTestStatusMsg("Pass", "createBindingToGroup", passMsg);

				testSequencer.nextTest().execute();
			}  else {	
				if (msg.arg1 == 422) { // allow recovery from previous failure
					String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "binding already exists", "createBindingToGroup");
					amlTestStatusMsg("Pass", "createBindingToGroup", passMsg);

					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createBindingToGroup");
					amlTestStatusMsg("Fail", "createBindingToGroup", errMsg);
				}
			}		
		}
	};

	private void amlTestGetBindings() {
		gblAmlTestZigbeeGateway.getBindings(getBindings, null, delayExecution);
	}

	private final Handler getBindings = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaBindingZigbee[] newBindings = AylaSystemUtils.gson.fromJson(jsonResults, AylaBindingZigbee[].class);

				if (newBindings.length == 1) {
					// Minimal check to see if the bindingName matches
					if (newBindings[0].bindingName.equals(gblAmlTestGroupBinding.bindingName)) {
						String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "bindingName", newBindings[0].bindingName, "getBindings");
						amlTestStatusMsg("Pass", "getBindings", passMsg);
	
						testSequencer.nextTest().execute();
					} else {
						String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected bindingName", newBindings[0].bindingName, "getBindings");
						amlTestStatusMsg("Fail", "getBindings", errMsg);					
					}
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s", "F", "amlTest", "unexpected number of bindings", newBindings.length, "getBindings");
					amlTestStatusMsg("Fail", "getBindings", errMsg);										
				}

			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getBindings");
				amlTestStatusMsg("Fail", "getBindings", errMsg);
			}	
	
		}

	};

	private void amlTestGetBinding() {
		
		gblAmlTestZigbeeGateway.getBinding(getBinding, gblAmlTestPropertyBinding, null, delayExecution);
	}

	private final Handler getBinding = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaBindingZigbee newBinding = AylaSystemUtils.gson.fromJson(jsonResults, AylaBindingZigbee.class);
				
				// Minimal check to see if the bindingName matches
				if (newBinding.bindingName.equals(gblAmlTestPropertyBinding.bindingName)) {
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestPropertyBinding.bindingName, "getBinding");
					amlTestStatusMsg("Pass", "getBinding", passMsg);

					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected bindingName", newBinding.bindingName, "getBinding");
					amlTestStatusMsg("Fail", "getBinding", errMsg);					
				}
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getBinding");
				amlTestStatusMsg("Fail", "getBinding", errMsg);
			}		
		}
	};
	
	private void amlTestDeletePropertyBinding() {
		// <---- Set BREAK POINT here so the user has time to test the new property binding before it is deleted
		gblAmlTestZigbeeGateway.deleteBinding(deletePropertyBinding, gblAmlTestPropertyBinding, null, delayExecution);
	}

	private void amlTestDeleteGroupBinding() {
		// <---- Set BREAK POINT here so the user has time to test the new group binding before it is deleted
		gblAmlTestZigbeeGateway.deleteBinding(deleteGroupBinding, gblAmlTestGroupBinding, null, delayExecution);
	}

	private final Handler deletePropertyBinding = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteBinding");
				amlTestStatusMsg("Pass", "deleteBinding", passMsg);

				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteBinding");
				amlTestStatusMsg("Fail", "deleteBinding", errMsg);
			}		
		}
	};
	
	private final Handler deleteGroupBinding = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteBinding");
				amlTestStatusMsg("Pass", "deleteBinding", passMsg);

				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteBinding");
				amlTestStatusMsg("Fail", "deleteBinding", errMsg);
			}		
		}
	};
	
	// -------------- scenes unit tests -----------------

	// getAllScenes and then delete them
	private void amlTestDeleteAllScenes_ORIG() {
		Thread thread = new Thread(new Runnable() {
			public void run()
			{
				AylaSceneZigbee[] scenes;

				AylaRestService rs1 = gblAmlTestZigbeeGateway.getScenes(null);
				msg = rs1.execute();

				String jsonResults = (String)msg.obj;
				if (msg.what == AylaNetworks.AML_ERROR_OK) {
					scenes = AylaSystemUtils.gson.fromJson(jsonResults, AylaSceneZigbee[].class);
					int len = scenes.length;
					for (int i = 0; i < len; i++) {
						AylaRestService rs2 = gblAmlTestZigbeeGateway.deleteScene(scenes[i], null);
						msg = rs2.execute();
					}
					jsonResults = (String)msg.obj;

					if (msg.what == AylaNetworks.AML_ERROR_OK) {
						passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "joinStatus", jsonResults, "deleteAllScenes");
					} else {
						errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllScenes");
					}
				} else {
					errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllScenes");
				}
			}
		});
		thread.start();
		try {
			thread.join();		// Wait for thread to finish
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		if (msg.arg1 >= 200 && msg.arg1 <= 299) {
			amlTestStatusMsg("Pass", "DeleteAllScenes", passMsg);
		} else {
			amlTestStatusMsg("Fail", "DeleteAllScenes", errMsg);
		}

		if (!(AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.LAN_MODE_ONLY) 
				&& !(AMLUnitTestConfig.amlTestSuite == AMLUnitTestConfig.testSuite.GATEWAY_LAN_MODE_ONLY)) {
			testSequencer.nextTest().execute();
		}
	}

	// getAllScenes and then delete them
	private void amlTestDeleteAllScenes() {
		gblAmlTestZigbeeGateway.getScenes(deleteAllScenes, null);
	}

	private final Handler deleteAllScenes = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaSceneZigbee[] scenes = AylaSystemUtils.gson.fromJson(jsonResults, AylaSceneZigbee[].class);
				int len = scenes.length;
				for (int i = 0; i < len; i++) {
					gblAmlTestZigbeeGateway.deleteScene(deleteOneScene, scenes[i], null);
				}
				
				new CountDownTimer(3000, 1000) {
					@Override
					public void onFinish() {
						testSequencer.nextTest().execute();
					}
					@Override
					public void onTick(long millisUntilFinished) {
					}
				}.start();
			} else {
				errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteAllScenes");
			}
		}
	};

	private final Handler deleteOneScene = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteScene");
				amlTestStatusMsg("Pass", "deleteScene", passMsg);
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteScene");
				amlTestStatusMsg("Fail", "deleteScene", errMsg);
			}		
		}
	};

	private final Handler sceneAck = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				if (msg.arg1 == 206) {		// Any failing nodes?
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "sceneAck");
					// BUG: This should be "Fail", but there is a node firmware bug with delete scene returning ZE_ZCB_INVALID_FIELD.
					// Deleting twice in a row works! :-)
					amlTestStatusMsg("Pass", "sceneAck", errMsg);
				} else {
					String passMsg = String.format("%s, %s, %s", "P", "amlTest", "sceneAck");
					amlTestStatusMsg("Pass", "sceneAck", passMsg);
				}		
				new CountDownTimer(5000, 1000) {
					@Override
					public void onFinish() {
						testSequencer.nextTest().execute();
					}
					@Override
					public void onTick(long millisUntilFinished) {
					}
				}.start();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "sceneAck");
				amlTestStatusMsg("Fail", "sceneAck", errMsg);
			}		
		}
	};

    private void amlTestCreateNodeScene() {
		AylaSceneZigbee scene = new AylaSceneZigbee();
		//scene.gatewayDsn = gblAmlTestZigbeeGateway.dsn;	// uses owner gateway on create
		scene.sceneName = "SmartPlugNodeScene";
		scene.nodeDsns = new String[1];
		scene.nodeDsns[0] = AMLUnitTestConfig.gblAmlTestSmartPlugNodeDsn;
		
		gblAmlTestZigbeeGateway.createScene(createScene, scene, null, delayExecution);
	}

	private void amlTestUpdateNodeScene() {
		gblAmlTestSmartPlugLightSocketNodeScene.sceneName = "SmartPlugLightSocketNodeScene";
		gblAmlTestSmartPlugLightSocketNodeScene.nodeDsns = new String[2];
		gblAmlTestSmartPlugLightSocketNodeScene.nodeDsns[0] = AMLUnitTestConfig.gblAmlTestSmartPlugNodeDsn;
		gblAmlTestSmartPlugLightSocketNodeScene.nodeDsns[1] = AMLUnitTestConfig.gblAmlTestLightSocketNodeDsn;
		
		gblAmlTestZigbeeGateway.updateScene(createScene, gblAmlTestSmartPlugLightSocketNodeScene, null, delayExecution);
//		gblAmlTestSmartPlugLightSocketNodeScene.update(createScene, null, delayExecution);
	}

	private void amlTestCreateGroupScene() {
		AylaSceneZigbee scene = new AylaSceneZigbee();
		//scene.gatewayDsn = gblAmlTestZigbeeGateway.dsn;	// uses owner gateway on create
		scene.sceneName = "SmartPlugLightSocketGroupScene";
		scene.groups = new Integer[1];
		scene.groups[0] = gblAmlTestSmartPlugLightSocketGroup.getId();
		
		gblAmlTestZigbeeGateway.createScene(createScene, scene, null, delayExecution);
	}

	private final Handler createScene = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaSceneZigbee scene = AylaSystemUtils.gson.fromJson(jsonResults, AylaSceneZigbee.class);
				if (scene.sceneName.equals("SmartPlugNodeScene") || scene.sceneName.equals("SmartPlugLightSocketNodeScene")) {
					gblAmlTestSmartPlugLightSocketNodeScene = scene;
				} else if (scene.sceneName.equals("SmartPlugLightSocketGroupScene")) {
					gblAmlTestSmartPlugLightSocketGroupScene = scene;
				}

				String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", scene.sceneName, "createOrUpdateScene");
				amlTestStatusMsg("Pass", "createOrUpdateScene", passMsg);

				testSequencer.nextTest().execute();
			}  else {	
				if (msg.arg1 == 422) { // allow recovery from previous failure
					String passMsg = String.format("%s, %s, %s:%s, %s", "W", "amlTest", "warning", "scene already exists", "createOrUpdateScene");
					amlTestStatusMsg("Pass", "createOrUpdateScene", passMsg);

					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "createOrUpdateScene");
					amlTestStatusMsg("Fail", "createOrUpdateScene", errMsg);
				}
			}		
		}
	};

	private void amlTestGetScenes() {
		// optional call parameters
		Map<String, Object> callParams = new HashMap<String, Object>();
		callParams.put(AylaNetworksZigbee.kAylaZigbeeNodeParamDetail, "false");		// default is "false"
		//callParams.put(AylaNetworksZigbee.kAylaZigbeeNodeParamStatusFilter, "all");	// default is "active"
		
		gblAmlTestZigbeeGateway.getScenes(getScenes, callParams, delayExecution);
	}

	private final Handler getScenes = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaSceneZigbee[] newScenes = AylaSystemUtils.gson.fromJson(jsonResults, AylaSceneZigbee[].class);

				if (newScenes.length == 1) {
					// Minimal check to see if the sceneName matches
					if (newScenes[0].sceneName.contains(gblAmlTestSmartPlugLightSocketNodeScene.sceneName)) {
						String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", newScenes[0].sceneName, "getScenes");
						amlTestStatusMsg("Pass", "getScenes", passMsg);
	
						testSequencer.nextTest().execute();
					} else {
						String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected sceneName", newScenes[0].sceneName, "getScenes");
						amlTestStatusMsg("Fail", "getScenes", errMsg);					
					}
				} else {
					String errMsg = String.format("%s, %s, %s:%d, %s", "F", "amlTest", "unexpected number of scenes", newScenes.length, "getScenes");
					amlTestStatusMsg("Fail", "getScenes", errMsg);										
				}

			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getScenes");
				amlTestStatusMsg("Fail", "getScenes", errMsg);
			}	
	
		}

	};

	private void amlTestGetScene() {
		
		gblAmlTestZigbeeGateway.getScene(getScene, gblAmlTestSmartPlugLightSocketGroupScene, null, delayExecution);
	}

	private final Handler getScene = new Handler() {
		public void handleMessage(Message msg) {
			String jsonResults = (String)msg.obj;
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				AylaSceneZigbee newScene = AylaSystemUtils.gson.fromJson(jsonResults, AylaSceneZigbee.class);
				
				// Minimal check to see if the sceneName matches
				if (newScene.sceneName.equals(gblAmlTestSmartPlugLightSocketGroupScene.sceneName)) {
					String passMsg = String.format("%s, %s, %s:%s, %s", "P", "amlTest", "name", gblAmlTestSmartPlugLightSocketGroupScene.sceneName, "getScene");
					amlTestStatusMsg("Pass", "getScene", passMsg);

					testSequencer.nextTest().execute();
				} else {
					String errMsg = String.format("%s, %s, %s:%s, %s", "F", "amlTest", "unexpected sceneName", newScene.sceneName, "getScene");
					amlTestStatusMsg("Fail", "getScene", errMsg);					
				}
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "getScene");
				amlTestStatusMsg("Fail", "getScene", errMsg);
			}		
		}
	};
	
	private void amlTestDeleteNodeScene() {
		AylaSceneZigbee scene = new AylaSceneZigbee();
		//scene.id = gblAmlTestSmartPlugLightSocketScene.id;

		gblAmlTestZigbeeGateway.deleteScene(deleteNodeScene, gblAmlTestSmartPlugLightSocketNodeScene, null, delayExecution);
	}

	private void amlTestDeleteGroupScene() {
		AylaSceneZigbee scene = new AylaSceneZigbee();
		//scene.id = gblAmlTestSmartPlugLightSocketScene.id;

		gblAmlTestZigbeeGateway.deleteScene(deleteGroupScene, gblAmlTestSmartPlugLightSocketGroupScene, null, delayExecution);
	}

	private final Handler deleteNodeScene = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteScene");
				amlTestStatusMsg("Pass", "deleteScene", passMsg);

//				gblAmlSceneAck = new SceneAck(sceneAck, gblAmlTestZigbeeGateway, gblAmlTestSmartPlugLightSocketNodeScene, true);
//				gblAmlSceneAck.waitForScenesAck();
				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteScene");
				amlTestStatusMsg("Fail", "deleteScene", errMsg);
			}		
		}
	};
	
	private final Handler deleteGroupScene = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "deleteScene");
				amlTestStatusMsg("Pass", "deleteScene", passMsg);

				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "deleteScene");
				amlTestStatusMsg("Fail", "deleteScene", errMsg);
			}		
		}
	};
	
	private void amlTestRecallNodeScene() {
		// <---- Set BREAK POINT here so the user has time to change node values before the node scene is recalled
		AylaSceneZigbee scene = new AylaSceneZigbee();
		//scene.id = gblAmlTestSmartPlugLightSocketScene.id;

		gblAmlTestZigbeeGateway.recallScene(recallNodeScene, gblAmlTestSmartPlugLightSocketNodeScene, null, delayExecution);
	}

	private final Handler recallNodeScene = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "recallScene");
				amlTestStatusMsg("Pass", "recallScene", passMsg);

//				gblAmlSceneAck = new SceneAck(sceneAck, gblAmlTestZigbeeGateway, gblAmlTestSmartPlugLightSocketNodeScene, false);
//				gblAmlSceneAck.waitForScenesAck();
				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "recallScene");
				amlTestStatusMsg("Fail", "recallScene", errMsg);
			}		
		}
	};
	
	private void amlTestRecallGroupScene() {
		// <---- Set BREAK POINT here so the user has time to change node values before the group scene is recalled
		AylaSceneZigbee scene = new AylaSceneZigbee();
		//scene.id = gblAmlTestSmartPlugLightSocketScene.id;

		gblAmlTestZigbeeGateway.recallScene(recallGroupScene, gblAmlTestSmartPlugLightSocketGroupScene, null, delayExecution);
	}

	private final Handler recallGroupScene = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == AylaNetworks.AML_ERROR_OK) {
				String passMsg = String.format("%s, %s, %s", "P", "amlTest", "recallScene");
				amlTestStatusMsg("Pass", "recallScene", passMsg);

				testSequencer.nextTest().execute();
			}  else {	
				String errMsg = String.format("%s, %s, %s:%d, %s, %s", "F", "amlTest", "error", msg.arg1, msg.obj, "recallScene");
				amlTestStatusMsg("Fail", "recallScene", errMsg);
			}		
		}
	};
	
	
	//-------------------------------------------------- End Unit Test ----------------------------------------------

	//----------------------------------- Status Message ------------------------------------
	private void amlTestStatusMsg(String passFail, String methodName, String methodMsg) {
		String testMsg = "";
		if (passFail.contains("Pass")) {
			//tv_test2.append(methodMsg + "\n");
			AylaSystemUtils.saveToLog("\n%s", methodMsg);
			testMsg = String.format("%s %s %d %s %d %s", methodName, "passed.", ++testsPassed, "of", numberOfTests, "passed.");
			if (!passFail.contains("Sync")) {
				tv_test2.append(testMsg + "\n\n");
			} else {
				Log.i(TAG, testMsg);
			}
		} else
			if (passFail.contains("Fail")){
				//tv_test3.append(methodMsg + "\n");
				AylaSystemUtils.saveToLog("\n%s", methodMsg);
				testMsg = String.format("%s %s %d %s %d %s",  methodName, "failed.", ++testsFailed, "of", numberOfTests, "failed.");
				if (!passFail.contains("Sync")) {
					tv_test3.append(testMsg + "\n\n");
				} else {
					Log.e(TAG, testMsg);
				}
			}
		AylaSystemUtils.saveToLog("%s", testMsg);


		if (testsFailed != 0) {
			testMsg = "Unit tests completed with errors";
			int passFailRate = ((++iterationsFailed*100)/++numberOfIterations);
			testMsg = String.format("%s, %s, %d %s %d %s:%d%s", "Unit test iteraton failed.", "F", iterationsFailed, "of", numberOfIterations, "failed", passFailRate, "%");
			tv_test3.append(testMsg + "\n");
			AylaSystemUtils.saveToLog("%s", testMsg);

			if (numberOfIterations < AMLUnitTestConfig.testIterations) {
				amlTestSelectTestSuite();
				// Keep things going.  We are on the "failure" path in the calling function, which does not have any more calls to nextTest().
				testSequencer.nextTest().execute();
			} else {
				iterationsFailed = iterationsPassed = numberOfIterations = 0;	// reset iteration counters
				testSequencer.endTestSuite();		// Make sure no more tests are executed
				AylaLanMode.pause(false);
			}
		} else {
			if ( (testsPassed + testsFailed) == numberOfTests) {
				if (testsPassed == numberOfTests) {
					int passFailRate = ((++iterationsPassed*100)/++numberOfIterations);
					tv_test1.setText("");
					tv_test2.setText("");
					long iterationEndTime = System.nanoTime();
					testMsg = String.format(Locale.getDefault(), "%s, %s, %d %s %d %s:%d%s %s %d %s",
							"Unit test iteration completed successfully", "P", iterationsPassed, "of", numberOfIterations, "passed", passFailRate, "%",
							"in", (iterationEndTime - iterationStartTime) / 1000000000L, "secs");
					if (numberOfIterations % 100 == 0) {
						tv_test3.setText("");
					}
					tv_test3.append(testMsg + "\n");
					AylaSystemUtils.saveToLog("%s", testMsg);
				}
				if (numberOfIterations < AMLUnitTestConfig.testIterations) {
					amlTestSelectTestSuite();
				} else {
					iterationsFailed = iterationsPassed = numberOfIterations = 0;	// reset iteration counters
					testSequencer.endTestSuite();		// Make sure no more tests are executed
					AylaLanMode.pause(false);
				}
			}

		}
	}


	@Override
	public void displayProgressInfo(final String msg) {
		tv_test1.append(msg);
	}


	@Override
	public void displayFailMessage(final String msg) {
		tv_test2.append(msg);
	}


	@Override
	public void displayPassMessage(final String msg) {
		tv_test3.append(msg);
	}

	@Override
	public void setTag(String tag) {
		// nothing for this class.
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}
		
}// end of AMLUnitTest class       

