//
//  AylaDatapoint.java
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 8/15/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//

package com.aylanetworks.aaml;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import org.json.JSONObject;

import com.aylanetworks.aaml.enums.CommandEntityBaseType;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;

import android.os.Handler;
import android.text.TextUtils;

class AylaDatapointContainer {
	@Expose
	public AylaDatapoint datapoint = null;
}

public class AylaDatapoint extends AylaSystemUtils {

	public static final String kAylaDataPointCount = "count";
	public static final String kAylaDataPointValue = "value";
	public static final String kAylaDataPointUpdatedAt = "updated_at";
	public static final String kAylaDataPointCreatedAt = "created_at";
	public static final String kAylaDataPointOwnerDSN = "owner_dsn";
	
	// properties from cloud service
	@Expose
	protected String createdAt;
	@Expose
	protected String updatedAt;
	@Expose 
	protected String value;
	@Expose
	private Number nValue;
	@Expose
	private String sValue;

	
	static int cmdId = 0;

	// getters and setters
	public String createdAt() {
		return createdAt;
	}
	public void createdAt(String _createdAt) {
		this.createdAt = _createdAt;
	}

	public String updatedAt() {
		return updatedAt;
	}
	public void updatedAt(String _updatedAt) {
		this.createdAt = _updatedAt;
	}
	
	public String value() {
		return value;
	}
	public void value(String _value) {
		this.value = _value;
	}

	public Number nValue() {
		return nValue;
	}
	public void nValue(Number _nValue) {
		this.nValue = _nValue;
	}

	public String sValue() {
		return sValue;
	}
	public void sValue(String _sValue) {
		this.sValue = _sValue;
	}


	@Override
	public String toString()
	{
		StringBuilder result = new StringBuilder();
		String NEW_LINE = System.getProperty("line.separator");

		result.append(this.getClass().getName() + " Object {" + NEW_LINE);
		result.append(" createdAt: " + createdAt + NEW_LINE);
		result.append(" nValue: " + nValue + NEW_LINE);
		result.append(" sValue: " + sValue + NEW_LINE );
		result.append("}");
		System.out.println(result);
		return result.toString();
	}

	public Number nValueFormatted(String baseType) {
		Number num = 0;
		try {
			if (this.value == null) {
				num = 0;
			} else if (TextUtils.equals(baseType, "integer")) {
				num = Integer.parseInt(this.value);
			} else if (TextUtils.equals(baseType, "string")) {
				num = 0;
			} else if (TextUtils.equals(baseType, "boolean")) { // checks TBD
				num = Integer.parseInt(this.value);
			} else if (TextUtils.equals(baseType, "decimal")) {
				num = Double.parseDouble(this.value);  // formating TBD
			} else if (TextUtils.equals(baseType, "float")) {
				num = Double.parseDouble(this.value);
			} else if (TextUtils.equals(baseType, "stream")||TextUtils.equals(baseType, "file")) {
				// Not Applicable.
			} else {
				if (!AML_LANMODE_IGNORE_BASETYPES.contains(baseType)) {// skip unnecessary base types
					AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "E", "Datapoints", "baseType", baseType, "error", "unsupported basetype", "getnValue");
				}
			}
		} catch (NumberFormatException nx) {
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "E", "Datapoints", kAylaDataPointValue, this.value, "exception", nx, "getnValue - Number format exception");
		}
		return num;
	}

	public String sValueFormatted(String baseType) {
		String valueStr = "";
		try {
			if (TextUtils.equals(baseType, "integer")) {
				valueStr = this.value;
			} else if (TextUtils.equals(baseType, "string")) {
				valueStr = this.value;
			} else if (TextUtils.equals(baseType, "boolean")) {               
				valueStr = this.value;
			} else if (TextUtils.equals(baseType, "decimal")) {
				valueStr = this.value; 					
			} else if (TextUtils.equals(baseType, "float")) {
				valueStr = this.value;
			} else if (TextUtils.equals(baseType, "stream")||TextUtils.equals(baseType, "file")) {
				valueStr = this.value;
			} else {
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "Datapoints", "baseType", baseType, "getsValue - unsupported base type");
			}
		} catch (NumberFormatException nx) {
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "E", "Datapoints", kAylaDataPointValue, this.value, "exception", nx, "getsValue - Number format exception");
		}	
		return valueStr;
	}

	
	/**
	 * Upon successful completion this instance method will post the value to the Ayla device service and instantiate a new datapoint object.
	 * 
	 * @param mHandle is where result would be returned.
	 * @param property is the propery that created datapoint would bind to.
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event.
	 * @return AylaRestService object
	 */
	public AylaRestService createDatapoint(AylaProperty property) {
		return createDatapoint(null, property, true);
	}
	public AylaRestService createDatapoint(Handler mHandle, AylaProperty property) {
		return createDatapoint(mHandle, property, false);
	}
	public AylaRestService createDatapoint(Handler mHandle, AylaProperty property, Boolean delayExecution) {
		Number propKey = property.getKey().intValue(); // Handle gson LazilyParsedNumber
		AylaRestService rs = null;
		String jsonDatapointContainer = "";
		String jsonDatapoint = "";
		this.convertValueToType(property);

		AylaDevice endpoint = AylaLanMode.device!=null? AylaLanMode.device.lanModeEdptFromDsn(property.owner): null;
		// create the datapoint on the device
		if (endpoint != null && endpoint.isLanModeActive()) {
			
			rs = new AylaRestService(mHandle, "createDatapointLanmode", AylaRestService.CREATE_DATAPOINT_LANMODE); // v2.30
		
			String propertyName = endpoint.lanModePropertyNameFromEdptPropertyName(property.name);
			if(propertyName == null) {
				saveToLog("%s, %s, %s:%s, %s", "E", "Datapoints", "LanMode", "PropertyNameCanNotBeTranslated", "createDatapoint_lanmode");
				returnToMainActivity(rs, jsonDatapoint, 400, 0);
				return rs;
			}
			
			this.createdAt = gmtFmt.format(new Date());
			this.updatedAt = this.createdAt;
			
			endpoint.property = endpoint.findProperty(property.name);
			
			if (endpoint.property != null) {
				this.lanModeEnable(property);
								
				cmdId = AylaLanMode.nextCommandOutstandingId();
				String toDeviceValue = endpoint.lanModeToDeviceUpdate(rs, endpoint.property, this.value, cmdId);
				if(toDeviceValue == null) {
					saveToLog("%s, %s, %s:%s, %s", "E", "Datapoints", "LanMode", "toDeviceValue null", "createDatapoint_lanmode");
					returnToMainActivity(rs, jsonDatapoint, 404, 0);
					return rs;
				}
				
				String lanPropertyName = endpoint.lanModePropertyNameFromEdptPropertyName(property.name);
				AylaProperty lanProperty = AylaLanMode.device.findProperty(lanPropertyName);
				
				if(lanProperty != null) {
//					jsonDatapointContainer = this.sendToLanModeDevice(rs, lanProperty, toDeviceValue, cmdId);
					jsonDatapointContainer = this.sendToLanModeDevice(rs, lanProperty, toDeviceValue, AML_COMMAND_ID_NOT_USED);
					if (!TextUtils.isEmpty(jsonDatapointContainer)) {
						try {
							jsonDatapoint = stripContainer(jsonDatapointContainer);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					saveToLog("%s, %s, %s:%s, %s", "I", "Datapoints", "mode", "LanMode", "createDatapoint");
					returnToMainActivity(rs, jsonDatapoint, 200, 0);
				}
				else {
					saveToLog("%s, %s, %s:%s, %s", "E", "Datapoints", "LanModeProperty", "PropertyNotFound", "createDatapoint_lanmode");
					returnToMainActivity(rs, jsonDatapoint, 404, 0);
				}
			} else {
				saveToLog("%s, %s, %s:%s, %s", "E", "Datapoints", "LanModeEdpt", "PropertyNotFound", "createDatapoint_lanmode");
				returnToMainActivity(rs, jsonDatapoint, 404, 0);
			}
		}
		// create the datapoint on the service
		else if (AylaReachability.isCloudServiceAvailable()) {
			//  params = {"datapoint":{"value":1}};
			
			final JsonObject datapoint = new JsonObject();
			jsonDatapoint = jsonDatapoint + "{\"datapoint\":";
			datapoint.addProperty(kAylaDataPointValue, this.value);
			jsonDatapoint = jsonDatapoint + datapoint.toString();
			jsonDatapoint = jsonDatapoint + "}";
			
			String url = String.format(Locale.getDefault(), "%s%s%d%s", deviceServiceBaseURL(), "properties/", propKey, "/datapoints.json");
			rs = new AylaRestService(mHandle, url, AylaRestService.CREATE_DATAPOINT);
			rs.setEntity(jsonDatapoint);
			saveToLog("%s, %s, %s:%s, %s%s, %s", "I", "Datapoints", "url", url, "datapointJson", jsonDatapoint, "createDatapoint");
			if (delayExecution == false) {
				rs.execute(); 
			}
		}
		// device is unreachable v2.02_ENG
		else {
			saveToLog("%s, %s, %s:%s, %s", "E", "Datapoints", "LanMode", "PropertyNotFound", "createDatapoint_lanmode");
			rs = new AylaRestService(mHandle, "createDatapointLanmode", AylaRestService.CREATE_DATAPOINT_LANMODE);
			jsonDatapoint = AylaSystemUtils.gson.toJson(property.datapoint,AylaDatapoint.class); // return original value
			returnToMainActivity(rs, jsonDatapoint, AML_ERROR_UNREACHABLE, 0);
		}
		return rs;
	}

	protected void convertValueToType(AylaProperty property) {
		// convert datapoint sValue and nValue to value
		String baseType = property.baseType();
		if (TextUtils.equals(baseType, "integer")) {
			this.value = this.nValue().toString();
		} else if (TextUtils.equals(baseType, "string")) {
			this.value = this.sValue();
		} else if (TextUtils.equals(baseType, "boolean")) {	// checks TBD
			this.value = this.nValue().toString();
		} else if (TextUtils.equals(baseType, "decimal")) {
			DecimalFormat myFormatter = new DecimalFormat("##0.00");
			String decString = myFormatter.format(this.nValue().doubleValue());
			this.value = decString;
		} else if (TextUtils.equals(baseType, "float")) {
			this.value = this.nValue().toString();
		} else if (TextUtils.equals(baseType, "stream") || TextUtils.equals(baseType, "file"))  {
			this.value = this.sValue();
		}
		else {
			saveToLog("%s, %s, %s:%s, %s", "E", "Datapoints", "baseType", baseType, "createDatapoint:unsupported base type");
		}
	}

	protected static String stripContainer(String jsonDatapointContainer) throws Exception {
		String jsonDatapoint = "";
		try {
			AylaDatapointContainer datapointContainer = AylaSystemUtils.gson.fromJson(jsonDatapointContainer,AylaDatapointContainer.class);
			AylaDatapoint datapoint = datapointContainer.datapoint;
			jsonDatapoint = AylaSystemUtils.gson.toJson(datapoint,AylaDatapoint.class);
			AylaSystemUtils.saveToLog("%s %s %s:%s %s", "I", "Datapoints", kAylaDataPointValue, datapoint.value, "stripContainer");
			return jsonDatapoint;
		} catch (Exception e) {
			AylaSystemUtils.saveToLog("%s %s %s:%s %s", "E", "Datapoints", "jsonDatapointContainer", jsonDatapointContainer, "stripContainer");
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * This instance method returns datapoints for a given property. getDatapointsByActivity returns datapoints in the order they were created.
	 * @param mHandle is where result would be returned.
	 * @param property is the property that retrieved datapoints should bind to.
	 * @param callParams is applied to qualify the datapoints returned and the maximum number of datapoints returned per query will be limited to maxCount in AylaSystemUtils.
	 * @param delayExecution could be set to true if you want setup this call but have it execute on an external event.
	 * @return AylaRestService object
	 */
	public AylaRestService getDatapointsByActivity(AylaProperty property, Map<String, String> callParams) {
		return getDatapointsByActivity(null, property, callParams, true);
	}
	public AylaRestService getDatapointsByActivity(Handler mHandle, AylaProperty property, Map<String, String> callParams) {
		return getDatapointsByActivity(mHandle, property, callParams, false);
	}
	public AylaRestService getDatapointsByActivity(Handler mHandle, AylaProperty property, Map<String, String> callParams, Boolean delayExecution) {
		Number propKey = property.getKey().intValue(); // Handle gson LazilyParsedNumber
		int count = 1;
		if (callParams != null) {
			Object obj = callParams.get(kAylaDataPointCount); //HashMap hm = (HashMap) obj; System.out.println(hm);
			count = Integer.parseInt( (String)obj );

			// limit count to maxCount
			int maxCount = AylaSystemUtils.maxCount;
			count = (count <= maxCount) ? count : maxCount;
			count = (count >= 1) ? count : 1; // get at least one datapoint
		}
		AylaRestService rs = null;
		// device is reachable, have a device, not the first time, need one datapoint
		if ( (AylaReachability.isDeviceLanModeAvailable() && AylaLanMode.device != null && AylaLanMode.device.property != null) &&
			 (count == 1) &&
			 (!AML_LANMODE_IGNORE_BASETYPES.contains(property.baseType))
		    )
		{
			// GET a datapoint from the device for this property
			try {
				rs = new AylaRestService(mHandle, "getDatapointLanmode", AylaRestService.GET_DATAPOINT_LANMODE);
				
				String ownerDsn = callParams.get(kAylaDataPointOwnerDSN);
				AylaDevice dev = AylaLanMode.device.lanModeEdptFromDsn(ownerDsn);
				String cmdRequest = null;
				if (dev != null) {
					if (dev instanceof AylaDeviceNode) {
						cmdRequest = dev.lanModeToDeviceCmd(rs, "GET", "datapoint.json", property);
					} else { // Assuming there are only AylaDeviceNode and AylaDeviceGateway for now
						cmdRequest = this.getFromLanModeDevice(rs, property);
					}
				}
				saveToLog("%s, %s, %s:%s, %s", "I", "Datapoints", "cmdRequest", cmdRequest, "getDatapointByActivity_lanMode");
			} catch (Exception e) {
				AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "E", "Datapoints", "exception", e.getCause(), "getDatapointsByActivity_lanMode");
				e.printStackTrace();
			}
		} else { // get datapoints from the device service
			String url = String.format(Locale.getDefault(), "%s%s%d%s%s%s", deviceServiceBaseURL(), "properties/", propKey, "/datapoints.json", "?limit=", count);
			rs = new AylaRestService(mHandle, url, AylaRestService.GET_DATAPOINTS); 

			saveToLog("%s, %s, %s:%s, %s%s, %s", "I", "Datapoints", "url", url, kAylaDataPointCount, count, "getDatapointsByActivity");
			if (delayExecution == false) {
				rs.execute(); // execute request, datapoints returned to stripContainers
			}
		}
		return rs;
	}

	
	protected static String stripContainers(String jsonDatapointContainers) throws Exception {
		int count = 0;
		String jsonDatapoints = "";
		try {
			AylaDatapointContainer[] datapointContainers = AylaSystemUtils.gson.fromJson(jsonDatapointContainers,AylaDatapointContainer[].class);
			AylaDatapoint[] datapoints = new AylaDatapoint[datapointContainers.length];
			for (AylaDatapointContainer datapointContainer : datapointContainers) {
				datapoints[count++]= datapointContainer.datapoint;   			
			}
			jsonDatapoints = AylaSystemUtils.gson.toJson(datapoints,AylaDatapoint[].class);
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s", "I", "Datapoints", kAylaDataPointCount, count, "stripContainers");
			return jsonDatapoints;
		} catch (Exception e) {
			AylaSystemUtils.saveToLog("%s, %s, %s:%s, %s:%s, %s", "E", "Datapoints", kAylaDataPointCount, count, "jsonDatapointContainers", jsonDatapointContainers, "stripContainers");
			e.printStackTrace();
			throw e;
		}
	}
	
	// ----------------------------- Lan Mode Helper methods -------------------
	
	protected String getFromLanModeDevice(AylaRestService rs, AylaProperty property) {
		String jsonCommand = "";
		if (lanModeState != lanMode.DISABLED) {
						
			int cmdId = AylaLanMode.nextCommandOutstandingId(); // match up async response with request
			// build property for lan mode device
			
			jsonCommand = jsonCommand + "{\"cmds\":[";
			jsonCommand = jsonCommand + "{\"cmd\":";
			jsonCommand = jsonCommand + "{\"cmd_id\":" + cmdId + ",";
			jsonCommand = jsonCommand + "\"method\":" + "\"GET\"" + ",";
			jsonCommand = jsonCommand + "\"resource\":" + "\"" + "property.json?name=" + property.name() + "\"" + ",";
			jsonCommand = jsonCommand + "\"data\":" + "\"" + "none" + "\"" + ",";
			jsonCommand = jsonCommand + "\"uri\":" + "\"" + serverPath+"/property/datapoint.json" + "\"";
			jsonCommand = jsonCommand + "}}";
			jsonCommand = jsonCommand + "]}";

			AylaLanCommandEntity entity = new AylaLanCommandEntity(jsonCommand, cmdId, CommandEntityBaseType.AYLA_LAN_COMMAND);
			AylaLanMode.sendToLanModeDevice(entity, rs);
		}
		return jsonCommand;
	}
	
	private void lanModeEnable(AylaProperty property) {
		if (lanModeState != lanMode.DISABLED) {
			String dateStr = gmtFmt.format(new Date());
			// update the lan mode device with the datapoint info
			
			AylaDevice endpoint = AylaLanMode.device!=null? AylaLanMode.device.lanModeEdptFromDsn(property.owner): null;
			
			if (endpoint != null) {
				endpoint.property.value = this.value;
				endpoint.property.dataUpdatedAt = dateStr;
				endpoint.property.updateDatapointFromProperty();

				endpoint.property.datapoint = this; // v1.61
				if (endpoint.property.datapoints == null) {
					endpoint.property.datapoints = new AylaDatapoint[1];
				}
				endpoint.property.datapoints[0]= this;
			}
		}
	}
	
	private String sendToLanModeDevice(AylaRestService rs, AylaProperty property, String value, int cmdId) {
		String datapointJson = "";
		
		if (TextUtils.equals("file", property.baseType()) 
				|| TextUtils.equals("stream", property.baseType())) {
			// baseType not supported in lan Mode.
			return datapointJson;
		}
		
		if (lanModeState != lanMode.DISABLED) {
			// build property for lan mode device
			String jsonProperty = "";
			jsonProperty = jsonProperty + "{\"properties\":[";
			jsonProperty = jsonProperty + "{\"property\":{";
			jsonProperty = jsonProperty + "\"base_type\":" + "\"" + property.baseType + "\"" + ",";
			
			String thisValue = value;
			if (TextUtils.equals(property.baseType, "string")) {
				try {
					thisValue = JSONObject.quote(value);
				} catch(Exception e) {
					saveToLog("%s, %s, %s:%s, %s", "E", "AylaDatapoints", "JSON_Exception", e.getCause(), "sendToLanModeDevice");
				}
			} 
			jsonProperty = jsonProperty + "\"" + kAylaDataPointValue + "\":" + thisValue + ",";
			jsonProperty = jsonProperty + "\"name\":" + "\"" + property.name + "\"";
			jsonProperty = jsonProperty + "}}";
			jsonProperty = jsonProperty + "]}";

			//Push to queue by calling AylaLanMode.sendToLanModeDevice
			AylaLanCommandEntity entity = new AylaLanCommandEntity(jsonProperty, cmdId, CommandEntityBaseType.AYLA_LAN_PROPERTY);
			AylaLanMode.sendToLanModeDevice(entity, rs);
//			AylaLanMode.sendToLanModeDevice(jsonProperty, AML_COMMAND_ID_NOT_USED, rs);

			//{
			//	"datapoint":{
			//	"created_at":"2011-11-15T06:22:44Z",
			//	"updated_at":"2011-11-15T06:22:44Z",
			//	"value":0
			//	}
			//}
			datapointJson = 			"{\"datapoint\":{";
			datapointJson = datapointJson + "\"" + kAylaDataPointCreatedAt + "\":" + "\"" + this.createdAt + "\",";
			datapointJson = datapointJson + "\"" + kAylaDataPointUpdatedAt + "\":" + "\"" + this.updatedAt + "\",";
			datapointJson = datapointJson + "\"" + kAylaDataPointValue + "\":" + thisValue ;
			datapointJson = datapointJson + "}}";
		}
		
		return datapointJson;
	}
	
	// TODO: Move to some common utils class    
	public static void returnToMainActivity(AylaRestService rs, String thisJsonResults, int thisResponseCode, int thisSubTaskId) {
		rs.jsonResults = thisJsonResults;
		rs.responseCode = thisResponseCode;
		rs.subTaskFailed = thisSubTaskId;
		
		rs.execute();
	}
}





