//
//  AylaTimer.java
//  Ayla Mobile Library
//
//  Created by Daniel Myers on 11/16/12.
//  Copyright (c) 2012 Ayla Networks. All rights reserved.
//
package com.aylanetworks.aaml;

import android.os.Handler;

class AylaTimer { 
	private int _interval; 
	private Handler handler; 
	private Runnable _tickHandler; 
	private Runnable delegate; 
	private Boolean ticking; 
	
	int getInterval() { return _interval; }
	
	Boolean getIsTicking() { return ticking; } 

	AylaTimer(int interval) { 
		_interval = interval; 
		handler = new Handler(); 
	} 

	AylaTimer(int interval, Runnable onTickHandler) { 
		_interval = interval; 
		setOnTickHandler(onTickHandler); 
		handler = new Handler(); 
	} 

	void start(int interval, Runnable onTickHandler) { 
		if (ticking) return; 
		_interval = interval; 
		setOnTickHandler(onTickHandler); 
		handler.postDelayed(delegate, _interval); 
		ticking = true; 
	} 

	void start() { 
		if (ticking) return; 
		handler.postDelayed(delegate, _interval); 
		ticking = true; 
	} 

	void stop() {
		handler.removeCallbacks(delegate); 
		ticking = false; 
	} 

	void setInterval(int delay) {
		_interval = delay;
		stop();
		start();
	} 
	
	void setOnTickHandler(Runnable onTickHandler) { 
		if (onTickHandler == null) return;
		
		_tickHandler = onTickHandler; 
		delegate = new Runnable() { 
			public void run() { 
				if (_tickHandler == null) return; 
				_tickHandler.run();
				if (ticking) {
					handler.postDelayed(delegate, _interval);
				}
			} 
		}; 
	} 
}





