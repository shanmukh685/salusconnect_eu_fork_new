"use strict";

define([
	"app",
	"bluebird",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/views/modal/ModalCloseButton.view",
	"consumer/views/SalusButtonPrimary.view",
	"common/model/salusWebServices/rules/Rule.model",
	"consumer/equipment/views/EquipmentCollection.view"
], function (App, P, constants, templates) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {
		Views.EquipmentListModal = Mn.LayoutView.extend({
			template: templates["oneTouch/equipmentListModal"],
			className: "equipment-list-modal modal-body",
			ui: {
				headerText: ".header-text"
			},
			regions: {
				closeButton: ".bb-btn-close",
				applyButton: ".bb-btn-apply",
				equipmentCollectionRegion: ".bb-equipment-coll-region"
			},
			initialize: function () {
				_.bindAll(this, "handleFinishedClick");

				this.deviceType = this.options.deviceType;
				this.rule = this.options.rule;

				var that = this,
						currDevices = new B.Collection();

				currDevices.add(App.salusConnector.getDeviceCollection().filter(function (device) {
                    
                    //必须在线
                    if(device.get("LeaveNetwork") && device.get("OnlineStatus_i") && (device.get("connection_status")!="Offline")){
                        if(device.getPropertyValue("LeaveNetwork")!=0){
                            return false;
                        }
                        if(device.getPropertyValue("OnlineStatus_i")!=1){
                            return false;
                        }
                    }else{
                        //没有那两个属性应该就是不在线的
                        return false;
                    }
                    
					return (that.deviceType === constants.modelTypes.SMARTPLUG ||
					that.deviceType === constants.modelTypes.MINISMARTPLUG) ? device.isSmartPlug() : that.deviceType === device.modelType;
				}));

				this.equipmentCollectionView = new App.Consumer.Equipment.Views.EquipmentCollectionView({
					collection: currDevices,
                    single: false
				});

				this.registerRegion("closeButton",  new App.Consumer.Views.ModalCloseButton({
					id: "close-equip-btn",
					buttonTextKey: "common.labels.cancel",
					classes: "width100"
				}));

				this.registerRegion("applyButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "finished-equip-btn",
					buttonTextKey: "common.labels.apply",
					className: "btn btn-primary",
					classes: "width100",
					clickedDelegate: this.handleFinishedClick
				}));
			},
			onRender: function () {
				var ruleName = App.translate(this.rule.get("name"));

				this.ui.headerText.text(App.translate("equipment.oneTouch.recommended.modal.titleFormatted", {
					ruleName: ruleName
				}));

				this.equipmentCollectionRegion.show(this.equipmentCollectionView);
			},
			handleFinishedClick: function () {
				var checkedItemKeys = this.equipmentCollectionView.getCheckedItemKeys(),
						that = this, ruleModel,pinToDashboardList=[];

				this.applyButton.currentView.showSpinner();
                
                if(this.savingStatus) {
                    return;
                } else {
                    this.savingStatus = true;
                }

				return P.all(checkedItemKeys.map(function(itemKey) {
					// make new JSON object with stringify and parse
					var recommendedJSON = JSON.parse(JSON.stringify(that.rule)),
							device = App.salusConnector.getDevice(itemKey);

					return device.getDevicePropertiesPromise().then(function () {
						ruleModel = new App.Models.RuleModel({dsn: App.getCurrentGatewayDSN()});

						// Moment object doesn't survive the JSON stringify+parse, so grab and set it if it exists.
						if (that.rule.get("conditions").length > 0) {
							var startTime = that.rule.get("conditions")[0].startTime;
							var endTime = that.rule.get("conditions")[0].endTime;
							recommendedJSON.conditions[0].startTime = startTime;
							recommendedJSON.conditions[0].endTime = endTime;
						}

						ruleModel.buildRuleFromRecommended(recommendedJSON, device);
                        
                        pinToDashboardList.push(ruleModel);

						return ruleModel.add();
					});
				})).then(function () {
                    
                    _.map(pinToDashboardList,function (o){
                        o.pinToDashboard();
                    });
                    
					that.applyButton.currentView.hideSpinner();
					App.hideModal();
					App.salusConnector.addToRuleCollection(ruleModel);
					that.trigger("deselect:rules");
				}).catch(function (err) {
					that.applyButton.currentView.hideSpinner();

					return P.reject(err);
				});
			}
		}).mixin([App.Consumer.Views.Mixins.RegisteredRegions, App.Consumer.Views.Mixins.SalusView]);
	});

	return App.Consumer.OneTouch.Views.EquipmentListModal;
});