"use strict";

define([
    "app",
    "common/constants",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusPage",
    "consumer/views/Breadcrumbs.view",
    "consumer/equipment/views/EquipmentTileList.view"
], function (App, constants, consumerTemplates, SalusPageMixin, BreadcrumbsView) {

    App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
        Views.GroupPageView = Mn.LayoutView.extend({
            id: "bb-group-page",
            className: "group-page container",
            template: consumerTemplates["equipment/myEquipment/groupPage"],
            regions: {
                breadcrumbs: ".bb-breadcrumbs",
                equipmentList: ".bb-equipment-list"
            },
            ui: {
                editIcon: ".bb-edit-icon"
            },
            events: {
                "click .bb-edit-icon": "editGroup"
            },
            initialize: function (options) {
                this.modelId = parseInt(options.key);
                this.model = App.salusConnector.getGroup(this.modelId);
            },
            onRender: function () {
                var that = this;

                App.salusConnector.getDataLoadPromise(["devices", "groups"]).then(function () {
                    if (!that.model) {
                        that.model = App.salusConnector.getGroup(that.modelId);

                        if (!that.model) {
                            // get out
                            App.navigate("equipment");
                            return;
                        }
                    }

                    that.breadcrumbs.show(new BreadcrumbsView.BreadcrumbsView({
                        crumbs: [
                            {
                                textKey: "equipment.myEquipment.title",
                                href: "/equipment"
                            },
                            {
                                text: that.model.get("name"),
                                active: true
                            }
                        ]
                    }));

                    var equipmentInGroup = _.map(that.model.get("devices"), function (deviceId) {
                        return App.salusConnector.getDevice(deviceId);
                    });

                    //删除设备时,判断到group只有当前设备的话,就会将设备删除,所以此处要加个判断,否则删除设备后跳回group会显示异常
                    if (equipmentInGroup.length && equipmentInGroup[0]) {
                        that.equipmentList.show(new Views.EquipmentTileListView({
                            collection: new B.Collection(equipmentInGroup),
                            type: constants.equipmentLists.group,
                            groupId: that.modelId
                        }));
                    }else{
                        App.navigate("equipment");
                        return;
                    }
                });
            },
            editGroup: function () {
                App.navigate("equipment/editGroup/" + this.modelId);
            }
        }).mixin([SalusPageMixin], {
            analyticsSection: "equipment",
            analyticsPage: "group" //TODO: This should be a function so the group Id is recorded.
        });
    });

    return App.Consumer.Equipment.Views.GroupPageView;
});