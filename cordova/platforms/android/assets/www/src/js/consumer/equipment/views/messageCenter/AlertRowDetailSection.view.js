"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusCollapsableView",
	"consumer/equipment/views/messageCenter/AlertDetailSectionComposites.view"
], function (App, templates, SalusCollapsableViewMixin) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn) {
		Views.AlertRowDetailView = Mn.LayoutView.extend({
			template: templates['equipment/messageCenter/alertRowDetailSection'],
			className: "col-xs-12 expanded-detail-content-region",

			regions: {
				"detailRegion": ".bb-detail-region",
				"statusRegion": ".bb-status-region",
				"criteriaRegion": ".bb-criteria-region"
			},

			initialize: function () {
				this.statusView = new Views.AlertDetailCompositeTable({ collection: new B.Collection(this.model.get('status')), tableType: "status" });
				this.detailView = new Views.AlertDetailCompositeTable({ collection: new B.Collection(this.model.get('details')), tableType: "detail" });
				this.criteriaView = new Views.CriteriaTableRow({model: this.model});
			},

			onRender: function () {
				this.statusRegion.show(this.statusView);
				this.detailRegion.show(this.detailView);
				this.criteriaRegion.show(this.criteriaView);
			}
		}).mixin([SalusCollapsableViewMixin]);
	});
});