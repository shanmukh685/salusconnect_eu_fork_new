"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions",
	"consumer/settings/views/EmptyList.view",
	"consumer/settings/views/DeviceListRow.view",
	"consumer/settings/views/DeviceForm.view"
], function (App, consumerTemplates, SalusView, RegisteredRegions) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
		Views.DeviceListView = Mn.CollectionView.extend({
			tagName: "ul",
			className: "list-unstyled list-table",
			emptyView: Views.EmptyListView,
			emptyViewOptions: {
				i18nTextKey: "settings.manageDevices.devices.empty"
			},
			childView: Views.DeviceListRowView,

			onBeforeRender: function () {
				this.usersShown = [];
			},

			// filter out shares with emails already displayed
			addChild: function(child){
				if (this.mode === "create" || !_.contains(this.usersShown, child.get("user_email"))) {
					this.usersShown.push(child.get("user_email"));
					Mn.CollectionView.prototype.addChild.apply(this, arguments);
				}
			}
		}).mixin([SalusView, RegisteredRegions], {});
	});

	return  App.Consumer.Settings.Views.DeviceListView;
});
