"use strict";

define([
	"app",
	"bluebird",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/Breadcrumbs.view",
	"consumer/myStatus/views/MyStatusNameEdit.view",
	"consumer/views/SalusLinkButton.view",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusCheckbox.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusAlertModal.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/modal/ModalCloseButton.view",
	"consumer/models/SalusAlertModalModel"
], function (App, P, config, consumerTemplates, SalusViewMixin, BreadcrumbsView,
			 MyStatusNameEdit, SalusLinkButton, SalusPage) {

	App.module("Consumer.MyStatus.Views", function (Views, App, B, Mn, $, _) {
		Views.MyStatusRule = Mn.ItemView.extend({
			template: consumerTemplates["myStatus/statusRule"],
			ui: {
				"ruleCheckbox": ".bb-rule-checkbox",
				"runWhenStart": ".bb-run-when-start",
				"switch": ".bb-switch"
			},
			events: {
				"click .bb-arrow": "_arrowClicked",
				"click .bb-switch": "_runSwitchClicked"
			},

			initialize: function (options) {
				_.bindAll(this, "_changeActiveState", "_runSwitchClicked");

				this.run = false;
				this.ruleGroup = options.ruleGroup;

				this.ruleCheckboxView = new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						id: "rule-checkbox",
						nonKeyLabel: this.model.get("name"),
						secondaryIconClass: "",
						isChecked: false
					})
				});

				this.listenTo(this.ruleCheckboxView, "clicked:checkbox", this._changeActiveState);
			},

			onRender: function () {
				if (this.ruleGroup && this.ruleGroup.hasRule(this.model)) {
					this.ruleCheckboxView.model.set("isChecked", true);
					this.ui.ruleCheckbox.removeClass("inactive");
					this.ui.ruleCheckbox.addClass("active");
					//this.ui.runWhenStart.removeClass("hidden");
				}

				this.ui.ruleCheckbox.append(this.ruleCheckboxView.render().$el);
			},

			onDestroy: function () {
				this.ruleCheckboxView.destroy();
			},

			_changeActiveState: function () {
				this.markMyRuleDirty();

				if (this.ruleCheckboxView.getIsChecked()) {
					this.ui.ruleCheckbox.removeClass("inactive");
					this.ui.ruleCheckbox.addClass("active");
					//this.ui.runWhenStart.removeClass("hidden");
					this._addRule();
				} else {
					this.ui.ruleCheckbox.removeClass("active");
					this.ui.ruleCheckbox.addClass("inactive");
					//this.ui.runWhenStart.addClass("hidden");
					this._deleteRule();
				}
			},

			_addRule: function () {
				var key = this.model.get("key");

				if (!this.ruleGroup.get("rules")) {
					this.ruleGroup.set("rules", []);
				}

				if (!this.ruleGroup.hasRule(key)) {
					this.ruleGroup.get("rules").push({key: key});
				}
			},

			_getIndex: function (key) {
				return _.findIndex(this.ruleGroup.get("rules"), {key: key});
			},

			_deleteRule: function () {
				var key = this.model.get("key"),
					index = this._getIndex(key);

				if (index >= 0) {
					this.ruleGroup.get("rules").splice(index, 1);
				}
			},

			markMyRuleDirty: function () {
				App.salusConnector.getRule(this.model.get("key")).markAsDirty();
			},

			_runSwitchClicked: function () {
				this.run = !this.run;

				this.ui.switch.toggleClass("switch-yes", this.run);
				this.ui.switch.toggleClass("switch-no", !this.run);

				//TODO implement on data
				this._notImplementedAlert();
			},

			_arrowClicked: function () {
				App.navigate("oneTouch/" + App.getCurrentGatewayDSN() + "/" + encodeURIComponent(this.model.get("key")));
			}
		}).mixin([SalusViewMixin]);

		Views.RuleCollectionView = Mn.CollectionView.extend({
			childView: Views.MyStatusRule,
			childViewOptions: function () {
				return {
					ruleGroup: this.options.ruleGroup
				};
			}
		}).mixin([SalusViewMixin]);

		Views.MakeMyStatusPageView = Mn.LayoutView.extend({
			id: "make-myStatus-page",
			template: consumerTemplates["myStatus/makeStatus"],
			className: "container",
			regions: {
				breadcrumbs: ".bb-breadcrumbs",
				nameRegion: ".bb-edit-name",
				cancelButtonRegion: ".bb-cancel-button",
				saveButtonRegion: ".bb-save-button",
				deleteButtonRegion: ".bb-delete-button",
				ruleAreaRegion: ".bb-rule-area"
			},
			ui: {
				badGatewayText: ".bb-bad-gateway-text",
				deleteButton: ".bb-delete-button",
				ruleArea: ".bb-rule-area"
			},
			bindings: {
				".bb-title": {
					observe: "name"
				},
				".bb-myStatus-selected": {
					observe: "selected",
					update: function ($el, selected) {
						if (App.hasCurrentGateway()) {
							$el.toggleClass("selected", selected).toggleClass("deselected", !selected);
						} else {
							this.ui.badGatewayText.removeClass("hidden");
							$el.addClass("invalid").removeClass("selected").removeClass("deselected");
						}
					}
				},
				".bb-make-status-tile": {
					observe: "icon",
					update: function ($el, iconClass) {
						if (App.hasCurrentGateway()) {
							$el.addClass(iconClass);
						} else {
							$el.addClass("disable-status");
						}
					}
				}
			},
			events: {
				"click .bb-edit": "handleEditClicked",
				"click .bb-create-new-one-touch": "handleOneTouchClicked",
				"click .bb-make-status-tile": "handleStatusTileClicked"
			},

			initialize: function () {
				_.bindAll(this,
					"handleEditClicked",
					"handleOneTouchClicked",
					"handleCancelClicked",
					"handleSaveClicked",
					"handleStatusTileClicked",
					"handleDeleteClicked",
					"_setMyStatus"
				);

				this.model = App.Consumer.MyStatus.Controller.currentPendingRuleGroup;

				this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					classes: "col-xs-6",
					className: "width100 btn btn-default cancel-button",
					clickedDelegate: this.handleCancelClicked
				});

				this.saveButton = new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.save",
					classes: "width100",
					clickedDelegate: this.handleSaveClicked
				});

				this.deleteButton = new SalusLinkButton({
					buttonTextKey: "myStatus.makeNewStatus.delete",
					className: "delete btn btn-link margin-r-10",
					clickedDelegate: this.handleDeleteClicked
				});
			},

			onRender: function () {
				var that = this, newModels = [];

				if (!this.model) {
					App.navigate("myStatus");
					return;
				}

				App.salusConnector.getDataLoadPromise(["devices", "rules"]).then(function () {
					that.ruleCollection = App.salusConnector.getDisplayRuleCollection();

					if (!that.ruleCollection || !that.ruleCollection.isEmpty()) {
						that.ui.ruleArea.removeClass("hidden");
					}
                    
                    if (that.ruleCollection && that.ruleCollection.models) {
                        _.each(that.ruleCollection.models, function (m) {
                            if(((m.getStatusKeys() && m.getStatusKeys().length === 0) || that.model.hasRule(m)) && !m.uxhint) {
                                newModels.push(m);
                            }
                        });
                        that.ruleCollection.models = newModels;
                    }
                    
					that.ruleAreaRegion.show(new Views.RuleCollectionView({
						collection: that.ruleCollection,
						ruleGroup: that.model
					}));
				});

				if (this.model && this.model.get("key")) {
					this.ui.deleteButton.removeClass("hidden");
				}

				this.breadcrumbs.show(new BreadcrumbsView.BreadcrumbsView({
					crumbs: [
						{
							textKey: "myStatus.title",
							href: "/myStatus"
						},
						{
							text: this.model ? this.model.get("name") : "",
							active: true
						}
					]
				}));

				this.cancelButtonRegion.show(this.cancelButton);
				this.saveButtonRegion.show(this.saveButton);
				this.deleteButtonRegion.show(this.deleteButton);
			},

			handleEditClicked: function () {
				var nameView = new MyStatusNameEdit(this.model),
					that = this;

				this.listenTo(nameView, "click:close", function () {
					nameView.destroy();
				});

				this.listenTo(nameView, "click:save", function (childView) {
					that.model.set("name", childView.ruleGroup.get("name"));
					nameView.destroy();

					//show updated breadcrumbs view
					that.breadcrumbs.show(new BreadcrumbsView.BreadcrumbsView({
						crumbs: [
							{
								textKey: "myStatus.title",
								href: "/myStatus"
							},
							{
								text: that.model.get("name"),
								active: true
							}
						]
					}));
				});

				this.nameRegion.show(nameView);
			},

			handleOneTouchClicked: function () {
				var path = "oneTouch/" + App.getCurrentGatewayDSN() + "/create";

				App.navigate(path);
			},

			handleCancelClicked: function () {
				_.each(this.model.getDirtyRules(), function (rule) {
					rule.markAsClean();
				});

				window.history.back();
			},

			_setMyStatus: function () {
				if (this.model.get("selected")) {
					return App.salusConnector.setMyStatus(this.model.get("key"));
				} else {
                    //判断是否是取消选择
                    var status=App.salusConnector.getMyStatus();
                    if(status===this.model.get("key")){
                        return App.salusConnector.setMyStatus("NULL");
                    }
                }
			},

			handleSaveClicked: function () {
				var that = this, promise, isNew;
                
                if(this.savingStatus) {
                    return;
                } else {
                    this.savingStatus = true;
                }

				this.saveButton.showSpinner();

				if (_.isNull(this.model.get("key"))) {
					promise = this.model.add();
					isNew = true;
				} else {
					promise = this.model.update();
				}
                
				return promise.then(that._setMyStatus).then(function () {
					return App.salusConnector.getRuleGroupCollection().refresh().then(function () {
						if (!!isNew) {
							App.Consumer.MyStatus.Controller.newMyStatus = that.model.get("key");
						}

						that.saveButton.hideSpinner();

						App.navigate("myStatus");
					});
				}).catch(function (error) {
					that.saveButton.hideSpinner();
					return P.reject(error);
				});
			},

			handleStatusTileClicked: function () {
				var selected = this.model.get("selected");

				this.model.set("selected", !selected);
			},

			handleDeleteClicked: function () {
				var that = this;

				App.modalRegion.show(new App.Consumer.Views.SalusAlertModalView({
					model: new App.Consumer.Models.AlertModalViewModel({
						iconClass: "icon-warning",
						primaryLabelText: App.translate("myStatus.makeNewStatus.deleteWarning"),
						secondaryLabelText: App.translate("settings.profile.deleteProfileWarningLine2"),
						rightButton: new App.Consumer.Views.SalusButtonPrimaryView({
							classes: "btn-danger width100",
							buttonTextKey: "common.labels.delete",
							clickedDelegate: function () {
								var self = this; // this is the button here

								if (that.model.get("key")) {
									this.showSpinner();

									return that.model.unregister().then(function () {
										return App.salusConnector.getRuleGroupCollection().refresh().then(function () {
											self.hideSpinner();
											App.hideModal();
											App.navigate("myStatus");
										});
									}).catch(function (err) {
										self.hideSpinner();

										return P.reject(err);
									});
								}
							}
						}),
						leftButton: new App.Consumer.Views.ModalCloseButton({
							classes: "width100",
							buttonTextKey: "common.labels.cancel"
						})
					})
				}));

				App.showModal();
			},

			/**
			 * do cleanup if they enabled the rule and navigate away without save
			 */
			onDestroy: function () {
				var that = this, gateway = App.getCurrentGateway();

				if (gateway) {
					var node = gateway.getGatewayNode();

					if (node) {
						node.getDevicePropertiesPromise().then(function () {
							that.model.set("selected", node.get("MyStatus").getSetterVal() === that.model.get("key"));
							that.model.isDirty = false;
						});
					}
				} else {
					this.model.set("selected", false);
				}
			}
		}).mixin([SalusPage], {
			analyticsSection: "myStatus",
			analyticsPage: "makeStatus"
		});
	});

	return App.Consumer.MyStatus.Views.MakeMyStatusPageView;
});
