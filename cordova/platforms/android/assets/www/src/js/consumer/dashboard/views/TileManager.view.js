"use strict";

define([
	"app",
	"common/constants",
	"draggabilly.pkgd",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/dashboard/views/Tile.view",
	"consumer/dashboard/views/tileContentViews/AddGatewayTile.view"
], function (App, constants, Draggabilly, consumerTemplates, SalusView) {

	App.module("Consumer.Dashboard.Views", function (Views, App, B, Mn, $, _) {
		Views.TileManagerView = Mn.CompositeView.extend({
			className: "bb-tile-manager tile-manager disable-text-select col-xs-12 margin-b-30",
			template: consumerTemplates["dashboard/tileManager"],
			ui: {
				tileContainer: ".bb-dashboard-tile-manager-container"
			},
			events: {
				"touchmove": "_handleDragEvent"
			},
			childView: Views.TileView,
			childViewContainer: ".bb-dashboard-tile-manager-container",
			initialize: function (options) {
				var that = this;

				options = options || {};

				_.bindAll(this,
					"_lockTiles",
					"_unlockTiles",
					"_setTileLockOnFlip",
					"_handleTileFlip",
					"_dismissWelcomeTile",
					"_updateTileOrder"
				);

				this.type = options.type || "large";
				this.dashboardManager = options.dashboardManager;

				if (options.shouldBeHidden) {
					this.$el.addClass("not-visible");
				}

				this.$el.attr("data-gateway-dsn", options.gatewayDSN);

				this.draggies = [];

				this.$el.addClass("tile-manager-type-" + this.type);

				// Add the tile drag backdrop to the dom
				this.$tileDragBackdrop = $(_.template("<div class='bb-tile-drag-backdrop dashboard-backdrop fade'></div>")());
				$('body').append(this.$tileDragBackdrop);

				this.listenTo(this.collection, "reorder:tileOrder", function () {
					that.collection.trigger("tileOrder:modification", that.collection);
				});
			},
			onAddChild: function (childView) {
				var that = this;

				if (childView.model.get("isFlipped")) {
					childView.model.set("isFlipped", false);
				}

				this.listenTo(childView, "flipComplete", this._handleTileFlip);
				this.listenTo(childView, "longPress", this._enableDragMode);
				this.listenTo(childView.backView, "unpinned", function () {
					// relayout packery upon unpinning
					setTimeout(function () {
						that.ui.tileContainer.packery("layout");
					}, 0);
				});

				// listen for the welcome tile to dismiss
				if (childView.model.get("referenceType") === "welcome") {
					this.listenTo(childView.frontView, "dismiss", this._dismissWelcomeTile);
				}
			},
			getTileModelsInOrder: function () {
				var that = this, models = [],
					tiles = this.ui.tileContainer.packery("getItemElements");

				_.each(tiles, function (tile) {
					var cid = $(tile).data("view-cid"),
						tileView = that.children.findByCid(cid);

					if (tileView) {
						models.push(tileView.model.toJSON());
					}
				});

				return models;
			},
			onRender: function () {
				if (this.collection && this.collection.gatewayDsn === constants.noGateway) {
					if (!this.addGatewayTile) {
						this.addGatewayTile = new App.Consumer.Dashboard.Views.TileContentViews.AddGatewayTile();
					}

					this.ui.tileContainer.append(this.addGatewayTile.render().$el);
				}
			},
			completeOnShow: function () {
				this.children.each(function (childView) {
					childView.completeOnShow();
				});

				this._triggerPackery();
				this._disableDragMode();

				if (this.collection && this.collection.firstLoad) {
					this._updateTileOrder();
					this.collection.firstLoad = false;
				}

				this.dashboardManager.get("sessionUser").dashboardFirstLoad = false;
			},
			/**
			 * Toggles the tile lock if any of the tiles are currently flipped over
			 * @private
			 */
			_setTileLockOnFlip: function (flippedView) {
				var $packeryContainer = $(".packery");

				if (this.anyTileFlipped()) {
					this._disableDragMode();
				}

				if (flippedView && !flippedView.shouldGrow) {
					// Note that this uses 'this.el' and not 'this.$el'
					setTimeout(function () {
						$packeryContainer.packery("fit", flippedView.el);
					}, 0);
				} else {
					// when the tile finishes aligning to grid, call layout to make them left justified again
					setTimeout(function () {
						$packeryContainer.packery("layout");
					}, 0);
				}
			},
			anyTileFlipped: function () {
				return this.$(".flipped").length > 0;
			},
			_handleTileFlip: function (flippedView) {
				this._setTileLockOnFlip(flippedView);

				this.children.each(function (childView) {
					if (childView !== flippedView) {
						childView.closeTile();
					}
				});
			},
			/**
			 * Starts packery
			 * @private
			 */
			_triggerPackery: function () {
				var itemElems, that = this, normalTileWidth;

				/**
				 * Since our tiles grow and shrink in size, set the packery column width to the hard coded value of
				 * a normal sized tile (145px). This allows packery to quickly calculate the width of a column of tiles
				 * when they grow / rearrange. This fixes an issue where if a thermostat tile in the first position of the
				 * container was flipped over and grown to a 2x2 size, the rest of the tiles would inherit the spacing
				 * of a 2x2 tile while visibly looking like a 1x1 tile.
				 */
				normalTileWidth = 145; // in px

				this.ui.tileContainer.packery({
					columnWidth: normalTileWidth,
					gutter: 10,
					itemSelector: ".tile-container",
					containerStyle: {margin: "0 auto", height: "auto"}
				});

				itemElems = this.ui.tileContainer.packery("getItemElements");

				if (!App.isMSBrowser()) {
					_.each(itemElems, function (item) {
						var draggie = new Draggabilly(item, {
							containment: ".packery" // Constrain the dragging tiles to the bounding box of the tile container
						});

						that.ui.tileContainer.packery('bindDraggabillyEvents', draggie);
						that.draggies.push(draggie);
					});

					this.ui.tileContainer.on("dragStart", function () {
						that._handleDragEventStart();
					});

					// when the tile finishes aligning to grid, call layout to make them left justified again
					this.ui.tileContainer.on("dragItemPositioned", function () {
						that._handleDragEventEnd();
						that._updateTileOrder();
					});
				}
			},
			_handleDragEventStart: function () {
				var $body = $("body");

				// tiles dragged outside body
				$body.addClass("overflow-hidden");

				// This will disable scrolling of the page while the user is interacting with an element inside it
				$body.attr("ontouchmove", "event.preventDefault();").attr("ontouchstart", "event.preventDefault();");
			},
			_handleDragEventEnd: function () {
				var $body;

				// This handles the case where the user is dragging multiple tiles around at once, drops one, and
				// continues dragging the another one. This prevents us from "locking" the tiles if a drag is still in progress
				if ($(".is-dragging").length === 0) {
					clearInterval(this.dragCancelTimer);
					this.dragCancelTimer = null;

					$body = $("body");
					$body.removeClass("overflow-hidden");
					$body.attr("ontouchstart", "").attr("ontouchmove", "");

					// lock tiles and layout packery
					this._disableDragMode();
				}
			},
			/**
			 * Handler for dragging a tile and triggering a scroll even on the screen
			 * when the tile is moved to the top or bottom of the screen
			 * @private
			 */
			_handleDragEvent: function () {
				// Only trigger assisted screen scroll when a tile is being dragged
				if (this.$el.hasClass("drag-mode-enabled")) {
					var screenHeight = screen.height,
						topBound = screenHeight * (1 / 5),
						bottomBound = screenHeight * (3 / 5),
						y = $(window).scrollTop(),
						topOfTilePixelsFromTopOfScreen = $(".is-pointer-down").offset().top - $(window).scrollTop();

					if (topOfTilePixelsFromTopOfScreen < topBound) {
						this._animateScrollTop(y - 250, 600);
					} else if (topOfTilePixelsFromTopOfScreen > bottomBound) {
						if (y + 15 < screenHeight) {
							// make sure we don't go below the screen
							this._animateScrollTop(y + 250, 600);
						}
					}
				}
			},
			/**
			 * Animate the window scroll during a tile drag. Smooth as butter.
			 * @param target
			 * @param duration
			 * @private
			 */
			_animateScrollTop: function (target, duration) {
				duration = duration || 16;

				var that = this,
					scrollTopProxy = {value: $(window).scrollTop()};

				if (scrollTopProxy.value !== target && !this.isScrolling) {
					this.isScrolling = true;

					$(scrollTopProxy).animate({value: target}, {
						duration: duration,
						easing: 'swing',
						step: function (stepValue) {
							var rounded = Math.round(stepValue);
							$(window).scrollTop(rounded);
						},
						complete: function () {
							setTimeout(function () {
								that.isScrolling = false;
							}, 400);
						}
					});
				}
			},
			_enableDragMode: function () {
				var that = this;

				// Flip over any tiles that are flipped so we can enable drag move from a flipped tile state
				if (this.anyTileFlipped()) {
					this._handleTileFlip();
				}

				this.$tileDragBackdrop.addClass("in");
				this.$el.addClass("drag-mode-enabled");
				this._unlockTiles();

				// Setup a drag cancel timer that checks if the user is still dragging and cancels it if they are not
				if (!this.dragCancelTimer) {
					this.dragCancelTimer = setInterval(function () {
						that._handleDragEventEnd();
					}, 1000);
				}
			},
			_disableDragMode: function () {
				var that = this;

				this.$tileDragBackdrop.removeClass("in");
				this.$el.removeClass("drag-mode-enabled");

				this._lockTiles();

				setTimeout(function () {
					that.ui.tileContainer.packery("layout");
				}, 0);
			},
			_lockTiles: function () {
				_.each(this.draggies, function (draggie) {
					draggie.disable();
				});
			},
			_unlockTiles: function () {
				_.each(this.draggies, function (draggie) {
					draggie.enable();
				});
			},
			_updateTileOrder: function () {
				this.collection.update(this.getTileModelsInOrder());
			},
			/**
			 * look for the welcome tile and remove it
			 * @private
			 */
			_dismissWelcomeTile: function () {
				var that = this, tiles = this.ui.tileContainer.packery("getItemElements");

				_.each(tiles, function (tile) {
					var cid = $(tile).data("view-cid"),
						tileView = that.children.findByCid(cid);

					if (tileView && tileView.model.get("referenceType") === "welcome") {
						that.ui.tileContainer.packery("remove", tileView.$el);

						// relayout packery
						setTimeout(function () {
							that.ui.tileContainer.packery("layout");
							App.salusConnector.unpinFromDashboard(null, "welcome");
						}, 0);
					}
				});
			},
			onBeforeDestroy: function () {
				// Fixes a bug where the drag mode backdrop would stick around when navigating
				// away from the page in the middle of a drag
				if (this.$tileDragBackdrop) {
					this.$tileDragBackdrop.remove();
				}

				this.ui.tileContainer.off();
				this.dashboardManager.destroy();
			},
			onDestroy: function () {
				if (this.addGatewayTile) {
					this.addGatewayTile.destroy();
				}
			}
		}).mixin([SalusView]);

		return Views.TileManagerView;
	});
	
	return App.Consumer.Dashboard.Views.TileManagerView;
});