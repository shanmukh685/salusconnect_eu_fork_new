"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view"
], function (App, consumerTemplates, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {
		Views.EquipmentAliasEdit = Mn.LayoutView.extend({
			getTemplate: function () {
				if (this.options.page === "settings") {
					return consumerTemplates["settings/equipment/settingsAliasEdit"];
				} else if (this.options.page === "equipment") {
					return consumerTemplates["equipment/widgets/equipmentAliasEdit"];
				}
			},
			id: "bb-equip-alias-edit",
			className: "equip-alias-edit",
			regions: {
				aliasRegion: ".bb-alias-textbox",
				cancelRegion: ".bb-alias-cancel",
				saveRegion: ".bb-alias-save"
			},
			initialize: function (/*options*/) {
				_.bindAll(this, "handleCancelClick", "handleSaveClick");
			},
			onRender: function () {
				this.aliasRegion.show(new App.Consumer.Views.FormTextInput({
					value: this.model.get("name") || "",
					labelText: "equipment.status.alias"
				}));

				this.cancelRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
					className: "btn btn-default width100",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: this.handleCancelClick
				}));

				this.saveRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "width100",
					buttonTextKey: "common.labels.save",
					clickedDelegate: this.handleSaveClick
				}));
			},
			handleCancelClick: function () {
				this.trigger("close");
			},
			handleSaveClick: function () {
 				var that = this,
						newAlias = this.aliasRegion.currentView.getValue();

				// allow set empty string
				if (_.isString(newAlias)) {
					this.model.set("alias", newAlias);
					this.model.set("product_name", newAlias);
					this.model.saveProductName();
					this.model.persist().then(function () {
						that.trigger("saved");
                        that.trigger("click:saveName",that);
					});
				}

				this.trigger("close");
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.EquipmentAliasEdit;
});