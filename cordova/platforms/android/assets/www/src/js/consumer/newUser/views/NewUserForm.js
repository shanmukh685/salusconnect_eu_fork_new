"use strict";

define([
	"app",
	"bluebird",
	"common/config",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixins",
	"consumer/models/SalusButtonPrimary",
	"consumer/models/SalusDropdownViewModel",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/SalusDropdown.view",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusCheckbox.view",
	"consumer/models/SalusCheckboxViewModel",
	"consumer/views/SalusTooltip.view",
	"consumer/views/SalusModal.view",
	"consumer/newUser/views/EUCookieModal.view"
], function (App, P, config, consumerTemplates, SalusViewMixin, ConsumerMixins) {

	App.module("Consumer.NewUser.Views", function (Views, App, B, Mn, $, _) {
		Views.NewUserForm = Mn.LayoutView.extend({
			template: consumerTemplates["newUser/newUserForm"],
			regions: {
				"firstNameRegion": ".bb-first-name",
				"lastNameRegion": ".bb-last-name",
				"emailRegion": ".bb-email",
				"emailConfirmRegion": ".bb-email-confirm",
				"passwordRegion": ".bb-password",
				"passwordConfirmRegion": ".bb-password-confirm",
				"countryRegion": ".bb-country",
				"submitButtonRegion": ".bb-submit-button",
				"cancelButtonRegion": ".bb-cancel-button"
			},
			ui: {
				"termsConditionsCheckbox": ".bb-toc-checkbox",
				"privacyPolicyCheckbox": ".bb-pp-checkbox"
			},
			modelEvents: {
				"change:country": "checkShouldShowEUModal"
			},
			events: {
				"click .bb-toc-checkbox": "validateForm",
				"click .bb-pp-checkbox": "validateForm",
				"mouseover .bb-submit-button": "validateForm"
			},
			initialize: function () {
				this.registeredRegions = {};

				_.bindAll(this,
						"_validatePasswordConfirm",
						"_validateEmailConfirm",
						"handleSubmitClicked",
						"gotoConfirmationPage",
						"validateForm",
						"checkShouldShowEUModal"
				);

				var countriesCollection = new App.Consumer.Models.DropdownItemViewCollection();

				this._fillDropdownCollection(config.countries, config.countryPrefix, countriesCollection);

				this.firstNameField = this.registerRegion("firstNameRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.firstName",
					required: true,
					model: this.model,
					modelProperty: "firstname"
				}));

				this.lastNameField = this.registerRegion("lastNameRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.lastName",
					required: true,
					model: this.model,
					modelProperty: "lastname"
				}));

				this.emailField = this.registerRegion("emailRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.email",
					required: true,
					type: "email",
					model: this.model,
					modelProperty: "email"
				}));

				this.emailConfirmField = this.registerRegion("emailConfirmRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.emailConfirm",
					validationMethod: this._validateEmailConfirm,
					required: true,
					type: "email",
					model: this.model,
					showFeedback: true,
					preventTrigger: true // textbox that is 2nd pass confirmation should have this
				}));

				this.passwordField = this.registerRegion("passwordRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.password",
					type: "password",
					required: true,
					model: this.model
				}));

				this.passwordConfirmField = this.registerRegion("passwordConfirmRegion", new App.Consumer.Views.FormTextInput({
					labelText: "setupWizard.newUserForm.formFields.passwordConfirm",
					type: "password",
					validationMethod: this._validatePasswordConfirm,
					required: true,
					model: this.model,
					showFeedback: true,
					preventTrigger: true // textbox that is 2nd pass confirmation should have this
				}));

				this.countryDropdown = this.registerRegion("countryRegion", new App.Consumer.Views.SalusDropdownWithValidation({
					collection: countriesCollection,
					viewModel: new App.Consumer.Models.DropdownViewModel({
						id: "countrySelect",
						dropdownLabelKey: "setupWizard.newUserForm.formFields.countrySelect"
					}),
					required: true,
					boundAttribute: "country",
					model: this.model
				}));

				this.registerRegion("submitButtonRegion", new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "setupWizard.newUserForm.formFields.submitButton",
					classes: "width100 disabled",
					clickedDelegate: this.handleSubmitClicked
				}));

				this.registerRegion("cancelButtonRegion", new App.Consumer.Views.SalusButtonPrimaryView({
					buttonTextKey: "common.labels.cancel",
					classes: "col-xs-6",
					className: "width100 btn btn-default",
					clickedDelegate: this.handleCancelClicked
				}));

				this.tocCheckboxView = new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						id: "bb-terms-conditions-checkbox",
						labelTextKey: "setupWizard.newUserForm.formFields.termsAndConditionsLabel",
						secondaryIconClass: "",
						isChecked: false
					})
				});

				this.ppCheckboxView =  new App.Consumer.Views.CheckboxView({
					model: new App.Consumer.Models.CheckboxViewModel({
						id: "bb-privacy-policy-checkbox",
						labelTextKey: "setupWizard.newUserForm.formFields.privacyPolicyLabel",
						secondaryIconClass: "",
						isChecked: false
					})
				});

				this.validationNeeded.push(this.firstNameField, this.lastNameField, this.emailField,
					this.emailConfirmField, this.passwordField, this.passwordConfirmField, this.countryDropdown);

				this.attachValidationListener(); // to validationNeeded items
			},
			onRender: function () {
				this.ui.termsConditionsCheckbox.prepend(this.tocCheckboxView.render().$el);
				this.ui.privacyPolicyCheckbox.prepend(this.ppCheckboxView.render().$el);
			},
			onDestroy: function () {
				this.tocCheckboxView.destroy();
				this.ppCheckboxView.destroy();
			},
			_fillDropdownCollection: function (list, prefix, collection) {
				_.each(list, function (key) {
					collection.add(new App.Consumer.Models.DropdownItemViewModel({
						value: key,
						displayText: prefix + "." + key
					}));
				});
			},
			_validatePasswordConfirm: function () {
				var pass1 = this.passwordRegion.currentView.getValue(),
					pass2 = this.passwordConfirmRegion.currentView.getValue();

				return this.model.validatePassword(pass1, pass2);
			},
			_validateEmailConfirm: function () {
				var email1 = this.emailRegion.currentView.getValue(),
					email2 = this.emailConfirmRegion.currentView.getValue();

				return this.model.validateEmail(email1, email2);
			},
			checkShouldShowEUModal: function () {
				var that = this,
						country = this.model.get("country");

				// don't need to show for US/RU or if privacy policy is checked already
				if (country === "us" || country === "ru" || this.ppCheckboxView.getIsChecked()) {
					return;
				}

				App.modalRegion.show(new App.Consumer.Views.SalusModalView({
					contentView: new Views.EUCookieModalView({
						acceptClickDelegate: function () {
							App.hideModal();
						}
					})
				}));

				App.showModal();

				// make span the full width of the screen
				// we don't need to clean this up because the modalRegion destroys when it hides, cleaning DOM
				$(".modal-dialog").addClass("full-width");
			},
			validateForm: function () {
				var valid = this.isValid() &&
					this.tocCheckboxView.getIsChecked() &&
					this.ppCheckboxView.getIsChecked();
				
				if (!valid) {
					this.submitButtonRegion.currentView.disable();
				} else {
					this.submitButtonRegion.currentView.enable();
				}

				return valid;
			},
			gotoConfirmationPage: function () {
				App.navigate("login/confirm");
			},
			handleSubmitClicked: function () {
				var pass1, pass2, that = this;

				// make sure the form validates before we submit
				if (!this.validateForm()) {
					return;
				}

				pass1 = this.passwordRegion.currentView.getValue();
				pass2 = this.passwordConfirmRegion.currentView.getValue();

				this.submitButtonRegion.currentView.showSpinner();

				// save new user
				this.model.persist(pass1, pass2).then(function () {
					that.submitButtonRegion.currentView.hideSpinner();
					that.gotoConfirmationPage();
				}).catch(function (err) {
					that.submitButtonRegion.currentView.hideSpinner();

					return P.reject(err);
				});
			},
			handleCancelClicked: function () {
				App.navigate(""); //empty string, goes back to login welcome screen.
			}
		}).mixin([SalusViewMixin, ConsumerMixins.RegisteredRegions, ConsumerMixins.FormWithValidation]);
	});

	return App.Consumer.NewUser.Views.NewUserForm;
});