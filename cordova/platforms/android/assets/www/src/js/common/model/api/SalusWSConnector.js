"use strict";

/**
 * The salus connector is for talking to TBD salus services. The intent is that this is a singleton for the app.
 * TODO:
 *  - should salus connector broker ayla connector calls, or just expose the ayla connector?
 */
define([
	"app",
	"common/config",
	"bluebird",
	"momentWrapper",
	"common/util/utilities",
	"common/AylaConfig",
	"common/util/PromiseHelper",
	"common/model/salusWebServices/DeviceWeather.model",
	"common/model/salusWebServices/rules/RuleParser",
	"common/model/salusWebServices/rules/RuleBuilder"
], function (App, config,  P, moment, utils, AylaConfig, PromiseHelper, DeviceWeather) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.SalusWSConnector = B.Model.extend({
			initialize: function (opts, sessionManager) {

				this._sessionManager = sessionManager;
				this.listenTo(this._sessionManager, "onLogin", this._doOnLogin);
				if (this._sessionManager.get("isLoggedIn")) {
					this._doOnLogin();
				}

				this.ruleParser = new Models.RuleParser();
				this.ruleBuilder = new Models.RuleBuilder();
			},
			/**
			 * things to do once login is triggered
			 * @private
			 */
			_doOnLogin: function () {
				//TODO any salus web service models that need to be set up should be done here
			},

			registerProxyUser: function (userData) {
				if (!userData || !userData.email || !userData.firstname || !userData.lastname || !userData.country) {
					throw Error("Proxy Users Are Required to have an email, first name, last name, and country");
				}

				var payload = {
					"user": userData
				};

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.share.createProxy, payload, "POST").catch(function () {
					// TODO remove this is suppressing and unknown promise error
				});
			},

			getWeatherForDevice: function (device, isLowPriority) {
				var url = _.template(AylaConfig.endpoints.device.weather)(device.toJSON());

				return App.salusConnector.makeAjaxCall(url, null, "GET", null, {isLowPriority: isLowPriority}, [400, 404, 500]).then(function (weatherData) {
					return new DeviceWeather(weatherData, {parse: true});
				});
			},

			propertyAggregator: function (device, property, startDate, endDate, timespan) {
				var url = AylaConfig.endpoints.device.property.getAggregate;
				var urlModel = {};
				var that = this;
                urlModel.dsn = device.get('dsn');
				urlModel.prop = property;
                urlModel.startDate = moment(startDate).format("YYYY-MM-DDTHH:00:00.000-00:00");
                urlModel.endDate = moment(endDate).format("YYYY-MM-DDTHH:59:59.999-00:00");
				
				urlModel.timespan = timespan;

				var smashedUrl = _.template(url)(urlModel);

				return App.salusConnector.makeAjaxCall(smashedUrl, null, "GET", "json").then(function (data) {
					var resultsUrl = _.template(AylaConfig.endpoints.device.property.aggregateResults)({ref: encodeURIComponent(data.datum.value)}),
						helperOptions = {
							aggregateCount: 0,
							results: []
						};

					return that.aggregateResultsHelper(resultsUrl, helperOptions);
				});
			},

			aggregateResultsHelper: function (url, helperOptions) {
				var that = this;
				return this.aggregateResultsCall(url).then(function (data) {
					if (++helperOptions.aggregateCount < 200) {
						if (data && !_.isNumber(data)) {
							helperOptions.results = helperOptions.results.concat(data);
                            return helperOptions.results;
                        //}
						} else if (_.isNumber(data) && data === 204) {
							return helperOptions.results;
						}

						return that.aggregateResultsHelper(url, helperOptions);
					} else {
						App.log("Aggregate Count Reached 200");
					}

					return data;
				});
			},

			aggregateResultsCall: function (url) {
				return App.salusConnector.makeDelayedAjaxCall(1000, url, null, "GET", "text", {returnFullStatus: true}).then(function (obj) {
					var data;
					if (!!obj.data) {
						data = obj.data.replace(/\n/g, ",");
						data = "[" + data.slice(0, -2) + "]";
						return JSON.parse(data);
					}

					if (obj.xhr.status === 204) {
						return 204;
					}

					return false;
				});
			},

			/**
			 * Gets the server generated url that a photo can be uploaded to.
			 * @returns {promise}
			 */
			getPhotoUploadUrl: function () {
				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.photo.create, null, "GET").then(function (webData) {
					var url;

					if (webData && webData.datum && webData.datum.key === "photouploadurl") {
						url = webData.datum.value;
					}

					if (url) {
						return url;
					}

					return P.reject("No upload url");
				});
			},

			log: function (obj) {
				var options = {
					allowAllRequestFailures: true,
					prevent401Reauthoization: true,
					headers: {
						"Authorization": config.splunkHeader
					}
				}, data = {
					"event": obj
				};

				return App.salusConnector.makeAjaxCall(AylaConfig.endpoints.log.create, data, "POST", "json", options);
			}
		});
	});


	return App.Models.SalusWSConnector;
});