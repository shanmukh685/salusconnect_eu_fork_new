"use strict";

define([
	"app",
	"common/constants"
], function (App, constants) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.RuleParser = B.Model.extend({

			initialize: function () {
				this._cleanOutput();
			},

			//main function;
			parseRule: function (rule) {
				this._cleanOutput();

				if (!rule || !rule.condition || !_.isArray(rule.action) || rule.action.length <= 0) {
					this.currentOutput.unparsable = true;
					this.currentOutput.active = false;
				} else {
					this.conditions = rule.condition;
					this.actions = rule.action;
					this.currentOutput.active = rule.active;

					this._parseButtonPress();

					// if the rule trigger isa  timerTrigger then this is a hidden down stream rule
					if (this.currentOutput.ruleTriggerKey && this.currentOutput.ruleTriggerKey.indexOf("timerTriggerKey") >= 0) {
						this.currentOutput.hiddenRule = true;
					}

					if (this.conditions && this.conditions.op === "AND") {
						this._parseStatuses();
						this._parseDayOfWeek();
						this._parseTimeOfDay();
						this._parsePropVal();
                        this._parseBtnPress();

					} else  if (this.conditions) {
						this.currentOutput.unparsable = true;
					}

					this._parseActions();
				}

				if (this.currentOutput.unparsable) {
					this.currentOutput.conditions = rule.condition || {};
					this.currentOutput.actions = rule.action || [];
				}

				this.currentOutput.uxhint = rule.uxhint;
				this.currentOutput.name = rule.name;
				this.currentOutput.key = rule.key;

				return this.currentOutput;
			},

			_cleanOutput: function () {
				this.currentOutput = {
					name: null,
					key: null,
					conditions: [],
					actions: [],
					delayedActions: [],
					uxhint: null,
					unparsable: false,
					active: false,
					hiddenRule: false
				};
			},

			_parseActions: function () {
				var that = this,
						outputs = [];

				_.each(this.actions, function (action) {
					var returnVar;
					try {
						if (action.op === "SetValue" && action.parameters[0].string === constants.ruleGatewayNodePseudoEUID && action.parameters[1].string.indexOf("AlertMessage") >= 0) {
							returnVar = that._parseMessage(action);
						} else if (action.op === "SetValue") {
							returnVar = that._parsePropChange(action);
						} else if (action.op === "StartTimer") {
							returnVar = that._parseStartTimer(action);
						}
					} catch (e) {
						that.currentOutput.unparsable = true;
						returnVar = {};
					}

					var prop = that._checkForThermostat(outputs, returnVar);
					// if it returned something push it to the actions array
					if (returnVar && !prop) {
						outputs.push(returnVar);
					} else if (prop) {
						prop.propName = [prop.propName, returnVar.propName];
					}
				});

				this.currentOutput.actions = this.currentOutput.actions.concat(outputs);
			},

			_checkForThermostat: function (currentOutputs, returnVar) {
				if (returnVar && returnVar.propName && (returnVar.propName.indexOf("SetHeating") >= 0 || returnVar.propName.indexOf("SetCooling") >= 0)) {
					var propToFind = returnVar.propName.indexOf("SetHeating") >= 0 ? "SetCooling": "SetHeating",
						props = _.filter(currentOutputs, function (prop) {
							return prop.EUID === returnVar.EUID && (prop.propName.indexOf(propToFind)!=-1);
						});

					return props.length > 0 ? props[0] : false;
				}
			},

			_parseStartTimer: function (action) {
				var timerValue = action.parameters[1].number / 60, // converting to minutes
						triggerKey = action.parameters[2].string;

				// add base data to delayedActions, will be filled in later
				this.currentOutput.delayedActions.push({
					totalTime: timerValue,
					timerTriggerKey: triggerKey,
					type: "timer",
					allowTimeChange: false
				});

				// set the timerTrigger on the rule as well for redundancy
				this.currentOutput.timerTriggerKey = triggerKey;
			},

			// TODO: this should be revisited when TriggersToUpdate are implemented
			_parseMessage: function (action) {
				var triggerKey = action.parameters[2].string;
				triggerKey = triggerKey.indexOf("trigger-") === 0 ? triggerKey : "message";

				return {
					type: triggerKey.split("-")[1],
					triggerKey: triggerKey
				};
			},

			_parsePropChange: function (action) {
				return {
					type: "propChange",
					EUID: action.parameters[0].string,
					propName: action.parameters[1].string,
					newVal: action.parameters[2].string || action.parameters[2].number
				};
			},

			_parseDayOfWeek: function () {
				var orOpsParams = _.where(this.conditions.parameters, {op: "OR"});
				var dayPattern = [], index = -1, that = this;

				_.each(orOpsParams, function (param) {
					try {
						if (param.parameters[0].parameters[0].parameters[1].string === "ep_0:sRule:Weekday_i") {
							index = that.conditions.parameters.indexOf(param);
						}
					} catch (e) {
						// ignoring error
						that.currentOutput.unparsable = true;
					}
				});

				if (index >= 0) {
					var dowParam = this.conditions.parameters[index];
					_.each(dowParam.parameters, function (param) {
						if (param.parameters[0].op === "GetValue") {
							dayPattern.push(parseInt(param.parameters[1].number));
						}
					});

					this._removeAllIndicesFromArray(this.conditions.parameters, [index]);

					this.currentOutput.conditions.push({
						type: "dow",
						dayPattern: dayPattern
					});
				}
			},

			_removeAllIndicesFromArray: function (array, indices) {
				array = $.grep(array, function (val, i) {
					return $.inArray(i, indices) === -1;
				});

				return array;
			},

			_parseButtonPress: function () {
				var ruleTriggerKey;

				try {
					if (this.conditions.parameters[0].op === "EQ") {
						if (this.conditions.parameters[0].parameters[0].parameters[1].string === "ep_0:sRule:TriggerRule") {
							ruleTriggerKey = this.conditions.parameters[0].parameters[1].string;
							this.conditions = this.conditions.parameters[1];
						} else if (this.conditions.parameters[0].parameters.length > 1 && this.conditions.parameters[0].parameters[1].parameters[1].string.indexOf("TimerActived") >= 0 ) {
							ruleTriggerKey = this.conditions.parameters[0].parameters[1].parameters[1].string.split("TimerActived")[1].substr(1);
							this.conditions = this.conditions.parameters[1];
						}

					}else if(this.conditions.op==="EQ"){
                        this.conditions={"op":"AND","parameters":[this.conditions]};
                    }
//                    else {
//						this.currentOutput.unparsable = true;
//					}

					if (ruleTriggerKey) {
						this.currentOutput.ruleTriggerKey = ruleTriggerKey;
					}
				} catch (e) {
//					this.currentOutput.unparsable = true;
				}
			},

			_parseStatuses: function () {
				// todo: this function just removes it from our this.conditions.parameters array
				// todo: function needs work to actually parse
				var orParams  = _.where(this.conditions.parameters, {op: "OR"}),
						statusParam,
						statusRuleIndex;

				_.each(orParams, function (orParam) {
					try {
						if (orParam.parameters[0].parameters[0].parameters[1].string.indexOf("MyStatus") >= 0) {
							statusParam = orParam;
						}
					} catch (err) {
						// ignoring
					}
				});

				if (statusParam) {
					statusRuleIndex = this.conditions.parameters.indexOf(orParams);
				}

				if (statusRuleIndex >= 0) {
					this.conditions.parameters.splice(statusRuleIndex, 1);
				}
			},

			_parseTimeOfDay: function () {
				var that = this, output = {}, indices = [];

				_.each(this.conditions.parameters, function (param, i) {
					try {
						var todParam = param.parameters[0];
						if (todParam.op === "GetValue" && todParam.parameters[1].string === "ep_0:sRule:Time_i") {
							if (param.op === "LT") {
								indices.push(i);
								output.endTime = param.parameters[1].number;
							} else if (param.op === "GTE") {
								indices.push(i);
								output.startTime = param.parameters[1].number;
							} else {
								that.currentOutput.unparsable = true;
							}

							if (!that.currentOutput.unparsable) {
								if (output.endTime - 100 === output.startTime) {
									output.endTime -= 100;
								}
							}
						}
					} catch (e) {
						// ignoring error
					}
				});

				if (Object.keys(output).length > 0) {
					this._removeAllIndicesFromArray(this.conditions.parameters, indices);

					output.type = "tod";
					this.currentOutput.conditions.push(output);
				}
			},

			_parsePropVal: function () {
				var outputs = [], indices = [];

				_.each(this.conditions.parameters, function (param, i) {
					try {
						var propValueParam, output = {}, deviceParam = param.parameters[0];

						// multiple properties - we want to make it look like 1 prop for user's sake
						if (param.op === "OR") {
							param = param.parameters[0];
							deviceParam = deviceParam.parameters[0];
						}

						if (deviceParam.op === "GetValue" && deviceParam.parameters[0].string !== constants.ruleGatewayNodePseudoEUID) { // make sure euid
							output.type = "propVal";
							output.EUID = deviceParam.parameters[0].string;
							output.propName = deviceParam.parameters[1].string;
							var alreadyExists = _.findWhere(outputs, output);

							var op = param.op.toLowerCase();

							if (alreadyExists) {
								output = alreadyExists;
							}

							propValueParam = param.parameters[1];

							output[op] = (propValueParam.number || propValueParam.number === 0) ? parseInt(propValueParam.number) : propValueParam.string;
							indices.push(i);

							if (!alreadyExists) {
								outputs.push(output);
							}
						}
					} catch (e) {
						// ignoring error
					}
				});

				if (outputs.length > 0 && indices.length > 0) {
					this._removeAllIndicesFromArray(this.conditions.parameters, indices);
					this.currentOutput.conditions = this.currentOutput.conditions.concat(outputs);
				}
			},
            
            _parseBtnPress: function() {
                var output = {};
                
                _.each(this.conditions.parameters, function(param) {
                    try {
						if (param.parameters[0].parameters[1].string === "ep_0:sRule:Btn_press") {
                            output.type = "buttonPress";
                            output.value = param.parameters[1].number;
						}
					} catch (e) {
						// ignoring error
					}
                });
                if(Object.keys(output).length > 0) {
                    this.currentOutput.conditions.push(output);
                }
            }
		});
	});

	return App.Models.RuleParser;
});