"use strict";

define([
    "app",
    "bluebird",
    "common/AylaConfig",
    "common/model/ayla/mixin.AylaBacked",
    "common/model/salusWebServices/ThermostatSingleControl.model"
], function (App, P, AylaConfig, AylaBackedMixin) {

    App.module("Models", function (Models, App, B, Mn, $, _) {
        Models.ThermostatSingleControlCollection = B.Collection.extend({
            model: App.Models.ThermostatSingleControlModel,
            initialize: function (data, options) {
                if (options && options.dsn) {
                    this.dsn = options.dsn;
                }

                this.listenTo(App.salusConnector._aylaConnector, "remove:thermostat", this.findAndRemoveThermostat);
            },
            load: function () {
                var that = this,
                        fetchUrlModel = {
                            dsn: this._getGatewayNodeDSN()
                        };

                if (!fetchUrlModel.dsn) {
                    this.reset([]);
                    return P.resolve();
                }

                return App.salusConnector.makeAjaxGet(_.template(AylaConfig.endpoints.device.linkedThermostats.list)(fetchUrlModel)).then(function (data) {
                    that.set(data.linkedDevices_list, {parse: true});
                });
            },
            setDSN: function (dsn) {
                if (_.isString(dsn)) {
                    this.dsn = dsn;
                }
            },
            refresh: function (isLowPriority) {
                var promise = this.load(isLowPriority);
                App.salusConnector.setDataLoadPromise("tscs", promise);

                return promise;
            },
            isLoaded: function () {
                return !!this.length; //todo
            },
            unregisterAll: function () {
                var urlModel = {dsn: this._getGatewayNodeDSN()};

                if (!urlModel.dsn) {
                    this.reset([]);
                    return P.resolve();
                }

                return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.device.linkedThermostats.list)(urlModel), null, "DELETE", "text");
            },
            /**
             * this will get called when a thermostat is removed from the account
             * @param thermostat
             */
            findAndRemoveThermostat: function (thermostat) {
                var EUID = thermostat instanceof B.Model ? thermostat.getEUID() : thermostat;

                // for each tsc, look for EUID in it's array of devices
                this.some(function (TSC) {
                    var devices = TSC.get("devices"), match;

                    match = _.findWhere(devices, {EUID: EUID});

                    if (match) {
                        TSC.set("devices", _.without(devices, match));
                        TSC.update();

                        return true;
                    }

                    return false;
                });
            },
            /**
             * returns thermostats not in this tsc for this gateway
             * @returns {Backbone.Collection}
             */
            getThermostatsWithIndividualControls: function () {
                var outArray = [],
                        tscCollectionArray = this.toArray(),
                        thermostats = App.salusConnector.getThermostatCollection();

                thermostats.each(function (thermostat) {
                    var isFound = false;

                    for (var i = 0; i < tscCollectionArray.length && !isFound; i++) {
                        if (tscCollectionArray[i].hasThermostat(thermostat)) {
                            isFound = true;
                        }
                    }

                    if (!isFound) {
                        outArray.push(thermostat);
                    }
                });

                return new B.Collection(outArray);
            },
            _getGatewayNodeDSN: function () {
                var node = App.salusConnector.getDevice(this.dsn).getGatewayNode();
                return node ? node.get("dsn") : null;
            },
            getSingleControlByThermostat: function (thermostat) {
                var EUID = thermostat.getEUID();
                var singleControl;

                // for each tsc, look for EUID in it's array of devices
                this.some(function (TSC) {
                    var devices = TSC.get("devices");

                    var res = _.findWhere(devices, {EUID: EUID});

                    if (res) {
                        singleControl = TSC;
                    }
                });

                return singleControl;
            }

        }).mixin([AylaBackedMixin], {
            apiWrapperObjectName: "linkedDevices"
        });
    });

    return App.Models.ThermostatSingleControlCollection;
});