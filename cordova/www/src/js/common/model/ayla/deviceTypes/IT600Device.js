"use strict";

define([
    "app",
    "bluebird",
    "common/constants",
    "common/config",
    "common/util/equipmentUtilities",
    "common/AylaConfig",
    "common/model/ayla/deviceTypes/mixin.BaseDevice",
    "common/model/ayla/mixin.AylaSchedule",
    "common/model/ayla/deviceTypes/mixin.thermostats"
], function (App, P, constants, config, equipUtils, AylaConfig, BaseAylaDevice) {

    App.module("Models", function (Models, App, B, Mn, $, _) {
        /**
         * this is a wrapper for testing ayla base device. it allows us to construct one and run unit tests and such
         */
        Models.IT600Device = B.Model.extend({
            isIT600: true,
            defaults: {
                // get properties
                FirmwareVersion: null, //string
                MACAddress: null, //string length 16
                SubDeviceName_c: null, //string - not used _c naming is as defined on device wiki
                DeviceType: null, //integer 769: Thermostat
                ManufactureName: null, //string length 32
                ModelIdentifier: null, //string length 32
                DeviceIndex: null, //integer no need display in app
                CloudySetpoint_x100: null, //integer Temp = data / 100
                CoolingSetpoint_x100: null, //integer Temp = data / 100
                FrostSetpoint_x100: null, //integer Temp = data / 100
                HeatingSetpoint_x100: null, //integer Temp = data / 100
                MoonSetpoint_x100: null, //integer Temp = data / 100
                MaxCoolSetpointLimit_x100: null, //integer
                MaxHeatSetpointLimit_x100: null, //integer
                MinCoolSetpointLimit_x100: null, //integer
                MinHeatSetpointLimit_x100: null, //integer
                //Start
                HeatingControl: null,
                TemperatureOffset: null,
                OUTSensorProbe: null,
                OUTSensorType: null,
                CoolingControl: null,
                ValveProtection: null,
                MaxHeatSetpoint_x100: null,
                MinCoolSetpoint_x100: null,
                FloorHeatingMax_x100: null,
                FloorHeatingMin_x100: null,
                FloorCoolingMin_x100: null,
                //End
                Error01: null, //boolean Thermostat paired TRV hardware issue
                Error02: null, //boolean Thermostat floor sensor overheating
                Error03: null, //boolean Thermostat floor sensor open
                Error04: null, //boolean Thermostat floor sensor short
                Error05: null, //boolean Thermostat lost link with Coordinator
                Error06: null, //boolean Thermostat lost link with Wiring Center
                Error07: null, //boolean Thermostat lost link with TRV
                Error08: null, //boolean Thermostat lost link with Receive 1
                Error09: null, //boolean Thermostat lost link with Receive 2
                Error21: null, //boolean Thermostat paired TRV lost link with Coordinator
                Error22: null, //boolean Thermostat paired TRV low battery
                Error23: null, //boolean Thermostat receive message form unpaired TRV or TRV receive message from unpaired Thermostat
                Error24: null, //boolean Thermostat reject by Wiring Centre
                Error25: null, //boolean Thermostat lost link with Parent
                Error30: null, //boolean Thermostat paired TRV gear issue
                Error31: null, //boolean Thermostat paired TRV adaptation issue
                DeviceSetting_d: null, //string
                GroupNumber: null, //integer
                LocalTemperature_x100: null, //integer

                // integer enum ->
                //0: Offline
                //1: Auto-Sunny
                //2: Auto-Cloudy
                //3: Auto-Moon
                //4: Sunny
                //5: Cloudy
                //6: Moon
                //7: Party
                //8: Away
                //9: Frost
                //10: Auto-Party
                //11: Auto-Away
                //12: Auto-Frost
                //15: Unknown
                OperationMode: null,
                PairedWCNumber: null, //integer
                PartyDuration: null, //integer 0 ~ 9
                PowerType: null, //integer
                RunningMode: null, //integer
                Schedule: null, //string
                ScheduleType: null, //integer
                Status_d: null, //string
                SunnySetpoint_x100: null, //integer
                SyncResponseDST_d: null, //integer
                SyncResponseTimeOffset_d: null, //integer
                SyncResponseTimeZone_d: null, //integer
                SystemMode: null, //integer
                ThermostatType: null, //integer [
                VacationDuration: null, //integer 0 ~ 99
                LockKey: null,
                ShutOffDisplay: null,
                // set properties
                SetIndicator: null, // set device indicator

            },
			initialize: function (/*data, options*/) {
				//TODO Setup
				var oemModel = this.get("oem_model");
                if (oemModel.indexOf("it600ThermHW") > -1) {
                    this._setToThermostat();
                } else if (oemModel.indexOf("it600HW") > -1) {
                    this._setToHotWaterHeater();
                }
			},
            _setToThermostat: function () {
                this._setThermoCategoryDefaults();
                this._setThermoEquipmentPageDefaults();
                this.modelType = constants.modelTypes.IT600THERMOSTAT;
                this.tileType = "thermostat";

                this.listenTo(App.salusConnector._aylaConnector, "remove:trv", this.checkAndRemoveTRV);
            },
            _setToHotWaterHeater: function () {
                this._setWHeaterCategoryDefaults();
                this._setWHeaterEquipmentPageDefaults();
                this.modelType = constants.modelTypes.WATERHEATER;
                this.tileType = "hotWaterHeater";

                // for cycling modes with a 1 sec delay
                this.waterHeaterModeTimeout = null;
                this.cachedMode = null;
            },
            _setThermoCategoryDefaults: function () {
                this.set({
                    device_category_id: 769,
                    device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_temp_idle.svg"),
                    device_category_type: constants.categoryTypes.THERMOSTATS,
                    device_category_name_key: "equipment.myEquipment.categories.types.thermostats"
                });
            },
            _setThermoEquipmentPageDefaults: function () {
                this.set({
                    equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_temp_grey.svg")
                });
            },
            _setWHeaterCategoryDefaults: function () {
                this.set({
                    device_category_id: null,
                    device_category_icon_url: App.rootPath("/images/icons/dashboard/icon_water_heater_on_white.svg"),
                    device_category_type: constants.categoryTypes.WATERHEATERS,
                    device_category_name_key: "equipment.myEquipment.categories.types.waterHeaters"
                });
            },
            _setWHeaterEquipmentPageDefaults: function () {
                this.set({
                    equipment_page_icon_url: App.rootPath("/images/icons/myEquipment/icon_water_heater_off_grey.svg")
                });
            },
            getBaseScheduleProps: function (scheduleId) {
                if (this.modelType === constants.modelTypes.IT600THERMOSTAT) {
                    return [{
                            name: this.getSetterPropNameIfPossible("AutoCoolingSetpoint_x100"),
                            base_type: "integer",
                            schedule_id: scheduleId
                        }, {
                            name: this.getSetterPropNameIfPossible("AutoHeatingSetpoint_x100"),
                            base_type: "integer",
                            schedule_id: scheduleId
                        }];
                } else {
                    return [{
                            name: this.getSetterPropNameIfPossible("AutoOnOff"),
                            base_type: "boolean",
                            schedule_id: scheduleId
                        }];
                }

            },
            /**
             * batch set associated trv's
             * a it600 can have 6 trv's connected at a time
             * a trv can be associated with only 1 it600
             *
             * trv id should be converted to base 16 string, uppercase
             *
             * @param trvs - expect deviceModels in array
             */
            associateTRVs: function (trvs) {
                var that = this, propString = "", shortID, shortIDHex;

                if (!_.isArray(trvs)) {
                    return P.reject("Expected an array of TRVs");
                }

                if (_.isArray(trvs) && trvs.length > 6) {
                    return P.reject("Can only pair 6 trv's!");
                }

                if (_.isArray(trvs) && !trvs.length) {
                    propString = " ";
                }

                _.each(trvs, function (trv) {
                    if(_.isString(trv)){
                        propString = propString + trv;
                    }
                    else{
                        shortID = trv.getPropertyValue("ShortID_d");

                        shortIDHex = equipUtils.integerTo64BitHex(shortID);

                        // create a property string of each id
                        propString = propString + (!!shortIDHex ? shortIDHex : "");

                    }
                });

                return this.getDevicePropertiesPromise().then(function () {
                    return that.setProperty("PairedTRVShortID", propString);
                });
            },
            hasTRV: function (trv) {
                var associated = this.getAssociatedTRVs();

                return _.some(associated, function (hex) {
                    return equipUtils.hexToInt(hex) === trv.getPropertyValue("ShortID_d");
                });
            },
            /**
             * get associated trv's on this device
             * @returns {Array}
             */
            getAssociatedTRVs: function () {
                var associated = "";

                if (this.get("PairedTRVShortID")) {
                    associated = this.getPropertyValue("PairedTRVShortID") || "";
                }

                // property may come down with many trailing white space
                associated = associated.trim();

                // match 4 characters and put into array, or empty array
                return associated.match(/.{4}/g) || [];
            },
            checkAndRemoveTRV: function (trv) {
                if (this.hasTRV(trv)) {
                    var without = _.without(this.getAssociatedTRVs(), equipUtils.integerTo64BitHex(trv.getPropertyValue("ShortID_d")));

                    this.associateTRVs(without);
                }
            },
            setToFollowSchedule: function () {
                var that = this, holdType = this.get("HoldType");

                if (holdType) {
                    holdType.setProperty(constants.it600HoldTypes.FOLLOW).then(function () {
                        that.temporaryHold = false;
                    });
                }
            },
            setToTempHold: function () {
                var that = this, holdType = this.get("HoldType");

                if (holdType) {
                    holdType.setProperty(constants.it600HoldTypes.TEMPHOLD).then(function () {
                        that.temporaryHold = true;
                    });
                }
            },
            setToPermHold: function () {
                var that = this, holdType = this.get("HoldType");

                if (holdType) {
                    holdType.setProperty(constants.it600HoldTypes.PERMHOLD).then(function () {
                        that.temporaryHold = false;
                    });
                }
            },
            setToFrostOff: function () {
                var that = this, holdType = this.get("HoldType");

                if (holdType) {
                    holdType.setProperty(constants.it600HoldTypes.OFF).then(function () {
                        that.temporaryHold = false;
                    });
                }
            },
            _isSetPointOnSchedule: function (schedule) {
                var value = schedule.get("schedule_actions").first().get("value");

                return this.getPropertyValue("HeatingSetpoint_x100") === value && this.getPropertyValue("CoolingSetpoint_x100") === value;
            },
            isSetToCurrentSchedule: function () {
                //defaulted to true if the device has no schedules
                if ((!this.get("schedules") || this.get("schedules").length <= 0) && !this.temporaryHold) {
                    return true;
                } else {
                    return !this.temporaryHold && this._isSetPointOnSchedule(this.get("schedules").getCurrentSchedule());
                }
            },
            getHoldProp: function () {
                return this.get("HoldType");
            },
            /**
             * toggles between on (continuous, 9), auto (schedule, 0), off (off, 7)
             * sets a timeout after 1 seconds to actually send off the mode
             * @returns {*}
             */
            switchWaterHeaterMode: function () {
                var that = this, modesArray, holdProp = this.get("HoldType");

                if (this.isThermostat()) {
                    throw new Error("Not a water heater");
                }

                // clear previous
                clearTimeout(this.waterHeaterModeTimeout);

                modesArray = [
                    constants.waterHeaterHoldTypes.CONTINUOUS,
                    constants.waterHeaterHoldTypes.SCHEDULE,
                    constants.waterHeaterHoldTypes.OFF
                ];

                if (_.isNull(this.cachedMode)) {
                    this.cachedMode = holdProp.getProperty();
                }

                if (_.contains(modesArray, this.cachedMode)) {
                    // cycle index's
                    this.cachedMode = modesArray[(_.indexOf(modesArray, this.cachedMode) + 1) % modesArray.length];
                } else {
                    // off default
                    this.cachedMode = constants.waterHeaterHoldTypes.OFF;
                }

                this.trigger("change:cachedMode", {mode: this.cachedMode});

                // return the set property promise inside the timeout
                return new P.Promise(function (resolve, reject) {
                    that.waterHeaterModeTimeout = setTimeout(function () {
                        //that.trigger("setting:HoldType");
                        that.setWaterHeaterMode()
                                .catch(function (err) {
                                    reject(err);
                                })
                                .then(function (data) {
                                    resolve(data);
                                });

                    }, config.waterHeaterSetModeDelay);
                });

            },
            /**
             * set water heater mode based off cachedMode, set up in above function
             * @returns {*}
             */
            setWaterHeaterMode: function () {
                var that = this;

                if (!_.isNumber(this.cachedMode)) {
                    return P.reject("Trying to set mode was not a number");
                }

                return this.getDevicePropertiesPromise().then(function () {
                    if (that.getPropertyValue("HoldType") === that.cachedMode) {
                        return P.reject("Will not send up the same mode it was already in!");
                    }
                    that.trigger("setting:HoldType");
                    return that.setProperty("HoldType", that.cachedMode).then(function () {
                        that.trigger("set:HoldType"); // tell stuff it should spin and block more interactions
                        that.cachedMode = null; // reset this

                        return P.resolve();
                    });
                });
            }
        }).mixin([BaseAylaDevice, Models.AylaScheduleMixin, Models.ThermostatModelMixin]);
    });

    return App.Models.IT600Device;
});
