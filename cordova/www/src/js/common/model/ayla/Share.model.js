"use strict";

define([
	"app",
	"common/AylaConfig",
	"common/model/ayla/mixin.AylaBacked",
	"common/util/utilities"
], function (App, AylaConfig, AylaBackedMixin, utilities) {

	App.module("Models", function (Models, App, B, Mn, $, _) {
		Models.ShareModel = B.Model.extend({
			defaults: {
				"end_date_at": null,
				"grant_id": null,
				"id": null,
				"operation": null,
				"owner_id": null,
				"resource_id": null,
				"resource_name": "device",
				"start_date_at": null,
				"status": null,
				"updated_at": null,
				"user_id": null,
				"user_email": null
			},

			/*
			"created_at":"2013 09 26T19:57:27Z",
				"end_date_at":null,
				"grant_id":19,
				"id":2,
				"operation":"read",
				"owner_id":1,
				"resource_id":"AC000W0000001136",
				"resource_name":"device",
				"start_date_at":null,
				"status":"pending",
				"updated_at":"2013 09 26T19:57:27Z",
				"user_id":1}}
			*/

			initialize: function () {
				this.listenTo(this, "created", this._onCreated);
			},

			add: function () {
				var that = this,
						url = utilities.appendEmailTemplateParameters(AylaConfig.endpoints.share.create,
								"resource_share", App.translate("email.resource_share.emailSubject"));

				return App.salusConnector.makeAjaxCall(url, this._buildPayload(), "POST").then(function (devData) {
					devData = devData.share ? devData.share : devData;
					that.set(devData);
					that.trigger("created", that);
				});
			},

			update: function () {
				var that = this,
						url = utilities.appendEmailTemplateParameters(_.template(AylaConfig.endpoints.share.update)(this.toJSON()),
								"resource_share", App.translate("email.resource_share.emailSubject"));

				return App.salusConnector.makeAjaxCall(url, this._buildPayload(), "PUT").then(function (devData) {
					devData = devData.share ? devData.share : devData;
					that.set(devData);
					that.trigger("updated", that);
				});
			},

			unregister: function () {
				var that = this;

				var urlModel = this.toJSON(); //TODO verify.

				return App.salusConnector.makeAjaxCall(_.template(AylaConfig.endpoints.share.delete)(urlModel), null, "DELETE", "text").then(function (){
					that.trigger("sync", that);
				});
			},

			isLoaded: function () {
				return this._detailPromise && this._detailPromise.isLoaded();
			},

			_buildPayload: function () {
				var data = {
					"id": this.get("id"),
					"owner_id": this.get("owner_id"),
					"resource_name": this.get("resource_name"),
					"resource_id": this.get("resource_id"),
					"user_email": this.get("user_email"),
					"operation": "write"
				};

				return this.wrapApiData(data);
			},

			_onCreated: function () {
				this.refresh();
			}
		}).mixin([AylaBackedMixin], {
			fetchUrl: AylaConfig.endpoints.share.read,
			apiWrapperObjectName: "share"
		});
	});

	return App.Models.ShareModel;
});