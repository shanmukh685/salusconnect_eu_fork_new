"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"smartmenus",
	"collapse",
	"consumer/views/mixins/mixin.salusView",
	"consumer/models/SceneViewModel",
	"consumer/views/base/HeaderScenesDesktop.view"
], function (App, consumerTemplates, smartmenu, collapse, SalusViewMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {

		Views.HeaderLinkItemView = Mn.ItemView.extend({
			tagName: "li",
			template: consumerTemplates["base/headerLink"],
			events: {
				"click": "handleClick"
			},
			templateHelpers: function () {
				return {
					hasSubMenu: false
				};
			},
			handleClick: function (evt) {
				if (this.model.get("clickHandler")) {
					evt.preventDefault();
					this.model.get("clickHandler")();
				}
			}
		}).mixin([SalusViewMixin]);

		Views.HeaderLinkDropdownView = Mn.CompositeView.extend({
			tagName: "li",
			template: consumerTemplates["base/headerDropdown"],
			childViewContainer: "ul",
			childView: Views.HeaderLinkItemView,
			templateHelpers: function () {
				return {
					hasSubMenu: false
				};
			}
		}).mixin([SalusViewMixin]);

		Views.HeaderLinkDesktopCollectionView = Mn.CollectionView.extend({
			tagName: "ul",
			className: "sm sm-clean header-links-list nav navbar-nav navbar-right",
			getChildView: function (model) {
				if (model.get("type") === "link") {
					return Views.HeaderLinkItemView;
				} else if (model.get("type") === "dropdown") {
					return Views.HeaderLinkDropdownView;
				} else if (model.get("type") === "myStatus") {
					return Views.HeaderScenesDesktopView;
				} else {
					return Views.HeaderLinkItemView;
				}
			},
			childViewOptions: function (model) {
				if (model.get("type") === "dropdown") {
					return {
						collection: new App.Consumer.Models.LinkViewModelCollection(model.get("subLinks")),
						model: model
					};
				} else {
					return {
						model: model
					};
				}
			},

			_initializeSmartmenus: function () {
				var options = {
					subIndicators: false,
					hideOnClick: false
				};
				this.$el.smartmenus(options);

				// Init SmartMenu's Bootstrap nav addon when we know that SmartMenu has fully initialized
				// http://vadikom.github.io/smartmenus/src/demo/bootstrap-navbar-fixed-top.html
				require([
					"smartmenus.bootstrap"
				], function () {
					$.SmartMenus.Bootstrap.init();
				});
			},

			onRender: function () {
				this._initializeSmartmenus();
			},
			
			onBeforeDestroy: function () {
				try {
					this.$el.smartmenus('destroy');
				} catch (e) {
					//do nothing.
				}
			},
			
			onDestroy : function() {
				this.collection.each(function(item) {
					item.stopListening();
				});
				this.stopListening();
			}
		});
	});

	return App.Consumer.Views;
});