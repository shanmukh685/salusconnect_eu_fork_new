"use strict";

define([
	"app",
	"modal",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.alertModal"
], function (App, BootstrapModal, consumerTemplates, SalusViewMixin, SalusAlertModalMixin) {

	App.module("Consumer.Views", function (Views, App, B, Mn) {
		Views.SalusAlertModalView = Mn.LayoutView.extend({
			className: "modal fade",
			template: consumerTemplates.salusAlertModal
		}).mixin([SalusAlertModalMixin]);
	});

	return App.Consumer.Views.SalusAlertModalView;
});