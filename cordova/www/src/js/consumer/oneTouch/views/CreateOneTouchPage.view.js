"use strict";

define([
	"app",
	"momentWrapper",
	"bluebird",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"common/util/UserUtils",
	"consumer/views/Breadcrumbs.view",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/oneTouch/views/OneTouchMenu.view",
	"consumer/oneTouch/views/OneTouchCriteriaCollection.view",
	"common/model/salusWebServices/rules/Rule.model",
	"common/model/salusWebServices/rules/RuleDetails.model",
	"common/model/salusWebServices/rules/RuleMenuCollection"
], function (App, moment, P, constants, consumerTemplates, SalusPage, SalusView, UserUtils, BreadcrumbsView) {

	App.module("Consumer.OneTouch.Views", function (Views, App, B, Mn, $, _) {

		/**
		 * CreateOneTouchPageView
		 * used for creating & editing a One Touch rule.
		 * to edit, the view should be initialized with options {edit: true}
		 */
		Views.CreateOneTouchPageView = Mn.LayoutView.extend({
			template: consumerTemplates["oneTouch/createOneTouchPage"],
			id: "create-one-touch-page",
			className: "container",
			ui: {
				pageTitle: "#bb-create-edit-one-touch-header"
			},
			regions: {
				nameRegion: ".bb-name-region",
				criteriaRegion: ".bb-criteria-region",
				saveButtonRegion: ".bb-rule-save-button",
				cancelButtonRegion: ".bb-rule-cancel-button",
				breadcrumbs: ".bb-breadcrumbs"
			},
			initialize: function (/*options*/) {
				_.bindAll(this, "handleSaveClick", "handleCancelClick");

				if (this.options && this.options.edit) {
					this.key = decodeURIComponent(this.options.key);
					this.isEdit = true;
                    
                    var rule = App.salusConnector.getRule(this.key);
                    if(rule){
                        if(rule.checkExistsInvalidDevice()){
                            this.disabledSave=true;
                        }
                    }
                    
				}

				this.cancelButtonView = new App.Consumer.Views.SalusButtonPrimaryView({
					className: "btn btn-default width100",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: this.handleCancelClick
				});

				this.saveButtonView = new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "width100",
					buttonTextKey: "common.labels.save",
					clickedDelegate: this.handleSaveClick
				});
                
                if(this.isEdit && this.disabledSave){
                    this.saveButtonView.$el.addClass("disabled");
                }
			},
			onRender: function () {
				var that = this;

				// set title text
				if (this.isEdit) {
					this.ui.pageTitle.text(App.translate("equipment.oneTouch.edit.title"));
				} else {
					this.ui.pageTitle.text(App.translate("equipment.oneTouch.create.title"));
				}

				App.salusConnector.getDataLoadPromise(["devices", "rules"]).then(function (/*arrayOfData*/) {
					if (that.isEdit) {
						var rule = App.salusConnector.getRule(that.key);

						if (!rule) {
							// gateway changed on us
							return App.navigate("oneTouch");
						}

						// to not destroy condition or actions models when user wants to edit...
						that.model = rule.deepCopy();

						// set it on the ruleMakerManager because we will be working with this rule now
						App.Consumer.OneTouch.ruleMakerManager.setCurrentRule(that.model);
					} else {
						// create the model after gateway, rules come in
						that.model = App.Consumer.OneTouch.ruleMakerManager.newRule();
					}

					// want to have our devices loaded before we initialize this view for StateOfEquipment menus
					that.criteriaView = new Views.CreateOneTouchCriteriaLayoutView({
						model: that.model
					});
                    
                    that.listenTo(that.criteriaView,"save:enabled",function (){
                        this.disabledSave=false;
                        this.saveButtonView.$el.removeClass("disabled");
                    });

					that.nameView = new Views.CreateOneTouchNameView({
						model: that.model
					});

					that.nameRegion.show(that.nameView);
					that.criteriaRegion.show(that.criteriaView);
					that.saveButtonRegion.show(that.saveButtonView);
					that.cancelButtonRegion.show(that.cancelButtonView);

					if (that.isEdit) {
						var uriKey = encodeURIComponent(that.model.get("key"));

						that.breadcrumbs.show(new BreadcrumbsView.BreadcrumbsView({
							crumbs: [
								{
									textKey: "equipment.oneTouch.automations.title",
									href: "/oneTouch"
								},
								{
									text: that.model.get("name"),
									href: "/oneTouch/" + App.getCurrentGatewayDSN() + "/" + uriKey
								},
								{
									textKey: "common.labels.edit",
									active: true
								}
							]
						}));
					} else {
						that.breadcrumbs.show(new BreadcrumbsView.BreadcrumbsView({
							crumbs: [
								{
									textKey: "equipment.oneTouch.automations.title",
									href: "/oneTouch"
								},
								{
									textKey: "common.labels.create",
									active:true
								}
							]
						}));
					}
				});
			},
			handleSaveClick: function () {
				var that = this, rawName = this.nameView.getName(),
					sanitizedRuleName = "";

				this.nameView.hideError();

				// check if name is empty
				if (_.isString(rawName) && rawName.length === 0) {
					// they did not provide a name so we default it.
					sanitizedRuleName = App.translate("equipment.oneTouch.create.defaultRuleName", {
						ruleNum: App.salusConnector.getDisplayRuleCollection().length + 1
					});
				} else {
					// SANITIZE INPUTS
					sanitizedRuleName = UserUtils.sanitizeInputs(rawName);
				}

				this.model.set("name", sanitizedRuleName);
                if(this.model.get("actions").models && this.model.get("actions").models.length === 0) {
					return;
                }

				// we don't care about criteria error checking when non-parsable
				if (!this.model.get("unparsable")) {
					// check that the rule contains at least one action or delayed action
					// we know that there will be a condition, since we always create button push
					if (!this.model.hasActions()) {
						// todo: this isn't the best place to display this kind of error
						this.nameView.showError("equipment.oneTouch.create.missingCriteriaError");
						return;
					}
				}
                if(this.savingStatus) {
                    return;
                } else {
                    this.savingStatus = true;
                }
                
				if (this.isEdit) {
					return this._handleUpdate();
				}

				return this.model.add().then(function () {
					var ruleGroupCollection = App.salusConnector.getRuleGroupCollection(),
							path = "oneTouch/" + App.getCurrentGatewayDSN();

					App.salusConnector.addToRuleCollection(that.model);

					if (!ruleGroupCollection || ruleGroupCollection.isEmpty()) {
						path += "/pin"; // bypass apply in status page
					} else {
						path += "/apply";
					}

					App.navigate(path);
				});
			},
			_handleUpdate: function () {
				var that = this;

				return this.model.update().then(function () {
					return App.salusConnector.getRuleCollection().refresh().then(function () {
						var uriKey = encodeURIComponent(that.key);

						App.navigate("oneTouch/"+ App.getCurrentGatewayDSN() + "/" + uriKey);
					});
				});
			},
			handleCancelClick: function () {
				window.history.back();
			}
		}).mixin([SalusPage], {
			analyticsSection: "oneTouch",
			analyticsPage: "create"
		});

		/**
		 * CreateOneTouchNameView
		 * Left hand section of the page, holds the Rule Name textbox
		 */
		Views.CreateOneTouchNameView = Mn.LayoutView.extend({
			template: consumerTemplates["oneTouch/oneTouchName"],
			className: "one-touch-name",
			regions: {
				textboxRegion: ".bb-one-touch-textbox"
			},
			initialize: function () {
				_.bindAll(this, "getName", "showError", "hideError");

				this.textboxView = new App.Consumer.Views.FormTextInput({
					labelText: "equipment.oneTouch.create.nameTextbox",
					value: this.model.get("name")
				});
			},
			onRender: function () {
				this.textboxRegion.show(this.textboxView);
			},
			getName: function () {
				return this.textboxView.getValue();
			},
			showError: function (errorTextKey) {
				this.textboxView.showErrors(errorTextKey);
			},
			hideError: function () {
				this.textboxView.hideErrors();
			}
		}).mixin([SalusView]);

		/**
		 * CreateOneTouchCriteriaLayoutView
		 * Right hand side of the page
		 * Holds menus for When conditions, Actions, and DelayedActions
		 * Also has an area above the menus for Rule Criteria
		 * If the rule is unparsable, show the unparsable message and do not show menus
		 */
		Views.CreateOneTouchCriteriaLayoutView = Mn.LayoutView.extend({
			template: consumerTemplates["oneTouch/oneTouchCriteria"],
			className: "one-touch-criteria",
			ui: {
				criteriaSection: ".bb-criteria-section"
			},
			regions: {
				conditionsMenuRegion: ".bb-conditions-section",
				actionsMenuRegion: ".bb-actions-section",
				delayedActionMenuRegion: ".bb-delayed-action-section"
			},
			initialize: function () {
				var that = this;

				_.bindAll(this, "_onSaveRule");

				this.listenTo(this.model, "save:rule", this._onSaveRule);

				// when parsable
				if (!this.model.get("unparsable")) {
					// rule criteria collection views
					this.conditionsCriteriaView = new Views.OneTouchCriteriaCollectionView({
						collection: this.model.get("conditions"),
						type: constants.oneTouchMenuTypes.condition
					});

					this.buttonPressCriteriaView = new Views.OneTouchCriteriaCollectionView({
						collection: new B.Collection([{type: "buttonPress"}]),
						type: constants.oneTouchMenuTypes.condition,
						readonly: true // don't allow delete button push
					});

					this.actionsCriteriaView = new Views.OneTouchCriteriaCollectionView({
						collection: this.model.get("actions"),
						type: constants.oneTouchMenuTypes.action
					});

					this.delayedActionsCriteriaView = new Views.OneTouchCriteriaCollectionView({
						collection: this.model.get("delayedActions"),
						type: constants.oneTouchMenuTypes.delayedAction
					});
                    
                    this.listenTo(this.conditionsCriteriaView,"itemDelete",function (){
                        if(!that.model.checkExistsInvalidDevice()){
                            that.trigger("save:enabled");
                        }
                    });
                    
                    this.listenTo(this.actionsCriteriaView,"itemDelete",function (){
                        if(!that.model.checkExistsInvalidDevice()){
                            that.trigger("save:enabled");
                        }
                    });
                    
                    this.listenTo(this.delayedActionsCriteriaView,"itemDelete",function (){
                        if(!that.model.checkExistsInvalidDevice()){
                            that.trigger("save:enabled");
                        }
                    });

					// menu views x3
					this.conditionsMenuView = new Views.OneTouchMenuCompositeView({
						model: new B.Model({
							uniqueView: false,
							type: constants.oneTouchMenuTypes.condition,
							isLeafMenu: false
						})
					});

					this.actionsMenuView = new Views.OneTouchMenuCompositeView({
						model: new B.Model({
							uniqueView: false,
							type: constants.oneTouchMenuTypes.action,
							isLeafMenu: false
						})
					});

					this.listenTo(this.model.get("delayedActions"), "update", function () {
						// empty out delayedActions when there's more than 1
						if (that.model.get("delayedActions").length > 0) {
							that.delayedActionMenuRegion.empty();
						} else {
							that.delayedActionMenuRegion.show(new Views.OneTouchMenuCompositeView({
								model: new B.Model({
									uniqueView: false,
									type: constants.oneTouchMenuTypes.delayedAction,
									isLeafMenu: false
								})
							}));
						}
					});
				} else {
					// not parsable
					this.unparsableCriteriaView = new Views.OneTouchCriteriaCollectionView({
						collection: new B.Collection([{type: "unparsable"}]),
						type: "unparsable",
						readonly: true
					});
				}
			},
			onRender: function () {
				// parsable
				if (!this.model.get("unparsable")) {
					// rule criteria
					this.ui.criteriaSection
							.append(this.conditionsCriteriaView.render().$el)
							.append(this.buttonPressCriteriaView.render().$el)
							.append(this.actionsCriteriaView.render().$el)
							.append(this.delayedActionsCriteriaView.render().$el);

					this.conditionsMenuRegion.show(this.conditionsMenuView);
					this.actionsMenuRegion.show(this.actionsMenuView);

					// only show delayedActions menu when there aren't any currently
					if (this.model.get("delayedActions").length === 0) {
						this.delayedActionMenuRegion.show(new Views.OneTouchMenuCompositeView({
							model: new B.Model({
								uniqueView: false,
								type: constants.oneTouchMenuTypes.delayedAction,
								isLeafMenu: false
							})
						}));
					}
				} else {
					// non-parsable
					this.ui.criteriaSection.append(this.unparsableCriteriaView.render().$el);
				}
			},
			onDestroy: function () {
				this.conditionsCriteriaView.destroy();
				this.buttonPressCriteriaView.destroy();
				this.actionsCriteriaView.destroy();
				this.delayedActionsCriteriaView.destroy();
			},
			_onSaveRule: function (/*rule*/) {
				// do cleanup of rule criteria on save
				if (!this.model.get("unparsable")) {
					this.conditionsCriteriaView.destroy();
					this.actionsCriteriaView.destroy();
					this.buttonPressCriteriaView.destroy();
					this.delayedActionsCriteriaView.destroy();
				} else {
					this.unparsableCriteriaView.destroy();
				}
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.OneTouch.Views.CreateOneTouchPageView;
});
