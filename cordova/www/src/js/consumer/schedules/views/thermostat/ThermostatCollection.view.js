"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"consumer/equipment/views/tsc/ThermostatSingleControlPage.view"
], function (App, constants, consumerTemplates, SalusViewMixin) {

	App.module("Consumer.Schedules.Views", function (Views, App, B, Mn) {

		Views.ThermostatNameView = Mn.ItemView.extend({
			className: "thermostat-name-view",
			template: consumerTemplates["schedules/thermostatNameView"],
			ui: {
				name: ".bb-name"
			},
			bindings: {
				".bb-name": "name"
			}
		}).mixin([SalusViewMixin]);

		Views.ThermostatCollectionView = Mn.CollectionView.extend({
			className: "thermostat-collection",
			childView: App.Consumer.Schedules.Views.ThermostatNameView
		}).mixin([SalusViewMixin]);
	});

	return App.Consumer.Schedules.Views.ThermostatNameView;
});

