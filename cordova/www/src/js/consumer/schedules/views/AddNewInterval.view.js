"use strict";

define([
    "app",
    "common/config",
    "moment",
    "common/util/utilities",
    "common/constants",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusView",
    "consumer/views/SalusButtonPrimary.view",
    "consumer/views/FormTextInput.view",
    "consumer/schedules/views/IntervalTypeRadioButtons.view",
    "consumer/models/SalusRadioViewModel",
    "consumer/views/SalusRadio.view"
], function (App, config, moment, utilities, constants, consumerTemplates, SalusViewMixin) {

    App.module("Consumer.Schedules.Views", function (Views, App, B, Mn, $, _) {

        Views.AddNewIntervalView = Mn.LayoutView.extend({
            className: "new-interval",
            template: consumerTemplates["schedules/addNewInterval"],
            regions: {
                startTimeRegion: ".bb-start-time-region",
                temperatureRegion: ".bb-temperature-region",
                radioOnRegion: ".bb-radio-on-region",
                radioOffRegion: ".bb-radio-off-region",
                addButtonRegion: ".bb-add-button",
                cancelButtonRegion: ".bb-cancel-button",
                radioRegion: ".bb-radio-region"
            },
            ui: {
                titleText: ".bb-add-new-interval-text"
            },
            events: {
                "click .bb-delete-interval": "_handleDeleteClicked"
            },
            bindings: {
                "input[type='radio']": {
                    observe: "value",
                    update: utilities.noOp,
                    onSet: function (value) {
                        //1 on 0 off
                        return value === "1" ? 1 : 0;
                    }
                }
            },
            initialize: function (options) {
                _.bindAll(this, "handleCancelClicked", "handleAddClicked");
                options = options || {};

                var that = this;

                this.onOffMode = !!options.onOffMode;
                this.collectionValid=options.isValidTime;
                
                if(options.device.get("MinHeatSetpoint_x100")){
                    this.minHeat=options.device.get("MinHeatSetpoint_x100").get("value")/100;
                }else{
                    this.minHeat=config.thermostatMinimumTemp;
                }
                
                if(options.device.get("MaxHeatSetpoint_x100")){
                    this.maxHeat=options.device.get("MaxHeatSetpoint_x100").get("value")/100;
                }else{
                    this.maxHeat=config.thermostatMaximumTemp;
                }

                if (!this.model) {
                    var value = this.onOffMode ? 0 : null;

                    this.model = new B.Model({
                        value: value,
                        time: null
                    });
                } else {
                    this._oldModel = this.model.toJSON();
                }

                this.startTime = new App.Consumer.Views.FormTextInput({
                    labelText: "schedules.schedule.newInterval.startTime",
                    model: this.model,
                    modelProperty: "time",
                    bindingOnGet: function (value) {
                        if (value) {
                            return moment(value, "HH:mm:ss").format("HHmm");
                        }
                        return null;
                    },
                    bindingOnSet: function (value) {
                        return that.convertTime(value);
                    },
                    validationMethod: function () {
                        var isValid1 = false,isValid2 = false,
                                message = App.translate("schedules.schedule.newInterval.timeError");

                        isValid1 = that.isValidTime(this.getValue());
                        
                        if(isValid1){
                            if(that.collectionValid){
                                var time = that.convertTime(this.getValue());
                                var key = that.model?that.model.get("key"):null;
                                if(key){
                                    isValid2 = that.collectionValid(time,key);
                                }else{
                                    isValid2 = that.collectionValid(time);
                                }
                                
                                if(!isValid2) {
                                    message = App.translate("schedules.schedule.newInterval.timeError1");
                                }
                                isValid1 = isValid1 && isValid2;
                            }
                        }
                        that.timeIsValid = isValid1;
                        that.errorMessage = message;

                        return {valid: isValid1, messageText: message};

                    }
                });

                this.temperature = new App.Consumer.Views.FormTextInput({
                    labelText: "schedules.schedule.newInterval.temperature",
                    model: this.model,
                    modelProperty: "value",
                    type: "number",
                    bindingOnSet: function (value) {
                        if (value) {
                            return parseFloat(value) * 100;
                        }

                        return null;
                    },
                    bindingOnGet: function (value) {
                        if (value) {
                            return value / 100;
                        }

                        return null;
                    },
                    validationMethod: function () {
                        var isValid,
                                message = App.translate("schedules.schedule.newInterval.temperatureError", {
                                    floor: that.minHeat, //TODO make this based on devices min thermostat temp
                                    ceiling: that.maxHeat //TODO make this based on devices max thermostat temp
                                });

                        isValid = that.isValidTemp(this.getValue());

                        that.tempIsValid = isValid;

                        return {valid: isValid, messageText: message};
                    }
                });

                this.radioOnView = new App.Consumer.Views.RadioView({
                    model: new App.Consumer.Models.RadioViewModel({
                        value: 1,
                        labelTextKey: "common.labels.turnOn",
                        name: "newIntervalOnOff",
                        isChecked: !!this.model.get("value"),
                        radioClass: "bb-radio-on",
                        classes: "new-interval-radio-button"
                    })
                });

                this.radioOffView = new App.Consumer.Views.RadioView({
                    model: new App.Consumer.Models.RadioViewModel({
                        value: 0,
                        labelTextKey: "common.labels.turnOff",
                        name: "newIntervalOnOff",
                        isChecked: !this.model.get("value"),
                        radioClass: "bb-radio-off",
                        classes: "new-interval-radio-button"
                    })
                });

                this.addButton = new App.Consumer.Views.SalusButtonPrimaryView({
                    buttonTextKey: !!this.model.isEditMode ? "schedules.schedule.newInterval.update" : "schedules.schedule.newInterval.add",
                    className: "width100 btn btn-primary",
                    clickedDelegate: this.handleAddClicked
                });

                this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
                    buttonTextKey: "common.labels.cancel",
                    className: "width100 btn btn-default",
                    clickedDelegate: this.handleCancelClicked
                });

                this.timeIsValid = this.isValidTime(moment(this.startTime.model.get("time"), "HH:mm:ss").format("HHmm"));
                this.tempIsValid = this.isValidTemp(this.temperature.model.get("value") / 100);
            },
            onRender: function () {
                this.startTimeRegion.show(this.startTime);
                this.cancelButtonRegion.show(this.cancelButton);
                this.addButtonRegion.show(this.addButton);

                if (this.onOffMode) {
                    this.radioOnRegion.show(this.radioOnView);
                    this.radioOffRegion.show(this.radioOffView);
                    this.$('.bb-temperature-row').hide();
                    //temp doesn't exist so make tempIsValid true so you can add the interval
                    this.tempIsValid = true;
                } else {
                    this.$(".bb-on-off-row").hide();
                    this.temperatureRegion.show(this.temperature);
                }


                if (this.model.isEditMode) {
                    this.$(".bb-delete-row").removeClass("hidden");
                    this.ui.titleText.addClass("hidden");
                    this.$(".bb-buttons-row").css("margin-right", 130).css("margin-top", 20);
                }
            },
            handleCancelClicked: function () {
                this.model.isEditMode = false;
                this.model.set(this._oldModel);
                this.trigger("click:close");
            },
            handleAddClicked: function () {
                if (this.timeIsValid && this.tempIsValid) {
                    if (!this.model.isEditMode) {
                        this.trigger("click:add", this.model.toJSON());
                    } else {
                        this.model.isEditMode = false;
                        this.trigger("intervalChange");
                        this.trigger("click:close");
                    }
                } else {

                    if (!this.tempIsValid) {
                        this.temperature.showErrors("schedules.schedule.newInterval.temperatureError", {
                            floor: this.minHeat,
                            ceiling: this.maxHeat
                        });
                    }

                    if (!this.timeIsValid) {
                        this.startTime.showErrors(this.errorMessage);
                    }
                }
            },
            _handleDeleteClicked: function () {
                this.model.destroy();
            },
            isValidTemp: function (valueString) {
                //make sure the valueString is actually a string
                valueString = valueString ? valueString.toString() : null;
                var value = valueString ? parseFloat(valueString) : null;

                if (value && !isNaN(value) && value <= this.maxHeat && value >= this.minHeat) {
                    if (utilities.isInteger(value) || valueString.split(".")[1] === "5" || valueString.split(".")[1] === "0") {
                        return true;
                    }
                }

                return false;
            },
            isValidTime: function (value) {
                if (value && value.length > 0 && !isNaN(parseInt(value.charAt(0)))) {

                    if (!this.convertTime(value)) {
                        return false;
                    }

                    return true;
                }

                return false;
            },
            //根据用户输入的缩写的时间来转换成正常的时间
            convertTime: function (value) {
                var format;

                if (value.indexOf(".") != -1) {
                    if (value.length != 5) {
                        var valueArr = value.split(".");
                        if (valueArr[0].length != 2) {
                            valueArr[0] = "0" + valueArr[0];
                        }
                        if (valueArr[1].length != 2) {
                            valueArr[1] = valueArr[1] + "0";
                        }
                        value = valueArr[0] + "." + valueArr[1];
                    }
                    format = "HH.mm";
                } else if (value.indexOf(":") != -1) {
                    if (value.length != 5) {
                        var valueArr = value.split(":");
                        if (valueArr[0].length != 2) {
                            valueArr[0] = "0" + valueArr[0];
                        }
                        if (valueArr[1].length != 2) {
                            valueArr[1] = valueArr[1] + "0";
                        }
                        value = valueArr[0] + ":" + valueArr[1];
                    }
                    format = "HH:mm";
                } else if (value.length == 1 || value.length == 2) {
                    if (value.length == 1) {
                        value = "0" + value;
                    }
                    if (value > 24) {
                        return false;
                    }
                    format = "HH";
                } else if (value.length == 3 || value.length == 4) {
                    if (value.length == 3) {
                        value = "0" + value;
                    }

                    var hh = value.substr(0, 2);
                    var mm = value.substr(2, 2);
                    if (hh > 24 || mm > 59) {
                        return false;
                    }

                    format = "HHmm";
                } else {
                    return false;
                }

//                   if (value.length == 3) {
//                        value = "0" + value;
//                        format = "HHmm";
//                    } 

                return moment(value, format).format("HH:mm:ss");


            }
        }).mixin([SalusViewMixin]);
    });
});

