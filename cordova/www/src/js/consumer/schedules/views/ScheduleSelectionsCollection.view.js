"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, constants, consumerTemplates, SalusViewMixin) {

	App.module("Consumer.Schedules.Views", function (Views, App, B, Mn, $, _) {
		var selections = [{
			title: "schedules.schedule.weekDropdown.workWeek",
			hidden: false,
			type: constants.scheduleConfigurationTypes.WORKWEEK
		}, {
			title: "schedules.schedule.weekDropdown.fullWeek",
			hidden: false,
			type: constants.scheduleConfigurationTypes.FULLWEEK
		}, {
			title: "schedules.schedule.weekDropdown.daily",
			hidden: false,
			type: constants.scheduleConfigurationTypes.DAILY
		}];

		Views.ScheduleSelectionItem = Mn.ItemView.extend({
			className: "schedule-selection-item",
			template: consumerTemplates["schedules/scheduleSelectionItem"],
			ui: {
				selection: ".bb-selection"
			},
			events: {
				"mouseover .bb-selection": "_toggleColor",
				"mouseout .bb-selection": "_toggleColor",
				"click .bb-selection": "_handleSelectionClicked"
			},

			initialize: function () {
				_.bindAll(this, "_toggleColor", "_handleSelectionClicked", "_setHidden");

				this.listenTo(this.model, "change:hidden", this._setHidden);
			},

			onRender: function () {
				this.ui.selection.text(App.translate(this.model.get("title")));
			},

			_toggleColor: function () {
				this.ui.selection.toggleClass("blue");
			},

			_handleSelectionClicked: function () {
				this.trigger("click:selection", this.model.get("type"));
			},

			_setHidden: function () {
				this.ui.selection.toggleClass("hidden", this.model.get("hidden"));
			}
		}).mixin([SalusViewMixin]);

		Views.ScheduleMenuView = Mn.CompositeView.extend({
			className: "thermostat-schedule-menu col-xs-12 col-md-9 ",
			template: consumerTemplates["schedules/scheduleMenu"],
			childView: Views.ScheduleSelectionItem,
			childViewContainer: ".bb-selection-collection-container",
			ui: {
				selectionCollection: ".bb-selection-collection-container",
				arrow: ".bb-arrow",
				titleText: ".bb-title-text"
			},
			events: {
				"click .bb-start": "_toggleSelections"
			},
			childEvents: {
				"click:selection": "_selectionClicked"
			},

			initialize: function (options) {
				options = options || {};
				_.bindAll(this, "_toggleSelections");
				this.selected = false;

				this.collection = new B.Collection(selections);
				this._initialType = options.initialType;
			},

			_showAllSelections: function () {
				this.collection.each(function (model) {
					model.set("hidden", false);
				});
			},

			_selectionClicked: function (childView, type) {
				this._displaySelection(childView.model);

				this.ui.selectionCollection.toggleClass("hidden");

				this.trigger("change:weekType", type);
			},

			_displaySelection: function (model) {
				this.ui.titleText.text(App.translate(model.get("title")));
				this._showAllSelections();
				model.set("hidden", true);
			},

			_toggleSelections: function () {
				this.ui.selectionCollection.toggleClass("hidden");
			},

			onRender: function () {
				if (this._initialType) {
					var model = this.collection.findWhere({
						type: this._initialType
					});

					this._displaySelection(model);
				}
			}
		}).mixin([SalusViewMixin]);
	});
});
