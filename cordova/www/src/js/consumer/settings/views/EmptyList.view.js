"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView"
], function (App, consumerTemplates, SalusView ) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn) {
		/**
		 * options: {
		 * i18nTextKey: ""
		 * }
		 */
		Views.EmptyListView = Mn.ItemView.extend({
			tagName: "li",
			className: "list-table-item",
			template: consumerTemplates["settings/manageDevices/emptyList"],
			initialize: function (options) {
				this.i18nTextKey = options.i18nTextKey || "MissingKey";
			},
			onRender: function () {
				this.$(".bb-empty-list-msg").text(App.translate(this.i18nTextKey));
			}

		}).mixin([SalusView], {});
	});

	return App.Consumer.Settings.Views.EmptyListView;
});
