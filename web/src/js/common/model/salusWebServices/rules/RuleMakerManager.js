"use strict";

define([
	"app",
	"bluebird",
	"common/AylaConfig",
	"common/model/salusWebServices/rules/Rule.model"
], function (App) {

	App.module("Models", function (Models, App, B) {
		Models.RuleMakerManager = B.Model.extend({
			defaults: {
				currentRule: null
			},

			newRule: function () {
				this.set("currentRule", new App.Models.RuleModel({dsn: App.getCurrentGatewayDSN()}));

				return this.get("currentRule");
			},

			setCurrentRule: function (rule) {
				this.set("currentRule", rule);
			},

			getCurrentRule: function () {
				return this.get("currentRule");
			},

			getConditions: function () {
				return this.get("currentRule").get("conditions");
			},

			getActions: function () {
				return this.get("currentRule").get("actions");
			},

			getDelayedActions: function () {
				return this.get("currentRule").get("delayedActions");
			},

			addCondition: function (condition) {
				this.get('currentRule').addCondition(condition);
			},

			addAction: function (action) {
				action.ActID = this._generateID();
				this.get('currentRule').addAction(action);
			},

			addDelayedAction: function (delayedAction) {
				this.get("currentRule").addDelayedAction(delayedAction);
			},
			
			setName: function (name) {
				this.get('currentRule').set("name", name);
			},

			getName: function () {
				return this.get('currentRule').get("name");
			},

			save: function (clearActionsConditions) {
				return this.get('currentRule').add(clearActionsConditions || false);
			},

			_generateID: function ()  {
				var S4 = function() {
					/*jslint bitwise: true */
					return (((1+Math.random())*0x10000)| 0).toString(16).substring(1);
				};
				return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
			}
		});
	});

	return App.Models.RuleMakerManager;
});
