"use strict";

define([
	"app",
	"common/constants",
	"common/model/salusWebServices/rules/Rule.model"
], function (App, constants) {
	App.module("Models", function (Models, App, B) {
		Models.ConditionMenu = B.Model.extend({
			defaults: {
				menu: null,
				equipmentArray: null
			},
			initialize: function (equipmentArray) {
				this.equipmentArray = equipmentArray;
				this._buildMenu();
			},
			_buildMenu: function (){
				this.set("menu", {
					menuModel: new B.Model({
						uniqueView: false,
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: false
					}),
					childMenuCollection: new B.Collection([
						new Models.RuleMenuItemModel({
							displayTextKey: "equipment.oneTouch.menus.when.root.stateOfEquipment"
						}, {childMenu: this._stateOfEquipment()}),
						new Models.RuleMenuItemModel({
							displayTextKey: "equipment.oneTouch.menus.when.root.timeOfDay"
						}, {childMenu: this._timeOfDay()})
//                        ,
//						new Models.RuleMenuItemModel({
//							displayTextKey: "equipment.oneTouch.menus.when.root.dayOfWeek"
//						}, {childMenu: this._dayOfWeekSelect()})
					])
				});
			},
			_stateOfEquipment: function () {
				return  {
					menuModel: new B.Model({
						uniqueView: false,
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: false
					}),
					childMenuCollection: new B.Collection(this.equipmentArray.getEquipmentMenuForRule(constants.oneTouchMenuTypes.condition))
				};
			},
			_timeOfDay: function () {
				return {
//					menuModel: new B.Model({
//						uniqueView: false,
//						type: constants.oneTouchMenuTypes.condition,
//						isLeafMenu: false
//					}),
//					childMenuCollection: new B.Collection([
//						new Models.RuleMenuItemModel({
//							displayTextKey: "equipment.oneTouch.menus.when.timeOfDay.specify"
//						}, { childMenu: {
							menuModel: new B.Model({
								uniqueView: "tod-picker",
								type: constants.oneTouchMenuTypes.condition,
								isLeafMenu: true
							}),
							uniqueViewType: "tod-picker"
//						}
//						})
//					])
				};
			},
			_dayOfWeekSelect: function (preselected) {
				return {
					menuModel: new B.Model({
						uniqueView: "dow-picker",
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: true
					}),
					uniqueViewType: "dow-picker",
					preselected: preselected || []
				};
			},
			_dayOfWeek: function () {
				return {
					menuModel: new B.Model({
						uniqueView: false,
						type: constants.oneTouchMenuTypes.condition,
						isLeafMenu: false
					}),
					childMenuCollection: new B.Collection([
						new Models.RuleMenuItemModel({
							displayTextKey: "equipment.oneTouch.menus.when.dayOfWeek.picker"
						}, {childMenu: this._dayOfWeekSelect()}),
						new Models.RuleMenuItemModel({
							displayTextKey: "equipment.oneTouch.menus.when.dayOfWeek.weekday"
						}, {childMenu: this._dayOfWeekSelect([2, 3, 4, 5, 6])}),
						new Models.RuleMenuItemModel({
							displayTextKey: "equipment.oneTouch.menus.when.dayOfWeek.weekend"
						}, {childMenu: this._dayOfWeekSelect([1, 7])}) //weekend days
					])
				};
			}
		});
	});

	return App.Models.ConditionMenu;
});