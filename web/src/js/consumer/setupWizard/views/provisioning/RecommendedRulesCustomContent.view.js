"use strict";

define([
	"app",
	"common/constants",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusView",
	"common/model/salusWebServices/rules/recommendedRules"
], function (App, constants, consumerTemplates, SalusView, ruleMap) {

	App.module("Consumer.SetupWizard.Provisioning.Views", function (Views, App, B, Mn, $, _) {
		Views.RecommendedRuleItemView = Mn.ItemView.extend({
			className: "recommended-rule col-xs-12 col-sm-6",
			template: consumerTemplates["setupWizard/provisioning/recommendedRule"],
			ui: {
				name: ".bb-one-touch-name",
				description: ".bb-one-touch-description",
				check: ".bb-check"
			},
			events: {
				"click": "toggleSelected"
			},
			initialize: function () {
				_.bindAll(this, "toggleSelected");

				// all start checked
				this.checked = true;
			},
			onRender: function () {
				this.ui.name.text(App.translate(this.model.get("name")));
				this.ui.description.text(App.translate(this.model.get("description")));
			},
			toggleSelected: function () {
				this.checked = !this.checked;
				this.ui.check.toggleClass("checked", this.checked);
			}
		}).mixin([SalusView]);

		Views.RecommendedRulesCustomContentView = Mn.CompositeView.extend({
			className: "rules-custom-content row",
			template: consumerTemplates["setupWizard/provisioning/recommendedRulesCustomContent"],
			childView: Views.RecommendedRuleItemView,
			childViewContainer: ".bb-one-touch-container",
			initialize: function () {
				_.bindAll(this, "getSelectedRecommended");

				this.collection = new B.Collection(ruleMap[this.model.modelType]);
			},
			getSelectedRecommended: function () {
				var recommended = [];

				this.children.each(function (childView) {
					if (childView.checked) {
						recommended.push(childView.model.toJSON());
					}
				});

				return recommended;
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.SetupWizard.Provisioning.Views.RecommendedRulesCustomContentView;
});