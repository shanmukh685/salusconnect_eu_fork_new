"use strict";

define([
	"app",
	"consumer/consumerTemplates",
	"consumer/models/SalusRadioViewModel",
	"consumer/views/SalusRadio.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.salusView",
	"consumer/views/mixins/mixin.registeredRegions"
], function (App, consumerTemplates) {

	App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {

		Views.DataCollectionLayout = Mn.LayoutView.extend({
			template: consumerTemplates["settings/dataCollectionLayout"],
			className: "container",
			regions: {
				"radioOn": ".bb-has-collection-on",
				"radioOff": ".bb-has-collection-off",
				"saveButton": ".bb-save-button"
			},
			ui: {
				infoSection: ".bb-data-collection-info"
			},
			events: {
				"change input[name=hasCollection]": "handleRadioChange"
			},
			initialize: function () {
				_.bindAll(this, "handleRadioChange", "handleSaveButtonClicked", "_toggleInfoSectionHide");
				var dataCollectionOff = App.getDataCollectionOff();
				this.registerRegion("radioOn", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: true,
						name: "hasCollection",
						isChecked: !dataCollectionOff,
						classes: "pull-right data-collection-radio"
					})
				}));

				this.registerRegion("radioOff", new App.Consumer.Views.RadioView({
					model: new App.Consumer.Models.RadioViewModel({
						value: false,
						name: "hasCollection",
						isChecked: !!dataCollectionOff,
						classes: "pull-right data-collection-radio"
					})
				}));

				this.saveButtonView = this.registerRegion("saveButton", new App.Consumer.Views.SalusButtonPrimaryView({
					id: "save-data-collection-btn",
					buttonTextKey: "common.labels.save",
					classes: "center-relative disabled",
					clickedDelegate: this.handleSaveButtonClicked
				}));
			},

			onRender: function () {
				this._toggleInfoSectionHide(!App.getDataCollectionOff());
			},

			_toggleInfoSectionHide: function (shouldHide) {
				if (!shouldHide) {
					this.ui.infoSection.show();
				} else {
					this.ui.infoSection.hide();
				}
			},

			handleRadioChange: function () {
				var selected = this.$("input[name=hasCollection]").filter(":checked").val() === "false";
				
				if(!App.getDataCollectionOff() == selected) {
					this.saveButtonView.enable();
				} else{
					this.saveButtonView.disable();
				}
					
				this._toggleInfoSectionHide(!selected);
			},

			// Commenting previous handleSaveButtonClicked, Srikanth N S
			// handleSaveButtonClicked: function () {
			// 	var selected = this.$("input[name=hasCollection]").filter(":checked").val(),
			// 			value = selected === "false", that = this;

			// 	this.saveButtonView.showSpinner();

			// 	this.model.set("dataCollectionOff", value ? 1 : 0);
			// 	this.model.saveDataCollection().then(function (dataCollectionOff) {
			// 		that.saveButtonView.hideSpinner();

			// 		if (!dataCollectionOff) {
			// 			window.location.reload(true);
			// 		}
			// 	}).catch(function () {
			// 		that.saveButtonView.hideSpinner();
			// 	});
			// }

			handleSaveButtonClicked: function () {
				var that = this;
				var selected = this.$("input[name=hasCollection]").filter(":checked").val(),
										value = selected === "false";
				var secondMsgText = "";
				var confrimationMsg = "settings.dataCollection.promptMsgs.switchon";
				if (selected == "false") {
					confrimationMsg = "settings.dataCollection.promptMsgs.switchoff";
					secondMsgText = "settings.dataCollection.promptMsgs.secondMsgText";
				}
				App.modalRegion.show(new App.Consumer.Views.SalusAlertModalView({
					model: new App.Consumer.Models.AlertModalViewModel({
						iconClass: "icon-warning",
						primaryLabelText: App.translate(confrimationMsg),	
						secondaryLabelText: App.translate(secondMsgText),					
						rightButton: new App.Consumer.Views.SalusButtonPrimaryView({
							classes: "btn-danger width100",
							buttonTextKey: "common.labels.ok",
							clickedDelegate: function () {
								
								App.hideModal();
								that.saveButtonView.showSpinner();

								that.model.set("dataCollectionOff", value ? 1 : 0);
								that.model.saveDataCollection().then(function (dataCollectionOff) {
									that.saveButtonView.hideSpinner();
									that.saveButtonView.disable();
									if (!dataCollectionOff) {																App.salusConnector._sessionManager.trigger("onLogin");
										App.navigate("dashboard");
									}else {										
										that.errorModal = new App.Consumer.Views.SalusAlertModalView({
				                            model: new App.Consumer.Models.AlertModalViewModel({
				                                primaryLabelText: App.translate("settings.dataCollection.statusMsgs.success"),
				                                clickedDelegate: function () {
													App.hideModal();												
												}
				                            })
				                        });
				                        App.modalRegion.show(that.errorModal);
			                        	App.showModal();
			                        	App.navigate("dashboard");
									}
								}).catch(function () {
									that.saveButtonView.hideSpinner();
									that.errorModal = new App.Consumer.Views.SalusAlertModalView({
			                            model: new App.Consumer.Models.AlertModalViewModel({
			                                iconClass: "icon-warning",
			                                primaryLabelText: App.translate("settings.dataCollection.statusMsgs.errorMsg")
			                            })
			                        });

			                        App.modalRegion.show(that.errorModal);
			                        App.showModal();
								});
							}
						}),

						leftButton: new App.Consumer.Views.ModalCloseButton({
							classes: "width100",
							buttonTextKey: "common.labels.cancel"
						})
					})
				}));
				
				App.showModal();

				
			}
		}).mixin([App.Consumer.Views.Mixins.SalusView, App.Consumer.Views.Mixins.RegisteredRegions]);
	});

	return App.Consumer.Settings.Views.DataCollectionLayout;
});
