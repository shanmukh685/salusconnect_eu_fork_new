"use strict";

define([
    "app",
    "bluebird",
    "common/config",
    "common/constants",
    "common/util/equipmentUtilities",
    "common/util/utilities",
    "consumer/consumerTemplates",
    "consumer/consumer.views",
    "consumer/views/mixins/mixin.salusPage",
    "consumer/views/mixins/mixin.salusView",
    "consumer/views/Breadcrumbs.view",
    "consumer/views/SalusLinkButton.view",
    "consumer/views/SalusModal.view",
    "consumer/equipment/views/DeleteEquipmentModal.view",
    "consumer/equipment/views/EquipmentSettingsWidget.view",
    "consumer/views/mixins/mixins",
    "consumer/equipment/views/widgets/EquipmentAliasEdit.view",
    "consumer/settings/models/SettingsContentViewModels",
    "consumer/models/SalusDropdownViewModel",
    "consumer/views/mixins/mixin.equipmentPanel",
    "consumer/settings/views/EmptyList.view",
    "consumer/equipment/views/ChangeAllDevicesModal.view",
    "consumer/views/SalusButtonPrimary.view",
    "consumer/views/SalusAlertModal.view",
    "consumer/settings/views/EnergyMeterSettingsContent.view",
    "consumer/models/SalusAlertModalModel"
], function (App, P, config, constants, equipUtils, utilities, templates, ConsumerViews, SalusPageMixin, SalusView, Breadcrumbs,
             SalusLinkButtonView, SalusModalView, DeleteEquipModalView, EquipmentSettingsWidget, ConsumerMixins) {

    App.module("Consumer.Settings.Views", function (Views, App, B, Mn, $, _) {
        Views.EquipmentSettingsPanelContentRow = Mn.LayoutView.extend({
            tagName: "div",
            className: "infoDivWrapper infoDiv settings-row row",
            template: templates["equipment/widgets/settingsRow"],
            regions: {
                content: ".bb-col-right"
            },
            ui: {
                title: ".col-left"
            },
            initialize: function (/*options*/) {
                var that = this;

                this.valueCollection = new App.Consumer.Models.DropdownItemViewCollection();

                _.each(this.model.get("collection"), function (object) {
                    that.valueCollection.add(new App.Consumer.Models.DropdownItemViewModel({
                        value: object.value,
                        textKey: object.key,
                        text: object.text
                    }));
                });

                if (this.model.get("isNetworkLanModeProperty")) {
                    this.listenTo(this.model, "change:value", function () {
                        App.Consumer.Settings.PageEventController.trigger("change:networkLanMode", that.model.get("value"));
                    });
                }

                if (this.model.get("isNetworkSetting")) {
                    var lanMode = this._hasLANMode();
                    if (lanMode) {
                        this._setReadOnly(!!lanMode.getProperty());
                        this.listenTo(App.Consumer.Settings.PageEventController, "change:networkLanMode", function (value) {
                            that._setReadOnly(!!value);
                            that.render();
                        });
                    }
                }
                
                if(this.model.get("isWifiProperty")) {
                    var newValue = utilities.decryptWithAES(this.model.get("value"), config.encryptParams.key , config.encryptParams.iv);
                    this.model.set("value", newValue);
                }
            },
            _hasLANMode: function () {
                if (_.isArray(this.options.deviceObj)) {
                    return _.reduce(this.options.deviceObj, function (prop, device) {
                        return prop || device.get("NetworkLANMode");
                    }, null);
                } else {
                    return this.options.deviceObj.get("NetworkLANMode");
                }
            },
            _setReadOnly: function (value) {
                this.model.set("readOnly", value);
            },
            onRender: function () {
                var myText = this.model.get("propertyModel").getDisplayNameTranslated();
                this.ui.title.text(myText);

                var modelType = this.model.get("type"),
                    control;

                if (modelType === "dropdown") {
                    control = new ConsumerViews.SalusDropdownView({
                        collection: this.valueCollection,
                        viewModel: new App.Consumer.Models.DropdownViewModel({
                            id: this.model.get("propertyModel").get("display_name") + "Dropdown",
                            dropdownLabelKey: "common.labels.select"
                        }),
                        required: true,
                        boundAttribute: "value",
                        model: this.model
                    });
                    this.listenTo(this.model, "change:value", this.onDropdownChange);
                } else if (modelType === "text" || modelType === "number" || modelType === "numberX100") {
                    var options = {
                        required: true,
                        model: this.model,
                        modelProperty: "value",
                        type: modelType === "text" ? "text" : "number", //HTML input element type.
                        isReadOnly: !!this.model.get("readOnly")
                    };

                    if (this.model.has("validationMethod")) {
                        options.validationMethod = _.bind(this.model.get("validationMethod"), this);
                    }

                    if (modelType === "numberX100") {
                        options.feedback = App.translate("common.symbols.celsius");
                        options.bindingOnSet = function (value) {
                            if (value) {
                                return Math.floor(parseFloat(value) * 100);
                            }
                            return null;
                        };

                        options.bindingOnGet = function (value) {
                            if (value) {
                                return (parseFloat(value) / 100).toFixed(2);
                            }
                            return null;
                        };
                    }

                    control = new ConsumerViews.FormTextInput(options);
                } else if(modelType === "password" || modelType === "text_") {
                    var options = {
                        required: false,
                        model: this.model,
                        modelProperty: "value",
                        type: modelType === "text_" ? "text" : "password"
                    };
                    if (this.model.has("validationMethod")) {
                        options.validationMethod = _.bind(this.model.get("validationMethod"), this);
                    }
                    control = new ConsumerViews.FormTextInput(options);
                }
                if (control) {
                    this.content.show(control);
                }
            },
            
            onDropdownChange: function() {
                this._parent._childViewRender();
                if($(".bb-button-save button").hasClass("disabled")) {
                    $(".bb-button-save button").removeClass("disabled");
                }
            },
            
            templateHelpers: function () {
                var value = this.model.get("value");

                if (this.model.get("type") === "readonly" &&
                    this.valueCollection && this.valueCollection.length > 0) {

                    var item = this.valueCollection.find(function (item) {
                        return item.get("value") === value;
                    });

                    if (item) {
                        value = item.get("text") || App.translate(item.get("textKey"));
                    }
                }

                return {
                    textValue: value
                };
            }
        }).mixin([SalusView]);
        
        Views.EquipmentSettingsPanelContent = Mn.CollectionView.extend({
            tagName: "div",
            className: "col-xs-12",
            childView: Views.EquipmentSettingsPanelContentRow,
            emptyView: App.Consumer.Settings.Views.EmptyListView,
            emptyViewOptions: {
                tagName: "div",
                className: "infoDivWrapper infoDiv settings-row row",
                i18nTextKey: "equipment.settings.empty"
            },
            childViewOptions: function () {
                return {
                    deviceObj: this.options.deviceObj
                };
            },
            
            onRender: function() {
                if(this.options.modelType === "gateway") {
                    var that = this;
                    _.each(this.children._views, function(childView) {
                        if(childView.model.get("type") !== "dropdown") {
                            that.validationNeeded.push(childView.content.currentView);
                        }
                    });
                    this.attachValidationListener();
                }
            },
            
            validateForm: function() {
                if(this.isValid()) {
                    $(".bb-button-save button").removeClass("disabled");
                } else {
                    $(".bb-button-save button").addClass("disabled");
                }
            },
            
            _childViewRender: function() {
                if(this.options.modelType === "gateway") {
                    var that = this;
                    that.validationNeeded = [];
                    _.each(this.children._views, function(childView) {
                        if(childView.model.get("type") !== "dropdown") {
                            that.validationNeeded.push(childView.content.currentView);
                        }
                    });
                    this.attachValidationListener();
                }
            }
        }).mixin([SalusView, ConsumerMixins.FormWithValidation]);


        Views.EquipmentSettingsPanel = Mn.LayoutView.extend({
            initialize: function () {
                this.listenTo(this.model, 'change:modelCollection', this.showContent);
            },
            onRender: function () {
                if (this.model.get("modelCollection")) {
                    this.showContent();
                }
            },
            showContent: function () { // check if emeter, then show custom emeter view
                if (this.model.get("deviceModel").modelType === constants.modelTypes.ENERGYMETER) {
                    this.contentRegion.show(new App.Consumer.Settings.Views.EnergyMeterSettingsContentView({
                        model: this.model.get("deviceModel")
                    }));
                    this.contentRegion.currentView.$el.addClass("e-meter-settings");
                } else {
 
                    this.contentRegion.show(new Views.EquipmentSettingsPanelContent({
                        collection: this.model.get("modelCollection"),
                        deviceObj: this.options.deviceObj,
                        modelType: this.model.get("deviceModel").modelType
                    }));
                }
            },
            updateModelCollection: function (collection) {
                this.model.set("modelCollection", collection);
            },
            resetSettings: function () {
                if (this.model.get("modelCollection")) {
                    this.model.get("modelCollection").revert();
                }
            },
            saveSettings: function (devices) {
                var that = this;

                if (this.model.get("modelCollection")) {
                    if (that.contentRegion.currentView) {
                        that.contentRegion.currentView.showSpinner({
                            textKey: constants.spinnerTextKeys.processing
                        });
                    }

                    this.model.get("modelCollection").save(devices).finally(function () {
                        if (that.contentRegion.currentView) {
                            that.contentRegion.currentView.hideSpinner();
                        }
                    });
                }
            }
        }).mixin([App.Consumer.Views.Mixins.EquipmentPanel]);


        Views.EquipmentSettingsPageView = Mn.LayoutView.extend({
            id: "bb-equipment-settings-page",
            className: "equipment-settings-page settings-page-widgets container",
            template: templates["settings/equipment/equipmentPage"],
            regions: {
                breadcrumbs: ".bb-breadcrumbs",
                removeEquipment: "#bb-remove-equipment",
                infoRegion: "#bb-info-region",
                settingsRegion: "#bb-settings-region",
                aliasRegion: ".bb-alias-edit",
                cancelButton: ".bb-button-cancel",
                saveButton: ".bb-button-save",
                pswValue: ".bb-text-psw",
                pswSaveButton: ".bb-button-psw-save"
            },
            ui: {
                icon: ".bb-equipment-icon",
                savingNetworkSettings: "#bb-saved-network-settings",
                passwordContainer: ".bb-equipment-page-password-row",
                savedLabel: ".saved-label",
                noSavedLabel: ".no-saved-label"
            },
            events: {
                "click .bb-pencil-icon": "_editDeviceAlias",
                "click .bb-button-psw-save": "onPasswordChange",
                "input": "onValueChanged"
            },
           
            initialize: function (options) {
                _.bindAll(this, "handleRemoveClick", "handleSubmitClicked", "handleCancelClick", "handleNetworkModalSaveClick", "displayNetworkSettingsModal", "noSavedTips", "_createNetworkTimeout", "onValueChanged", "_getNetworkMode");
                
                this.dsn = options.dsn;
            },
            onValueChanged: function(event) {
                if(this.deviceModel.isGateway()) {
                    return;
                }
                var type = event.originalEvent.target.type;
                if(type === "password") {
                    var password = this.pswValue.currentView.getValue();
                    if(password && password.trim() !== "") {
                        this.pswSaveButton.currentView.enable();
                    } else {
                        this.pswSaveButton.currentView.disable();
                    }
                } else {
                     this.saveButton.currentView.enable();
                }
            },
            
            onRender: function () {
                var that = this;
                

                App.salusConnector.getDataLoadPromise(["devices"]).then(function (/*arrayOfData*/) {
                    that.deviceModel = App.salusConnector.getDevice(that.dsn);

                    if (!that.deviceModel) {
                        App.navigate("equipment");
                    }

                    that.breadcrumbs.show(new Breadcrumbs.BreadcrumbsView(
                        {
                            crumbs: [
                                {
                                    textKey: "equipment.myEquipment.title",
                                    href: "/equipment"
                                },
                                {
                                    text: that.deviceModel.getDisplayName(),
                                    href: "/equipment/myEquipment/" + that.dsn,
                                    active: false
                                },
                                {
                                    textKey: "common.header.settings",
                                    active: true
                                }
                            ]
                        }
                    ));

                    that.addBinding(that.deviceModel, ".bb-equipment-name", {
                        observe: "name"
                    });

                    that.$(".bb-spinner").remove(); //Removes spinner.
                    that.ui.icon.css("background-image", "url(" + that.deviceModel.get("equipment_page_icon_url") + ")");

                    that.infoRegion.show(new EquipmentSettingsWidget({
                        type: constants.widgetTypes.INFORMATION,
                        model: that.deviceModel,
                        isSettingsPage: true
                    }));

                    var promise;
                    if (that.deviceModel.isGateway()) {
                        that.devices = App.salusConnector.getGatewayAndDelegates();

                        promise = P.all(that.devices.map(function (device) {
                            return device.getDeviceProperties();
                        }));
                    } else {
                        promise = that.deviceModel.getDeviceProperties();
                    }

                    var viewModelOption = {deviceModel: that.deviceModel};
                    that.settingPanel = new Views.EquipmentSettingsPanel({
                        header: {
                            textKey: "equipment.settings.title"
                        },
                        cssClass: "settingsWidget",
                        model: new App.Consumer.Settings.Models.EquipmentSettingsPanelModel(viewModelOption),
                        deviceObj: that.devices || that.deviceModel
                    });
                    that.settingsRegion.show(that.settingPanel);

                    promise.then(function () {
                        var modelType = that.deviceModel.modelType;
                        
                        that.settingsCollection = App.Consumer.Settings.Models.SettingsContentFactory.createViewCollectionUnValidate(that.devices || that.deviceModel);
                        if(modelType === constants.modelTypes.THERMOSTAT || modelType === constants.modelTypes.GATEWAY){
                            that.settingsCollection = App.Consumer.Settings.Models.SettingsContentFactory.createViewCollection(that.devices || that.deviceModel);
                        }
                                            
                        that.settingPanel.updateModelCollection(that.settingsCollection);
                        
                        that.listenTo(that.settingsCollection, "saved:networkSettings", that.displayNetworkSettingsModal);
                        that.listenTo(that.settingsCollection, "noSaved:networkSettings", that.noSavedTips);
                        if(that.settingsCollection.length > 0){
                            if ((modelType === constants.modelTypes.IT600THERMOSTAT ||
                                modelType === constants.modelTypes.WATERHEATER)) {
                                that.pswSaveButton.show(new App.Consumer.Views.SalusButtonPrimaryView({
                                   buttonTextKey: "common.labels.ok",
                                   classes: "width100 disabled"
                               }));
                               
                               that.pswValue.show(new App.Consumer.Views.FormTextInput({
                                    type: "password",
                                    value: "",
                                    labelText: App.translate("login.login.passwordLabel")
                                }));
                               that.$(".form-error").hide();

                           }
                            that.cancelButton.show(new App.Consumer.Views.SalusButtonPrimaryView({
                                buttonTextKey: "common.labels.cancel",
                                className: "btn btn-default min-width-btn pull-right",
                                clickedDelegate: that.handleCancelClick
                            }));

                            that.saveButton.show(new App.Consumer.Views.SalusButtonPrimaryView({
                                buttonTextKey: "common.labels.save",
                                classes: "min-width-btn disabled",
                                clickedDelegate: that.handleSubmitClicked
                            }));
                           
                        }
                        
                       
                    });
                    //if current gateway status is online,it can be show "remove device"
                    //Bug_CASBW-216 start
                    if(App.getCurrentGateway().get("connection_status") === "Online"){
                    //Bug_CASBW-216 end
                        that.removeEquipment.show(new SalusLinkButtonView({
                            buttonText: App.translate("equipment.delete.removeEquipmentFormatted", {
                                modelTypeName: App.translate("equipment.names." + (that.deviceModel.modelType || constants.modelTypes.GENERIC))
                            }),
                            cssClass: "center-block" + (equipUtils.shouldPreserveCategoryCase(that.deviceModel) ? " preserve-case" : ""),
                            clickedDelegate: that.handleRemoveClick
                        }));    
                    }
                   
                });
            },
            
            noSavedTips: function(){
                var that = this;
				this.ui.noSavedLabel.removeClass("invisible");
				window.setTimeout(function () {
					if (!that.isDestroyed) {
						that.ui.noSavedLabel.addClass("invisible");
					}
				}, 3000);
            },
            
            // Bug_CASBW-22 Start
            displayNetworkSettingsModal: function (networkSettings, dirtyModels) {
                this._networkSettings = networkSettings;
                
                var  valid = _.some(dirtyModels, function(model) {
                    return model.get("isNetworkSetting");
                });
                
                if(!valid) {
                    this._showSaveMessage();
                } else {
                    this.networkModal = new App.Consumer.Views.SalusAlertModalView({
                        staticBackdrop: true,
                        model: new App.Consumer.Models.AlertModalViewModel({
                            iconClass: "icon-warning",
                            primaryLabelText: networkSettings ? App.translate("equipment.settings.networksSettingsSaveConfirmPrimary") : App.translate("equipment.settings.networksSettingsSaveConfirmPrimary1"),
                            secondaryLabelText: networkSettings ? App.translate("equipment.settings.networkSettingsSaveConfirmSecondary") : App.translate("equipment.settings.networkSettingsSaveConfirmSecondary1"),
                            rightButton: new App.Consumer.Views.SalusButtonPrimaryView({
                                classes: "btn-danger width100",
                                buttonTextKey: "common.labels.save",
                                clickedDelegate: this.handleNetworkModalSaveClick
                            }),
                            leftButton: new App.Consumer.Views.ModalCloseButton({
                                classes: "width100",
                                buttonTextKey: "common.labels.cancel"
                            })
                        })
                    });
                    
                    App.modalRegion.show(this.networkModal);
                    App.showModal();
                    this._showSaveMessage();
                }
            },
            
            _showSaveMessage: function () {
				var that = this;
				this.ui.savedLabel.removeClass("invisible");
				window.setTimeout(function () {
					if (!that.isDestroyed) {
						that.ui.savedLabel.addClass("invisible");
					}
				}, 3000);
			},
            // Bug_CASBW-22 End
            
            handleSubmitClicked: function () {
                var devices, skipModal = false,
                    that = this;
                if(!this.saveButton.currentView.isEnabled()){
                    return;
                }
                if (this.deviceModel.modelType === constants.modelTypes.GATEWAY) {
                    devices = App.salusConnector.getGatewayAndDelegates();
                    skipModal = true;
                } else {
                    devices = _.filter(App.salusConnector.getDeviceCollection().models, function (deviceModel) {
                        return deviceModel.modelType === that.deviceModel.modelType;
                    });
                }

                // show modal if we have multiple devices with the same 'modelType'
                if (devices.length > 1 && !skipModal) {
                    App.modalRegion.show(new SalusModalView({
                        contentView: new App.Consumer.Equipment.Views.ChangeAllDevicesModalView({
                            devices: devices,
                            modelType: devices[0].modelType,
                            okClickedDelegate: function () {
                                that.settingPanel.saveSettings(devices);
                                that.saveButton.currentView.disable();
                                App.hideModal();
                            },
                            cancelClickedDelegate: function () {
                                that.settingPanel.saveSettings();
                                that.saveButton.currentView.disable();
                                App.hideModal();
                            },
                            cancelTextKey: "equipment.dialog.changeThisDevice"
                        })
                    }));

                    App.showModal();
                } else {
                    this.settingPanel.saveSettings(devices);
                    that.saveButton.currentView.disable();
                }
            },
            onPasswordChange: function(){
                var that = this;
                var value  = this.pswValue.currentView.getValue();
                if(!this.pswSaveButton.currentView.isEnabled()){
                    return;
                }
                if(value === "49"){
                     that.settingsCollection = App.Consumer.Settings.Models.SettingsContentFactory.createViewCollection(that.devices || that.deviceModel);                  
                     that.settingPanel.updateModelCollection(that.settingsCollection);
                     this.ui.passwordContainer.hide();
                }
                else{
                        this.errorModal = new App.Consumer.Views.SalusAlertModalView({
                         staticBackdrop: true,
                         model: new App.Consumer.Models.AlertModalViewModel({
                             iconClass: "icon-warning",
                             primaryLabelText: App.translate("equipment.settings.passowrdIncorrect"),
                             rightButton: new App.Consumer.Views.ModalCloseButton({
                                 classes: "btn-danger width100",
                                 buttonTextKey: "common.labels.ok",
                                 clickedDelegate: App.hideModal()
                             }),
                             leftButton: new App.Consumer.Views.ModalCloseButton({
                                 classes: "width100",
                                 buttonTextKey: "common.labels.cancel",
                                 clickedDelegate: App.hideModal()
                             })
                         })
                     });

                     App.modalRegion.show(this.errorModal);
                     App.showModal();
                }
            },

            handleNetworkModalSaveClick: function () {
                var that = this, promises = [];

                this.networkModal.model.set("primaryLabelText", App.translate("equipment.settings.networksSettingsSavingPrimary"));
                this.networkModal.model.set("secondaryLabelText", App.translate("equipment.settings.networksSettingsSavingSecondary"));

                this.networkModal.showInternalSpinner();
                this.networkModal.hideButtons();
                this.networkModal.hideXButton();

                if (this._networkSettings && this._networkSettings.length > 0) {
                    _.each(this._networkSettings, function (setting) {
                        var deviceProperty = that._getProperty(setting.get("propertyModel").get("display_name"));
                        if (deviceProperty) {
                            promises.push(deviceProperty.setProperty(setting.get("value")));
                        }
                    });

                    P.all(promises).then(this._createNetworkTimeout);
                } else {
                    this._createNetworkTimeout(30000);
                }
            },

            handleCancelClick: function () {
                //TODO disable buttons
                this.settingPanel.resetSettings();

                window.history.back();
            },

            handleRemoveClick: function () {
                App.modalRegion.show(new SalusModalView({
                    contentView: new DeleteEquipModalView({model: this.deviceModel})
                }));

                App.showModal();
                
            },
           
            onBeforeDestroy: function () {
                clearTimeout(this.saveTimeout);
                clearTimeout(this.networkSaveTimeout);
                clearInterval(this.networkSaveInterval);
            },
            /**
             * show a box for editing the alias on pencil click
             * @private
             */
            _editDeviceAlias: function () {
                var that = this;
                var aliasView = new App.Consumer.Equipment.Views.EquipmentAliasEdit({
                    model: this.deviceModel,
                    page: "settings"
                });

                this.listenTo(aliasView, "close", function () {
                    aliasView.destroy();
                });
                
                this.listenTo(aliasView, "click:saveName", function (childView) {
                    
                     that.breadcrumbs.show(new Breadcrumbs.BreadcrumbsView(
                        {
                            crumbs: [
                                {
                                    textKey: "equipment.myEquipment.title",
                                    href: "/equipment"
                                },
                                {
                                    text: that.deviceModel.getDisplayName(),
                                    href: "/equipment/myEquipment/" + that.dsn,
                                    active: false
                                },
                                {
                                    textKey: "common.header.settings",
                                    active: true
                                }
                            ]
                        }
                    ));
                });

                this.aliasRegion.show(aliasView);
            },

            _getProperty: function (name) {
                if (this.devices) {
                    return _.reduce(this.devices, function (prop, device) {
                        return prop || device.get(name);
                    }, null);
                } else if (this.deviceModel) {
                    return this.deviceModel.get(name);
                }
            },
            
            _getNetworkMode: function(lanMode) {
                var that = this, promise;
                App.salusConnector.getDataLoadPromise(["devices"]).then(function() {
                    that.devices = App.salusConnector.getGatewayAndDelegates();
                    
                    promise = P.all(that.devices.map(function (device) {
                        return device.getDeviceProperties();
                    }));
                    // Bug_CASBW-22 Start
                    promise.then(function() {
                        var networkLanMode;
                        _.each(that.devices, function(device) {
                            if(device.get("NetworkLANMode")) {
                                networkLanMode = device.get("NetworkLANMode").get("getterProperty").getProperty();
                            }
                        });
                        if(lanMode === networkLanMode) {
                            clearTimeout(that.saveTimeout);
                            clearTimeout(that.networkSaveTimeout);
                            clearInterval(that.networkSaveInterval);
                            that.networkModal.hideInternalSpinner();
                            App.hideModal();
                            // Bug_CASBW-22 Start
                            that.settingsCollection = App.Consumer.Settings.Models.SettingsContentFactory.createViewCollection(that.devices);
                            that.listenTo(that.settingsCollection, "saved:networkSettings", that.displayNetworkSettingsModal);
                            that.listenTo(that.settingsCollection, "noSaved:networkSettings", that.noSavedTips);
                            that.settingsRegion.currentView.updateModelCollection(that.settingsCollection);
                            // Bug_CASBW-22 End
                        }
                    });
                    // Bug_CASBW-22 End
                });
            },

            _createNetworkTimeout: function (arg1) {
                var milliSeconds, that = this, lanMode = 1;
                if (_.isNumber(arg1)) {
                    milliSeconds = arg1;
                } else {
                    milliSeconds = 30000;
                }
                _.each(this.settingsCollection.models, function(model) {
                    if(model.get("isNetworkLanModeProperty")) {
                        lanMode = model.get("value");
                    }
                });
                
                this.networkSaveTimeout = setTimeout(function () {
                    that._getProperty("NetworkLANMode").setProperty(lanMode).then(function () {
                        // Bug_CASBW-22 Start
                        that.networkSaveInterval = setInterval(function() {
                            that._getNetworkMode(lanMode);
                        }, 5000);
                        
                        that.saveTimeout = setTimeout(function() {
                            clearTimeout(that.saveTimeout);
                            clearTimeout(that.networkSaveTimeout);
                            clearInterval(that.networkSaveInterval);
                            
                            that.networkModal.hideInternalSpinner();
                            that.networkModal.model.set("primaryLabelText", App.translate("equipment.settings.networksSettingsSaveError"));
                            that.networkModal.model.set("secondaryLabelText", null);
                            that.networkModal.rightButton = new App.Consumer.Views.SalusButtonPrimaryView({
                                classes: "btn-danger width100",
                                buttonTextKey: "common.header.logout",
                                clickedDelegate: that.logout
                            });
                            that.networkModal.leftButton = null;
                            that.networkModal.render();
                            that.networkModal.hideXButton();
                        }, 60000 * 3.5);
                        // Bug_CASBW-22 End
                    });
                }, milliSeconds);
            },
            
            // Bug_CASBW-22
            logout: function() {
                App.hideModal();
                App.logout();
            }
        }).mixin([SalusPageMixin], {
            analyticsSection: "settings",
            analyticsPage: "equipment" //TODO: This should be a function so the equipment Id is recorded.
        });
    });

    return App.Consumer.Settings.Views.EquipmentSettingsPageView;
});
