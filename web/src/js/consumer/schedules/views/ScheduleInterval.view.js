"use strict";

define([
    "app",
    "moment",
    "common/util/utilities",
    "common/constants",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusView",
    "consumer/schedules/views/AddNewInterval.view"
], function (App, moment, utilities, constants, consumerTemplates, SalusViewMixin) {

    App.module("Consumer.Schedules.Views", function (Views, App, B, Mn) {

        Views.ScheduleIntervalView = Mn.LayoutView.extend({
            className: "interval-view",
            template: consumerTemplates["schedules/scheduleInterval"],
            ui: {
                time: ".bb-time",
                temperature: ".bb-temperature",
                delete: ".bb-delete-interval",
                confirmDelete: ".bb-confirm-delete-interval"
            },
            events: {
                "click .bb-edit-interval": "_editClicked",
                "click .bb-delete-interval": "_deleteClicked",
                "click .bb-confirm-delete-interval": "_confirmDeleteClicked"
            },
            bindings: {
                ".bb-time": {
                    observe: "time",
                    onGet: function (time) {
                        if (this.timeFormat24Hour) {
                            return moment(time, "HH:mm:ss").format("HH:mm");
                        } else {
                            return moment(time, "HH:mm:ss").format("hh:mm a");
                        }
                    }
                },
                ".bb-temperature": {
                    observe: "value",
                    onGet: function (value) {
                        var heatNumberString, heatSplitNumberString, heatValue = "-",
                                coolNumberString, coolSplitNumberString, coolValue = "-";

                        if (this.onOffMode) {
                            this.ui.temperature.addClass("mode");

                            if (!value) {
                                this.ui.temperature.addClass("lighter");
                            }
                            return value ? App.translate("common.labels.on") : App.translate("common.labels.off");
                        }

                        if(value[constants.scheduleActionPropertyKeys.heat]){
                            heatNumberString = utilities.roundHalf(value[constants.scheduleActionPropertyKeys.heat] / 100);
                            heatSplitNumberString = heatNumberString.split(".");

                            heatValue = (heatSplitNumberString[1] === "0" ? heatSplitNumberString[0] : heatNumberString) + "\u00B0";
                        }
                        
                        if(value[constants.scheduleActionPropertyKeys.cool]){
                            coolNumberString = utilities.roundHalf(value[constants.scheduleActionPropertyKeys.cool] / 100);
                            coolSplitNumberString = coolNumberString.split(".");

                            coolValue = (coolSplitNumberString[1] === "0" ? coolSplitNumberString[0] : coolNumberString) + "\u00B0";
                        }
                        
                        return heatValue + " / " + coolValue;
                        
                        
                        
                    }
                }
            },
            initialize: function (options) {
                options = options || {};
                this.onOffMode = !!options.onOffMode;
                this.timeFormat24Hour = options.timeFormat24Hour;

                this.$el.addClass(this.onOffMode ? "on-off-mode" : "");
            },
            _editClicked: function () {
                this.model.isEditMode = true;
                this.trigger("enabled:editMode");
            },
            _deleteClicked: function () {
                this.ui.delete.addClass("hidden");
                this.ui.confirmDelete.removeClass("hidden");
            },
            _confirmDeleteClicked: function () {
                this.trigger("intervalChange");
                this.model.destroy();
            }


        }).mixin([SalusViewMixin]);
    });

    return App.Consumer.Schedules.Views.ScheduleIntervalView;
});
