"use strict";

define([
    "app",
    "momentWrapper",
    "common/constants",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusView",
    "consumer/views/SalusTabbedSection.view",
    "consumer/schedules/views/ScheduleIntervalCollection.view",
    "consumer/schedules/views/AddNewInterval.view",
    "consumer/views/SalusButtonPrimary.view"
], function (App, moment, constants, consumerTemplates, SalusViewMixin, SalusTabbedSection) {

    App.module("Consumer.Schedules.Views", function (Views, App, B, Mn, $, _) {

        Views.ScheduleDaysView = Mn.LayoutView.extend({
            template: consumerTemplates["schedules/scheduleDays"],
            regions: {
                intervalsRegion: ".bb-schedule-intervals",
                addNewIntervalRegion: ".bb-add-new-interval-region"
            },
            events: {
                "click .bb-add-interval": "_handleAddIntervalClick"
            },
            ui: {
                day: ".bb-days",
                addIntervalButton: ".bb-add-interval-button",
                addNewIntervalButton: ".bb-add-new-interval-button",
                valueHeader: ".bb-schedule-value-header",
                valueIcon: ".bb-schedule-value-icon"
            },
            initialize: function (options) {
                _.bindAll(this, "_handleAddIntervalClick");
                
                var that=this;
                
                this.device = options.device;
                this.mobile = this.options.mobile;
                this.scheduleCollection = options.scheduleCollection;
                this.isValidTime =function (time){
                    var res=_.filter(that.scheduleIntervals.collection.models,function (item){
                        return item.get("time")==time;
                    });
                    return res.length==0;
                }
                

                this.scheduleIntervals = new App.Consumer.Schedules.Views.ScheduleIntervalCollectionView({
                    collection: this.scheduleCollection,
                    device: this.device
                });
                this.listenTo(this.scheduleIntervals,"intervalChange",function (){
                    that.intervalChange();
                });
            },
            intervalChange:function (){
                this.trigger("intervalChange");
            },
            _renderValueHeader: function () {
                var text, iconClass;
                if (this.device.isThermostat()) {
                    text = App.translate("equipment.thermostat.menus.modeLabels.heating").toUpperCase() + " / " + App.translate("equipment.thermostat.menus.modeLabels.cooling").toUpperCase();
                    iconClass = "temperature-header-icon";
                    this.ui.valueHeader.text(text);
                } else {
                    iconClass = "mode-header-icon";
                    this.ui.valueHeader.text(App.translate("schedules.schedule.columnLabels.mode"));
                }
                
                this.ui.valueIcon.addClass(iconClass);
            },
            onRender: function () {
                this._renderValueHeader();
                this.ui.day.text(App.translate(this.model.get("day")));
                this.intervalsRegion.show(this.scheduleIntervals);
            },
            _handleAddIntervalClick: function () {
                var that = this;

                this.ui.addNewIntervalButton.toggleClass("hidden");
                this.ui.addIntervalButton.toggleClass("hidden");

                this.newIntervalView = new App.Consumer.Schedules.Views.AddNewIntervalView({
                    device: this.device,
                    onOffMode: !this.device.isThermostat(),
                    isValidTime:this.isValidTime
                });

                this.addNewIntervalRegion.show(this.newIntervalView);

                this.listenTo(this.newIntervalView, "click:add", function (data) {
                    
                    that.intervalChange();
                    
                    that.newIntervalView.destroy();
                    that.ui.addIntervalButton.toggleClass("hidden");
                    that.ui.addNewIntervalButton.toggleClass("hidden");
                    that.scheduleIntervals.collection.push(data);
                });

                this.listenTo(this.newIntervalView, "click:close", function () {
                    that.newIntervalView.destroy();
                    that.ui.addIntervalButton.toggleClass("hidden");
                    that.ui.addNewIntervalButton.toggleClass("hidden");
                });
            }
        }).mixin([SalusViewMixin]);


        Views.ScheduleDaysCollectionView = Mn.CollectionView.extend({
            childView: App.Consumer.Schedules.Views.ScheduleDaysView,
            childEvents: {
                "intervalChange":"intervalChange"
			},
            childViewOptions: function (model) {
                return {
                    scheduleCollection: model.get("scheduleCollection"),
                    device: this.options.device || this.options.tsc
                };
            },
            initialize: function () {
                _.bindAll(this, "changeWeekType");
                this.model = this.options.device || this.options.tsc;

                this._currentType = this.options.startingConfigurationType;
                this._oldCollectionJSON = {};
            },
            intervalChange: function (){
                this.trigger("intervalChange");
            },
            setCollection: function (type, collection){
                this._currentType=type;
                this.collection.reset(collection);
            },
            changeWeekType: function (type) {
                var tempArray = [];
                this._oldCollectionJSON[this._currentType] = this.collection.toJSON();
                this._currentType = type;

                if (this._oldCollectionJSON[type]) {
                    this.collection.reset(this._oldCollectionJSON[type]);
                } else {
                    if (type === constants.scheduleConfigurationTypes.WORKWEEK) {
                        this.collection.reset([
                            {
                                "type": constants.scheduleDaysTypes.WORKWEEK,
                                "day": App.translate("schedules.schedule.weekConfigurations.MO-FR"),
                                "scheduleCollection": new B.Collection([])
                            },
                            {
                                "type": constants.scheduleDaysTypes.WEEKEND,
                                "day": App.translate("schedules.schedule.weekConfigurations.SA-SU"),
                                "scheduleCollection": new B.Collection([])
                            }
                        ]);
                    } else if (type === constants.scheduleConfigurationTypes.FULLWEEK) {
                        this.collection.reset([
                            {
                                "type": constants.scheduleDaysTypes.FULLWEEK,
                                "day": App.translate("schedules.schedule.weekConfigurations.MO-SU"),
                                "scheduleCollection": new B.Collection([])
                            }
                        ]);
                    } else if (type === constants.scheduleConfigurationTypes.DAILY) {
                        for (var i = 1; i <= 7; i++) {
                            tempArray.push({
                                "type": i,
                                "day": App.translate("common.daysOfWeek.abbreviations." + constants.daysOfWeekMap[i - 1]),
                                "scheduleCollection": new B.Collection([])
                            });

                            this.collection.reset(tempArray);
                        }
                    }
                }
            }
        }).mixin([SalusViewMixin]);

        Views.ScheduleDaysMobileView = Mn.LayoutView.extend({
            template: consumerTemplates["schedules/scheduleDaysMobile"],
            regions: {
                addNewIntervalRegion: ".bb-add-new-interval-region",
                cancelButtonRegion: ".bb-cancel-button-mobile",
                addButtonRegion: ".bb-add-button-mobile",
                defaultButtonRegion: ".bb-default-button-mobile",
                deleteAllButtonRegion: ".bb-delete-all-button-mobile"
            },
            ui: {
                addIntervalText: ".bb-text",
                addIntervalButton: ".bb-add-interval-button",
                addNewIntervalButton: ".bb-add-new-interval-button",
                tabView: ".bb-tab-view-region",
                valueHeader: ".bb-schedule-value-header",
                valueIcon: ".bb-schedule-value-icon",
                deleteAllButton: ".bb-delete-all-button-mobile",
                defaultButton: ".bb-default-button-mobile",
                applyButton: ".bb-add-button-mobile",
                cancelButton: ".bb-cancel-button-mobile",
                editButtonArea: ".bb-edit-button-area"
            },
            events: {
                "click .bb-add-interval": "_handleAddIntervalClick",
                "click .bb-edit-button-area": "_handleEditButtonClick"
            },
            initialize: function () {
                _.bindAll(this, "_handleAddIntervalClick", "changeWeekType", "handleCancelClicked", "handleAddClicked", "handleDefaultClicked", "handleDeleteAllClicked", "_handleEditButtonClick");
                var that=this;

                this.model = this.options.device || this.options.tsc;
                this.scheduleCollection = this.options.collection;
                this.mobile = this.options.mobile;
                this.deviceCategory = this.options.deviceCategory;
                this.type = this.options.type;
                this.viewType = this.options.viewType;
                this._idIndex = App.Consumer.Schedules.Controller.idIndex.toString();
                this.isValidTime =function (time){
                    var collection;
                    if (that.weekType === constants.scheduleConfigurationTypes.WORKWEEK) {
                        collection=that.workWeekTabbedSection.getActiveView().collection
                    } else if (that.weekType === constants.scheduleConfigurationTypes.FULLWEEK) {
                        collection=that.fullWeekTabbedSection.getActiveView().collection
                    } else if (that.weekType === constants.scheduleConfigurationTypes.DAILY) {
                        collection=that.dailyTabbedSection.getActiveView().collection
                    }
                    
                    var res=_.filter(collection.models,function (item){
                        return item.get("time")==time;
                    });
                    
                    return res.length==0;
                }
            },
            onRender: function () {
                var that = this;
                var scheduleConfigTypes = constants.scheduleConfigurationTypes;
                this.weekType = this.model.getSchedules().getWeekType();

                this.clearFullWeekCollection();
                this.clearDailyCollection();
                this.clearWorkWeekCollection();

                if (this.weekType === scheduleConfigTypes.WORKWEEK) {
                    this.workWeekCollection = new B.Collection(this.scheduleCollection.models);
                } else if (this.weekType === scheduleConfigTypes.FULLWEEK) {
                    this.fullWeekCollection = new B.Collection(this.scheduleCollection.models);
                } else if (this.weekType === scheduleConfigTypes.DAILY) {
                    this.dailyCollection = new B.Collection(this.scheduleCollection.models);
                } else {
//                    this.weekType = scheduleConfigTypes.FULLWEEK;
//                    this.fullWeekCollection = new B.Collection(this.scheduleCollection.models);
                    this.weekType = scheduleConfigTypes.WORKWEEK;
                    this.workWeekCollection = new B.Collection(this.scheduleCollection.models);

                }

                this.workWeekIntervals = new Views.ScheduleIntervalCollectionView({
                    collection: this.workWeekCollection.first().get("scheduleCollection"),
                    device: this.model
                });
                this.listenTo(this.workWeekIntervals, "intervalChange", function () {
                    that.showAddButtonAndCancelButton();
                });


                this.weekendIntervals = new Views.ScheduleIntervalCollectionView({
                    collection: this.workWeekCollection.at(1).get("scheduleCollection"),
                    device: this.model
                });
                this.listenTo(this.weekendIntervals, "intervalChange", function () {
                    that.showAddButtonAndCancelButton();
                });

                this.workWeekIntervals.render().$el.attr("id", "bb-workWeek-tab" + this._idIndex);
                this.weekendIntervals.render().$el.attr("id", "bb-weekend-tab" + this._idIndex);

                var workWeekOptions = {
                    className: "bb-work-week-tabs work-week-tabs",
                    tabs: [
                        {
                            key: "bb-workWeek-tab" + this._idIndex,
                            classes: "workWeek-tab",
                            i18n: "schedules.schedule.weekConfigurations.MO-FR",
                            view: this.workWeekIntervals
                        },
                        {
                            key: "bb-weekend-tab" + this._idIndex,
                            classes: "weekend-tab",
                            i18n: "schedules.schedule.weekConfigurations.SA-SU",
                            view: this.weekendIntervals
                        }
                    ]
                };

                this.fullWeekIntervals = new Views.ScheduleIntervalCollectionView({
                    collection: this.fullWeekCollection.first().get("scheduleCollection"),
                    device: this.model
                });
                this.listenTo(this.fullWeekIntervals, "intervalChange", function () {
                    that.showAddButtonAndCancelButton();
                });

                this.fullWeekIntervals.render().$el.attr("id", "bb-fullWeek-tab" + this._idIndex);

                var fullWeekOptions = {
                    className: "bb-full-week-tabs full-week-tabs",
                    tabs: [
                        {
                            key: "bb-fullWeek-tab" + this._idIndex,
                            classes: "fullWeek-tab",
                            i18n: "schedules.schedule.weekConfigurations.MO-SU",
                            view: this.fullWeekIntervals
                        }
                    ]
                };

                var dailyOptions = this._createDailyOptions();

                this.workWeekTabbedSection = new SalusTabbedSection(workWeekOptions);
                this.fullWeekTabbedSection = new SalusTabbedSection(fullWeekOptions);
                this.dailyTabbedSection = new SalusTabbedSection(dailyOptions);

                this.ui.tabView.append(this.dailyTabbedSection.render().$el);
                this.ui.tabView.append(this.fullWeekTabbedSection.render().$el);
                this.ui.tabView.append(this.workWeekTabbedSection.render().$el);

                this.$('.bb-tabbed-bottom-effect').removeClass("hidden");
                this.$('.bb-tab-content-area').prepend(consumerTemplates["schedules/mobileIntervalsHeader"]);

                this._changeWeekType(this.weekType);

                this._renderValueHeader();
                App.Consumer.Schedules.Controller.idIndex++;

                this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
                    buttonTextKey: "common.labels.cancel",
                    className: "width100 btn btn-default",
                    clickedDelegate: this.handleCancelClicked
                });

                this.addButton = new App.Consumer.Views.SalusButtonPrimaryView({
                    buttonTextKey: "common.labels.save",
                    className: "width100 btn btn-primary",
                    clickedDelegate: this.handleAddClicked
                });

                this.defaultButton = new App.Consumer.Views.SalusButtonPrimaryView({
                    buttonTextKey: "schedules.edit.thermostatSettings.defaultSchedule",
                    className: "width100 btn btn-primary",
                    clickedDelegate: this.handleDefaultClicked
                });

                this.deleteAllButton = new App.Consumer.Views.SalusButtonPrimaryView({
                    buttonTextKey: "schedules.edit.thermostatSettings.deleteAllIntervals",
                    className: "width100 btn btn-primary",
                    clickedDelegate: this.handleDeleteAllClicked
                });

                this.cancelButtonRegion.show(this.cancelButton);
                this.addButtonRegion.show(this.addButton);
                this.deleteAllButtonRegion.show(this.deleteAllButton);
                if (this.deviceCategory != constants.categoryTypes.SMARTPLUGS) {
                    this.defaultButtonRegion.show(this.defaultButton);
                }
                this.hideAddButtonAndCancelButton();
            },
            
            //当schedule有改动时,才显示save跟cancel两个按钮
            showAddButtonAndCancelButton: function () {
                this.ui.applyButton.removeClass("hidden");
                this.ui.cancelButton.removeClass("hidden");
                
                this.ui.deleteAllButton.addClass("hidden");
                this.ui.defaultButton.addClass("hidden");
                
            },
            hideAddButtonAndCancelButton: function () {
                this.ui.applyButton.addClass("hidden");
                this.ui.cancelButton.addClass("hidden");
                
                if(this.viewType !== "widget"){
                    var existsSchedule=this.scheduleCollection.find(function (day){
                        if(day.get("scheduleCollection").length){
                            return day;
                        }
                    });

                    if(existsSchedule){
                        this.ui.deleteAllButton.removeClass("hidden");
                    } else {
                        this.ui.deleteAllButton.addClass("hidden");
                    }

                    this.ui.defaultButton.removeClass("hidden");
                }
            },
            onDestroy: function () {
                this.dailyTabbedSection.destroy();
                this.fullWeekTabbedSection.destroy();
                this.workWeekTabbedSection.destroy();
            },
            changeWeekType: function (type) {
                this._changeWeekType(type);

                this.weekType = type;
                
                this.hideAddButtonAndCancelButton();
            },
            changeCollection: function (collection) {
                this.scheduleCollection = collection;
                this.render();
            },
            _changeWeekType: function (type) {
                this.$('.bb-work-week-tabs').hide();
                this.$('.bb-full-week-tabs').hide();
                this.$('.bb-daily-tabs').hide();

                if (type === constants.scheduleConfigurationTypes.WORKWEEK) {
                    this.$('.bb-work-week-tabs').show();
                    this.scheduleCollection = this.workWeekCollection;
                } else if (type === constants.scheduleConfigurationTypes.FULLWEEK) {
                    this.$('.bb-full-week-tabs').show();
                    this.scheduleCollection = this.fullWeekCollection;
                } else if (type === constants.scheduleConfigurationTypes.DAILY) {
                    this.$('.bb-daily-tabs').show();
                    this.scheduleCollection = this.dailyCollection;
                }
            },
            _handleAddIntervalClick: function () {
                var that = this;

                this.ui.addNewIntervalButton.toggleClass("hidden");
                this.ui.addIntervalButton.toggleClass("hidden");

                this.newIntervalView = new App.Consumer.Schedules.Views.AddNewIntervalView({
                    device: this.model,
                    onOffMode: !this.model.isThermostat(),
                    isValidTime: that.isValidTime
                });

                this.addNewIntervalRegion.show(this.newIntervalView);

                this.listenTo(this.newIntervalView, "click:add", function (intervalData) {

                    this.showAddButtonAndCancelButton();

                    that.newIntervalView.destroy();
                    that.ui.addIntervalButton.toggleClass("hidden");
                    that.ui.addNewIntervalButton.toggleClass("hidden");

                    if (this.weekType === constants.scheduleConfigurationTypes.WORKWEEK) {
                        this.workWeekTabbedSection.getActiveView().collection.push(intervalData);
                    } else if (this.weekType === constants.scheduleConfigurationTypes.FULLWEEK) {
                        this.fullWeekTabbedSection.getActiveView().collection.push(intervalData);
                    } else if (this.weekType === constants.scheduleConfigurationTypes.DAILY) {
                        this.dailyTabbedSection.getActiveView().collection.push(intervalData);
                    } else {
                        App.error("weekType invalid");
                    }
                });

                this.listenTo(this.newIntervalView, "click:close", function () {
                    that.newIntervalView.destroy();
                    that.ui.addIntervalButton.toggleClass("hidden");
                    that.ui.addNewIntervalButton.toggleClass("hidden");
                });
            },
            _renderValueHeader: function () {
                var text, iconClass;
                if (this.model.isThermostat()) {
                    text = App.translate("equipment.thermostat.menus.modeLabels.heating").toUpperCase() + " / " + App.translate("equipment.thermostat.menus.modeLabels.cooling").toUpperCase();
                    iconClass = "temperature-header-icon";
                    this.$('.bb-schedule-value-header').text(text);
                } else {
                    iconClass = "mode-header-icon";
                    this.$('.bb-schedule-value-header').text(App.translate("schedules.schedule.columnLabels.mode"));
                }

                this.$('.bb-time-header').text(App.translate("schedules.schedule.columnLabels.startTime"));
                
                this.$('.bb-schedule-value-icon').addClass(iconClass);

                if (this.viewType !== "widget") {
                    this.ui.deleteAllButton.removeClass("hidden");
                    this.ui.defaultButton.removeClass("hidden");
                    this.ui.applyButton.removeClass("hidden");
                    this.ui.cancelButton.removeClass("hidden");
                } else {
                    this.$(".bb-saved-text").addClass("hidden");
                    this.ui.editButtonArea.removeClass("hidden");
                }
            },
            getWeekType: function () {
                return this.weekType;
            },
            clearWorkWeekCollection: function () {
                var collectionArray = [
                    {
                        "type": "workWeek",
                        "day": App.translate("schedules.schedule.weekConfigurations.MO-FR"),
                        "scheduleCollection": new B.Collection([])
                    },
                    {
                        "type": "weekend",
                        "day": App.translate("schedules.schedule.weekConfigurations.SA-SU"),
                        "scheduleCollection": new B.Collection([])
                    }
                ];

                if (this.workWeekCollection) {
                    this.workWeekIntervals.collection.reset([]);
                    this.weekendIntervals.collection.reset([]);
                } else {
                    this.workWeekCollection = new B.Collection(collectionArray);
                }
            },
            clearFullWeekCollection: function () {
                var collectionArray = [{
                        "type": "fullWeek",
                        "day": App.translate("schedules.schedule.weekConfigurations.MO-SU"),
                        "scheduleCollection": new B.Collection([])
                    }];

                if (this.fullWeekCollection) {
                    this.fullWeekIntervals.collection.reset([]);
                } else {
                    this.fullWeekCollection = new B.Collection(collectionArray);
                }
            },
            clearDailyCollection: function () {
                var collectionArray = [];

                for (var i = 1; i <= constants.weekLength; i++) {
                    collectionArray.push({
                        "type": i,
                        "day": App.translate(constants.daysOfWeekAbbr[i - 1]),
                        scheduleCollection: new B.Collection([])
                    });
                }

                if (this.dailyCollection) {
                    _.each(this.dailyIntervalViews, function (dailyIntervalView) {
                        dailyIntervalView.collection.reset([]);
                    });
                } else {
                    this.dailyCollection = new B.Collection(collectionArray);
                }
            },
            _configureForPage: function () {
                if (this.viewType === "widget") {
                    this.ui.addIntervalButton.addClass("hidden");
                }
            },
            _displaySavedText: function () {
                var $savedText = this.$(".bb-saved-text");
                $savedText.css("opacity", 1);
                $savedText.animate({opacity: 0}, 3000);
            },
            handleAddClicked: function () {
                //dev为当前ayla数据库中所有的schedule(包含active为false的)
                var dev = this.model.getSchedules(),
                        that = this;

                this.trigger("save:schedule", this.scheduleCollection.first().get("type"));
                this._clearCollections();

                dev.sortAndMarkSaveSchedule(this.scheduleCollection);
                this.addButton.showSpinner();

                dev.saveAll().then(function () {
                    that.addButton.hideSpinner();
                    that._displaySavedText();
                    that.hideAddButtonAndCancelButton();
                }).catch(function () {
                    that.addButton.hideSpinner();
                });

            },
            handleDefaultClicked: function () {

                var that = this;

                var defaultSchedule = constants.defaultSchedule;

                this.clearWorkWeekCollection();
                this.clearFullWeekCollection();
                this.clearDailyCollection();

                switch (this.weekType) {
                    case constants.scheduleConfigurationTypes.WORKWEEK:
                        var monToFridayArr;
                        var satToSunArr;
                        if (this.deviceCategory == constants.categoryTypes.THERMOSTATS) {
                            monToFridayArr = defaultSchedule.workingWeek.monToFriday.thermostat;
                            satToSunArr = defaultSchedule.workingWeek.satToSun.thermostat;
                        } else {
                            monToFridayArr = defaultSchedule.workingWeek.monToFriday.hotWater;
                            satToSunArr = defaultSchedule.workingWeek.satToSun.hotWater;
                        }

                        _.map(monToFridayArr, function (value) {
                            that.workWeekCollection.models[0].get("scheduleCollection").push(value);
                        });

                        _.map(satToSunArr, function (value) {
                            that.workWeekCollection.models[1].get("scheduleCollection").push(value);
                        });

                        break;
                    case constants.scheduleConfigurationTypes.FULLWEEK:
                        var monToSunArr;
                        if (this.deviceCategory == constants.categoryTypes.THERMOSTATS) {
                            monToSunArr = defaultSchedule.homeMostOfTheTime.monToSun.thermostat;
                        } else {
                            monToSunArr = defaultSchedule.homeMostOfTheTime.monToSun.hotWater;
                        }

                        _.map(monToSunArr, function (value) {
                            that.fullWeekCollection.models[0].get("scheduleCollection").push(value);
                        });
                        break;
                    case constants.scheduleConfigurationTypes.DAILY:
                        var moArr, tuArr, weArr, thArr, frArr, saArr, suArr;

                        if (this.deviceCategory == constants.categoryTypes.THERMOSTATS) {
                            moArr = defaultSchedule.daily.mo.thermostat;
                            tuArr = defaultSchedule.daily.tu.thermostat;
                            weArr = defaultSchedule.daily.we.thermostat;
                            thArr = defaultSchedule.daily.th.thermostat;
                            frArr = defaultSchedule.daily.fr.thermostat;
                            saArr = defaultSchedule.daily.sa.thermostat;
                            suArr = defaultSchedule.daily.su.thermostat;
                        } else {
                            moArr = defaultSchedule.daily.mo.hotWater;
                            tuArr = defaultSchedule.daily.tu.hotWater;
                            weArr = defaultSchedule.daily.we.hotWater;
                            thArr = defaultSchedule.daily.th.hotWater;
                            frArr = defaultSchedule.daily.fr.hotWater;
                            saArr = defaultSchedule.daily.sa.hotWater;
                            suArr = defaultSchedule.daily.su.hotWater;
                        }

                        _.map(suArr, function (value) {
                            that.dailyCollection.models[0].get("scheduleCollection").push(value);
                        });
                        _.map(moArr, function (value) {
                            that.dailyCollection.models[1].get("scheduleCollection").push(value);
                        });
                        _.map(tuArr, function (value) {
                            that.dailyCollection.models[2].get("scheduleCollection").push(value);
                        });
                        _.map(weArr, function (value) {
                            that.dailyCollection.models[3].get("scheduleCollection").push(value);
                        });
                        _.map(thArr, function (value) {
                            that.dailyCollection.models[4].get("scheduleCollection").push(value);
                        });
                        _.map(frArr, function (value) {
                            that.dailyCollection.models[5].get("scheduleCollection").push(value);
                        });
                        _.map(saArr, function (value) {
                            that.dailyCollection.models[6].get("scheduleCollection").push(value);
                        });


                        break;
                }
                
                this.showAddButtonAndCancelButton();

            },
            handleDeleteAllClicked: function () {
                this.clearWorkWeekCollection();
                this.clearFullWeekCollection();
                this.clearDailyCollection();
                
                this.showAddButtonAndCancelButton();
            },
            _clearCollections: function () {
                var type = this.scheduleCollection.first().get("type"),
                        configTypes = constants.scheduleConfigurationTypes;

//				_.map(this.model.get("devices"), function (device) {
//				    _.map(App.salusConnector.getDeviceByEUID(device.EUID).get("schedules"),function (schedule){
//
//				    });
//				});

                switch (type) {
                    case configTypes.FULLWEEK:
                        this.clearWorkWeekCollection();
                        this.clearDailyCollection();
                        break;
                    case configTypes.WORKWEEK:
                        this.clearDailyCollection();
                        this.clearFullWeekCollection();
                        break;
                    case constants.daysOfWeek.SUNDAY:
                        this.clearWorkWeekCollection();
                        this.clearFullWeekCollection();
                        break;
                    default:
                        throw new Error("Invalid schedule configuration type");
                }
            },
            handleCancelClicked: function () {
//                window.history.back();
                this.trigger("cancelSchedule");
            },
            _createDailyOptions: function () {
                var that=this;
                var keys = ["sunday-tab", "monday-tab", "tuesday-tab", "wednesday-tab", "thursday-tab", "friday-tab",
                    "saturday-tab"],
                        tabs = [];

                this.dailyIntervalViews = [];

                for (var i = 1; i <= constants.weekLength; i++) {
                    var intervalView = new Views.ScheduleIntervalCollectionView({
                        collection: this.dailyCollection.at(i - 1).get("scheduleCollection"),
                        device: this.model
                    });
                    this.listenTo(intervalView, "intervalChange", function () {
                        that.showAddButtonAndCancelButton();
                    });

                    /* Add an id based on the day key and current idIndex. Used for making unique ids for tab recognition.
                     ** Each view needs a unique id in the DOM because there may be multiple bb-monday-tabs on a page.
                     ** The idIndex makes sure each id is unique and is incremented for each piece of equipment.
                     */
                    intervalView.render().$el.attr("id", "bb-" + keys[i - 1] + this._idIndex);

                    this.dailyIntervalViews.push(intervalView);

                    tabs.push({
                        key: "bb-" + keys[i - 1] + this._idIndex,
                        classes: keys[i - 1],
                        i18n: constants.daysOfWeekAbbr[i - 1],
                        view: intervalView
                    });
                }

                return {
                    className: "bb-daily-tabs daily-tabs",
                    tabs: tabs
                };
            },
            _handleEditButtonClick: function () {
                var deviceCategory = this.model.get("device_category_type"),
                        categoryType = constants.categoryTypes,
                        navigateString;

                switch (deviceCategory) {
                    case categoryType.THERMOSTATS:
                        navigateString = "/schedules/thermostat";
                        break;
                    case categoryType.WATERHEATERS:
                        navigateString = "/schedules/waterHeater";
                        break;
                    case categoryType.SMARTPLUGS:
                        navigateString = "/schedules/smartPlug";
                        break;
                }

                App.navigate(navigateString + "/" + this.model.get("key"));
            }
        }).mixin([SalusViewMixin]);
    });
});
