"use strict";

define([
    "app",
    "common/constants",
    "consumer/consumerTemplates",
    "consumer/views/mixins/mixin.salusView",
    "consumer/views/mixins/mixin.thermostatArcView",
    "consumer/views/modal/ModalDefaultHeader.view",
    "consumer/views/SalusModal.view",
    "consumer/schedules/views/modal/EditSingleControlModal.view",
    "consumer/equipment/views/tsc/ThermostatSingleControlPage.view",
    "consumer/schedules/views/thermostat/ThermostatCollection.view",
    "consumer/schedules/views/ScheduleSelectionsCollection.view",
    "consumer/schedules/views/ScheduleDaysCollection.view",
    "consumer/schedules/views/modal/duplicate/DuplicateScheduleModal.view",
    "consumer/views/SalusModal.view",
    "consumer/views/modal/ModalDefaultHeader.view",
    "roundslider"
], function (App, constants, consumerTemplates, SalusViewMixin, ArcViewMixin) {
    App.module("Consumer.Schedules.Views", function (Views, App, B, Mn, $, _) {
        Views.ScheduleLayoutView = Mn.LayoutView.extend({
            id: "schedule-layout-view",
            className: "schedule col-xs-12",
            template: consumerTemplates["schedules/scheduleLayout"],
            ui: {
                title: ".bb-title",
                editButton: ".bb-edit-icon-container",
                tstatCount: ".bb-tstat-count",
                tstatControlGroup: ".bb-tstat-control-group",
                thermostatIconContainer: ".bb-thermostat-icon-container",
                waterHeaterIconContainer: ".bb-water-heater-icon-container",
                smartPlugIconContainer: ".bb-smart-plug-icon-container",
                smartPlugIcon: "bb-smart-plug-icon",
                individualControl: ".bb-individual-control-icon",
                scheduleDaysMobileContainer: ".bb-schedule-days-mobile",
                addButton:".bb-add-button",
                cancelButton:".bb-cancel-button",
                deleteAllButton:".bb-delete-all-button",
                defaultButton:".bb-default-button"
            },
            events: {
                "click .bb-duplicate-button": "handleDuplicateClicked"
            },
            regions: {
                thermostatCollectionRegion: ".bb-thermostat-collection-region",
                scheduleMenuRegion: ".bb-schedule-menu",
                scheduleDaysRegion: ".bb-schedule-days",
                cancelButtonRegion: ".bb-cancel-button",
                addButtonRegion: ".bb-add-button",
                defaultButtonRegion: ".bb-default-button",
                deleteAllButtonRegion: ".bb-delete-all-button",
                duplicateButtonRegion: ".bb-duplicate-button"
            },
            bindings: {
                ".bb-title": "name"
            },
            initialize: function () {
                _.bindAll(this,
                        "_getDeviceCount",
                        "handleDuplicateClicked",
                        "handleAddClicked",
                        "handleDefaultClicked",
                        "handleDeleteAllClicked",
                        "refreshCollecAndRender",
                        "handleCancelClicked",
                        "_displaySavedText"
                        );
                
                var that=this;

                this.noMixinEvents = true;

                var menuOptions, categories = constants.categoryTypes,
                        collectionObj, that = this;

                this.type = this.options.type;
                this.deviceCategory = this.options.deviceCategory;

                this.deviceModels = _.map(this.model.get("devices"), function (device) {
                    return App.salusConnector.getDeviceByEUID(device.EUID);
                });

                if (this.deviceCategory === categories.THERMOSTATS && this.type === constants.scheduleTypes.thermostats.singleControl) {
                    this.thermostatCollectionView = new App.Consumer.Schedules.Views.ThermostatCollectionView({
                        collection: new B.Collection(this.deviceModels)
                    });
                }

                collectionObj = this.model.getSchedules().getDisplayCollectionObj();

                menuOptions = {};
                if (!collectionObj.isEmpty) {
                    if (collectionObj.collection.length === 7) {
                        menuOptions.initialType = constants.scheduleConfigurationTypes.DAILY;
                    } else if (collectionObj.collection.length === 2) {
                        menuOptions.initialType = constants.scheduleConfigurationTypes.WORKWEEK;
                    } else if (collectionObj.collection.length === 1) {
                        menuOptions.initialType = constants.scheduleConfigurationTypes.FULLWEEK;
                    }
                }

                this.sameWeekType = true;
                this.savedType = menuOptions.initialType;

                this.scheduleMenu = new App.Consumer.Schedules.Views.ScheduleMenuView(menuOptions);
                this.listenTo(this.model, "duplicatedSchedule",function (){
                    that.scheduleMenu.changeSelection(that.model.getSchedules().getWeekType());
                })

                this.scheduleDays = new App.Consumer.Schedules.Views.ScheduleDaysCollectionView({
                    collection: collectionObj.collection,
                    tsc: this.model,
                    deviceCategory: this.deviceCategory,
                    type: this.type,
                    startingConfigurationType: menuOptions.initialType
                });
                this.listenTo(this.scheduleDays,"intervalChange",function (){
                    that.showAddButtonAndCancelButton();
                });

                this.scheduleDaysMobile = new App.Consumer.Schedules.Views.ScheduleDaysMobileView({
                    collection: collectionObj.collection,
                    device: this.model,
                    type: this.type,
                    deviceCategory: this.deviceCategory
                });
                this.listenTo(this.scheduleDaysMobile,"cancelSchedule",this.refreshCollecAndRender);

                this.cancelButton = new App.Consumer.Views.SalusButtonPrimaryView({
                    buttonTextKey: "common.labels.cancel",
                    className: "width100 btn btn-default",
                    clickedDelegate: this.handleCancelClicked
                });

                this.addButton = new App.Consumer.Views.SalusButtonPrimaryView({
                    buttonTextKey: "common.labels.save",
                    className: "width100 btn btn-primary",
                    clickedDelegate: this.handleAddClicked
                });

                this.defaultButton = new App.Consumer.Views.SalusButtonPrimaryView({
                    buttonTextKey: "schedules.edit.thermostatSettings.defaultSchedule",
                    className: "width100 btn btn-primary",
                    clickedDelegate: this.handleDefaultClicked
                });

                this.deleteAllButton = new App.Consumer.Views.SalusButtonPrimaryView({
                    buttonTextKey: "schedules.edit.thermostatSettings.deleteAllIntervals",
                    className: "width100 btn btn-primary",
                    clickedDelegate: this.handleDeleteAllClicked
                });



                this.listenTo(App.Consumer.Schedules.PageEventController, "duplicated:schedule", this.refreshCollecAndRender);
                this.listenTo(this.scheduleMenu, "change:weekType", function (type) {
                    that._canDuplicate();
                    that.scheduleDays.changeWeekType(type);
                    that.scheduleDaysMobile.changeWeekType(type);
                    that.sameWeekType = that.savedType === type;
                    that.hideAddButtonAndCancelButton();
                });
                this.listenTo(this.scheduleDaysMobile, "save:schedule", function (type) {
                    that.savedType = type;
                    that.sameWeekType = true;
                    that._canDuplicate();
                });
            },
            onRender: function () {
                switch (this.deviceCategory) {
                    case constants.categoryTypes.THERMOSTATS:
                        if (this.type === constants.scheduleTypes.thermostats.singleControl) {
                            this._getDeviceCount();
                            this.thermostatCollectionRegion.show(this.thermostatCollectionView);
                            this.ui.thermostatIconContainer.removeClass("hidden");
                        } else if (this.type === constants.scheduleTypes.thermostats.individualControl) {
                            this.ui.individualControl.removeClass("hidden");
                            this.ui.tstatControlGroup.removeClass("hidden");
                        }
                        break;
                    case constants.categoryTypes.WATERHEATERS:
                        this.ui.waterHeaterIconContainer.removeClass("hidden");
                        break;

                    case constants.categoryTypes.SMARTPLUGS:
                        this.ui.smartPlugIconContainer.removeClass("hidden");
                        break;
                    default:
                        throw new Error("Invalid device category");
                }

                //下拉列表框
                this.scheduleMenuRegion.show(this.scheduleMenu);

                this.scheduleDaysRegion.show(this.scheduleDays);
                //mobile 的day跟schedule item的内容
                this.ui.scheduleDaysMobileContainer.append(this.scheduleDaysMobile.render().$el);


                this.cancelButtonRegion.show(this.cancelButton);
                this.addButtonRegion.show(this.addButton);
                this.deleteAllButtonRegion.show(this.deleteAllButton);
                if (this.deviceCategory != constants.categoryTypes.SMARTPLUGS) {
                    this.defaultButtonRegion.show(this.defaultButton);
                }
                
                this.hideAddButtonAndCancelButton();


                this._canDuplicate();
            },
            //当schedule有改动时,才显示save跟cancel两个按钮
            showAddButtonAndCancelButton:function (){
                this.ui.addButton.removeClass("hidden");
                this.ui.cancelButton.removeClass("hidden");
                
                this.ui.deleteAllButton.addClass("hidden");
                this.ui.defaultButton.addClass("hidden");
            },
            hideAddButtonAndCancelButton:function (){
                this.ui.addButton.addClass("hidden");
                this.ui.cancelButton.addClass("hidden");
                
                var existsSchedule=this.scheduleDaysMobile.collection.find(function (day){
                    if(day.get("scheduleCollection").length){
                        return day;
                    }
                });
                
                if(existsSchedule){
                    this.ui.deleteAllButton.removeClass("hidden");
                } else {
                    this.ui.deleteAllButton.addClass("hidden");
                }
                
                this.ui.defaultButton.removeClass("hidden");
            },
            onDestroy: function () {
                this.scheduleDaysMobile.destroy();
            },
            onShow: function () {
                if (this.deviceCategory === constants.categoryTypes.THERMOSTATS &&
                        this.type === constants.scheduleTypes.thermostats.individualControl) {
                    this.handleDomLoadComplete();

                    if (!!this.ui.tstatArc) {
                        this.ui.tstatArc.roundSlider({
                            readOnly: true
                        });
                    }
                }
            },
            _getDeviceCount: function () {
                this.ui.tstatCount.text(this.deviceModels.length);
            },
            handleDuplicateClicked: function () {
                var that = this;

                if (this.duplicateEnabled) {
                    App.modalRegion.show(new App.Consumer.Views.SalusModalView({
                        size: "modal-md",
                        headerView: new App.Consumer.Views.ModalDefaultHeader(),
                        contentView: new App.Consumer.Schedules.Views.Modal.Duplicate.DuplicateScheduleModalView({
                            deviceCategory: this.deviceCategory,
                            model: that.model
                        })
                    }));

                    //TODO Give modal margin-bottom: 320px so lower on page
                    App.showModal();
                }
            },
            refreshCollecAndRender: function () {
                var collection = this.model.getSchedules().getDisplayCollectionObj().collection;
                this.scheduleDays.setCollection(this.model.getSchedules().getWeekType(), collection.toArray());
                this.scheduleMenu.changeSelection(this.model.getSchedules().getWeekType());
                this.scheduleDaysMobile.changeCollection(collection);
                
            },
            _displaySavedText: function () {
                var $savedText = this.$(".bb-saved-text");
                $savedText.css("opacity", 1);
                $savedText.animate({opacity: 0}, 3000);
            },
            handleAddClicked: function () {
                var dev = this.model.getSchedules(),
                        that = this;

                this.savedType = this.scheduleDays.collection.first().get("type");
                this.sameWeekType = true;
                this._canDuplicate();
                this.scheduleDays._oldCollectionJSON = {};

                dev.sortAndMarkSaveSchedule(this.scheduleDays.collection);
                this.addButton.showSpinner();

                dev.saveAll().then(function () {
                    that.addButton.hideSpinner();
                    that._displaySavedText();
                    that.hideAddButtonAndCancelButton();
                });
            },
            handleDefaultClicked: function () {

                var that = this;

                var collection = this.scheduleDays.collection;

                var defaultSchedule = constants.defaultSchedule;

                //清空当前所有schedule
                _.map(collection.models, function (day) {
                    day.get("scheduleCollection").reset([]);
                });

                switch (this.scheduleDays._currentType) {
                    case constants.scheduleConfigurationTypes.FULLWEEK:
                        var monToSunArr;
                        if (this.deviceCategory == constants.categoryTypes.THERMOSTATS) {
                            monToSunArr = defaultSchedule.homeMostOfTheTime.monToSun.thermostat;
                        } else {
                            monToSunArr = defaultSchedule.homeMostOfTheTime.monToSun.hotWater;
                        }

                        _.map(monToSunArr, function (value) {
                            collection.models[0].get("scheduleCollection").push(value);
                        });
                        break;
                    case constants.scheduleConfigurationTypes.DAILY:
                        var moArr, tuArr, weArr, thArr, frArr, saArr, suArr;

                        if (this.deviceCategory == constants.categoryTypes.THERMOSTATS) {
                            moArr = defaultSchedule.daily.mo.thermostat;
                            tuArr = defaultSchedule.daily.tu.thermostat;
                            weArr = defaultSchedule.daily.we.thermostat;
                            thArr = defaultSchedule.daily.th.thermostat;
                            frArr = defaultSchedule.daily.fr.thermostat;
                            saArr = defaultSchedule.daily.sa.thermostat;
                            suArr = defaultSchedule.daily.su.thermostat;
                        } else {
                            moArr = defaultSchedule.daily.mo.hotWater;
                            tuArr = defaultSchedule.daily.tu.hotWater;
                            weArr = defaultSchedule.daily.we.hotWater;
                            thArr = defaultSchedule.daily.th.hotWater;
                            frArr = defaultSchedule.daily.fr.hotWater;
                            saArr = defaultSchedule.daily.sa.hotWater;
                            suArr = defaultSchedule.daily.su.hotWater;
                        }

                        _.map(suArr, function (value) {
                            collection.models[0].get("scheduleCollection").push(value);
                        });
                        _.map(moArr, function (value) {
                            collection.models[1].get("scheduleCollection").push(value);
                        });
                        _.map(tuArr, function (value) {
                            collection.models[2].get("scheduleCollection").push(value);
                        });
                        _.map(weArr, function (value) {
                            collection.models[3].get("scheduleCollection").push(value);
                        });
                        _.map(thArr, function (value) {
                            collection.models[4].get("scheduleCollection").push(value);
                        });
                        _.map(frArr, function (value) {
                            collection.models[5].get("scheduleCollection").push(value);
                        });
                        _.map(saArr, function (value) {
                            collection.models[6].get("scheduleCollection").push(value);
                        });


                        break;
                    default:
                        var monToFridayArr;
                        var satToSunArr;
                        if (this.deviceCategory == constants.categoryTypes.THERMOSTATS) {
                            monToFridayArr = defaultSchedule.workingWeek.monToFriday.thermostat;
                            satToSunArr = defaultSchedule.workingWeek.satToSun.thermostat;
                        } else {
                            monToFridayArr = defaultSchedule.workingWeek.monToFriday.hotWater;
                            satToSunArr = defaultSchedule.workingWeek.satToSun.hotWater;
                        }

                        _.map(monToFridayArr, function (value) {
                            collection.models[0].get("scheduleCollection").push(value);
                        });

                        _.map(satToSunArr, function (value) {
                            collection.models[1].get("scheduleCollection").push(value);
                        });

                        break;
                }
                
                this.showAddButtonAndCancelButton();
            },
            handleDeleteAllClicked: function () {
                //清空当前所有schedule
                _.map(this.scheduleDays.collection.models, function (day) {
                    day.get("scheduleCollection").reset([]);
                });
                
                this.showAddButtonAndCancelButton();
            },
            handleCancelClicked: function () {
//                window.history.back();
                this.refreshCollecAndRender();
                this.hideAddButtonAndCancelButton();
            },
            _canDuplicate: function () {
                var that = this,
                        count = 0;

                App.salusConnector.getDataLoadPromise(["devices", "tscs"]).then(function () {
                    switch (that.deviceCategory) {
                        case constants.categoryTypes.THERMOSTATS:
                            count += App.salusConnector.getTSCCollection().length;
                            count += App.salusConnector.getIndividualControlThermostats().length;
                            break;
                        case constants.categoryTypes.WATERHEATERS:
                            count += App.salusConnector.getWaterHeaterCollection().length;
                            break;
                        case constants.categoryTypes.SMARTPLUGS:
                            count += App.salusConnector.getSmartPlugCollection().length;
                            break;
                        default:
                            throw new Error("Cannot retrieve invalid device type");
                    }

                    if (!!count && that.sameWeekType) {
                        that.duplicateEnabled = true;
                        that.$('.bb-duplicate-button').removeClass("disabled");
                    } else {
                        that.duplicateEnabled = false;
                        that.$('.bb-duplicate-button').addClass("disabled");
                    }
                });
            }
        }).mixin([SalusViewMixin, ArcViewMixin]);
    });

    return App.Consumer.Schedules.Views.ScheduleLayoutView;
});
