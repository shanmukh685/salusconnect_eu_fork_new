"use strict";

define([
	"app",
	"bluebird",
	"consumer/consumerTemplates",
	"consumer/views/mixins/mixin.salusPage",
	"consumer/views/mixins/mixin.salusView",
	"consumer/equipment/views/EquipmentCollection.view",
	"common/model/salusWebServices/ThermostatSingleControl.model",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/FormTextInput.view",
	"consumer/views/SalusButtonPrimary.view",
	"consumer/views/mixins/mixin.validation",
	"consumer/views/SalusLinkButton.view",
	"consumer/schedules/views/modal/SingleControlModalWidget.view"
], function (App, P, consumerTemplates, SalusPageMixin, SalusView) {

	App.module("Consumer.Equipment.Views", function (Views, App, B, Mn, $, _) {

		/**
		 * ThermostatSingleControlPageView
		 * acts as a landing page - navigates and shows based on state
		 * if there is already existing TSC's then we navigate to a create/edit page straight away
		 * if not, it takes them to an intial page giving some info about TSC's
		 */
		Views.ThermostatSingleControlPageView = Mn.LayoutView.extend({
			id: "bb-thermostat-single-control-page",
			className: "thermostat-single-control-page container",
			template: consumerTemplates["equipment/tsc/thermostatSingleControlPage"],
			regions: {
				contentRegion: ".bb-content-region"
			},
			initialize: function () {
				_.bindAll(this, "goBack", "goToCreatePage", "goToInitialPage");
			},
			onRender: function () {
				var that = this;

				App.salusConnector.getDataLoadPromise(["devices", "tscs"]).then(function (/*arrayOfData*/) {
					// tscCollection for current gateway
					var tscCollection = App.salusConnector.getTSCCollection(),
							gatewayDSN = App.getCurrentGatewayDSN();
					tscCollection.refresh();

					if (tscCollection.dsn !== gatewayDSN) {
						tscCollection.setDSN(gatewayDSN);
					}

					if (tscCollection.isEmpty()) {
						that.goToInitialPage();
					} else {
						// they've done this before, let them create and show existing
						that.goToCreateWithExistingPage();
					}
				});
			},
			goBack: function () {
				window.history.back();
			},
			goToInitialPage: function () {
				var view = new Views.ThermostatSingleControlInitialSetup();

				this.listenTo(view, "yesClick", this.goToCreatePage);
				this.listenTo(view, "noClick", this.goBack);

				this.contentRegion.show(view);
			},
			goToCreatePage: function () {
				var view = new Views.ThermostatSingleControlCreateView();

				this.listenTo(view, "cancelClick", this.goToInitialPage);
				this.listenTo(view, "finished", this.goToThermostatsPage);
				this.listenTo(view, "added", this.goToCreateWithExistingPage);

				this.contentRegion.show(view);
			},
			/**
			 * like the above but different listeners and stuff
			 */
			goToCreateWithExistingPage: function () {
				var that = this,
						view = new Views.ThermostatSingleControlCreateView();

				this.listenTo(view, "cancelClick", this.goBack);
				this.listenTo(view, "finished", this.goToThermostatsPage);
				this.listenTo(view, "added", this.goToCreateWithExistingPage);
				this.listenTo(view, "remove:tsc", function () {
					if (App.salusConnector.getTSCCollection().isEmpty()) {
						that.goToCreatePage();
					} else {
						that.goToCreateWithExistingPage();
					}
				});

				this.listenTo(view, "disableEdit", this.goToCreateWithExistingPage);

				this.contentRegion.show(view);
			},
			/**
			 * navigate back to the Category:Thermostats page
			 */
			goToThermostatsPage: function () {
				App.navigate("equipment/categories/thermostats");
			}
		}).mixin([SalusPageMixin], {
			analyticsSection: "equipment",
			analyticsPage: "tsc"
		});

		/**
		 * ThermostatSingleControlInitialSetup
		 * this is the initial page that would be shown if there are no existing tsc's
		 */
		Views.ThermostatSingleControlInitialSetup = Mn.LayoutView.extend({
			className: "tsc-initial-setup row",
			template: consumerTemplates["equipment/tsc/thermostatSingleControlInitialSetup"],
			regions: {
				buttonYes: ".bb-button-yes",
				buttonNo: ".bb-button-no"
			},
			initialize: function () {
				_.bindAll(this, "handleNoClick", "handleYesClick");
			},
			onRender: function () {
				this.buttonNo.show(new App.Consumer.Views.SalusButtonPrimaryView({
					className: "btn btn-default width100",
					buttonTextKey: "common.labels.no",
					clickedDelegate: this.handleNoClick
				}));

				this.buttonYes.show(new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "width100",
					buttonTextKey: "common.labels.yes",
					clickedDelegate: this.handleYesClick
				}));
			},
			handleNoClick: function () {
				this.trigger("noClick");
			},
			handleYesClick: function () {
				this.trigger("yesClick");
			}
		}).mixin([SalusView]);

		/**
		 * Create a TSC in this view
		 * it will handle editing the tsc as well
		 */
		Views.ThermostatSingleControlCreateView = Mn.LayoutView.extend({
			className: "create-tsc-setup",
			template: consumerTemplates["equipment/tsc/thermostatSingleControlCreate"],
			regions: {
				existingRegion: ".bb-existing-region",
				tscNameTextbox: ".bb-tsc-name-textbox",
				thermostatsRegion: ".bb-thermostats-available",
				finishButtonRegion: ".bb-finish-button",
				cancelButtonRegion: ".bb-cancel-button",
				addButtonRegion: ".bb-add-button"
			},
			ui: {
				pageSubheader: "#bb-tsc-page-subheader",
				error: ".bb-error"
			},
			events: {
				"click .bb-edit-button": "editTSCs"
			},
			initialize: function (/*options*/) {
				_.bindAll(this, "handleFinishClick", "exitEditMode", "handleAddClick");

				this.tscModel = null;

				// flag for button functionality
				this.inEditMode = false;

				//listen for ajax request to complete, then refresh the collection
				this.listenTo(App.Consumer.Equipment.PageEventController, "tsc:post:success", this._refreshTSCCollection);
				this.listenTo(App.Consumer.Equipment.PageEventController, "tsc:post:fail", this._showError);
			},
			onRender: function () {
				var tscCollection = App.salusConnector.getTSCCollection();

				if (!tscCollection.isEmpty()) {
					this.ui.pageSubheader.text(App.translate("equipment.thermostatSingleControl.createPage.titleExisting"));
					this.existingRegion.show(new Views.ThermostatSingleControlExistingView());
					this.existingRegion.$el.append('<div role="button" class="bb-edit-button edit-button"></div>');
					this.existingRegion.$el.removeClass("hidden");
				} else {
					this.ui.pageSubheader.text(App.translate("equipment.thermostatSingleControl.createPage.titleNoExisting"));
				}

				this.tscNameTextbox.show(new App.Consumer.Views.FormTextInput({
					labelText: "equipment.thermostatSingleControl.createPage.newTSCText"
				}));

				this.thermostatsRegion.show(new App.Consumer.Equipment.Views.EquipmentCollectionView({
					collection: App.salusConnector.getExistEuidIndividualControlThermostats(),
					deviceType: "thermostats"
				}));

				this.addButtonRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "width100",
					buttonTextKey: "equipment.thermostatSingleControl.createPage.addButtonText",
					clickedDelegate: this.handleAddClick
				}));

				this.finishButtonRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
					classes: "width100",
					buttonTextKey: "common.labels.finished",
					clickedDelegate: this.handleFinishClick
				}));

				this.cancelButtonRegion.show(new App.Consumer.Views.SalusButtonPrimaryView({
					className: "btn btn-default width100",
					buttonTextKey: "common.labels.cancel",
					clickedDelegate: this.exitEditMode
				}));
			},
			_showError: function () {
				// display save error
				this.ui.error.toggleClass("hidden", false);
				this.finishButtonRegion.currentView.hideSpinner();
			},
			_refreshTSCCollection: function () {
				var that = this;
				App.salusConnector.getTSCCollection().refresh().then(function () {
					that.finishButtonRegion.currentView.hideSpinner();
					that.exitEditMode();
				});
			},
			exitEditMode: function () {
				if (this.inEditMode) {
					this.inEditMode = false;
					this.trigger("disableEdit", this);
				} else {
					this.trigger("cancelClick");
				}
			},
			handleFinishClick: function () {
				var that = this;

				this.tscNameTextbox.currentView.hideErrors();

				if (this.inEditMode) {
					this.finishButtonRegion.currentView.showSpinner();
					App.Consumer.Equipment.PageEventController.trigger("tsc:save:start");
				} else {
					this._createTSC().then(function () {
						App.salusConnector.getTSCCollection().refresh().then(function () {
							that.trigger("finished");
						});
					}).catch(function (err) {
						if (_.isString(err)) {
							if (err.indexOf("at least two") > -1) {
								that.tscNameTextbox.currentView.showErrors("equipment.thermostatSingleControl.createPage.deviceError");
							} else if (err.indexOf("valid name") > -1) {
								// todo: find some other place for the error other than the textbox
								that.tscNameTextbox.currentView.showErrors("equipment.thermostatSingleControl.createPage.nameError");
							} else if (err.indexOf("single type") > -1) {
								that.tscNameTextbox.currentView.showErrors("equipment.thermostatSingleControl.createPage.typeError");
							}
						} else {
							// general error
							that.tscNameTextbox.currentView.showErrors("equipment.thermostatSingleControl.createPage.error");
						}
					});
				}
			},
			handleAddClick: function () {
				var that = this;

				this.tscNameTextbox.currentView.hideErrors();
                
				this._createTSC().then(function () {
					App.salusConnector.getTSCCollection().refresh().then(function () {
						that.trigger("added");
					});
				}).catch(function (err) {
					if (_.isString(err)) {
						if (err.indexOf("at least two") > -1) {
							that.tscNameTextbox.currentView.showErrors("equipment.thermostatSingleControl.createPage.deviceError");
						} else if (err.indexOf("valid name") > -1) {
							// todo: find some other place for the error other than the textbox
							that.tscNameTextbox.currentView.showErrors("equipment.thermostatSingleControl.createPage.nameError");
						} else if (err.indexOf("single type") > -1) {
							that.tscNameTextbox.currentView.showErrors("equipment.thermostatSingleControl.createPage.typeError");
						}
					} else {
						// general error
						that.tscNameTextbox.currentView.showErrors("equipment.thermostatSingleControl.createPage.error");
					}
				});
			},
			/**
			 * create a tsc
			 * @returns {*}
			 * @private
			 */
			_createTSC: function () {
				var deviceKeys = this.thermostatsRegion.currentView.getCheckedItemKeys(),
						deviceModels,
						oemModels = [],
						name = this.tscNameTextbox.currentView.getValue();

				if (deviceKeys && deviceKeys.length <= 1) {
					return P.reject("Need at least two devices for linked devices");
				}

				if (!_.isString(name) || (_.isString(name) && name.length === 0)) {
					return P.reject("Not valid name");
				}
                
                var TSCCollection = App.salusConnector.getTSCCollection();
                var valid = _.some(TSCCollection.models, function(model) {
                    return model.get("name") === name;
                });
                if(valid) {
                    this.tscNameTextbox.currentView.showErrors(App.translate("equipment.error.renameError"));
                    return P.reject("Duplicate name");
                }

				deviceModels = _.map(deviceKeys, function (key) {
					var device = App.salusConnector.getDevice(key);
					oemModels.push(device.get("oem_model"));
					return {
						EUID: device.getEUID(),
						oem_model: device.get("oem_model")
					};
				});

				if (_.uniq(oemModels).length > 1) {
                    if(!(_.indexOf(oemModels,"it600ThermHW-AC")!=-1 && _.indexOf(oemModels,"it600ThermHW")!=-1)){
                        return P.reject("A linked thermostat group must only be for a single type of thermostat.");
                    }
				}

				this.tscModel = new App.Models.ThermostatSingleControlModel({
					dsn: App.getCurrentGatewayDSN(),
					devices: deviceModels,
					name: name
				});

				return this.tscModel.add();
			},
			/**
			 * update the tscModels in the existing area
			 * @private
			 */
			_updateTSCs: function () {
				var childViews = this.existingRegion.currentView.children._views,
						dirtyModels = [],
						promiseArray = [];

				_.each(childViews, function (view) {
					if (view.dirty) {
						dirtyModels.push(view.model);
					}
				});

				promiseArray = _.map(dirtyModels, function (model) {
					return model.update();
				});

				return P.all(promiseArray);
			},
			/**
			 * on edit click,
			 * enter edit mode
			 */
			editTSCs: function () {
				var that = this,
						view = new Views.ThermostatSingleControlExistingEditView();

				this.listenTo(view, "remove:tsc", function () {
					that.trigger("remove:tsc");
				});
                
                this.listenTo(App.Consumer.Schedules.PageEventController,"singleControl:removed",function (){
                    that.exitEditMode();
                })

				this.existingRegion.show(view);

				this.addButtonRegion.$el.addClass("hidden");

				this.inEditMode = true;

				// adjust padding - needs less on top and bottom
				this.existingRegion.$el.css("background-color", "#F4FAFE");
				this.existingRegion.$el.css("padding", "4px 20px 0 20px");

				this.$(".bottom-blurb").addClass("hidden");

				// wipe out everything about creating one while in edit mode
				// show the individual control thermostat in a view like we see for the existing
				// couldn't chain these calls because replaceWith returns the DOM element that is then gone after
				this.individualControlsListView = new Views.ThermostatSingleControlListView({
					individualControls: true
				});
				this.$("#new-tsc-area").replaceWith('<div id="individual-controls-row" class="row"></div>');
				this.$("#individual-controls-row").append(this.individualControlsListView.render().$el);
			},
			onDestroy: function () {
				if (this.individualControlsListView) {
					this.individualControlsListView.destroy();
				}
			}
		}).mixin([SalusView]);

		/**
		 * Thermostat device view
		 * name of the thermostat + a check mark if selected - in edit mode
		 * childView of ThermostatSingleControlListView
		 */
		Views.ThermostatSingleControlDeviceView = Mn.ItemView.extend({
			template: consumerTemplates["equipment/tsc/thermostatSingleControlDevice"],
			className: "tsc-device",
			bindings: {
				".bb-thermostat-name": {
					observe: "name"
				}
			},
			ui: {
				name: ".bb-thermostat-name",
				check: ".bb-check-icon"
			},
			onRender: function () {
				if (this.options.editMode) {
					this.$el.addClass("edit");
				}

				if (this.options.selected) {
					this.$el.addClass("selected");
					this.ui.check.removeClass("hidden");
				}
			}
		}).mixin([SalusView]);

		/**
		 * ThermostatSingleControlListView
		 * Represents a TSC (name) and it's devices
		 * it will show when a TSC is created or when the page loads up and there are existing TSC's
		 */
		Views.ThermostatSingleControlListView = Mn.CompositeView.extend({
			template: consumerTemplates["equipment/tsc/thermostatSingleControlListView"],
			className: "existing-tsc col-xs-12 col-sm-4",
			childView: Views.ThermostatSingleControlDeviceView,
			childViewContainer: ".bb-tsc-devices",
			ui: {
				name: ".bb-tsc-name"
			},
			initialize: function (/*options*/) {
				if (this.options.individualControls) {
					this.collection = App.salusConnector.getIndividualControlThermostats();
				} else {
					// get the device models of the keys we have in this tsc
					// we have to do this because the TSC model only holds device keys
					var deviceModels = _.map(this.model.get("devices"), function (element) {
						return App.salusConnector.getDeviceByEUID(element.EUID);
					});

					this.collection = new B.Collection(deviceModels);
				}
			},
			onRender: function () {
				if (this.options.individualControls) {
					this.ui.name.text(App.translate("equipment.thermostatSingleControl.createPage.individualControls"));
				} else {
					this.ui.name.text(this.model.get("name"));
				}
			}
		}).mixin([SalusView]);

		/**
		 * Collection of all TSC's
		 * readonly
		 */
		Views.ThermostatSingleControlExistingView = Mn.CollectionView.extend({
			className: "row",
			childView: Views.ThermostatSingleControlListView,
			childViewOptions: function (model) {
				return {
					individualControls: false,
					model: model
				};
			},
			initialize: function () {
				this.collection = App.salusConnector.getTSCCollection();
			}
		}).mixin([SalusView]);

		/**
		 * mirrors the ThermostatSingleControlListView
		 * This view is specifically for edit mode
		 */
		Views.ThermostatSingleControlListEditView = Mn.CompositeView.extend({
			template: consumerTemplates["equipment/tsc/thermostatSingleControlListView"],
			className: "existing-tsc col-xs-12 col-sm-4",
			childView: Views.ThermostatSingleControlDeviceView,
			childViewContainer: ".bb-tsc-devices",
			ui: {
				name: ".bb-tsc-name"
			},
			childViewOptions: function (thermostat) {
				return {
					model: thermostat,
					editMode: true,
					selected: this.model.hasThermostat(thermostat)
				};
			},
			initialize: function () {
				_.bindAll(this, "handleRemoveClick");
				// this list has all thermostats
				this.collection = App.salusConnector.getThermostatCollection();

				this.dirty = false;
			},
			onRender: function () {
				var that = this;

				this.textBoxView = new App.Consumer.Views.FormTextInput({
					labelText: "equipment.thermostatSingleControl.createPage.newTSCText",
					value: this.model.get("name")
				});

				this.listenTo(this.textBoxView, "change:value", function (textBox) {
					that.dirty = true;
					that.model.nameChange = textBox.getValue();
				});

				this.ui.name.replaceWith(this.textBoxView.render().$el);

				this.salusDeleteButtonView = new App.Consumer.Views.SalusLinkButtonView({
					buttonText: App.translate("equipment.thermostatSingleControl.createPage.delete"),
					cssClass: "center-block delete-tsc",
					clickedDelegate: this.handleRemoveClick
				});

				this.$el.append(this.salusDeleteButtonView.render().$el);
			},
			onDestroy: function () {
				if (this.textBoxView) {
					this.textBoxView.destroy();
				}
			},
			handleRemoveClick: function () {
				// no designs for popup modal?
				var that = this;

				return this.model.unregister().then(function () {
					that.trigger("remove:tsc");

					return App.salusConnector.getTSCCollection().refresh();
				});
			}
		}).mixin([SalusView]);

		/**
		 * Collection of all TSC's in edit mode
		 */
		Views.ThermostatSingleControlExistingEditView = Mn.CollectionView.extend({
			className: "row",
			emptyView: App.Consumer.Settings.Views.EmptyListView,
			emptyViewOptions: {
				tagName: "div",
				i18nTextKey: "schedules.edit.modal.emptySingleControls"
			},
			childView: App.Consumer.Schedules.Views.Modal.SingleControlModalWidget,
			childEvents: {
				"render": "_onAddChildView",
				"remove:tsc": "_onTscRemove"
			},
			initialize: function () {
				this.childViews = [];
				this.collection = new B.Collection();
				this.collection.set(App.salusConnector.getTSCCollection().models);

				this.listenTo(App.Consumer.Equipment.PageEventController, "tsc:save:start", this._saveAllSingleControls);
			},
			// destroy unsaved singleControlWidget if delete is clicked
			_onTscRemove: function (childView) {
				var that = this;

				childView.destroy();
				return this.model.unregister().then(function () {
					that.trigger("remove:tsc");

					return App.salusConnector.getTSCCollection().refresh();
				});
			},
			_onAddChildView: function (childView) {
				if (childView.hasOwnProperty("isEdit")) {
					this.childViews.push(childView);
				}
			},
			onAddChild: function (childView) {
				var that = this;

				this.listenTo(childView, "remove:tsc", function () {
					that.trigger("remove:tsc");
				});
			},
			_hasOneThermostatType: function(views) {
				var oemModels = [];
				_.each(views, function(view) {
					_.each(view.thermostatsRegion.currentView.getCheckedDevices(), function(device) {
						oemModels.push(device.oem_model);
					});
				});

				return _.uniq(oemModels).length === 1;
			},
			_hasCheckedDuplicateTstats: function(views) {
				var checkedThermostats = [];
				_.each(views, function(view) {
					_.each(view.thermostatsRegion.currentView.getCheckedItemKeys(), function(key) {
						checkedThermostats.push(key);
					});
				});
				var uniqueCheckedTstats = _.uniq(checkedThermostats);

				return checkedThermostats.length !== uniqueCheckedTstats.length;
			},
			_saveAllSingleControls: function () {
				var promiseChain;
				var views = _.filter(this.childViews, function(view) {
					return !view.isDestroyed;
				});

				if (!this._hasCheckedDuplicateTstats(views) && this._hasOneThermostatType(views)) {
					// Running the promises in series because of a save timing issue w/ services
					_.each(this.childViews, function (child, index) {
						if (index === 0) {
							promiseChain = child.saveTSC();
						} else {
							promiseChain = promiseChain.then(function () {
								return child.saveTSC();
							});
						}
					});
				} else {
					promiseChain = P.reject("TSC save error");
				}
				return promiseChain.then(function () {
					App.Consumer.Equipment.PageEventController.trigger("tsc:post:success");
				}).catch(function () {
					App.Consumer.Equipment.PageEventController.trigger("tsc:post:fail");
				});
			}
		}).mixin([SalusView]);
	});

	return App.Consumer.Equipment.Views.ThermostatSingleControlPageView;
});